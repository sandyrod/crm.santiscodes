<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCensusFieldsToClientTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clients', function (Blueprint $table) {
            $table->integer('survey_number')->nullable();
            $table->boolean('sells_lubricants')->nullable();
            $table->boolean('sells_chronus')->nullable();
            $table->string('area', 100)->nullable();
            $table->boolean('autoservice')->nullable();
            $table->string('visibility', 2)->nullable();
            $table->string('other_services', 100)->nullable();
            $table->string('purchase_interest', 2)->nullable();
            $table->integer('dependents_number')->nullable();
            $table->string('person_contact', 100)->nullable();
            $table->string('charge_contact', 100)->nullable();
            $table->string('buyer', 100)->nullable();
            $table->string('other_contact', 100)->nullable();
            $table->string('other_contact_charge', 100)->nullable();
            $table->string('quality_frame', 100)->nullable();
            $table->boolean('census_talkers')->nullable();
            $table->boolean('census_posters')->nullable();
            $table->boolean('census_exhibitor')->nullable();
            $table->boolean('census_pennants')->nullable();
            $table->string('observations')->nullable();
            $table->string('cdv', 50)->nullable();
            $table->string('brand_a', 50)->nullable();
            $table->string('brand_b', 50)->nullable();
            $table->string('brand_c', 50)->nullable();
            $table->string('census_by', 100)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clients', function (Blueprint $table) {
            $table->dropcolumn(['survey_number']);
            $table->dropcolumn(['sells_lubricants']);
            $table->dropcolumn(['sells_chronus']);
            $table->dropcolumn(['area']);
            $table->dropcolumn(['autoservice']);
            $table->dropcolumn(['visibility']);
            $table->dropcolumn(['other_services']);
            $table->dropcolumn(['purchase_interest']);
            $table->dropcolumn(['dependents_number']);
            $table->dropcolumn(['person_contact']);
            $table->dropcolumn(['charge_contact']);
            $table->dropcolumn(['buyer']);
            $table->dropcolumn(['other_contact']);
            $table->dropcolumn(['other_contact_charge']);
            $table->dropcolumn(['quality_frame']);
            $table->dropcolumn(['census_talkers']);
            $table->dropcolumn(['census_posters']);
            $table->dropcolumn(['census_exhibitor']);
            $table->dropcolumn(['census_pennants']);
            $table->dropcolumn(['observations']);
            $table->dropcolumn(['cdv']);
            $table->dropcolumn(['brand_a']);
            $table->dropcolumn(['brand_b']);
            $table->dropcolumn(['brand_c']);
            $table->dropcolumn(['census_by']);
        });
    }
}
