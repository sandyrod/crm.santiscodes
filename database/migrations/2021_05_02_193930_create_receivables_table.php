<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReceivablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('receivables', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('client_id')->nullable()->unsigned();
            $table->bigInteger('order_id')->nullable()->unsigned();
            $table->bigInteger('user_id')->unsigned();
            $table->float('amount')->default(0);
            $table->date('duedate_at');
            $table->integer('status')->default(0)
                ->description('0 anulada, 1 activa, 2 pago_parcial, 3 pagada');
            $table->string('description')->nullable();
            $table->date('close_at')->nullable()->description('fecha de cierre');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('receivables');
    }
}
