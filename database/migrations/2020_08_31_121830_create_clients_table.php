<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id');
            $table->string('businessmame')->nullable();
            $table->string('businesscode')->nullable();
            $table->string('rif');
            $table->boolean('specialtaxpayer');
            $table->string('businesssmail')->nullable();
            $table->string('ownersname')->nullable();
            $table->string('ownersdni')->nullable();
            $table->string('ownersmail')->nullable();
            $table->date('ownersbirthday')->nullable();
            $table->string('managersname')->nullable();
            $table->string('managersdni')->nullable();
            $table->string('managersmail')->nullable();
            $table->foreignId('distribution_channel_id')->nullable();
            $table->foreignId('business_type_id')->nullable();
            $table->foreignId('sales_type_id')->nullable();
            $table->double('creditlimit')->nullable();
            $table->integer('visitfrecuency')->nullable();
            $table->integer('visitsequence')->nullable();
            $table->date('anniversary')->nullable();
            $table->string('dropsizeprom')->nullable();
            $table->double('maximumorder')->nullable();
            $table->boolean('pennants')->nullable()->comment('banderines');
            $table->boolean('display')->nullable()->comment('exhibicion');
            $table->boolean('frame')->nullable()->comment('bastidor');
            $table->boolean('pop')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
