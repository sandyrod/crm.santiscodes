<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTablePurchase extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchases', function (Blueprint $table){
            $table->increments('id');
            $table->string('number')->nullable();
            $table->bigInteger('user_id')->nullable()->unsigned();
            $table->bigInteger('provider_id')->nullable()->unsigned();
            $table->bigInteger('warehouse_id')->nullable()->unsigned();
            $table->integer('status')->default(1)->unsigned()
                ->description('0 Anulada, 1 Activa');
            $table->integer('invoice_type_id')->default(1)->unsigned()
                ->description('Tipo de documento de entrada');
            $table->timestamps();

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchases');
    }
}
