<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBankTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bank_transactions', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('bankaccount_id')->unsigned();
            $table->bigInteger('user_id')->unsigned();
            $table->string('transactioncode')->description('codigo de la transaccion');
            $table->float('amount')->unsigned();
            $table->integer('transaction_type_id')->unsigned()
                ->description('0 credito, 1 debito');
            $table->integer('transaction_method_id')->unsigned()
                ->description('0 deposito, 1 retiro, 2 transferencia, 3 cheque depositado,
                    4 pago movil entrante, 5 pago movil saliente');
            $table->integer('status')->unsigned();
            $table->string('description')->nullable;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bank_transactions');
    }
}
