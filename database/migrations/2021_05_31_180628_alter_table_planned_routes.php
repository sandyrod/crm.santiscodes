<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTablePlannedRoutes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('planned_routes', function (Blueprint $table) {
            $table->date('begin_at')->nullable()->after('status');
            $table->boolean('monday')->nullable()->after('user_id');
            $table->boolean('tuesday')->nullable()->after('monday');
            $table->boolean('wednesday')->nullable()->after('tuesday');
            $table->boolean('thursday')->nullable()->after('wednesday');
            $table->boolean('friday')->nullable()->after('thursday');
            $table->boolean('saturday')->nullable()->after('friday');
            $table->boolean('sunday')->nullable()->after('saturday');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('planned_routes', function (Blueprint $table) {
            $table->dropcolumn(['begin_at', 'monday', 'tuesday', 'wednesday',
                'thursday', 'friday', 'saturday', 'sunday']);
        });
    }
}
