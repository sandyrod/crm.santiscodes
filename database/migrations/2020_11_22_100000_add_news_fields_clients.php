<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNewsFieldsClients extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clients', function (Blueprint $table) {
            $table->string('registered_name', 100)->nullable();
            $table->string('registered_charge')->nullable();
            $table->integer('open_establishment')->nullable();
            $table->integer('owner_attend')->nullable();
            $table->integer('persons_attending')->nullable();
            $table->integer('stickers')->nullable();
            $table->string('other_pop')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clients', function (Blueprint $table) {
            $table->dropcolumn(['registered_name']);
            $table->dropcolumn(['registered_charge']);
            $table->dropcolumn(['open_establishment']);
            $table->dropcolumn(['owner_attend']);
            $table->dropcolumn(['persons_attending']);
            $table->dropcolumn(['stickers']);
            $table->dropcolumn(['other_pop']);
        });
    }
}
