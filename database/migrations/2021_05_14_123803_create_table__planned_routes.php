<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTablePlannedRoutes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('planned_routes', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('distributionroute_id')->unsigned()->nullable();
            $table->string('name')->nullable();
            $table->integer('clientsperday');
            $table->bigInteger('client_id')->unsigned()->nullable();
            $table->bigInteger('user_id')->unsigned();
            $table->boolean('status')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('planned_routes');
    }
}
