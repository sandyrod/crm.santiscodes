<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldUserToRouteDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('planed_route_details', function (Blueprint $table) {
            $table->bigInteger('user_id')->nullable()->unsigned()->after('client_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('planed_route_details', function (Blueprint $table) {
            $table->dropcolumn(['user_id']);
        });
    }
}
