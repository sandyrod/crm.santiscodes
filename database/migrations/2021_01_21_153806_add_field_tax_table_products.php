<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldTaxTableProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->string('code', 15)->nullable()->after('name');
            $table->float('price')->default(0)->nullable()->after('description')->change();
            $table->float('tax')->nullable()->after('price');
            $table->float('sale_price')->nullable()->after('tax');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->float('price', 100)->nullable()->after('description')->change();
        });
        Schema::table('products', function (Blueprint $table) {
            $table->dropcolumn(['tax', 'sale_price', 'code']);
        });
    }
}
