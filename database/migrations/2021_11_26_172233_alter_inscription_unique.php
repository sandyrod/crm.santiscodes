<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterInscriptionUnique extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('inscriptions', function (Blueprint $table) {
            $table->dropUnique('inscriptions_identification_unique');
            $table->dropUnique('inscriptions_email_unique');
            $table->dropUnique('inscriptions_rif_company_unique');
            $table->dropUnique('inscriptions_instagram_personal_unique');
            $table->dropUnique('inscriptions_instagram_company_unique');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('inscriptions', function (Blueprint $table) {
            $table->string('identification',10)->nullable()->unique()->change();
            $table->string('email')->nullable()->unique()->change();
            $table->string('rif_company')->nullable()->unique()->change();
            $table->string('instagram_personal')->nullable()->unique()->change();
            $table->string('instagram_company')->nullable()->unique()->change();
        });
    }
}
