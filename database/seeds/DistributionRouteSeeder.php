<?php

use Illuminate\Database\Seeder;
use App\Models\Distributionroute;

class DistributionRouteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Distributionroute::firstOrCreate(['name' => '101', 'description' => 'Ruta 1']);
        Distributionroute::firstOrCreate(['name' => '102', 'description' => 'Ruta 2']);
    }
}
