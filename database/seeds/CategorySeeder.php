<?php

use Illuminate\Database\Seeder;
use App\Models\Category;
class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::firstOrCreate(['id' => 1,'name' => 'SINTETICOS']);
        Category::firstOrCreate(['id' => 2,'name' => 'SEMI SINTETICOS']);
        Category::firstOrCreate(['id' => 3,'name' => 'MINERAL']);
        Category::firstOrCreate(['id' => 4,'name' => 'PARA MOTORES A DIESEL']);
        Category::firstOrCreate(['id' => 5,'name' => 'TRANSMIoCION AUTOMATICA']);
        Category::firstOrCreate(['id' => 6,'name' => 'DIRECCIN HIDRAULICA']);
        Category::firstOrCreate(['id' => 7,'name' => 'MOTOS']);
        Category::firstOrCreate(['id' => 8,'name' => 'MARINOS']);
        Category::firstOrCreate(['id' => 9,'name' => 'ACUOSOS']);
        Category::firstOrCreate(['id' => 10,'name' => 'ESPECIALIDAD AUTOMOTRIZ']);
        Category::firstOrCreate(['id' => 11,'name' => 'HIDRAULICO PESADO']);
        Category::firstOrCreate(['id' => 12,'name' => 'ENGRANAJES']);
        Category::firstOrCreate(['id' => 13,'name' => 'INDUSTRIAL']);
        Category::firstOrCreate(['id' => 14,'name' => 'PARA MOTORES A GASOLINA']);
    }
}
