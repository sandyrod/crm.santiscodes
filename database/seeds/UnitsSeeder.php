<?php

use Illuminate\Database\Seeder;
use App\Models\Unit;

/**
 * Class UnitsSeeder
 */
class UnitsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Unit::firstOrCreate(['id' => 1, 'name' => 'CUARTO', 'capacity' => '5.676', 'packing_unit' => '6 x 1/4']);
        Unit::firstOrCreate(['id' => 2, 'name' => 'GALÓN', 'capacity' => '11.35', 'packing_unit' => '3 x 1']);
        Unit::firstOrCreate(['id' => 3, 'name' => 'PINTA', 'capacity' => '3.784', 'packing_unit' => '8 x 1/8']);
        Unit::firstOrCreate(['id' => 4, 'name' => 'PAILA', 'capacity' => '19', 'packing_unit' => 'PAILA']);
        Unit::firstOrCreate(['id' => 5, 'name' => 'TAMBOR', 'capacity' => '208', 'packing_unit' => 'TAMBOR']);
    }
}
