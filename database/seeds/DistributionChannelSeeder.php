<?php

use Illuminate\Database\Seeder;
use App\Models\Distribution_channel;

class DistributionChannelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Distribution_channel::firstOrCreate(['name' => 'ATL', 'description' => 'Cambiar descripcion']);
        Distribution_channel::firstOrCreate(['name' => 'CBA', 'description' => 'Cambiar descripcion']);
        Distribution_channel::firstOrCreate(['name' => 'FER', 'description' => 'Cambiar descripcion']);
        Distribution_channel::firstOrCreate(['name' => 'MTS', 'description' => 'Cambiar descripcion']);
        Distribution_channel::firstOrCreate(['name' => 'OTC', 'description' => 'Cambiar descripcion']);
        Distribution_channel::firstOrCreate(['name' => 'TCV', 'description' => 'Cambiar descripcion']);
        Distribution_channel::firstOrCreate(['name' => 'VLU', 'description' => 'Cambiar descripcion']);
        Distribution_channel::firstOrCreate(['name' => 'VTA', 'description' => 'Cambiar descripcion']);
        Distribution_channel::firstOrCreate(['name' => 'VTL', 'description' => 'Cambiar descripcion']);
        Distribution_channel::firstOrCreate(['name' => 'VTR', 'description' => 'Cambiar descripcion']);
    }
}
