<?php

use Illuminate\Database\Seeder;
use App\Models\Business_type;
class BussinessTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Business_type::firstOrCreate(['name' => 'A', 'description' => 'Cambiar descripcion']);
        Business_type::firstOrCreate(['name' => 'B', 'description' => 'Cambiar descripcion']);
        Business_type::firstOrCreate(['name' => 'C', 'description' => 'Cambiar descripcion']);
        Business_type::firstOrCreate(['name' => 'D', 'description' => 'Cambiar descripcion']);
    }
}
