<?php

use Illuminate\Database\Seeder;
use App\Models\Product;
use App\Models\ProductUnit;
use App\Models\ProductCategory;

class ProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Product::firstOrCreate(['id' => 1,'code'=>'100810023',
            'name'=>'ECO LOGIC FULL SYNTHETIC MULTIGRADE MOTOR OIL API SN PLUS SAE 5W20',
             'ue'=>'6x1/4', 'price' =>'36.21', 'tax' => '16', 'sale_price' => '42']);
        ProductUnit::firstOrCreate(['product_id' =>1, 'unit_id' => 1]);
        ProductCategory::firstOrCreate(['product_id' => 1, 'category_id' => 13]);

        Product::firstOrCreate(['id' => 2,'code'=>'100610048',
            'name'=>'ECO LOGIC FULL SYNTHETIC MULTIGRADE MOTOR OIL API SN PLUS SAE 5W30',
             'ue'=>'6x1/4', 'price' =>'36.21', 'tax' => '16', 'sale_price' => '42']);
        ProductUnit::firstOrCreate(['product_id' =>2, 'unit_id' => 1]);
        ProductCategory::firstOrCreate(['product_id' => 2, 'category_id' => 13]);

        Product::firstOrCreate(['id' => 3,'code'=>'100610086',
            'name'=>'ECO CLASSIC SYNTHETIC BLEND MULTIGRADE MOTOR OIL API SN/SL SAE 15W40',
             'ue'=>'6x1/4', 'price' =>'19.83', 'tax' => '16', 'sale_price' => '23']);
        ProductUnit::firstOrCreate(['product_id' =>3, 'unit_id' => 1]);
        ProductCategory::firstOrCreate(['product_id' => 3, 'category_id' => 13]);


        Product::firstOrCreate(['id' => 4,'code'=>'101010024',
            'name'=>'ECO CLASSIC SYNTHETIC BLEND MULTIGRADE MOTOR OIL API SN/SL SAE 15W40',
             'ue'=>' 8x1/8', 'price' =>'12.93', 'tax' => '16', 'sale_price' => '15']);
        ProductUnit::firstOrCreate(['product_id' =>4, 'unit_id' => 3]);
        ProductCategory::firstOrCreate(['product_id' => 4, 'category_id' => 13]);

        Product::firstOrCreate(['id' => 5,'code'=>'100610067',
            'name'=>'ECO CLASSIC MULTIGRADE MOTOR OIL API SL SAE 20W50', 'ue'=>'6x1/4',
             'price' =>'18.10', 'tax' => '16', 'sale_price' => '21']);
        ProductUnit::firstOrCreate(['product_id' =>5, 'unit_id' => 1]);
        ProductCategory::firstOrCreate(['product_id' => 5, 'category_id' => 13]);

        Product::firstOrCreate(['id' => 6,'code'=>'100711084',
            'name'=>'ECO CLASSIC MULTIGRADE MOTOR OIL API SL SAE 20W50',
             'ue'=>' 8x1/8', 'price' =>'12.07', 'tax' => '16', 'sale_price' => '14']);
        ProductUnit::firstOrCreate(['product_id' =>6, 'unit_id' => 3]);
        ProductCategory::firstOrCreate(['product_id' => 6, 'category_id' => 13]);

        Product::firstOrCreate(['id' => 7,'code'=>'102811085',
            'name'=>'ECO CLASSIC HEAVY DUTY DIESEL MULTIGRADE MOTOR OIL API CI-4 SAE 15W40   ',
             'ue'=>'3x1', 'price' =>'36.21', 'tax' => '16', 'sale_price' => '42']);
        ProductUnit::firstOrCreate(['product_id' =>7, 'unit_id' => 2]);
        ProductCategory::firstOrCreate(['product_id' => 7, 'category_id' => 4]);

        Product::firstOrCreate(['id' => 8,'code'=>'100812026',
            'name'=>'BIO MATIC AUTOMATIC TRANSMISSION FLUID DEXRON III H',
             'ue'=>'6x1/4', 'price' =>'18.10', 'tax' => '16', 'sale_price' => '21']);
        ProductUnit::firstOrCreate(['product_id' =>8, 'unit_id' => 1]);
        ProductCategory::firstOrCreate(['product_id' => 8, 'category_id' => 5]);

        Product::firstOrCreate(['id' => 9,'code'=>'101012029',
            'name'=>'BIO MATIC AUTOMATIC TRANSMISSION FLUID DEXRON III H',
             'ue'=>'8x1/8', 'price' =>'12.93', 'tax' => '16', 'sale_price' => '15']);
        ProductUnit::firstOrCreate(['product_id' =>9, 'unit_id' => 3]);
        ProductCategory::firstOrCreate(['product_id' => 9, 'category_id' => 5]);

        Product::firstOrCreate(['id' => 10,'code'=>'101012031',
            'name'=>'BIO MATIC AUTOMATIC TRANSMISSION FLUID MULTI-VEHICLE MERCON V',
             'ue'=>' 8x1/8', 'price' =>'26.72', 'tax' => '16', 'sale_price' => '31']);
        ProductUnit::firstOrCreate(['product_id' =>10, 'unit_id' => 3]);
        ProductCategory::firstOrCreate(['product_id' => 10, 'category_id' => 5]);

        Product::firstOrCreate(['id' => 11,'code'=>'101012032',
            'name'=>'BIO MATIC AUTOMATIC TRANSMISSION FLUID MULTI-VEHICLE PREMIUM DEXRON VI',
             'ue'=>' 8x1/8', 'price' =>'26.72', 'tax' => '16', 'sale_price' => '31']);
        ProductUnit::firstOrCreate(['product_id' =>11, 'unit_id' => 3]);
        ProductCategory::firstOrCreate(['product_id' => 11, 'category_id' => 5]);

        Product::firstOrCreate(['id' => 12,'code'=>'101012030',
            'name'=>'BIO MATIC POWER STEERING FLUID',
             'ue'=>' 8x1/8', 'price' =>'13.79', 'tax' => '16', 'sale_price' => '16']);
        ProductUnit::firstOrCreate(['product_id' =>12, 'unit_id' => 3]);
        ProductCategory::firstOrCreate(['product_id' => 12, 'category_id' => 6]);

        Product::firstOrCreate(['id' => 13,'code'=>'101012036',
            'name'=>'BIO MARINE 2 CYCLE OUTBOARD WATER COOLED MOTOR OIL TC-W3',
             'ue'=>' 8x1/8', 'price' =>'21.55', 'tax' => '16', 'sale_price' => '25']);
        ProductUnit::firstOrCreate(['product_id' =>13, 'unit_id' => 3]);
        ProductCategory::firstOrCreate(['product_id' => 13, 'category_id' => 8]);

        Product::firstOrCreate(['id' => 14,'code'=>'101015038',
            'name'=>'BIO MARINE PROPELLER GEAR OIL API GL-5 SAE 80W90',
             'ue'=>' 8x1/8', 'price' =>'0', 'tax' => '16', 'sale_price' => '0']);
        ProductUnit::firstOrCreate(['product_id' =>14, 'unit_id' => 3]);
        ProductCategory::firstOrCreate(['product_id' => 14, 'category_id' => 8]);

        Product::firstOrCreate(['id' => 15,'code'=>'101014034',
            'name'=>'ECO CYCLE 4 STROKE MOTORCYCLE SYNTHETIC BLEND JASO MA/MB SAE 20W50 ',
             'ue'=>' 8x1/8', 'price' =>'15.52', 'tax' => '16', 'sale_price' => '18']);
        ProductUnit::firstOrCreate(['product_id' =>15, 'unit_id' => 3]);
        ProductCategory::firstOrCreate(['product_id' => 15, 'category_id' => 7]);

        Product::firstOrCreate(['id' => 16,'code'=>'100718061',
            'name'=>'CAR MINE BRAKE FLUID - Fluido para frenos',
             'ue'=>' 8x1/8', 'price' =>'20.69', 'tax' => '16', 'sale_price' => '24']);
        ProductUnit::firstOrCreate(['product_id' =>16, 'unit_id' => 3]);
        ProductCategory::firstOrCreate(['product_id' => 16, 'category_id' => 9]);

        Product::firstOrCreate(['id' => 17,'code'=>'101017064',
            'name'=>'CAR MINE SHAMPOO - Champú para lavado de carrocería',
             'ue'=>' 8x1/8', 'price' =>'7.76', 'tax' => '16', 'sale_price' => '9']);
        ProductUnit::firstOrCreate(['product_id' =>17, 'unit_id' => 3]);
        ProductCategory::firstOrCreate(['product_id' => 17, 'category_id' => 9]);

        Product::firstOrCreate(['id' => 18,'code'=>'101017043',
            'name'=>'CAR MINE COOLANT FLUID - Refrigerante 50% MEG y Anticorrosivo',
             'ue'=>'3x1', 'price' =>'23.28', 'tax' => '16', 'sale_price' => '27']);
        ProductUnit::firstOrCreate(['product_id' =>18, 'unit_id' => 2]);
        ProductCategory::firstOrCreate(['product_id' => 18, 'category_id' => 9]);

        Product::firstOrCreate(['id' => 19,'code'=>'100917040',
            'name'=>'CAR MINE ENGINE DEGREASER - Desengrasante multiuso',
             'ue'=>'3x1', 'price' =>'13.79', 'tax' => '16', 'sale_price' => '16']);
        ProductUnit::firstOrCreate(['product_id' =>19, 'unit_id' => 2]);
        ProductCategory::firstOrCreate(['product_id' => 19, 'category_id' => 9]);

        Product::firstOrCreate(['id' => 20,'code'=>'100917039',
            'name'=>'CAR MINE WINDSHIELD WASHER FLUID - Líquido Limpiaparabrisas',
             'ue'=>'3x1', 'price' =>'10.34', 'tax' => '16', 'sale_price' => '12']);
        ProductUnit::firstOrCreate(['product_id' =>20, 'unit_id' => 2]);
        ProductCategory::firstOrCreate(['product_id' => 20, 'category_id' => 9]);

        Product::firstOrCreate(['id' => 21,'code'=>'100310062',
            'name'=>'TAMBOR SYNTHETIC BLEND OIL API SN/SL SAE 15W40',
             'ue'=>'Tambor', 'price' =>'595.69', 'tax' => '16', 'sale_price' => '691']);
        ProductUnit::firstOrCreate(['product_id' =>21, 'unit_id' => 5]);
        ProductCategory::firstOrCreate(['product_id' => 21, 'category_id' => 13]);

        Product::firstOrCreate(['id' => 22,'code'=>'100310083',
            'name'=>'ECO CLASSIC MULTIGRADE MOTOR OIL API SL SAE 20W50 ',
             'ue'=>'Paila', 'price' =>'49.14', 'tax' => '16', 'sale_price' => '57']);
        ProductUnit::firstOrCreate(['product_id' =>22, 'unit_id' => 4]);
        ProductCategory::firstOrCreate(['product_id' => 22, 'category_id' => 13]);

        Product::firstOrCreate(['id' => 23,'code'=>'100310066',
            'name'=>'ECO CLASSIC MULTIGRADE MOTOR OIL API SL SAE 20W50 ',
             'ue'=>'Tambor', 'price' =>'517.24', 'tax' => '16', 'sale_price' => '600']);
        ProductUnit::firstOrCreate(['product_id' =>23, 'unit_id' => 5]);
        ProductCategory::firstOrCreate(['product_id' => 23, 'category_id' => 13]);

        Product::firstOrCreate(['id' => 24,'code'=>'DCI001',
            'name'=>'15W40 CI-4',
             'ue'=>'Paila', 'price' =>'51.72', 'tax' => '16', 'sale_price' => '60']);
        ProductUnit::firstOrCreate(['product_id' =>24, 'unit_id' => 4]);
        ProductCategory::firstOrCreate(['product_id' => 24, 'category_id' => 13]);
        ProductCategory::firstOrCreate(['product_id' => 24, 'category_id' => 4]);

        Product::firstOrCreate(['id' => 25,'code'=>'DCI002','name'=>'15W40 CI-4',
         'ue'=>'Tambor', 'price' =>'539.65', 'tax' => '16', 'sale_price' => '626']);
        ProductUnit::firstOrCreate(['product_id' =>25, 'unit_id' => 5]);
        ProductCategory::firstOrCreate(['product_id' => 25, 'category_id' => 13]);
        ProductCategory::firstOrCreate(['product_id' => 25, 'category_id' => 4]);

        Product::firstOrCreate(['id' => 26,'code'=>'100411055','name'=>'ECO PLUS MONOGRA DE MOTOR OIL API CF SAE 50 TBN 10',
         'ue'=>'Paila', 'price' =>'47.41', 'tax' => '16', 'sale_price' => '55']);
        ProductUnit::firstOrCreate(['product_id' =>26, 'unit_id' => 4]);
        ProductCategory::firstOrCreate(['product_id' => 26, 'category_id' => 13]);
        ProductCategory::firstOrCreate(['product_id' => 26, 'category_id' => 4]);

        Product::firstOrCreate(['id' => 27,'code'=>'100311058','name'=>'SAE CF 50',
         'ue'=>'Tambor', 'price' =>'495.69', 'tax' => '16', 'sale_price' => '575']);
        ProductUnit::firstOrCreate(['product_id' =>27, 'unit_id' => 5]);
        ProductCategory::firstOrCreate(['product_id' => 27, 'category_id' => 13]);
        ProductCategory::firstOrCreate(['product_id' => 27, 'category_id' => 4]);

        Product::firstOrCreate(['id' => 28,'code'=>'100413054','name'=>'HIDRÁULICO 68',
         'ue'=>'Paila', 'price' =>'44.83', 'tax' => '16', 'sale_price' => '52']);
        ProductUnit::firstOrCreate(['product_id' =>28, 'unit_id' => 4]);
        ProductCategory::firstOrCreate(['product_id' => 28, 'category_id' => 13]);
        ProductCategory::firstOrCreate(['product_id' => 28, 'category_id' => 11]);

        Product::firstOrCreate(['id' => 29,'code'=>'HHD002','name'=>'HIDRÁULICO 68',
         'ue'=>'Tambor', 'price' =>'450.86', 'tax' => '16', 'sale_price' => '523']);
        ProductUnit::firstOrCreate(['product_id' =>29, 'unit_id' => 5]);
        ProductCategory::firstOrCreate(['product_id' => 29, 'category_id' => 13]);
        ProductCategory::firstOrCreate(['product_id' => 29, 'category_id' => 11]);

        Product::firstOrCreate(['id' => 30,'code'=>'ENG001','name'=>'GEAR 85W140',
         'ue'=>'Paila', 'price' =>'62.07', 'tax' => '16', 'sale_price' => '72']);
        ProductUnit::firstOrCreate(['product_id' =>30, 'unit_id' => 4]);
        ProductCategory::firstOrCreate(['product_id' => 30, 'category_id' => 13]);
        ProductCategory::firstOrCreate(['product_id' => 30, 'category_id' => 12]);

        Product::firstOrCreate(['id' => 31,'code'=>'ENG002','name'=>'SAE GEAR 80W90 ',
         'ue'=>'Paila', 'price' =>'59.49', 'tax' => '16', 'sale_price' => '69']);
        ProductUnit::firstOrCreate(['product_id' =>31, 'unit_id' => 4]);
        ProductCategory::firstOrCreate(['product_id' => 31, 'category_id' => 13]);
        ProductCategory::firstOrCreate(['product_id' => 31, 'category_id' => 12]);

        Product::firstOrCreate(['id' => 32,'code'=>'100313070','name'=>'TAMBOR TURBINE OIL HT 32',
         'ue'=>'Tambor', 'price' =>'584.48', 'tax' => '16', 'sale_price' => '678']);
        ProductUnit::firstOrCreate(['product_id' =>32, 'unit_id' => 5]);
        ProductCategory::firstOrCreate(['product_id' => 32, 'category_id' => 13]);

    }
}
