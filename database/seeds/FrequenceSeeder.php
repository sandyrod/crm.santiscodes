<?php

use Illuminate\Database\Seeder;
use App\Models\Frequence;

class FrequenceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Frequence::firstOrCreate(['name' => 'SEMANAL', 'description' => 'UNA VEZ POR SEMANA']);
        Frequence::firstOrCreate(['name' => 'QUINCENAL', 'description' => 'DOS VECES POR MES']);
    }
}
