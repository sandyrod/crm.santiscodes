<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(AdminUserSeeder::class);
        $this->call(UnitsSeeder::class);
        $this->call(BussinessTypeSeeder::class);
        $this->call(DistributionChannelSeeder::class);
        $this->call(FrequenceSeeder::class);
        $this->call(CategorySeeder::class);
        $this->call(BrandsSeeder::class);
        $this->call(ProductsSeeder::class);
    }
}
