<?php

use Illuminate\Database\Seeder;
use App\Models\Brand;

class BrandsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Brand::firstOrCreate(['id' => 1, 'name' => 'CHRONUS']);
        Brand::firstOrCreate(['id' => 2, 'name' => 'ACEDELCO']);
        Brand::firstOrCreate(['id' => 3, 'name' => 'CARDENAL']);
        Brand::firstOrCreate(['id' => 4, 'name' => 'CHEBRON']);
        Brand::firstOrCreate(['id' => 5, 'name' => 'ENERGI']);
        Brand::firstOrCreate(['id' => 6, 'name' => 'FC']);
        Brand::firstOrCreate(['id' => 7, 'name' => 'GULF']);
        Brand::firstOrCreate(['id' => 8, 'name' => 'INCA']);
        Brand::firstOrCreate(['id' => 9, 'name' => 'KIXX']);
        Brand::firstOrCreate(['id' => 10, 'name' => 'LANCO']);
        Brand::firstOrCreate(['id' => 11, 'name' => 'MOTOKRAF']);
        Brand::firstOrCreate(['id' => 12, 'name' => 'MOBIL']);
        Brand::firstOrCreate(['id' => 13, 'name' => 'SLYNG']);
        Brand::firstOrCreate(['id' => 14, 'name' => 'PDV']);
        Brand::firstOrCreate(['id' => 15, 'name' => 'REIKE']);
        Brand::firstOrCreate(['id' => 16, 'name' => 'SHELL']);
        Brand::firstOrCreate(['id' => 17, 'name' => 'SKY']);
        Brand::firstOrCreate(['id' => 18, 'name' => 'SUPREMA']);
        Brand::firstOrCreate(['id' => 19, 'name' => 'ULTRALUX']);
        Brand::firstOrCreate(['id' => 20, 'name' => 'VENOCO']);
        Brand::firstOrCreate(['id' => 21, 'name' => 'WIX']);
    }
}
