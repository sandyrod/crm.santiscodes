<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Auth;
use DB;

use App\Models\Product;
use App\Models\ProductCategory;
use App\Models\ProductPhoto;
use App\Models\ProductUnit;
use App\Models\Warehouse;
use App\Models\Category;
use App\Models\Provider;
use App\Models\Unit;
use App\Models\Visit;
use App\Models\VisitDetail;
use App\Models\Client;
use App\Repositories\ProductsRepository;
use App\Repositories\OrdersRepository;

class Visitcard extends Component
{
    public $data, $search, $data_visit,
        $name, $description, $clients, $visit_id, $cliente, $inventory, 
        $products_sel, $client_id;
    public $error = '';

    public function mount (Request $request)
    {
        $this->initProps();
        $this->clients = Client::get();
        $this->inventory = [];

        if (! $request->id)
            return;

        $this->setValues($request->id);
    }

    private function setValues($client_id)
    {
        $client = Client::Find($client_id);
        if (! $client)
            return;

        $this->client_id = $client_id;
        $this->cliente = $client->businessname;
        $visit = Visit::whereDate('created_at', '=', Carbon::now()->format('Y-m-d'))
                ->where('client_id', '=', $client_id)->first();

        if(! $visit)
            $visit = Visit::Create(['client_id' => $client_id, 'user_id' => auth()->user()->id]);

        $this->visit_id = $visit->id;
    }

    private function initProps()
    {
        $this->search = '';
        $this->error = '';
        $this->products_sel = null;
        $this->client_id = null;
        $this->cliente = null;
        $this->visit_id = null;
    }

    public function render()
    {
        $this->data = ProductsRepository::getProducts($this->search);

        $this->products_sel = ProductsRepository::getVisitProducts($this->data->pluck('id')->toArray(), $this->visit_id);

        return view('livewire.visitcard');
    }

    public function save()
    {
        if (! $this->client_id){
            $this->error = 'Debe seleccionar el cliente';
            return;
        }

        $this->setValues($this->client_id);

        if (! $this->visit_id){

            $visit = Visit::create([
                'client_id' => $this->client_id,
                'user_id' => Auth::user()->id
            ]);
            $this->visit_id = $visit->id;
        }

        $this->saveDetails();
    }

    private function saveDetails()
    {
        $visit_detail = new VisitDetail();
        if(! $this->visit_id){
            $visit = Visit::create([
                'client_id' => $this->client_id,
                'user_id' => Auth::user()->id
            ]);
            $this->visit_id = $visit->id;
        }
        foreach ($this->products_sel as $product_id => $detail){
            $visit = VisitDetail::where('visit_id', $this->visit_id)->where('product_id', $product_id)->first();

            if ($visit){
                $visit->distribution = array_key_exists('distribution', $detail) && $detail['distribution'] !='' ? $detail['distribution'] : 0;
                $visit->inventory = array_key_exists('inventory', $detail) && $detail['inventory'] !='' ? $detail['inventory'] : 0;
                $visit->suggested = array_key_exists('suggested', $detail) && $detail['suggested'] !='' ? $detail['suggested'] : 0;
                $visit->sold = array_key_exists('sold', $detail) && $detail['sold'] !='' ? $detail['sold'] : 0;
                $visit->save();
            } else {
                $visit = VisitDetail::create([
                    'visit_id' => $this->visit_id,
                    'product_id' => $product_id,
                    'inventory' => array_key_exists('inventory', $detail) && $detail['inventory'] !='' ? $detail['inventory'] : 0,
                    'distribution' => array_key_exists('distribution', $detail) && $detail['distribution'] !='' ? $detail['distribution'] : 0,
                    'suggested' => array_key_exists('suggested', $detail) && $detail['suggested'] !='' ? $detail['suggested'] : 0,
                    'sold' => array_key_exists('sold', $detail) && $detail['sold'] !='' ? $detail['sold'] : 0
                ]);
            }
        }

        $this->emit('notify:toast', ['type'  => 'success', 'title' => 'Registro actualizado...']);

    }

    public function generateOrder()
    {
        $order = OrdersRepository::generateOrder($this->visit_id);
        return  redirect()->route('orders', $order->id);
    }
}
