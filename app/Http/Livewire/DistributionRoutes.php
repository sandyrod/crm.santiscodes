<?php

namespace App\Http\Livewire;

use App\User;
use App\Models\Distributionroute;
use App\Models\DistributionrouteUser;

use Livewire\Component;

class DistributionRoutes extends Component
{
    public $data, $name, $description, $search, $distributionroute_id, $users, $user_id, $users_id;
    public $updateMode = false;

    public function mount()
    {
        $users = User::Where('email','!=',null)->get();
        $this->distributionroute_id = '';
        $this->name ='';
        $this->description = '';
        $this->search = '';
        $this->data = [];
        // $this->users = $users;
        $this->user_id = '';
        $this->users_id = [];
    }

    public function render()
    {
        $this->data = ($this->search)
            ? Distributionroute::where('name','like', '%'.$this->search.'%')
                    ->orWhere('description', 'like', '%'.$this->search.'%')
                    ->get()
            : Distributionroute::orderBy('name')->get();
        return view('livewire.distribution-route');
    }

    private function emitUpdates()
    {
        $this->emit('selectUpdates', [ 'users_id' => $this->users_id ] );
    }

    // private function updateUsers($route_id, $mode='update')
    // {
    //     DistributionrouteUser::where('distributionroutes_id', $route_id)->delete();
    //     if ($mode == 'update'){
    //         foreach ($this->users_id as $item)
    //             DistributionrouteUser::create([
    //                 'distributionroutes_id' => $route_id,
    //                 'user_id' => $item
    //             ]);
    //     }
    // }

    public function edit($id)
    {
        $record = Distributionroute::findOrFail($id);
        $this->distributionroute_id = $id;
        $this->name = $record->name;
        $this->description = $record->description;
        $this->updateMode = true;

        // $this->users_id = $record->users->pluck('user_id')->toArray() ?? [];

        $this->emit('notify:toast', ['type'  => 'success', 'title' => 'Registro cargado...']);
        $this->emitUpdates();

    }

    public function update()
    {
        $this->validate([
            'distributionroute_id' => 'required|numeric',
            'name' => 'required|min:3'
        ]);
        if ($this->distributionroute_id) {
            $record = Distributionroute::find($this->distributionroute_id);
            $record->update([
                'name' => strtoupper($this->name),
                'description' => strtoupper($this->description)
            ]);

            // if ($this->users_id)
            //     $this->updateUsers($record->id);

            $this->emit('notify:toast', ['type'  => 'success', 'title' => 'Registro actualizado...']);

            $this->resetInput();
            $this->updateMode = false;
        }
    }

    public function destroy($id)
    {
        if ($id) {
            $record = Distributionroute::find($id);
            // $this->updateUsers($record->id, 'delete');

            $record->delete();

            $this->emit('notify:toast', ['type'  => 'success', 'title' => 'Registro eliminado...']);
            $this->resetInput();
        }
    }

    public function resetInput()
    {
        $this->name = null;
        $this->description = null;
        $this->distributionroute_id = '';
        $this->updateMode = false;
        $this->users_id = [];

        $this->emitUpdates();
    }

    public function save()
    {
        if ($this->updateMode)
            return $this->update();

        return $this->store();
    }

    public function store()
    {
        $this->validate([
            'name' => 'required|min:3'
        ]);
        $record = Distributionroute::create([
            'name' => strtoupper($this->name),
            'description' => strtoupper($this->description)
        ]);

        if ($this->users_id)
            // $this->updateUsers($record->id);

        $this->emit('notify:toast', ['type'  => 'success', 'title' => 'Registro creado...']);

        $this->resetInput();
    }
}
