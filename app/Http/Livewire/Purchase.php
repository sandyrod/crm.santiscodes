<?php

namespace App\Http\Livewire;

use App\Models\Product;
use App\Models\Provider;
use App\Models\Purchaseorder;
use App\Models\Purchase_detail;
use App\Models\Warehouse;
use App\Models\Stock;

use App\Repositories\ProductsRepository;

use Auth;

use Livewire\Component;

class Purchase extends Component
{
    public $data, $products, $product_id, $search, $price, $quantity, $purchase_id,
    $providers, $providers_id, $description, $number, $warehouses_id, $warehouses,
    $purchasedetails;
    public $updateMode = false;

    public function mount()
    {
        $this->price=0;
        $this->quantity=1;
        $this->providers = Provider::All();
        $this->products = Product::All();
        $this->warehouses = Warehouse::orderBy('name', 'ASC')->get();
        $this->description = '';
        $this->number = '';
    }

    public function render()
    {
        $this->data = ($this->search)
                ? Purchaseorder::where('id', 'like', '%'.$this->search.'%')
                        ->orWhereIn('client_id', function( $query ){
                            $query->select('id')
                            ->from('clients')
                            ->where('businessname', 'like', '%'.$this->search.'%')
                            ->orWhere('rif', 'like', '%'.$this->search.'%');
                        })
                        ->orWhereIn('user_id', function( $query ){
                            $query->select('id')
                            ->from('users')
                            ->where('name', 'like', '%'.$this->search.'%');
                        })
                        ->orderBy('id', 'DESC')
                        ->get()
                : Purchaseorder::orderBy('id', 'DESC')->get();
        return view('livewire.purchase');
    }

    public function save()
    {
        if ($this->updateMode)
            return $this->update();

        return $this->store();
    }

    public function store()
    {
        $this->validate([
            'number' => 'required',
            'warehouses_id' => 'required',
            'providers_id' => 'required'
        ]);

        $purchase = Purchaseorder::create([
            'number' => $this->number,
            'user_id' => Auth::user()->id,
            'warehouse_id' => $this->warehouses_id,
            'provider_id' => $this->providers_id
        ]);
        $this->purchase_id = $purchase->id;

        $this->emit('notify:toast', ['type'  => 'success', 'title' => 'Registro creado...']);
        $this->showProducts();
    }

    public function edit($id)
    {
        $record = Purchaseorder::findOrFail($id);
        $this->purchase_id = $id;
        $this->user_id = $record->client_id;
        $this->provider_id = $record->provider_id ?? '';
        $this->warehouses_id = $record->warehouse_id ?? '';
        $this->number = $record->number;

        $this->showProducts();
        $this->emitUpdates();
        $this->emit('notify:toast', ['type'  => 'success', 'title' => 'Registro cargado...']);
    }

    public function showProducts()
    {
        $this->updateMode = true;
        $this->purchasedetails = Purchase_detail::where('purchase_id', $this->purchase_id)->get();
        $this->resetProduct();
    }

    private function resetProduct()
    {
        $this->product_id = '';
        $this->unit = '';
        $this->price = 0;
        $this->quantity = 1;

        $this->emitDetailUpdates();
    }

    private function emitDetailUpdates()
    {
        $this->emit('selectDetailUpdates', [ 'product_id' => $this->product_id ] );
    }

    public function update()
    {
        $this->validate([
            'number' => 'required',
            'product_id' => 'required',
            'price' => 'required',
            'quantity' => 'required'
        ]);

        if ($this->purchase_id) {
            $record = Purchaseorder::find($this->purchase_id);
            $record->update([
                'client_id' => $this->client_id,
                'user_id' => Auth::user()->id,
                'warehouse_id' => $this->warehouses_id,
                'provider_id' => $this->providers_id
            ]);
            //$this->resetInput();
            //$this->updateMode = false;
            $this->emit('notify:toast', ['type'  => 'success', 'title' => 'Registro actualizado...']);
        }
    }

    public function addProduct()
    {
        if (! $this->purchase_id || ! $this->product_id || ! $this->quantity )
            return;

        $purchasedetails = Purchase_detail::create([
            'purchase_id' => $this->purchase_id,
            'product_id' => $this->product_id,
            'quantity' => $this->quantity,
            'price' => $this->price
        ]);
        $stock = Stock::where('product_id', $this->product_id)
            ->where('warehouse_id',$this->warehouses_id)
            ->first();

        $newStock = ProductsRepository::getStock($this->product_id, $this->warehouses_id) ?? 0;
        ProductsRepository::setPrice($this->product_id, $this->price);
        $stock->quantity = $newStock;
        $stock->save();

        $this->showProducts();
    }

    private function emitUpdates()
    {
        $this->emit('selectUpdates', ['providers_id'=>$this->providers_id, 'warehouses_id'=>$this->warehouses_id]);
    }

    public function destroy($id)
    {
        if ($id) {
            Purchase_detail::where('purchase_id', $id)->delete();
            $record = Purchaseorder::find($id);
            $record->delete();
            $this->resetInput();
        }
    }

    public function destroyProduct($id)
    {
        if ($id) {
            $record = Purchase_detail::find($id);
            $record->delete();
            $this->emit('notify:toast', ['type'  => 'success', 'title' => 'Registro eliminado...']);
            $this->resetProduct();
        }
    }

    public function resetInput()
    {
        $this->price=0;
        $this->quantity=1;
        $this->providers = Provider::All();
        $this->products = Product::All();
        $this->warehouses = Warehouse::orderBy('name', 'ASC')->get();
        $this->description = '';
        $this->number = '';
        $this->updateMode = false;
        $this->purchasedetails = null;

        $this->emitUpdates();
    }
}
