<?php

namespace App\Http\Livewire;
use App\Models\Brand;

use Livewire\Component;

class Brands extends Component
{
    public $data, $name, $description, $brand_id, $search;
    public $updateMode = false;

    public function mount()
    {
        $this->name = '';
        $this->description = '';
        $this->brand_id = null;
        $this->search = '';
        $this->data = [];
    }

    public function render()
    {
    	$this->data = ($this->search) 
                ? Brand::where('name', 'like', '%'.$this->search.'%')
                		->orWhere('description', 'like', '%'.$this->search.'%')
                		->orderBy('name', 'ASC')
                        ->get()
                : Brand::orderBy('name', 'ASC')->get();
        return view('livewire.Brands');
    }

    public function resetInput()
    {
        $this->name = null;
        $this->description = null;
        $this->updateMode = false;
    }

    public function save()
    {
    	if ($this->updateMode)
    		return $this->update();

    	return $this->store();
    }

    public function store()
    {
        $this->validate([
            'name' => 'required|min:3'
        ]);
        Brand::create([
            'name' => strtoupper($this->name),
            'description' => strtoupper($this->description)
        ]);
        $this->emit('notify:toast', ['type'  => 'success', 'title' => 'Registro creado...']);
        $this->resetInput();
    }
    public function edit($id)
    {
        $record = Brand::findOrFail($id);
        $this->brand_id = $id;
        $this->name = $record->name;
        $this->description = $record->description;
        $this->updateMode = true;
        $this->emit('notify:toast', ['type'  => 'success', 'title' => 'Registro cargado...']);
    }
    public function update()
    {
        $this->validate([
            'brand_id' => 'required|numeric',
            'name' => 'required|min:3'
        ]);
        if ($this->brand_id) {
            $record = Brand::find($this->brand_id);
            $record->update([
                'name' => strtoupper($this->name),
                'description' => strtoupper($this->description)
            ]);
            $this->emit('notify:toast', ['type'  => 'success', 'title' => 'Registro actualizado...']);
            $this->resetInput();
            $this->updateMode = false;
        }
    }
    public function destroy($id)
    {
        if ($id) {
            $record = Brand::find($id);
            $record->delete();
            $this->emit('notify:toast', ['type'  => 'success', 'title' => 'Registro eliminado...']);
            $this->resetInput();
        }
    }
}
