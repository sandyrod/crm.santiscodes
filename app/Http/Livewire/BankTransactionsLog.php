<?php

namespace App\Http\Livewire;

use App\Models\Bank;
use App\Models\Bankaccount;
use App\Models\BankTransaction;

use Illuminate\Support\Facades\Auth;

use Livewire\Component;

class BankTransactionsLog extends Component
{
    public $data, $search, $banks, $bankid, $bankaccount_id, $bankaccounts, $status,
        $description, $transaction_type_id, $transaction_method_id, $amount,
        $transactioncode, $banktransaction_id, $methods;
    public $statuslist;

    public $updateMode = false;

    public function mount()
    {
        $this->banks = Bank::All();
        $this->bankaccounts = Bankaccount::All();
        $this->transaction_method_id = null;
        $this->transaction_type_id = null;
        $this->description = '';
        $this->bankaccount_id = null;
        $this->bankid = null;
        $this->amount = 0;
        $this->methods = ['0' => 'Deposito',
            '1' => 'Retiro',
            '2' => 'Transferencia',
            '3' => 'Cheque',
            '4' => 'Pago movil',
            '5' => 'Otro'];
    }

    public function render()
    {
        $this->data = ($this->search)
                ? BankTransaction::where('id', 'like', '%'.$this->search.'%')
                        ->orWhereIn('user_id', function( $query ){
                            $query->select('id')
                            ->from('users')
                            ->where('name', 'like', '%'.$this->search.'%');
                        })
                        ->orderBy('id', 'DESC')
                        ->get()
                : BankTransaction::orderBy('id', 'DESC')->get();
        return view('livewire.bank-transactions-log');
    }

    public function resetInput()
    {
        $this->banks = Bank::All();
        $this->bankaccounts = Bankaccount::All();
        $this->transaction_method_id = null;
        $this->transaction_type_id = null;
        $this->description = '';
        $this->bankaccount_id = null;
        $this->bankid = null;
        $this->amount = 0;

        $this->emitUpdates();
    }

    public function save()
    {
        if ($this->updateMode)
            return $this->update();

        return $this->store();
    }

    public function store()
    {
        $this->validate([
            'amount'                => 'required|numeric',
            'transactioncode'       => 'required|min:4',
            'bankaccount_id'        => 'required',
            'status'                => 'required',
            'transaction_method_id' => 'required',
            'transaction_type_id'   => 'required'
        ]);
        BankTransaction::create([
            'bankaccount_id'        => $this->bankaccount_id,
            'user_id'               => Auth::user()->id,
            'transactioncode'       => $this->transactioncode,
            'status'                => $this->status,
            'description'           => $this->description,
            'amount'                => $this->amount,
            'transaction_type_id'   => $this->transaction_type_id,
            'transaction_method_id' => $this->transaction_method_id
        ]);

        $this->emit('notify:toast', ['type'  => 'success', 'title' => 'Registro creado...']);
        $this->resetInput();
    }

    public function edit($id)
    {
        $record = BankTransaction::findOrFail($id);
        $this->banktransaction_id      = $id;
        $this->user_id                  = $record->user_id;
        $this->transactioncode          = $record->transactioncode;
        $this->status                   = $record->status;
        $this->description              = $record->description;
        $this->amount                   = $record->amount;
        $this->transaction_method_id    = $record->transaction_method_id;
        $this->transaction_type_id      = $record->transaction_type_id;
        $this->bankid                  = $record->bankaccount->bank->id;
        $this->bankaccount_id           = $record->bankaccount_id;

        $this->emitUpdates();

        $this->updateMode = true;

        $this->emit('notify:toast', ['type'  => 'success', 'title' => 'Registro cargado...']);
    }

    public function update()
    {
        $this->validate([
            'amount'                => 'required|numeric',
            'transactioncode'       => 'required|min:4',
            'bankaccount_id'        => 'required',
            'status'                => 'required',
            'transaction_method_id' => 'required',
            'transaction_type_id'   => 'required'
        ]);
        if ($this->banktransaction_id) {
            $record = BankTransaction::find($this->banktransaction_id);
            $record->update([
                'banktransaction_id'    => $this->banktransaction_id,
                'bankaccount_id'        => $this->bankaccount_id,
                'user_id'               => Auth::user()->id,
                'transactioncode'       => $this->transactioncode,
                'status'                => $this->status,
                'description'           => $this->description,
                'amount'                => $this->amount,
                'transaction_type_id'   => $this->transaction_type_id,
                'transaction_method_id' => $this->transaction_method_id
            ]);
            $this->emit('notify:toast', ['type'  => 'success', 'title' => 'Registro actualizado...']);
            $this->resetInput();
            $this->updateMode = false;
        }
    }

    public function destroy($id)
    {
        if ($id) {
            $record = BankTransactionsLog::find($id);
            $record->delete();

            $this->emit('notify:toast', ['type'  => 'success', 'title' => 'Registro eliminado...']);
            $this->resetInput();
        }
    }

    public function emitUpdates()
    {
        $this->emit('selectUpdates',
            [ 'bankaccount_id'          => $this->bankaccount_id,
              'bankid'                  => $this->bankid,
              'transaction_method_id'   => $this->transaction_method_id ] );
    }


    public function updatedbankid($value)
    {
        //$this->bankaccounts = Bankaccount::where('bankid',$this->bankid)->get();
        //.    bankaccounts
        $this->bankaccounts = Bankaccount::where('bank_id',$this->bankid)->get();
        $this->emit('notify:toast', ['type'  => 'success', 'title' => $this->bankid]);
    }

}
