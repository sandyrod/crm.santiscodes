<?php

namespace App\Http\Livewire;
use App\Models\Category;

use Livewire\Component;

class Categories extends Component
{
    public $data, $name, $description, $parent_id, $category_id, $search;
    public $updateMode = false;

    public function mount()
    {
        $this->name = '';
        $this->description = '';
        $this->parent_id = null;
        $this->category_id = null;
        $this->search = '';
        $this->data = [];
    }

    public function render()
    {
    	$this->data = ($this->search) 
                ? Category::where('name', 'like', '%'.$this->search.'%')
                		->orWhere('description', 'like', '%'.$this->search.'%')
                		->orderBy('name', 'ASC')
                        ->get()
                : Category::orderBy('name', 'ASC')->get();
        return view('livewire.Categories');
    }

    public function resetInput()
    {
        $this->name = null;
        $this->description = null;
        $this->parent_id = null;
        $this->updateMode = false;;
    }
    public function save()
    {
    	if ($this->updateMode)
    		return $this->update();
    	
    	return $this->store();
    }

    public function store()
    {
        $this->validate([
            'name' => 'required|min:3'
        ]);
        Category::create([
            'name' => strtoupper($this->name),
            'description' => strtoupper($this->description),
            'parent_id' => $this->parent_id
        ]);
        $this->emit('notify:toast', ['type'  => 'success', 'title' => 'Registro creado...']);
        $this->resetInput();
    }
    public function edit($id)
    {
        $record = Category::findOrFail($id);
        $this->category_id = $id;
        $this->name = $record->name;
        $this->description = $record->description;
        $this->parent_id = $record->parent_id;
        $this->updateMode = true;
        $this->emit('notify:toast', ['type'  => 'success', 'title' => 'Registro cargado...']);
    }
    public function update()
    {
        $this->validate([
            'category_id' => 'required|numeric',
            'name' => 'required|min:3'
        ]);
        if ($this->category_id) {
            $record = Category::find($this->category_id);
            $record->update([
                'name' => strtoupper($this->name),
                'description' => strtoupper($this->description),
                'parent_id' => $this->parent_id ?? null
            ]);
            $this->emit('notify:toast', ['type'  => 'success', 'title' => 'Registro actualizado...']);
            $this->resetInput();
            $this->updateMode = false;
        }
    }
    public function destroy($id)
    {
        if ($id) {
            $record = Category::find($id);
            $record->delete();
            $this->emit('notify:toast', ['type'  => 'success', 'title' => 'Registro eliminado...']);
            $this->resetInput();
        }
    }
}
