<?php

namespace App\Http\Livewire;
use App\Models\Product;
use App\Models\ProductCategory;
use App\Models\ProductPhoto;
use App\Models\ProductProvider;
use App\Models\ProductUnit;
use App\Models\Warehouse;
use App\Models\Category;
use App\Models\Provider;
use App\Models\Unit;

use Livewire\Component;
use Livewire\WithFileUploads;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Log;

class Products extends Component
{
    use WithFileUploads;

    public $photo, $product_photos, $providers_id;
    public $data, $name, $description, $warehouses, $units, $categories,
    $providers, $product_id, $search, $categories_id, $units_id, $price, $tax, $sale_price, $code, $ue;
    public $updateMode = false;

    public function mount()
    {
        $this->name = '';
        $this->code = '';
        $this->description = '';
        $this->product_id = null;
        $this->price = 0;
        $this->tax=0;
        $this->sale_price=0;
        $this->search = '';
        $this->warehouses = Warehouse::orderBy('name', 'ASC')->get();
        $this->categories = Category::orderBy('name', 'ASC')->get();
        $this->providers = Provider::orderBy('name', 'ASC')->get();
        $this->units = Unit::orderBy('name', 'ASC')->get();
        $this->product_photos = [];
        $this->categories_id = [];
        $this->providers_id = [];
        $this->units_id = [];
        $this->data = [];
        $this->ue = '';
    }

    public function render()
    {
    	$this->data = ($this->search)
                ? Product::where('name', 'like', '%'.$this->search.'%')
                		->orWhere('description', 'like', '%'.$this->search.'%')
                        ->orWhere('code', 'like', '%'.$this->search.'%')
                        ->orWhereIn('id', function( $query ){
                            $query->select('product_id as id')
                            ->from('products_categories')
                            ->leftJoin('categories', 'categories.id', 'products_categories.category_id')
                            ->where('name', 'like', '%'.$this->search.'%');
                        })
                        ->orWhereIn('id', function( $query ){
                            $query->select('product_id as id')
                            ->from('products_units')
                            ->leftJoin('units', 'units.id', 'products_units.unit_id')
                            ->where('name', 'like', '%'.$this->search.'%');
                        })
                        ->orWhereIn('id', function( $query ){
                            $query->select('product_id as id')
                            ->from('products_providers')
                            ->leftJoin('providers', 'providers.id', 'products_providers.provider_id')
                            ->where('name', 'like', '%'.$this->search.'%');
                        })
                		->orderBy('name', 'ASC')
                        ->get()
                : Product::orderBy('name', 'ASC')->get();
        return view('livewire.Products');
    }

    private function emitUpdates()
    {
        $this->emit('selectUpdates', [ 'categories_id' => $this->categories_id, 'units_id' => $this->units_id, 'providers_id' => $this->providers_id ] );
    }

    public function resetInput()
    {
        $this->name = null;
        $this->code = null;
        $this->description = null;
        $this->categories_id = [];
        $this->providers_id = [];
        $this->units_id = [];
        $this->product_photos = [];
        $this->product_id = '';
        $this->price = 0;
        $this->tax = 0;
        $this->sale_price = 0;
        $this->emitUpdates();
        $this->ue = null;

        $this->updateMode = false;
    }
    public function save()
    {
    	if ($this->updateMode)
    		return $this->update();

    	return $this->store();
    }

    public function savePhoto()
    {
        $this->validate([
            'photo' => 'image|max:1024', // 1MB Max
        ]);

        $name = md5($this->photo . microtime()).'.'.$this->photo->extension();
        $this->photo->storeAs('img/products', $name);

        $product_photo = ProductPhoto::firstOrCreate([
            'photo' => $name,
            'product_id' => $this->product_id,
            'main' => 0
        ]);

        $record = Product::findOrFail($this->product_id);
        $this->product_photos = $record->photos ?? [];

        $this->photo = '';
    }

    public function deletePhoto($id)
    {
        if ($id) {
            $record = ProductPhoto::find($id);
            $product_id = $record->product_id;
            Storage::delete('img/products/'.$record->photo);

            $record->delete();

            $product = Product::findOrFail($product_id);

            $this->product_photos = $product->photos ?? [];
        }
    }

    public function setMainPhoto($id)
    {
        if ($id) {
            $record = ProductPhoto::find($id);
            ProductPhoto::where('product_id', $record->product_id)->where('id', '!=', $id)->update(['main'=>0]);
            ProductPhoto::where('id', $id)->update(['main'=>1]);

            $product = Product::findOrFail($record->product_id);

            $this->product_photos = $product->photos ?? [];
        }
    }

    public function store()
    {
        $this->validate([
            'name' => 'required|min:3'
        ]);
        $product = Product::create([
            'name' => strtoupper($this->name),
            'code' => strtoupper($this->code),
            'description' => strtoupper($this->description),
            'ue' => strtoupper($this->ue),
            'price' => $this->price,
            'tax' => $this->tax,
            'sale_price' => $this->sale_price
        ]);

        if ($this->categories_id)
            $this->updateCategories($product->id);

        if ($this->units_id)
            $this->updateUnits($product->id);

        if ($this->providers_id)
            $this->updateProviders($product->id);

        $this->emit('notify:toast', ['type'  => 'success', 'title' => 'Registro creado...']);
        $this->resetInput();
    }

    private function updateCategories($product_id)
    {
        ProductCategory::where('product_id', $product_id)->delete();
        foreach ($this->categories_id as $item)
            ProductCategory::create([
                'product_id' => $product_id,
                'category_id' => $item
            ]);
    }

    private function updateUnits($product_id)
    {
        ProductUnit::where('product_id', $product_id)->delete();
        foreach ($this->units_id as $item)
            ProductUnit::create([
                'product_id' => $product_id,
                'unit_id' => $item
            ]);
    }

    private function updateProviders($product_id)
    {
        ProductProvider::where('product_id', $product_id)->delete();
        foreach ($this->providers_id as $item)
            ProductProvider::create([
                'product_id' => $product_id,
                'provider_id' => $item
            ]);
    }

    public function edit($id)
    {
        $record = Product::with('categories')->with('units')->findOrFail($id);
        $this->product_id = $id;
        $this->name = $record->name;
        $this->code = $record->code;
        $this->description = $record->description;
        $this->price = $record->price;
        $this->tax = $record->tax;
        $this->sale_price = $record->sale_price;
        $this->ue = $record->ue;
        $this->product_photos = $record->photos ?? [];

        $this->categories_id = $record->categories->pluck('id')->toArray() ?? [];
        $this->providers_id = $record->providers->pluck('id')->toArray() ?? [];
        $this->units_id = $record->units->pluck('id')->toArray() ?? [];
        $this->emitUpdates();

        $this->emit('notify:toast', ['type'  => 'success', 'title' => 'Registro cargado...']);

        $this->updateMode = true;
    }
    public function update()
    {
        $this->validate([
            'product_id' => 'required|numeric',
            'name' => 'required|min:3',
            'price' => 'required|numeric',
            'tax' => 'required|numeric',
        ]);
        if ($this->product_id) {
            $record = Product::find($this->product_id);
            $record->update([
                'name' => strtoupper($this->name),
                'code' => strtoupper($this->code),
                'description' => strtoupper($this->description),
                'price' => $this->price,
                'tax' => $this->tax,
                'ue' => $this->ue,
                'sale_price' => $this->price * ($this->tax/100+1)
            ]);

            if ($this->categories_id)
                $this->updateCategories($record->id);

            if ($this->units_id)
                $this->updateUnits($record->id);

            if ($this->providers_id)
                $this->updateProviders($record->id);

            $this->emit('notify:toast', ['type'  => 'success', 'title' => 'Registro actualizado...']);
            $this->resetInput();
            $this->updateMode = false;
        }
    }
    public function destroy($id)
    {
        if ($id) {
            $record = Product::find($id);

            ProductCategory::where('product_id', $id)->delete();
            ProductProvider::where('product_id', $id)->delete();
            ProductUnit::where('product_id', $id)->delete();

            $record->delete();

            $this->emit('notify:toast', ['type'  => 'success', 'title' => 'Registro eliminado...']);
            $this->resetInput();
        }
    }
}
