<?php

namespace App\Http\Livewire;
use App\Models\Unit;

use Livewire\Component;

class Units extends Component
{
    public $data, $name, $description, $packing_unit, $unit_id, $search, $capacity;
    public $updateMode = false;

    public function mount()
    {
        $this->name = '';
        $this->description = '';
        $this->packing_unit = '';
        $this->unit_id = null;
        $this->search = '';
        $this->capacity = '';
        $this->data = [];
    }

    public function render()
    {
    	$this->data = ($this->search) 
                ? Unit::where('name', 'like', '%'.$this->search.'%')
                		->orWhere('description', 'like', '%'.$this->search.'%')
                        ->orderBy('name', 'ASC')
                        ->get()
                : Unit::orderBy('name', 'ASC')->get();
        return view('livewire.Units');
    }

    public function resetInput()
    {
        $this->name = null;
        $this->description = null;
        $this->packing_unit = null;
        $this->updateMode = false;;
    }
    public function save()
    {
    	if ($this->updateMode)
    		return $this->update();
    	
    	return $this->store();
    }

    public function store()
    {
        $this->validate([
            'name' => 'required|min:',
            'packing_unit' => 'max:150'
        ]);
        Unit::create([
            'name' => strtoupper($this->name),
            'description' => strtoupper($this->description),
            'packing_unit' => $this->packing_unit,
            'capacity' => $this->capacity
        ]);
        $this->emit('notify:toast', ['type'  => 'success', 'title' => 'Registro creado...']);
        $this->resetInput();
    }
    public function edit($id)
    {
        $record = Unit::findOrFail($id);
        $this->unit_id = $id;
        $this->name = $record->name;
        $this->description = $record->description;
        $this->packing_unit = $record->packing_unit;
        $this->capacity = $record->capacity;
        $this->updateMode = true;
        $this->emit('notify:toast', ['type'  => 'success', 'title' => 'Registro cargado...']);
    }
    public function update()
    {
        $this->validate([
            'unit_id' => 'required|numeric',
            'name' => 'required|min:3',
            'packing_unit' => 'max:150'
        ]);
        if ($this->unit_id) {
            $record = Unit::find($this->unit_id);
            $record->update([
                'name' => strtoupper($this->name),
                'description' => strtoupper($this->description),
                'packing_unit' => $this->packing_unit,
                'capacity' => $this->capacity
            ]);
            $this->emit('notify:toast', ['type'  => 'success', 'title' => 'Registro actualizado...']);
            $this->resetInput();
            $this->updateMode = false;
        }
    }
    public function destroy($id)
    {
        if ($id) {
            $record = Unit::find($id);
            $record->delete();
            $this->emit('notify:toast', ['type'  => 'success', 'title' => 'Registro eliminado...']);
        }
    }
}
