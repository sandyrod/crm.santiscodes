<?php

namespace App\Http\Livewire;
use App\Models\Client;
use App\Models\Distributionroute;
use App\Models\PlannedRoute;
use App\Models\PlannedRouteDetail;
use App\Models\Visit;

use Auth;

use Livewire\Component;

class Permanentroutes extends Component
{
    public $data, $name, $description, $search, $distributionroute_id;
    public $updateMode = false;
    public $visit;

    public function mount()
    {
        $this->search = '';
    }

    public function render()
    {
        $this->description = Auth::user()->hasRole('ADMIN');
        if(Auth::user()->hasRole('ADMIN')){
            $this->data = PlannedRouteDetail::select('planed_route_details.visit_at','planed_route_details.order','clients.id as id', 'clients.businessname as businessname', 'addresses.description', 'addresses.municipality', 'addresses.urbanization', 'clients.rif as rif')
                ->leftJoin('clients','planed_route_details.client_id', 'clients.id')
                ->leftJoin('addresses', 'addresses.client_id', 'clients.id')
                ->where('planed_route_details.plannedroute_id', 1)
                ->get();
        }else{
            $this->data = PlannedRouteDetail::select('planed_route_details.visit_at','planed_route_details.order','clients.id as id', 'clients.businessname as businessname', 'addresses.description', 'addresses.municipality', 'addresses.urbanization', 'clients.rif as rif')
                ->leftJoin('clients','planed_route_details.client_id', 'clients.id')
                ->leftJoin('addresses', 'addresses.client_id', 'clients.id')
                ->where('planed_route_details.plannedroute_id', 1)
                ->where('planed_route_details.visit_at',date('Y-m-d'))
                ->where('planed_route_details.user_id', Auth::user()->id)
                ->get();
            $this->visit = Visit::where('created_at', '>=', date('Y-m-d'))->get();
        }
        return view('livewire.permanent-routes');
    }
}
