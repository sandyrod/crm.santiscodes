<?php

namespace App\Http\Livewire;

use App\Models\DebtoPay;
use App\Models\Purchaseorder;
use App\Models\Client;
use App\Models\Provider;

use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class Debtopays extends Component
{
    public $data, $search, $debtopay_id, $user_id, $duedate_at, $status,
        $amount, $orders, $order_id, $close_at, $clients, $provider_id,
        $description, $providers;
    public $statuslist;

    public $updateMode = false;

    public function mount()
    {
        $this->providers = Provider::All();
        $this->provider_id = null;
        $this->order_id = null;
        $this->amount = 0;
        $this->close_at = null;
        $this->duedate_at = null;
        $this->status = null;
        $this->statuslist = Array('0' => 'Anulada', '1' => 'Activa', '2' => 'Pago Parcial',
        '3' => 'Pagada');
        $this->orders = Purchaseorder::All();
        $this->clients = Client::All();
    }

    public function render()
    {
        $this->data = ($this->search)
                ? Debtopay::where('id', 'like', '%'.$this->search.'%')
                        ->orWhereIn('provider_id', function( $query ){
                            $query->select('id')
                            ->from('clients')
                            ->where('businessname', 'like', '%'.$this->search.'%')
                            ->orWhere('rif', 'like', '%'.$this->search.'%');
                        })
                        ->orWhereIn('user_id', function( $query ){
                            $query->select('id')
                            ->from('users')
                            ->where('name', 'like', '%'.$this->search.'%');
                        })
                        ->orderBy('id', 'DESC')
                        ->get()
                : Debtopay::orderBy('id', 'DESC')->get();
        return view('livewire.debtopay');
    }

    public function resetInput()
    {
        $this->provider_id = null;
        $this->order_id = null;
        $this->amount = 0;
        $this->close_at = null;
        $this->duedate_at = null;
        $this->status = null;
        $this->orders = Purchaseorder::All();
        $this->clients = Client::All();
        $this->description = null;
        $this->emitUpdates();
    }

    public function save()
    {
        if ($this->updateMode)
            return $this->update();

        return $this->store();
    }

    public function store()
    {
        $this->validate([
            'amount'        => 'required',
            'duedate_at'    => 'required',
            'provider_id'     => 'required',
            'status'        => 'required'
        ]);
        Debtopay::create([
            'provider_id'     => $this->provider_id,
            'user_id'       => Auth::user()->id,
            'order_id'      => $this->order_id,
            'duedate_at'    => $this->duedate_at,
            'status'        => $this->status,
            'description'   => $this->description,
            'close_at'      => $this->close_at,
            'amount'        => $this->amount
        ]);
        $this->emit('notify:toast', ['type'  => 'success', 'title' => 'Registro creado...']);
        $this->resetInput();
    }

    public function edit($id)
    {
        $record = debtopay::findOrFail($id);
        $this->debtopay_id = $id;
        $this->provider_id = $record->provider_id;
        $this->order_id = $record->order_id;
        $this->duedate_at = $record->duedate_at;
        $this->status = $record->status;
        $this->description = $record->description;
        $this->close_at = $record->close_at;
        $this->amount = $record->amount;
        $this->emitUpdates();

        $this->updateMode = true;

        $this->emit('notify:toast', ['type'  => 'success', 'title' => 'Registro cargado...']);
    }

    public function update()
    {
        $this->validate([
            'debtopay_id' => 'required',
            'amount'        => 'required',
            'duedate_at'    => 'required',
            'provider_id'     => 'required',
            'status'        => 'required'
        ]);
        if ($this->debtopay_id) {
            $record = debtopay::find($this->debtopay_id);
            $record->update([
                'debtopay_id' => $this->debtopay_id,
                'provider_id'     => $this->provider_id,
                'order_id'      => $this->order_id,
                'duedate_at'    => $this->duedate_at,
                'close_at'      => $this->close_at,
                'status'        => $this->status,
                'description'   => $this->description,
                'amount'        => $this->amount
            ]);
            $this->emit('notify:toast', ['type'  => 'success', 'title' => 'Registro actualizado...']);
            $this->resetInput();
            $this->updateMode = false;
        }
    }

    public function destroy($id)
    {
        if ($id) {
            $record = debtopay::find($id);
            $record->delete();

            $this->emit('notify:toast', ['type'  => 'success', 'title' => 'Registro eliminado...']);
            $this->resetInput();
        }
    }

    private function emitUpdates()
    {
        $this->emit('selectUpdates',
            [ 'provider_id' => $this->provider_id,
              'order_id' => $this->order_id,
              'status' => $this->status ] );
    }
}
