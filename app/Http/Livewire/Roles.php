<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Models\RoleHasPermission;

class Roles extends Component
{
    public $name, $guard_name, $search, $data, $rol_id, $permissions, $permission_id;

    public $updateMode = false;

    public function mount()
    {
        $this->name='';
        $this->guard_name='';
        $this->data = [];
        $this->rol_id = null;
        $this->permission_id = [];
        $this->permissions = Permission::All();
    }

    public function render()
    {
        $this->data = ($this->search)
                ? Role::where('name', 'like', '%'.$this->search.'%')
                        ->orWhere('guard_name', 'like', '%'.$this->search.'%')
                        ->orderBy('name', 'ASC')
                        ->get()
                : Role::orderBy('name', 'ASC')->get();
        return view('livewire.roles');
    }

    public function edit($id)
    {
        $record = Role::findOrFail($id);
        $this->rol_id = $id;
        $this->name = $record->name;
        $this->permission_id = [];
        $permisions = RoleHasPermission::select('permission_id')->where('role_id', $this->rol_id)->get()->toArray() ?? [];
        foreach ($permisions as $key) {
            $this->permission_id[] = $key['permission_id'];
        }
        $this->emitUpdates();

        $this->emit('notify:toast', ['type'  => 'success', 'title' => 'Rol cargado...']);

        $this->updateMode = true;
    }

    private function emitUpdates()
    {
         $this->emit('selectUpdates', [ 'permission_id' => $this->permission_id ] );
    }

    public function resetInput()
    {
        $this->name = null;
        $this->permission_id = [];
        $this->updateMode = false;
    }

    public function save()
    {
        if ($this->updateMode)
            return $this->update();

        return $this->store();
    }

    public function store()
    {
        $this->validate([
            'name' => 'required|min:4'
        ]);
        $product = Role::create([
            'name' => strtoupper($this->name)
        ]);

        if ($this->permission_id)
            $this->updateRolpersimissions($product->id);


        $this->emit('notify:toast', ['type'  => 'success', 'title' => 'Rol creado...']);
        $this->resetInput();
    }

    public function update()
    {
        $this->validate([
            'name' => 'required|min:4'
        ]);
        if ($this->rol_id) {
            $record = Role::find($this->rol_id);
            $record->update([
                'name' => strtoupper($this->name)
            ]);

            if ($this->permission_id){
                $this->updateRolpersimissions($record->id);
            }

            $this->emit('notify:toast', ['type'  => 'success', 'title' => 'Registro actualizado...']);
            $this->resetInput();
            $this->updateMode = false;
        }
    }

    public function destroy($id)
    {
        if ($id) {
            $record = Role::find($id);

            $record->delete();

            $this->emit('notify:toast', ['type'  => 'success', 'title' => 'Registro eliminado...']);
            $this->resetInput();
        }
    }

    private function updateRolpersimissions($rol_id)
    {
        RoleHasPermission::where('role_id', $rol_id)->delete();
        foreach ($this->permission_id as $item)
            RoleHasPermission::create([
                'role_id' => $rol_id,
                'permission_id' => $item
            ]);
    }
}
