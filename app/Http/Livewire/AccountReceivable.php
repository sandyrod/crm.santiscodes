<?php

namespace App\Http\Livewire;

use App\Models\Order;
use App\Models\Client;
use App\Models\Receivable;

use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class AccountReceivable extends Component
{
    public $data, $search, $receivable_id, $user_id, $duedate_at, $status,
        $amount, $orders, $order_id, $close_at, $clients, $client_id,
        $description;
    public $statuslist;

    public $updateMode = false;

    public function mount()
    {
        $this->client_id = null;
        $this->order_id = null;
        $this->amount = 0;
        $this->close_at = null;
        $this->duedate_at = null;
        $this->status = null;
        $this->statuslist = Array('0' => 'Anulada', '1' => 'Activa', '2' => 'Pago Parcial',
        '3' => 'Pagada');
        $this->orders = Order::All();
        $this->clients = Client::All();
    }

    public function render()
    {
        $this->data = ($this->search)
                ? Receivable::where('id', 'like', '%'.$this->search.'%')
                        ->orWhereIn('client_id', function( $query ){
                            $query->select('id')
                            ->from('clients')
                            ->where('businessname', 'like', '%'.$this->search.'%')
                            ->orWhere('rif', 'like', '%'.$this->search.'%');
                        })
                        ->orWhereIn('user_id', function( $query ){
                            $query->select('id')
                            ->from('users')
                            ->where('name', 'like', '%'.$this->search.'%');
                        })
                        ->orderBy('id', 'DESC')
                        ->get()
                : Receivable::orderBy('id', 'DESC')->get();
        return view('livewire.account-receivable');
    }

    public function resetInput()
    {
        $this->client_id = null;
        $this->order_id = null;
        $this->amount = 0;
        $this->close_at = null;
        $this->duedate_at = null;
        $this->status = null;
        $this->orders = Order::All();
        $this->clients = Client::All();
        $this->description = null;
        $this->emitUpdates();
    }

    public function save()
    {
        if ($this->updateMode)
            return $this->update();

        return $this->store();
    }

    public function store()
    {
        $this->validate([
            'amount'        => 'required',
            'duedate_at'    => 'required',
            'client_id'     => 'required',
            'status'        => 'required'
        ]);
        Receivable::create([
            'client_id'     => $this->client_id,
            'user_id'       => Auth::user()->id,
            'order_id'      => $this->order_id,
            'duedate_at'    => $this->duedate_at,
            'status'        => $this->status,
            'description'   => $this->description,
            'close_at'      => $this->close_at,
            'amount'        => $this->amount
        ]);
        $this->emit('notify:toast', ['type'  => 'success', 'title' => 'Registro creado...']);
        $this->resetInput();
    }

    public function edit($id)
    {
        $record = Receivable::findOrFail($id);
        $this->receivable_id = $id;
        $this->client_id = $record->client_id;
        $this->order_id = $record->order_id;
        $this->duedate_at = $record->duedate_at;
        $this->status = $record->status;
        $this->description = $record->description;
        $this->close_at = $record->close_at;
        $this->amount = $record->amount;
        $this->emitUpdates();

        $this->updateMode = true;

        $this->emit('notify:toast', ['type'  => 'success', 'title' => 'Registro cargado...']);
    }

    public function update()
    {
        $this->validate([
            'receivable_id' => 'required',
            'amount'        => 'required',
            'duedate_at'    => 'required',
            'client_id'     => 'required',
            'status'        => 'required'
        ]);
        if ($this->receivable_id) {
            $record = Receivable::find($this->receivable_id);
            $record->update([
                'receivable_id' => $this->receivable_id,
                'client_id'     => $this->client_id,
                'order_id'      => $this->order_id,
                'duedate_at'    => $this->duedate_at,
                'close_at'      => $this->close_at,
                'status'        => $this->status,
                'description'   => $this->description,
                'amount'        => $this->amount
            ]);
            $this->emit('notify:toast', ['type'  => 'success', 'title' => 'Registro actualizado...']);
            $this->resetInput();
            $this->updateMode = false;
        }
    }

    public function destroy($id)
    {
        if ($id) {
            $record = Receivable::find($id);
            $record->delete();

            $this->emit('notify:toast', ['type'  => 'success', 'title' => 'Registro eliminado...']);
            $this->resetInput();
        }
    }

    private function emitUpdates()
    {
        $this->emit('selectUpdates',
            [ 'client_id' => $this->client_id,
              'order_id' => $this->order_id,
              'status' => $this->status ] );
    }
}
