<?php

namespace App\Http\Livewire;

use App\Models\Bank;
use App\Models\Bankaccount;

use Livewire\Component;

class Banksaccounts extends Component
{
    public $data, $search, $banks, $bank_id, $number, $owner, $account_type,
        $route, $iban, $swift, $amount, $bankaccount_id;

    public $updateMode = false;

    public function mount ()
    {
        $this->bank_id = null;
        $this->banks = Bank::All();
        $this->number = '';
        $this->owner = '';
        $this->account_type = null;
        $this->route = '';
        $this->iban = '';
        $this->swift = '';
        $this->amount = 0;
    }

    public function render()
    {
        $this->data = ($this->search)
            ? Bankaccount::where('number','like', '%'.$this->search.'%')
                    ->orWhere('owner', 'like', '%'.$this->search.'%')
                    ->orWhere('swift', 'like', '%'.$this->search.'%')
                    ->get()
            : Bankaccount::orderBy('owner')->get();
        return view('livewire.banksaccounts');
    }

    public function resetInput()
    {
        $this->bank_id = null;
        $this->banks = Bank::All();
        $this->number = '';
        $this->owner = '';
        $this->account_type = 0;
        $this->route = '';
        $this->iban = '';
        $this->swift = '';
        $this->amount = 0;
        $this->updateMode = false;
    }

    public function save()
    {
        if ($this->updateMode)
            return $this->update();

        return $this->store();
    }

    public function store()
    {
        $this->validate([
            'bank_id'       => 'required',
            'number'        => 'required|min:10',
            'owner'         => 'required|min:3',
            'account_type'  => 'required'
        ]);
        Bankaccount::create([
            'bank_id'       => $this->bank_id,
            'owner'         => strtoupper($this->owner),
            'number'        => $this->number,
            'account_type'  => $this->account_type,
            'route'         => $this->route,
            'iban'          => $this->iban,
            'swift'         => $this->swift,
            'amount'        => $this->amount
        ]);
        $this->emit('notify:toast', ['type'  => 'success', 'title' => 'Registro creado...']);
        $this->resetInput();
    }

    public function edit($id)
    {
        $record = Bankaccount::findOrFail($id);
        $this->bankaccount_id = $id;
        $this->bank_id = $record->bank_id;
        $this->number = $record->number;
        $this->owner = $record->owner;
        $this->account_type = $record->account_type;
        $this->route = $record->route;
        $this->iban = $record->iban;
        $this->swift = $record->swift;
        $this->amount = $record->amount;

        $this->updateMode = true;

        $this->emit('notify:toast', ['type'  => 'success', 'title' => 'Registro cargado...']);
    }

    public function update()
    {
        $this->validate([
            'bank_id'       => 'required',
            'owner'         => 'required',
            'number'        => 'required',
            'account_type'  => 'required'
        ]);
        if ($this->bankaccount_id) {
            $record = Bankaccount::find($this->bankaccount_id);
            $record->update([
                'bank_id'   => $this->bank_id,
                'number'    => $this->number,
                'owner' => strtoupper($this->owner),
                'account_type' => $this->account_type,
                'route' => $this->route,
                'iban' => $this->iban,
                'swift' => $this->swift,
                'amount' => $this->amount
            ]);
            $this->emit('notify:toast', ['type'  => 'success', 'title' => 'Registro actualizado...']);
            $this->resetInput();
            $this->updateMode = false;
        }
    }

    public function destroy($id)
    {
        if ($id) {
            $record = Bankaccount::find($id);
            $record->delete();

            $this->emit('notify:toast', ['type'  => 'success', 'title' => 'Registro eliminado...']);
            $this->resetInput();
        }
    }
}
