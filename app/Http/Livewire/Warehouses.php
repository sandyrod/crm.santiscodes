<?php

namespace App\Http\Livewire;
use App\Models\Warehouse;

use Livewire\Component;

class Warehouses extends Component
{
    public $data, $name, $description, $warehouse_id, $search;
    public $updateMode = false;

    public function mount()
    {
        $this->name = '';
        $this->description = '';
        $this->warehouse_id = null;
        $this->search = '';
        $this->data = [];
    }

    public function render()
    {
    	$this->data = ($this->search) 
                ? Warehouse::where('name', 'like', '%'.$this->search.'%')
                		->orWhere('description', 'like', '%'.$this->search.'%')
                		->orderBy('name', 'ASC')
                        ->get()
                : Warehouse::orderBy('name', 'ASC')->get();
        return view('livewire.Warehouses');
    }

    public function resetInput()
    {
        $this->name = null;
        $this->description = null;
        $this->updateMode = false;;
    }
    public function save()
    {
    	if ($this->updateMode)
    		return $this->update();
    	
    	return $this->store();
    }

    public function store()
    {
        $this->validate([
            'name' => 'required|min:3'
        ]);
        Warehouse::create([
            'name' => strtoupper($this->name),
            'description' => strtoupper($this->description)
        ]);
        $this->emit('notify:toast', ['type'  => 'success', 'title' => 'Registro creado...']);
        $this->resetInput();
    }
    public function edit($id)
    {
        $record = Warehouse::findOrFail($id);
        $this->warehouse_id = $id;
        $this->name = $record->name;
        $this->description = $record->description;
        $this->updateMode = true;
        $this->emit('notify:toast', ['type'  => 'success', 'title' => 'Registro cargado...']);
    }
    public function update()
    {
        $this->validate([
            'warehouse_id' => 'required|numeric',
            'name' => 'required|min:3'
        ]);
        if ($this->warehouse_id) {
            $record = Warehouse::find($this->warehouse_id);
            $record->update([
                'name' => strtoupper($this->name),
                'description' => strtoupper($this->description)
            ]);
            $this->emit('notify:toast', ['type'  => 'success', 'title' => 'Registro actualizado...']);
            $this->resetInput();
            $this->updateMode = false;
        }
    }
    public function destroy($id)
    {
        if ($id) {
            $record = Warehouse::find($id);
            $record->delete();
            $this->emit('notify:toast', ['type'  => 'success', 'title' => 'Registro eliminado...']);
            $this->resetInput();
        }
    }
}
