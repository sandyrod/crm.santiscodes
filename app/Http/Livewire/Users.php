<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\User;
use Spatie\Permission\Models\Role;
use App\Models\ModelHasRol;
use Illuminate\Support\Facades\Hash;

class Users extends Component
{

    public $name, $email, $password, $search, $data, $user_id, $rol_id, $rols;

    public $updateMode = false;

    public function mount()
    {
        $this->name='';
        $this->email='';
        $this->password='';
        $this->data = [];
        $this->user_id = null;
        $this->rol_id=[];
        $this->rols = Role::select('id','name')->get();
    }

    public function render()
    {
        $this->data = ($this->search)
                ? User::where('name', 'like', '%'.$this->search.'%')
                        ->orWhere('email', 'like', '%'.$this->search.'%')
                        ->orderBy('name', 'ASC')
                        ->get()
                : User::orderBy('name', 'ASC')->get();
        return view('livewire.users');
    }

    public function edit($id)
    {
        $record = User::findOrFail($id);
        $this->user_id = $id;
        $this->name = $record->name;
        $this->email = $record->email;
        $this->rol_id = [];
        $roles = ModelHasRol::select('role_id')->where('model_id', $this->user_id)->get()->toArray() ?? [];
        foreach ($roles as $key) {
            $this->rol_id[] = $key['role_id'];
        }
        $this->emitUpdates();

        $this->emit('notify:toast', ['type'  => 'success', 'title' => 'Usuariocargado...']);

        $this->updateMode = true;
    }

    private function emitUpdates()
    {
         $this->emit('selectUpdates', [ 'rol_id' => $this->rol_id] );
    }

    public function resetInput()
    {
        $this->name = null;
        $this->email = null;
        $this->password = null;
        $this->rol_id = [];
        $this->emitUpdates();
    }

    public function save()
    {
        if ($this->updateMode)
            return $this->update();

        return $this->store();
    }

    public function store()
    {
        $this->validate([
            'name' => 'required|min:4',
            'email' => 'required|min:5',
        ]);
        $user = User::create([
            'name' => strtoupper($this->name),
            'email' => strtoupper($this->email),
            'password' => Hash::make($this->password)
        ]);
        if ($this->rol_id)
            $this->updateRoles($user->id);

        $this->emit('notify:toast', ['type'  => 'success', 'title' => 'Usuario creado...']);
        $this->resetInput();
    }

    public function update()
    {
        $this->validate([
            'name' => 'required|min:4',
            'email' => 'required|min:5'
        ]);
        if ($this->user_id) {
            $record = User::find($this->user_id);
            if (!$this->password){
                $entro="por el if";
                $record->update([
                    'name' => strtoupper($this->name),
                    'email' => strtoupper($this->email)
                ]);
            }else{
                $entro="por el else";
                $record->update([
                    'name' => strtoupper($this->name),
                    'email' => strtoupper($this->email),
                    'password' => Hash::make($this->password)
                ]);
            }

            if ($this->rol_id)
            $this->updateRoles($record->id);

            $this->emit('notify:toast', ['type'  => 'success', 'title' => 'Registro actualizado...']);
            $this->resetInput();
            $this->updateMode = false;
        }
    }

    public function destroy($id)
    {
        if ($id) {
            $record = User::find($id);

            $record->delete();

            $this->emit('notify:toast', ['type'  => 'success', 'title' => 'Registro eliminado...']);
            $this->resetInput();
        }
    }

    private function updateRoles($user_id)
    {
        ModelHasRol::where('model_id', $user_id)->delete();
        foreach ($this->rol_id as $item)
            ModelHasRol::create([
                'role_id' => $item,
                'model_id' => $user_id,
                'model_type' => 'App\User'
            ]);
    }
}
