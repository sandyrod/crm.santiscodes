<?php

namespace App\Http\Livewire;
use App\Models\Order;
use App\Models\Client;
use App\Models\OrderDetail;
use App\Models\Unit;
use App\Models\Product;
use App\Models\ProductUnit;
use App\Models\Distributionroute;
use Illuminate\Http\Request;

use Livewire\Component;
use Auth;

class Orders extends Component
{
    protected $listeners = ['changeClient' => 'updateRoute', 'changeProduct' => 'updateProduct'];

    public $client_id, $route, $price, $product_id, $unit_id, $unit, $user_id,
     $search, $quantity, $order_id, $paidCondition, $document;
    public $payment, $other, $data, $distributionroutes, $clients, $products,
     $orderdetails, $updateMode = false;

    public function mount(Request $request)
    {
        //if ($request->id){
        //    $this->edit(4);
        //}else{
            $this->resetInput();
            $this->search = '';
            $this->distributionroutes = Distributionroute::orderBy('name', 'ASC')->get();
            $this->clients = Client::orderBy('businessname', 'ASC')->get();
            $this->products = Product::orderBy('name', 'ASC')->get();
            $this->data = [];
        //}
    }

    private function emitUpdates()
    {
        $this->emit('selectUpdates',
            ['client_id'        =>  $this->client_id,
             'payment'          =>  $this->payment,
             'paidCondition'    =>  $this->paidCondition]);
    }

    private function emitDetailUpdates()
    {
        $this->emit('selectDetailUpdates', [ 'product_id' => $this->product_id ] );
    }

    public function updateRoute()
    {
        $record = Client::find($this->client_id);
        $this->route = $record->distribution_routes->name ?? '';
    }

    public function updateProduct()
    {
        if($this->product_id){
            $record = Product::find($this->product_id);
            if  ($this->document == 'NOTA DE DESPACHO'){
                $this->price = $record->price ?? 0;
            }else{
                $this->price = $record->sale_price ?? 0;
            }
            $unit = $record->units()->first();
            $this->unit_id = $unit->id ?? '';
            $this->unit = $unit->name ?? '';
        }
    }

    public function render(Request $request)
    {
        if($request->id){
            $this->search = $request->id;
        }
        $today= date('Y-m-d');
        if(Auth::user()->hasRole('ADMIN')){
    	    $this->data = ($this->search)
                ? Order::where('id', 'like', '%'.$this->search.'%')
                		->orWhereIn('client_id', function( $query ){
                            $query->select('id')
                            ->from('clients')
                            ->where('businessname', 'like', '%'.$this->search.'%')
                            ->orWhere('rif', 'like', '%'.$this->search.'%');
                        })
                        ->orWhereIn('user_id', function( $query ){
                            $query->select('id')
                            ->from('users')
                            ->where('name', 'like', '%'.$this->search.'%');
                        })
                		->orderBy('id', 'DESC')
                        ->get()
                : Order::orderBy('id', 'DESC')->get();
        }else{
            $this->data = ($this->search)
                ? Order::where('id', 'like', '%'.$this->search.'%')
                        ->where('user_id', Auth::user()->id)
                        ->where('created_at', '>', $today)
                        ->orWhereIn('client_id', function( $query ){
                            $query->select('id')
                            ->from('clients')
                            ->where('businessname', 'like', '%'.$this->search.'%')
                            ->orWhere('rif', 'like', '%'.$this->search.'%');
                        })
                        ->orWhereIn('user_id', function( $query ){
                            $query->select('id')
                            ->from('users')
                            ->where('name', 'like', '%'.$this->search.'%');
                        })
                        ->orderBy('id', 'DESC')
                        ->get()
                : Order::orderBy('id', 'DESC')
                    ->where('user_id', Auth::user()->id)
                    ->where('created_at', '>', $today)
                    ->get();
        }
        return view('livewire.Orders');
    }

    public function resetInput()
    {
        $this->client_id = '';
        $this->route = '';
        $this->payment = 'EFECTIVO';
        $this->other = '';
        $this->order_id = '';
        $this->user_id = '';
        $this->updateMode = false;
        $this->orderdetails = null;
        $this->document = 'NOTA DE CREDITO';

        $this->emitUpdates();
    }

    private function resetProduct()
    {
        $this->product_id = '';
        $this->unit_id = '';
        $this->unit = '';
        $this->price = null;
        $this->quantity = 1;

        $this->emitDetailUpdates();
    }

    public function save()
    {
    	if ($this->updateMode)
    		return $this->update();

    	return $this->store();
    }

    public function store()
    {

        $this->validate([
            'client_id' => 'required'
        ]);

        $order = Order::create([
            'client_id'     => $this->client_id,
            'payment'       => $this->payment,
            'other'         => $this->other,
            'paidCondition' => $this->paidCondition,
            'document'      => $this->document
        ]);
        $this->order_id = $order->id;

        $this->emit('notify:toast', ['type'  => 'success', 'title' => 'Registro creado...']);
        $this->showProducts();
    }

    public function addProduct()
    {
        if (! $this->order_id || ! $this->product_id || ! $this->quantity )
            return;

        $order = OrderDetail::create([
            'order_id' => $this->order_id,
            'product_id' => $this->product_id,
            'unit_id' => $this->unit_id,
            'quantity' => $this->quantity,
            'price' => $this->price
        ]);

        $this->showProducts();
    }

    public function showProducts()
    {
        $this->updateMode = true;
        $this->orderdetails = OrderDetail::where('order_id', $this->order_id)->get();
        $this->resetProduct();
    }

    public function edit($id)
    {
        $record = Order::findOrFail($id);
        $this->order_id         = $id;
        $this->client_id        = $record->client_id;
        $this->route            = $record->client->distribution_routes->name ?? '';
        $this->payment          = $record->payment;
        $this->paidCondition    = $record->paidCondition;
        $this->other            = $record->other;
        $this->document         = $record->document;

        $this->showProducts();
        $this->emitUpdates();
        $this->emit('notify:toast', ['type'  => 'success', 'title' => 'Registro cargado...']);
    }

    public function update()
    {
        $this->validate([
            'client_id' => 'required'
        ]);
        if ($this->order_id) {
            $record = Order::find($this->order_id);
            $record->update([
                'client_id'     => $this->client_id,
                'payment'       => $this->payment,
                'other'         => $this->other,
                'paidCondition' => $this->paidCondition
            ]);

            $this->emit('notify:toast', ['type'  => 'success', 'title' => 'Registro actualizado...']);
        }
    }

    public function destroy($id)
    {
        if ($id) {
            $record = Order::find($id);
            $record->delete();
            $this->resetInput();
        }
    }

    public function destroyProduct($id)
    {
        if ($id) {
            $record = OrderDetail::find($id);
            $record->delete();
            $this->emit('notify:toast', ['type'  => 'success', 'title' => 'Registro eliminado...']);
            $this->resetProduct();
        }
    }

}
