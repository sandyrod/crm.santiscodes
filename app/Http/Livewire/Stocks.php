<?php

namespace App\Http\Livewire;
use App\Models\Stock;
use App\Models\StockLog;
use App\Models\Product;
use App\Models\Warehouse;

use App\Repositories\ProductsRepository;

use Livewire\Component;

class Stocks extends Component
{
    protected $listeners = ['getStock' => 'setMaxStock'];
    public $data, $quantity, $warehouse_id, $unit_id, $operation, $description, $act, $product_id, $products, $warehouses, $stock_id, $search;
    public $updateMode = false, $error_stock;

    public function mount()
    {
        $this->quantity = '';
        $this->error_stock = '';
        $this->act = 0;
        $this->warehouse_id = '';
        $this->product_id = '';
        $this->unit_id = '';
        $this->description = '';
        $this->operation = 1;
        $this->stock_id = null;
        $this->search = '';
        $this->warehouses = Warehouse::orderBy('name', 'ASC')->get();
        $this->products = Product::orderBy('name', 'ASC')->get();
        $this->data = [];
    }

    public function render()
    {
    	$this->data = ($this->search)
                ? Stock::leftJoin('products', 'stocks.product_id', 'products.id')
                        ->whereIn('product_id', function( $query ){
                            $query->select('id')
                            ->from('products')
                            ->where('name', 'like', '%'.$this->search.'%')
                            ->orWhere('description', 'like', '%'.$this->search.'%');
                        })
                        ->orWhereIn('warehouse_id', function( $query ){
                            $query->select('id')
                            ->from('warehouses')
                            ->where('name', 'like', '%'.$this->search.'%');
                        })
                        ->orWhereIn('unit_id', function( $query ){
                            $query->select('id')
                            ->from('units')
                            ->where('name', 'like', '%'.$this->search.'%');
                        })
                        ->orWhere('quantity', 'like', '%'.$this->search.'%')
                		->orderBy('name', 'ASC')
                        ->get()
                : Stock::leftJoin('products', 'stocks.product_id', 'products.id')
                        ->orderBy('name', 'ASC')->get();
        return view('livewire.Stocks');
    }

    private function emitUpdates()
    {
        $this->emit('selectUpdates', [ 'product_id' => $this->product_id, 'warehouse_id' => $this->warehouse_id ] );
    }

    public function setMaxStock()
    {
        $stock = Stock::where('product_id', $this->product_id)->where('warehouse_id', $this->warehouse_id)->first();
        $this->act = $stock->quantity ?? 0;
    }

    public function resetInput()
    {
        $this->act = 0;
        $this->error_stock = '';
        $this->product_id = null;
        $this->unit_id = null;
        $this->warehouse_id = null;
        $this->operation = 1;
        $this->description = null;
        $this->quantity = 1;
        $this->stock_id = '';
        $this->updateMode = false;

        $this->emitUpdates();
    }
    public function save()
    {
    	return $this->store();
    }

    public function store()
    {
        $this->validate([
            'product_id' => 'required',
            'warehouse_id' => 'required',
            'description' => 'required',
            'quantity' => 'required|integer|min:1'
        ]);

        if ($this->operation == 0 && $this->quantity > $this->act){
            $this->quantity = $this->act;
            $this->error_stock = 'Supera la existencia actual';
            return;
        }

        $stock = StockLog::create([
            'product_id' => $this->product_id,
            'warehouse_id' => $this->warehouse_id,
            'quantity' => $this->quantity,
            'description' => strtoupper($this->description),
            'operation' => $this->operation
        ]);

        $this->emit('notify:toast', ['type'  => 'success', 'title' => 'Registro actualizado...']);

        $this->resetInput();
    }

}
