<?php

namespace App\Http\Livewire;

use App\Models\Dispatchorder;
use App\Models\Dispatch_detail;
use App\Models\Products;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\Stock;

use App\Repositories\OrdersRepository;
use App\Repositories\ProductsRepository;

use Auth;
use Illuminate\Support\Facades\Log;

use Livewire\Component;

class Dispatch extends Component
{
    public $data, $search, $warehouses_id, $warehouse_id,
    $warehouses, $dispatchdetails, $orders, $warehouse_name, $client,
    $client_id, $order_id, $dispatch_id, $dispatch_detail;
    public $product_name, $products, $product_id, $requested, $quantity,
    $detail_id, $total;

    public $updateMode = false;

    protected $listeners = ['getOrder' => 'getOrder'];

    public function mount()
    {
        $this->client_id='';
        $this->order_id = '';
        $this->orders = Order::where('status',1)->orWhere('status',2)->get();
        $this->warehouses_name = '';
        $this->client = '';
        $this->dispatch_id ='';
        $this->quantity = 0;
    }

    public function render()
    {
        $this->data = ($this->search)
                ? Dispatchorder::where('id', 'like', '%'.$this->search.'%')
                        ->orWhereIn('client_id', function( $query ){
                            $query->select('id')
                            ->from('clients')
                            ->where('businessname', 'like', '%'.$this->search.'%')
                            ->orWhere('rif', 'like', '%'.$this->search.'%');
                        })
                        ->orWhereIn('user_id', function( $query ){
                            $query->select('id')
                            ->from('users')
                            ->where('name', 'like', '%'.$this->search.'%');
                        })
                        ->orderBy('id', 'DESC')
                        ->get()
                : Dispatchorder::orderBy('id', 'DESC')->get();

        return view('livewire.dispatch');
    }

    public function edit($id, $new =1)
    {
        $record = Dispatchorder::findOrFail($id);
        $this->purchase_id = $id;
        $this->client_id = $record->client_id;
        $this->client = $record->client->businessname;
        $this->user_id = $record->user_id ?? '';
        $this->warehouse_id = $record->warehouse_id ?? '';
        $this->warehouse_name = $record->warehouse->name ?? '';
        $this->order_id = $record->order_id;
        $this->dispatch_id = $id;

        $this->showProducts();
        if($new)
            $this->emitUpdates();
        $this->emit('notify:toast', ['type'  => 'success', 'title' => 'Registro cargado...']);
    }

    public function showProducts()
    {
        $this->updateMode = true;
        $this->dispatchdetails = Dispatch_detail::where('dispatch_id', $this->dispatch_id)->get();
        if (!$this->dispatchdetails->count()){
            $order_detail = OrderDetail::where('order_id', $this->order_id)->get();
            foreach ($order_detail as $detail) {
                if($requested = OrdersRepository::getQuantityRemaining($this->order_id, $detail->product_id))
                Dispatch_detail::create(['dispatch_id'=> $this->dispatch_id,
                                    'product_id' => $detail->product_id,
                                    'requested' => $requested,
                                    'quantity' => 0]);
            }
        }
        $this->dispatchdetails = Dispatch_detail::where('dispatch_id', $this->dispatch_id)->get();
        $this->resetProduct();
    }

    public function editProduct($id)
    {
        $dispatch_detail = Dispatch_detail::find($id);
        $this->product_name = $dispatch_detail->product->name;
        $this->product_id = $dispatch_detail->product_id;
        $this->requested = $dispatch_detail->requested;
        $this->quantity = $dispatch_detail->quantity;
        $this->detail_id = $dispatch_detail->id;
    }

    public function resetProduct()
    {
        //$this->emitDetailUpdates();
    }

    private function emitUpdates()
    {
        $this->emit('selectUpdates', ['order_id'=>$this->order_id]);
    }

    public function resetInput()
    {
        $this->purchase_id = '';
        $this->client_id = '';
        $this->client = '';
        $this->user_id = '';
        $this->warehouse_id = '';
        $this->warehouse_name = '';
        $this->order_id = '';
        $this->orders = Order::where('status',1)->orWhere('status',2)->get();
        $this->dispatch_id = '';

        $this->emitUpdates();
    }

    public function resetInputDetail()
    {
        $this->product_name = null;
        $this->product_id = null;
        $this->quantity=0;
        $this->requested=0;
    }

    public function addProduct()
    {
        $dispacth_item = Dispatch_detail::find($this->detail_id);
        $dispacth_item->quantity = $this->quantity;
        $dispacth_item->save();
        $stock = Stock::where('product_id', $dispacth_item->product_id)
            ->where('warehouse_id',$this->warehouse_id)
            ->first();
        $newStock = ProductsRepository::getStock($dispacth_item->product_id, $this->warehouse_id) ?? 0;
        $stock->quantity = $newStock;
        $stock->save();

        $this->resetInputDetail();
        $this->showProducts();
        if (OrdersRepository::getOrderRemaining($this->order_id)<=0){
            OrdersRepository::setOrderStatus($this->order_id, 3);
        }else{
            OrdersRepository::setOrderStatus($this->order_id, 2);
        }
    }

    public function getOrder()
    {
        if(!$this->dispatch_id){
        $order = Order::find($this->order_id);
            if($order){
                if ($order->status==1 or $order->status==2){
                    if (OrdersRepository::getOrderRemaining($this->order_id)>0){
                        $dispatch = Dispatchorder::create([
                            'client_id'=>$order->client_id,
                            'user_id' => Auth::user()->id,
                            'order_id' => $order->id,
                            'warehouse_id' => 1,
                            'status' => 1]);
                        $this->edit($dispatch->id, 0);
                    }else{
                        OrdersRepository::setOrderStatus($this->order_id, 3);
                    }
                }
            }
        }
    }

    public function destroy($id)
    {
        if ($id) {
            Dispatch_detail::where('dispatch_id', $id)->delete();
            $record = Dispatchorder::find($id);
            $order_id = $record->order_id;
            $record->delete();
            if (OrdersRepository::getOrderRemaining($order_id)>0){
                if(Dispatchorder::where('order_id', $order_id)->count('id')>0)
                    OrdersRepository::setOrderStatus($order_id, 2);
                else
                    OrdersRepository::setOrderStatus($order_id, 1);
            }
            $this->resetInput();
        }
    }
}
