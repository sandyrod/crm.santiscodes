<?php

namespace App\Http\Livewire;

use App\Models\Bank;
use App\Models\Country;

use Livewire\Component;

class Banks extends Component
{
    public $data, $bank_id, $name, $isNational, $country, $description, $search,
        $countries, $code;

    public $updateMode = false;

    public function mount ()
    {
        $this->data = [];
        $this->name = '';
        $this->isNational = 1;
        $this->country = 58;
        $this->code = '';
        $this->countries = Country::All();
        $this->description = '';
    }


    public function render()
    {
        $this->data = ($this->search)
                ? Bank::where('name', 'like', '%'.$this->search.'%')
                        ->orWhere('description', 'like', '%'.$this->search.'%')
                        ->orderBy('name', 'ASC')
                        ->get()
                : Bank::orderBy('name', 'ASC')->get();
        return view('livewire.banks');
    }

    public function resetInput()
    {
        $this->name = '';
        $this->isNational = null;
        $this->country = Country::All();
        $this->description = '';
        $this->code = '';
        $this->updateMode = false;
    }

    public function save()
    {
        if ($this->updateMode)
            return $this->update();

        return $this->store();
    }

    public function store()
    {
        $this->validate([
            'name' => 'required|min:3',
            'country' => 'required|min:1'
        ]);
        Bank::create([
            'name' => strtoupper($this->name),
            'isNational' => $this->isNational ?? 1,
            'country' => $this->country ?? 58,
            'code' => $this->code ?? NULL,
            'description' => strtoupper($this->description)
        ]);
        $this->emit('notify:toast', ['type'  => 'success', 'title' => 'Registro creado...']);
        $this->resetInput();
    }

    public function edit($id)
    {
        $record = Bank::findOrFail($id);
        $this->bank_id = $id;
        $this->name = $record->name;
        $this->isNational = $record->isNational;
        $this->country = $record->country;
        $this->description = $record->description;

        $this->updateMode = true;

        $this->emit('notify:toast', ['type'  => 'success', 'title' => 'Registro cargado...']);
    }

    public function update()
    {
        $this->validate([
            'bank_id' => 'required|numeric',
            'name' => 'required|min:3'
        ]);
        if ($this->bank_id) {
            $record = Bank::find($this->bank_id);
            $record->update([
                'name' => strtoupper($this->name),
                'isNational' => $this->isNational ?? 1,
                'country' => $this->country ?? 58,
                'description' => strtoupper($this->description)
            ]);
            $this->emit('notify:toast', ['type'  => 'success', 'title' => 'Registro actualizado...']);
            $this->resetInput();
            $this->updateMode = false;
        }
    }

    public function destroy($id)
    {
        if ($id) {
            $record = Bank::find($id);
            $record->delete();

            $this->emit('notify:toast', ['type'  => 'success', 'title' => 'Registro eliminado...']);
            $this->resetInput();
        }
    }
}
