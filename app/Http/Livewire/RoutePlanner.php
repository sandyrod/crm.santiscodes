<?php

namespace App\Http\Livewire;

use App\Models\Client;
use App\Models\Distributionroute;
use App\Models\PlannedRoute;
use App\Models\PlannedRouteDetail;
use App\Models\RouteUser;
use App\User;

use Livewire\Component;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;

class RoutePlanner extends Component
{
    public $data, $search, $updateMode = false;
    public $clients, $routes, $name, $plannedroutes_id, $status, $client_id,
        $routeid, $clientsperday, $users, $user_id, $users_id, $otro, $plann_id;
    public $lat1, $lat2, $lon1, $lon2;
    public $monday, $tuesday, $wednesday, $thursday, $friday, $saturday,
        $sunday, $begin_at;

    public function mount()
    {
        $this->users    = User::All();
        $this->clients  = Client::All();
        $this->routes   = Distributionroute::All();
        $this->user_id = '';
        $this->users_id = [];
    }

    public function render()
    {
        $this->data = ($this->search)
                ? PlannedRoute::where('id', 'like', '%'.$this->search.'%')
                        ->orWhereIn('client_id', function( $query ){
                            $query->select('id')
                            ->from('clients')
                            ->where('name', 'like', '%'.$this->search.'%');
                        })
                        ->orderBy('id', 'DESC')
                        ->get()
                : PlannedRoute::orderBy('id', 'DESC')->get();
        return view('livewire.route-planner');
    }

    public function resetInput()
    {
        $this->clients          = Client::All();
        $this->routes           = Distributionroute::All();
        $this->name             = '';
        $this->clientsperday    = 0;
        $this->status           = true;
        $this->emitUpdates();
    }

    public function save()
    {
        if ($this->updateMode)
            return $this->update();

        return $this->store();
    }

    public function store()
    {
        $this->validate([
            'clientsperday'         => 'required|numeric',
            'name'                  => 'required',
        ]);
        $record = PlannedRoute::create([
            'user_id'               => Auth::user()->id,
            'name'                  => $this->name,
            'clientsperday'         => $this->clientsperday,
            'status'                => $this->status ?? false,
            'client_id'             => $this->client_id,
            'distributionroute_id'  => $this->routeid,
            'monday'                => $this->monday,
            'tuesday'               => $this->tuesday,
            'wednesday'             => $this->wednesday,
            'thursday'              => $this->thursday,
            'friday'                => $this->friday,
            'saturday'              => $this->saturday,
            'sunday'                => $this->sunday,
            'begin_at'              => $this->begin_at
        ]);
        $this->plann_id = $record->id;
        //Log::error('Lunes: '. $this->monday);

        if ($this->users_id)
                $this->updateusers($record->id);

        $this->emit('notify:toast', ['type'  => 'success', 'title' => 'Registro creado...']);
        $this->resetInput();
    }

    public function edit($id)
    {
        $record = PlannedRoute::findOrFail($id);
        $this->user_id              = Auth::user()->id;
        $this->name                 = $record->name;
        $this->clientsperday        = $record->clientsperday;
        $this->status               = $record->status;
        $this->users_id             = $record->user->pluck('id')->toArray() ?? [];
        $this->client_id            = $record->client_id;
        $this->plannedroutes_id     = $record->id;
        $this->routeid              = $record->distributionroute_id;
        $this->monday               = $record->monday;
        $this->tuesday              = $record->tuesday;
        $this->wednesday            = $record->wednesday;
        $this->thursday             = $record->thursday;
        $this->friday               = $record->friday;
        $this->saturday             = $record->saturday;
        $this->sunday               = $record->sunday;
        $this->begin_at             = $record->begin_at;
        $this->plann_id             = $record->id;

        $this->emitUpdates();
        $this->updateMode = true;

        $this->emit('notify:toast', ['type'  => 'success', 'title' => 'Registro cargado...']);
    }

    public function update()
    {
        $this->validate([
            'clientsperday'         => 'required|numeric',
            'name'                  => 'required',
        ]);
        if ($this->plannedroutes_id) {
            $record = PlannedRoute::find($this->plannedroutes_id);
            $record->update([
                'user_id'               => Auth::user()->id,
                'status'                => $this->status ?? false,
                'name'                  => $this->name,
                'clientsperday'         => $this->clientsperday,
                'client_id'             => $this->client_id,
                'distributionroute_id'  => $this->routeid,
                'monday'                => $this->monday,
                'tuesday'               => $this->tuesday,
                'wednesday'             => $this->wednesday,
                'thursday'              => $this->thursday,
                'friday'                => $this->friday,
                'saturday'              => $this->saturday,
                'sunday'                => $this->sunday,
                'begin_at'              => $this->begin_at
            ]);

            // Log::error('Tuesday: '. $this->tuesday);
            if ($this->users_id)
                $this->updateusers($record->id);

            $this->emit('notify:toast', ['type'  => 'success', 'title' => 'Registro actualizado...']);
            $this->resetInput();
            $this->updateMode = false;
        }
    }

    public function destroy($id)
    {
        if ($id) {
            $record = PlannedRoute::find($id);
            $record->delete();

            $this->emit('notify:toast', ['type'  => 'success', 'title' => 'Registro eliminado...']);
            $this->resetInput();
        }
    }

    public function emitUpdates()
    {
        $this->emit('selectUpdates',
            [ 'client_id'   => $this->client_id,
              'routeid'    => $this->routeid,
              'users_id'    => $this->users_id
          ]);
    }

    private function updateUsers($plannedroutes_id)
    {
        RouteUser::where('planned_route_id', $plannedroutes_id)->delete();
        foreach ($this->users_id as $item)
            RouteUser::create([
                'planned_route_id' => $plannedroutes_id,
                'user_id' => $item
            ]);
    }

    public function updatedrouteid()
    {
        $this->clients  = Client::where('distributionroutes_id',$this->routeid)->get();
        $this->emit('updateclients', ['data' => $this->clients]);
    }

    public function generatePlan()
    {
        $details        = [];
        $distances      = [];
        $firstPoint     = Client::find($this->client_id);
        $distances[]    = 0;
        $details[]      = array($this->client_id, 0);
        // $lat1 = $firstPoint->address->first()->latitude ?? 9.881467;
        // $lon1 = $firstPoint->address->first()->longitude ?? -68.242782;
        $lat1           = 9.881467;
        $lon1           = -68.242782;
        $clients        = Client::where('distributionroutes_id', $firstPoint->distributionroutes_id)
            ->where('client_type', 1)
            ->where('id','!=',$firstPoint->id)->get();
        Log::error('clients en la ruta'. $clients->count());
        foreach ($clients as $client) {
            $distance =$this->distance($lat1, $lon1, $client->address->first()->latitude ?? 0, $client->address->first()->longitude ?? 0);
            $details[] = array($client->id, $distance);
            $distances[] = $distance;
        }
        array_multisort($distances, SORT_ASC, $details);
        $i=1;
        $j = $this->clientsperday;
        Log::error($this->clientsperday);
        $visit = Carbon::createFromFormat('Y-m-d', $this->begin_at);
        //$visit = $carbonBase->addDay(-1);
        foreach ($details as $detail) {
            //Log::error($i%$j);
            if(($i%$j)==0){
                $visit = $visit->addDay(1);
                Log::error('visit: '. $visit . 'i: ' . $i . 'J: ' . $j);
                $visit=$this->validateDay($visit);
            }
            PlannedRouteDetail::create([
                'plannedroute_id'   => $this->plann_id,
                'client_id'         => $detail[0],
                'order'             => $i++,
                'visit_at'          => $visit,
            ]);
        }

    }

    public function validateDay($day)
    {
        $dayofweek = $day->dayOfWeekIso;
        $planned_route = PlannedRoute::find($this->plannedroutes_id);
        switch ($dayofweek) {
            case 1:
                if ($planned_route->monday != 1) {
                    $day = $this->validateDay($day->addDay(1));
                }
                break;
            case 2:
                if ($planned_route->tuesday != 1) {
                    $day = $this->validateDay($day->addDay(1));
                }
                break;
            case 3:
                if ($planned_route->wednesday != 1) {
                    $day = $this->validateDay($day->addDay(1));
                }
                break;
            case 4:
                if ($planned_route->thursday != 1) {
                    $day = $this->validateDay($day->addDay(1));
                }
                break;
            case 5:
                if ($planned_route->friday != 1) {
                    $day = $this->validateDay($day->addDay(1));
                }
                break;
            case 6:
                if ($planned_route->saturday != 1) {
                    $day = $this->validateDay($day->addDay(1));
                }
                break;
            case 7:
                if ($planned_route->sunday != 1) {
                    $day = $this->validateDay($day->addDay(1));
                }
                break;

            default:
                # code...
                break;
        }

        return $day;
    }

    public function distance($lat1, $lon1, $lat2=0, $lon2=0, $unit = "K") {
      if($lat2 == 0){
        $this->generateRandomCoord();
        $lat2 = $this->lat2;
        $lon2 = $this->lon2;
      }
      $theta = $lon1 - $lon2;
      $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
      $dist = acos($dist);
      $dist = rad2deg($dist);
      $miles = $dist * 60 * 1.1515;
      $unit = strtoupper($unit);
      if ($unit == "K") {
        return ($miles * 1.609344);
      } else if ($unit == "N") {
          return ($miles * 0.8684);
        } else {
            return $miles;
          }
    }

    public function generateRandomCoord()
    {
        $this->lat2 = mt_rand(9,10) + (mt_rand(1, 999999)/1000000);
        $this->lon2 = (mt_rand(68,69)*-1) + (mt_rand(1, 999999)/1000000);
        Log::error($this->lat2. '|' . $this->lon2);
    }

}
