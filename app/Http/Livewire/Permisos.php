<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Spatie\Permission\Models\Permission;

class Permisos extends Component
{
    public $name, $guard_name, $search, $data, $permission_id;

    public $updateMode = false;

    public function mount()
    {
        $this->name='';
        $this->guard_name='';
        $this->data = [];
        $this->permission_id = null;
    }

    public function render()
    {
        $this->data = ($this->search)
                ? Permission::where('name', 'like', '%'.$this->search.'%')
                        ->orWhere('guard_name', 'like', '%'.$this->search.'%')
                        ->orderBy('name', 'ASC')
                        ->get()
                : Permission::orderBy('name', 'ASC')->get();
        return view('livewire.permisos');
    }

    public function edit($id)
    {
        $record = permission::findOrFail($id);
        $this->permission_id = $id;
        $this->name = $record->name;
        // $this->emitUpdates();

        $this->emit('notify:toast', ['type'  => 'success', 'title' => 'Permiso cargado...']);

        $this->updateMode = true;
    }

    private function emitUpdates()
    {
        // $this->emit('selectUpdates', [ 'categories_id' => $this->categories_id, 'units_id' => $this->units_id, 'providers_id' => $this->providers_id ] );
    }

    public function resetInput()
    {
        $this->name = null;
    }

    public function save()
    {
        if ($this->updateMode)
            return $this->update();

        return $this->store();
    }

    public function store()
    {
        $this->validate([
            'name' => 'required|min:4'
        ]);
        $product = Permission::create([
            'name' => strtoupper($this->name)
        ]);

        $this->emit('notify:toast', ['type'  => 'success', 'title' => 'Permiso creado...']);
        $this->resetInput();
    }

    public function update()
    {
        $this->validate([
            'name' => 'required|min:4'
        ]);
        if ($this->permission_id) {
            $record = Permission::find($this->permission_id);
            $record->update([
                'name' => strtoupper($this->name)
            ]);

            $this->emit('notify:toast', ['type'  => 'success', 'title' => 'Registro actualizado...']);
            $this->resetInput();
            $this->updateMode = false;
        }
    }

    public function destroy($id)
    {
        if ($id) {
            $record = Permission::find($id);

            $record->delete();

            $this->emit('notify:toast', ['type'  => 'success', 'title' => 'Registro eliminado...']);
            $this->resetInput();
        }
    }
}
