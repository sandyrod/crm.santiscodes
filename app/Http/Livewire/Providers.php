<?php

namespace App\Http\Livewire;
use App\Models\Provider;

use Livewire\Component;
use Manny;

class Providers extends Component
{
    public $data, $name, $address, $phone, $email, $provider_id, $search, $rif;
    public $updateMode = false;

    public function mount()
    {
        $this->name = '';
        $this->address = '';
        $this->phone = '';
        $this->email = '';
        $this->provider_id = null;
        $this->search = '';
        $this->data = [];
        $this->rif = '';
    }

    public function render()
    {
    	$this->data = ($this->search) 
                ? Provider::where('name', 'like', '%'.$this->search.'%')
                		->orWhere('address', 'like', '%'.$this->search.'%')
                        ->orWhere('phone', 'like', '%'.$this->search.'%')
                        ->orWhere('email', 'like', '%'.$this->search.'%')
                        ->orderBy('name', 'ASC')
                        ->get()
                : Provider::orderBy('name', 'ASC')->get();
        return view('livewire.Providers');
    }

    public function resetInput()
    {
        $this->name = null;
        $this->address = null;
        $this->phone = null;
        $this->email = null;
        $this->provider_id = null;
        $this->updateMode = false;
        $this->rif = null;
    }
    public function save()
    {
    	if ($this->updateMode)
    		return $this->update();
    	
    	return $this->store();
    }

    public function store()
    {
        $this->validate([
            'name' => 'required|min:3',
            'rif' => 'required|min:8'
        ]);
        Provider::create([
            'name' => strtoupper($this->name),
            'address' => strtoupper($this->address),
            'rif' => strtoupper($this->rif),
            'phone' => $this->phone,
            'email' => $this->email
        ]);
        $this->emit('notify:toast', ['type'  => 'success', 'title' => 'Registro creado...']);
        $this->resetInput();
    }
    public function edit($id)
    {
        $record = Provider::findOrFail($id);
        $this->provider_id = $id;
        $this->name = $record->name;
        $this->rif = $record->rif;
        $this->address = $record->address;
        $this->phone = $record->phone;
        $this->email = $record->email;
        $this->updateMode = true;
    }
    public function update()
    {
        $this->validate([
            'provider_id' => 'required|numeric',
            'name' => 'required|min:3',
            'rif' => 'required|min:8'
        ]);
        if ($this->provider_id) {
            $record = Provider::find($this->provider_id);
            $record->update([
                'name' => strtoupper($this->name),
                'rif' => strtoupper($this->rif),
                'address' => strtoupper($this->address),
                'phone' => $this->phone,
                'email' => $this->email
            ]);
            $this->emit('notify:toast', ['type'  => 'success', 'title' => 'Registro actualizado...']);
            $this->resetInput();
            $this->updateMode = false;
        }
    }
    public function destroy($id)
    {
        if ($id) {
            $record = Provider::find($id);
            $record->delete();
            $this->emit('notify:toast', ['type'  => 'success', 'title' => 'Registro eliminado...']);
        }
    }

    public function updated($field)
    {
        if ($field == 'rif') {
            $this->rif = strtoupper(Manny::mask($this->rif, "A-111111111"));
        }
    }
}
