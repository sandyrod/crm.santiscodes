<?php

namespace App\Http\Controllers;

use App\Models\Distribution_channel;
use Illuminate\Http\Request;

class DistributionChannelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return view('distribution_channel.distribution_channelform',
            ['distribution_channel'=>Distribution_channel::find($request->id),
            'distribution_channels'=>Distribution_channel::paginate()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $distribution_channels = Distribution_channel::find($request->id) ?? new distribution_channel();
        $distribution_channels->name = strtoupper($request->name);
        $distribution_channels->description = strtoupper($request->description);
        if($distribution_channels->save()){
            return redirect("distribution_channel")->with('status', 'Tipo de negocio Actualizado!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Frequence  $distribution_channel
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        $distribution_channel = Distribution_channel::find($request->id);
        if($distribution_channel->delete()){
            return redirect("distribution_channels")->with('status', 'Tipo de negocio Borrado!');
        }else{
            return redirect("distribution_channels")->with('warning', 'No se elimino ningun tipo de negocio!');
        }
    }
}
