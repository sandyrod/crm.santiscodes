<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PDF;
use App\Models\OrderDetail;

class ProductsController extends Controller
{
    public function print()
    {
        $products = Product::get();
        $today = \Carbon\Carbon::now()->format('d/m/Y');

        $pdf = \PDF::loadView('products.print', compact('products', 'today'));
 
        return $pdf->download('lista_productos.pdf');
    }

    public function ventas()
    {
        $venta = OrderDetail::select('b.id', 'b.code', 'b.name', 'a.quantity', 'c.capacity', DB::raw("(SUM(a.quantity)) * (SUM(a.price)) as tota"))
            ->from('order_details AS a')
            ->JOIN('products AS b',  'b.id', '=', 'a.product_id')
            ->JOIN('units AS c', 'a.unit_id', '=', 'c.id')
            ->GROUPBY('a.product_id', 'c.capacity')
            ->get(); 
        $title = 'Ventas';
        
         $pdf = PDF::loadView('products.pdf.ventas', ['venta' => $venta, 'title' => $title, 'i' => 1 ])->setPaper('legal', 'landscape');
        return $pdf->download('Ventas.pdf'); 
        /*return view('client.pdf.ventas',['venta' => $venta, 'title' => $title, 'i' => 1 ]);*/
    }
}
