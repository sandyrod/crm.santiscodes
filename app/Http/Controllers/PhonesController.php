<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use PDF;

use App\Models\Client;
use App\Models\Phone;

class PhonesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        return response()->json([
            'phone' =>  Phone::find($request->id),
            'phones' =>  Phone::where('client_id', $request->client_id)->get()
        ], 200);
    }

    public function search(Request $request, $id)
    {
        if (!$id)
            return;

        return response()->json(['phone' =>  Phone::find($id)], 200);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $phone = ($request->phone_id) ? Phone::find($request->phone_id) : new Phone();
        $phone->client_id = $request->client_id;
        $phone->number = $request->phone;
        $phone->wsp = $request->wsp ?? 0;
        $phone->principal = $request->main ?? 0;

        $phone->save();

        return response()->json(['phones' =>  Phone::where('client_id', $request->client_id)->get()], 200);
    }

    public function delete(Request $request)
    {
        $phone = Phone::find($request->id);
        $client_id = $phone->client_id;
        if($phone->delete()){
             return response()->json(['phones' =>  Phone::where('client_id', $client_id)->get()], 200);
        }
    }

}
