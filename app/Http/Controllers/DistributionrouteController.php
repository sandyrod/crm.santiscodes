<?php

namespace App\Http\Controllers;

use App\Distributionroute;
use Illuminate\Http\Request;

class DistributionrouteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Distributionroute  $distributionroute
     * @return \Illuminate\Http\Response
     */
    public function show(Distributionroute $distributionroute)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Distributionroute  $distributionroute
     * @return \Illuminate\Http\Response
     */
    public function edit(Distributionroute $distributionroute)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Distributionroute  $distributionroute
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Distributionroute $distributionroute)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Distributionroute  $distributionroute
     * @return \Illuminate\Http\Response
     */
    public function destroy(Distributionroute $distributionroute)
    {
        //
    }
}
