<?php

namespace App\Http\Controllers;

use App\Models\Sequence;
use Illuminate\Http\Request;

class SequenceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return view('sequence.sequenceform',
            ['sequence'=>Sequence::find($request->id),
            'sequences'=>Sequence::paginate()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $sequences = Sequence::find($request->id) ?? new sequence();
        $sequences->name = strtoupper($request->name);
        $sequences->description = strtoupper($request->description);
        if($sequences->save()){
            return redirect("sequence")->with('status', 'Tipo de negocio Actualizado!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Sequence  $sequence
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        $sequence = Sequence::find($request->id);
        if($sequence->delete()){
            return redirect("sequences")->with('status', 'Tipo de negocio Borrado!');
        }else{
            return redirect("sequences")->with('warning', 'No se elimino ningun tipo de negocio!');
        }
    }
}
