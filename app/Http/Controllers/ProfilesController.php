<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

use App\Models\Profile;

class ProfilesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $profile = Profile::find($request->id)? Profile::find($request->id): new profile();
        $profile->user_id = Auth::user()->id;
        $profile->firstname = strtoupper($request->firstname);
        $profile->lastname = strtoupper($request->lastname);
        $profile->dni = strtoupper($request->dni);
        $profile->rif = strtoupper($request->rif);
        if($profile->save()){
            return redirect('profileedit')->with('status', 'Perfil Actualizado!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function show(profile $profile)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function edit(profile $profile)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, profile $profile)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function destroy(profile $profile)
    {
        //
    }

    public function profileEdit(){
        $profile = Auth::user()->profile;
        return view('profile.profileform', ['profile' => $profile]);
    }

    public function password(){
        $user = Auth::user();
        return view('auth.frmpassword', ['user' => $user]);
    }

    public function resetpass(Request $request){
        $user = Auth::user();
        if($request->password == $request->password_confirmation){
            $user->password = Hash::make($request->password);
            if($user->save()){
                Auth::logout();
                return redirect('/');
            }else{
                return redirect('resetpasst')->with('status', 'No se actualizo la clave');
            }
        }else{
            return redirect('resetpasst')->with('status', 'Las claves y su confirmacion deben ser identicas');
        }
        
    }
}
