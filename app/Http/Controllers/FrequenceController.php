<?php

namespace App\Http\Controllers;

use App\Models\Frequence;
use Illuminate\Http\Request;

class FrequenceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return view('frequence.frequenceform',
            ['frequence'=>Frequence::find($request->id),
            'frequences'=>Frequence::paginate()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $frequences = Frequence::find($request->id) ?? new frequence();
        $frequences->name = strtoupper($request->name);
        $frequences->description = strtoupper($request->description);
        if($frequences->save()){
            return redirect("frequence")->with('status', 'Tipo de negocio Actualizado!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Frequence  $frequence
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        $frequence = Frequence::find($request->id);
        if($frequence->delete()){
            return redirect("frequences")->with('status', 'Tipo de negocio Borrado!');
        }else{
            return redirect("frequences")->with('warning', 'No se elimino ningun tipo de negocio!');
        }
    }
}
