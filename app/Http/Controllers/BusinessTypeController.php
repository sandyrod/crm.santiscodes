<?php

namespace App\Http\Controllers;

use App\Models\Business_type;
use Illuminate\Http\Request;

class BusinessTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return view('business_type.business_typeform',
            ['business_type'=>Business_type::find($request->id),
            'business_types'=>Business_type::paginate()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $business_types = Business_type::find($request->id) ?? new Business_type();
        $business_types->name = strtoupper($request->name);
        $business_types->description = strtoupper($request->description);
        if($business_types->save()){
            return redirect("business_types")->with('status', 'Tipo de negocio Actualizado!');
        }

    }

    public function delete(Request $request)
    {
        $business_type = Business_type::find($request->id);
        if($business_type->delete()){
            return redirect("business_types")->with('status', 'Tipo de negocio Borrado!');
        }else{
            return redirect("business_types")->with('warning', 'No se elimino ningun tipo de negocio!');
        }
    }
}
