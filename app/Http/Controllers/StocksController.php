<?php

namespace App\Http\Controllers;

use App\Models\Stock;
use Illuminate\Http\Request;

class StocksController extends Controller
{
    public function print()
    {
        $stocks = Stock::get();

        $pdf = \PDF::loadView('stocks.print', compact('stocks'));

        return $pdf->stream('listastock.pdf');
    }
}
