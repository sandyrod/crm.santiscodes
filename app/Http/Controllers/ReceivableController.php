<?php

namespace App\Http\Controllers;

use App\receivable;
use Illuminate\Http\Request;

class ReceivableController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\receivable  $receivable
     * @return \Illuminate\Http\Response
     */
    public function show(receivable $receivable)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\receivable  $receivable
     * @return \Illuminate\Http\Response
     */
    public function edit(receivable $receivable)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\receivable  $receivable
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, receivable $receivable)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\receivable  $receivable
     * @return \Illuminate\Http\Response
     */
    public function destroy(receivable $receivable)
    {
        //
    }
}
