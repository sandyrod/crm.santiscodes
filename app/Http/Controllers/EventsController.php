<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Events;
use App\Models\Inscriptions;
use Illuminate\Support\Str;
use PDF;
use Maatwebsite\Excel\Facades\Excel;
use Carbon\Carbon;


class EventsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $events = Events::paginate(20);
        //dd($events);

        return view('events.index')->with('events', $events);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('events.events_create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $evt = new Events();

        //dd($request);

        $request->validate([
            'name' => 'required',
            'slug' => 'required',
            'start_date' => 'required',
            'due_date' => 'required',
            'limit' => 'required',
        ]);

        $evt->name = $request->get('name');
        $evt->slug = $request->get('slug');
        $evt->logo = $request->get('logo');
        $evt->start_date = $request->get('start_date');
        $evt->due_date = $request->get('due_date');
        $evt->limit = $request->get('limit');

         if($request->hasFile("logo")){


            $imagen = $request->file("logo");
            $nombreimagen = Str::slug($request->logo).".".$imagen->guessExtension();
            $ruta = public_path("img/event/");

            //$imagen->move($ruta,$nombreimagen);
            copy($imagen->getRealPath(),$ruta.$nombreimagen);

            $evt->logo = $nombreimagen;

        }

        $evt->save();

        return redirect(route('events'))->with('status', 'Registro exitoso');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {

        $events = Events::where('slug', '=', $slug)->first();

        $inscritos = Inscriptions::where('inscriptions.id_events', '=', $events->id)
        ->count();
        $date = Carbon::now()->format("Y-m-d");
        if($events){
            //dd($date);
            if((($events->limit == 0) or ($events->limit > $inscritos)) and ($date < $events->due_date)){

                return view('events.event_incriptions')->with('events', $events);

            }
            else{
                $status = "";
                if($date > $events->due_date){
                    $status .= "La fecha de inscripcion culmino!";
                // dd(var_dump(($date < $events->due_date).'-'.$date .'-'. $events->due_date)) ;
                }
                if($events->limit <= $inscritos){
                    $status .= "  El curso llego a su limite de participantes!" . $events->limit .'-'. $inscritos;
                }
                return redirect(route('events'))->with('status', $status);
            }
        }
        else{

            return view('events.event_incriptions_disable')->with('events', $events);
            
        }        
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $events = Events::find($id);
        //dd($events);
        return view('events.events_update')->with('events', $events);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $events = Events::find($id);

        //dd($events);
        $request->validate([
            'name' => 'required',
            'slug' => 'required',
            'start_date' => 'required',
            'due_date' => 'required',
            'limit' => 'required',
        ]);

         $events->name = $request->get('name');
         $events->slug = $request->get('slug');
         $events->start_date = $request->get('start_date');
         $events->due_date = $request->get('due_date');
         $events->limit = $request->get('limit');
    
         $events->save();

        if($events->save()){
            return redirect(route('events'))->with('status', 'Evento Actualizado con Exito!');
        }else{
            return redirect(route('events'))->with('warning', 'No se Actualizo ningun Evento!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function reporte()
    {
        $events = Events::all();
        
        //dd($events);

        $title = 'Eventos';
        $pdf = PDF::loadView('events.pdf.reporte', ['events' => $events, 'title' => $title, 'i' => 1 ])->setPaper('legal', 'landscape');
        return $pdf->download('Eventos.pdf');
    }

    public function delete(Request $request)
    {
        $events = Events::find($request->id);
        
        //dd($participantes);

        if($events->delete()){
            return redirect(route('events'))->with('status', 'Participante Borrado!');
        }else{
            return redirect(route('events'))->with('warning', 'No se elimino ningun Participante!');
        }
    }
    public function event_list($slug)
    {

        $events = Events::where('slug', '=', $slug)->first();
        //dd($events);
         $participantes = Inscriptions::select('inscriptions.identification','inscriptions.name', 'inscriptions.rif_company', 'inscriptions.name_company', 'inscriptions.created_at')
            ->where('inscriptions.id_events', '=', $events->id)
            ->get(); 
            
        //dd($participantes);    
        return view('events.events_list')->with('events', $events)->with('participantes', $participantes);
    }
    
    public function report_pdf($id)
    {
        $events = Inscriptions::select('inscriptions.identification','inscriptions.name', 'inscriptions.rif_company', 'inscriptions.name_company', 'inscriptions.created_at', 'events.name as name_event', 'events.logo' )
            ->join('events', 'inscriptions.id_events', '=', 'events.id')
            ->where('inscriptions.id_events', '=', $id)
            ->get();
        
        // dd($events);

        $title = 'Personas Inscritas En El Evento  ';
        $pdf = PDF::loadView('events.pdf.report_list', ['events' => $events, 'title' => $title, 'i' => 1 ])->setPaper('legal', 'landscape');
        return $pdf->download('Personas_Eventos.pdf');
    }
    
    public function stores(Request $request)
    {
        $request->validate([
            'identification' => 'required|unique:inscriptions',
            'name' => 'required',
            'last_name' => 'required',
            'phone' => 'required',
            'email' => 'required|unique:inscriptions',
            'rif_company' => 'required',
            'name_company' => 'required',
            'instagram_personal' => 'required',
            'instagram_company' => 'required',
        ]);
        

        $inscriptions = new Inscriptions();

        $inscriptions->identification = $request->get('identification');
        $inscriptions->name = $request->get('name');
        $inscriptions->last_name = $request->get('last_name');
        $inscriptions->phone = $request->get('phone');
        $inscriptions->email = $request->get('email');
        $inscriptions->rif_company = $request->get('rif_company');
        $inscriptions->name_company = $request->get('name_company');
        $inscriptions->instagram_personal = $request->get('instagram_personal');
        $inscriptions->instagram_company = $request->get('instagram_company');
        $inscriptions->id_events = $request->get('id_events');

        //dd($inscriptions);

        //$inscriptions->save();
        if(auth()->guest()){
            if($inscriptions->save()){
                return  redirect()->back()->with('status', 'Participante Registrado con Exito!');
            }else{
                return redirect()->back()->with('warning', 'Error No se pudo Registrar El Participante!');
            }
        }else{
            if($inscriptions->save()){
                return redirect(route('events'))->with('status', 'Participante Registrado con Exito!');
            }else{
                return redirect(route('events'))->with('warning', 'Error No se pudo Registrar El Participante!');
            }
        }

    }

}
