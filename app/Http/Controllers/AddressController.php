<?php

namespace App\Http\Controllers;

use App\Models\Address;
use Illuminate\Http\Request;

class AddressController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function index(Request $request)
    {
        
        return response()->json([
            'address' =>  Address::find($request->id),
            'addresses' =>  Address::where('client_id', $request->client_id)->get()
        ], 200);
    }

    public function search(Request $request, $id)
    {
        if (!$id)
            return;

        return response()->json(['address' =>  Address::find($id)], 200);
    }

   
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $address = ($request->address_id) ? Address::find($request->address_id) : new Address();
        $address->client_id = $request->client_id;
        $address->urbanization = strtoupper($request->urbanization);
        $address->sector = strtoupper($request->sector);
        $address->street = strtoupper($request->street);
        $address->description = strtoupper($request->description);
        $address->latitude = $request->latitude;
        $address->longitude = $request->longitude;
        $address->google_place_id = $request->google_place_id;
        $address->municipality = strtoupper($request->municipality);
        $address->parish = strtoupper($request->parish);
        $address->streets_between = strtoupper($request->streets_between);
        $address->city = strtoupper($request->city);
        $address->state = strtoupper($request->state);

        $address->save();

        return response()->json(['addresses' =>  Address::where('client_id', $request->client_id)->get()], 200);
    }

    public function delete(Request $request)
    {
        $address = Address::find($request->id);
        $client_id = $address->client_id;
        if($address->delete()){
             return response()->json(['addresses' =>  Address::where('client_id', $client_id)->get()], 200);
        }
    }
}
