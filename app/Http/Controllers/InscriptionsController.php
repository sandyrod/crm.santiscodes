<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Inscriptions;
use Illuminate\Support\Facades\DB;
use PDF;
use App\Exports\InscriptionsExport;
use Maatwebsite\Excel\Facades\Excel;


class InscriptionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('inscriptions.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('inscriptions.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'identification' => 'required',
            'name' => 'required',
            'last_name' => 'required',
            'phone' => 'required',
            'email' => 'required|unique:inscriptions',
            'rif_company' => 'required|unique:inscriptions',
            'name_company' => 'required',
            'instagram_personal' => 'required',
            'instagram_company' => 'required',
        ]);
        

        $inscriptions = new Inscriptions();

        $inscriptions->identification = $request->get('identification');
        $inscriptions->name = $request->get('name');
        $inscriptions->last_name = $request->get('last_name');
        $inscriptions->phone = $request->get('phone');
        $inscriptions->email = $request->get('email');
        $inscriptions->rif_company = $request->get('rif_company');
        $inscriptions->name_company = $request->get('name_company');
        $inscriptions->instagram_personal = $request->get('instagram_personal');
        $inscriptions->instagram_company = $request->get('instagram_company');
        $inscriptions->id_events = 2;
        $inscriptions->save();

        return redirect(route('inscriptions'))->with('status', 'Registro exitoso');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $participante = Inscriptions::find($id);
        //dd($participante);
        return view('inscriptions.participante_update')->with('participante',$participante);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        

        $participante = Inscriptions::find($id);

        //dd($participante);
        $request->validate([
            'identification' => 'required',
            'name' => 'required',
            'last_name' => 'required',
            'phone' => 'required',
            'email' => 'required',
            'rif_company' => 'required',
            'name_company' => 'required',
            'instagram_personal' => 'required',
            'instagram_company' => 'required',
        ]);

         $participante->identification = $request->get('identification');
         $participante->name = $request->get('name');
         $participante->last_name = $request->get('last_name');
         $participante->phone = $request->get('phone');
         $participante->email = $request->get('email');
         $participante->rif_company = $request->get('rif_company');
         $participante->name_company = $request->get('name_company');
         $participante->instagram_personal = $request->get('instagram_personal');
         $participante->instagram_company = $request->get('instagram_company');

         $participante->save();
        //return view('inscriptions.participantes');

        if($participante->save()){
            return redirect(route('participantes'))->with('status', 'Participante Actualizado con Exito!');
        }else{
            return redirect(route('participantes'))->with('warning', 'No se Actualizo ningun Participante!');
        }
         

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function participantes()
    {
        $participantes = Inscriptions::paginate(20);
        //dd($participantes);
        return view('inscriptions.participantes')->with('participantes', $participantes);
    }

    public function reporte()
    {
        $participantes = Inscriptions::all();
        
        //dd($participantes);

        $title = 'Participantes y Empresas registradas';
        $pdf = PDF::loadView('inscriptions.pdf.report', ['participantes' => $participantes, 'title' => $title, 'i' => 1 ])->setPaper('legal', 'landscape');
        return $pdf->download('Participantes.pdf');
    }

    public function report_excell()
    {
        //dd('hola');
        return Excel::download(new InscriptionsExport, 'evento.xlsx');

    }

    public function delete(Request $request)
    {
        $participantes = Inscriptions::find($request->id);
        
        //dd($participantes);

        if($participantes->delete()){
            return redirect(route('participantes'))->with('status', 'Participante Borrado!');
        }else{
            return redirect(route('participantes'))->with('warning', 'No se elimino ningun Participante!');
        }
    }

    
}
