<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use PDF;
use Maatwebsite\Excel\Facades\Excel;

//Excel
use App\Exports\ClientExport;
use App\Exports\CensusChannelPointsExport;
use App\Exports\CensusMasterUserExport;
use App\Exports\CensusReportExport;

//Modelos
use App\Models\Brand;
use App\Models\Client;
use App\Models\Address;
use App\Models\Phone;
use App\Models\Product;
use App\Models\Category;
use App\Models\Sequence;
use App\Models\Frequence;
use App\Models\Sales_type;
use App\Models\Business_type;
use App\Models\ClientBrand;
use App\Models\ClientCategory;
use App\Models\ClientsProducts;
use App\Models\Distributionroute;
use App\Models\Distribution_channel;

//Requests
use App\Http\Requests\ClientRequest;



class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $product = new Product();


        return view('client.clientform',
            ['client'=>Client::find($request->id),
            'clients'=>Client::orderBy('status')
                ->orderBy('businessname')->paginate(),
            'business_types' => Business_type::All(),
            'products' => $product->getProductAssigned($request->id),
            'sales_types' => Sales_type::All(),
            'frecuencies' => Frequence::All(),
            'sequences' => Sequence::All(),
            'distribution_channels' => Distribution_channel::All(),
            'distribution_routes' => Distributionroute::All(),
            ]);
    }

    public function search(Request $request, $rif)
    {
        if (!$rif)
            return;

        return response()->json([
                'client' =>  Client::where('rif', $rif)->first()
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $client = Client::find($request->id)? Client::find($request->id): new Client();
        if ($request->rif)
            $client->rif = $request->rif;

        if ($request->client_type==1){
            $client->ownersname = strtoupper($request->ownersname);
            $client->ownersdni = strtoupper($request->ownersdni);
            $client->ownersmail = $request->ownersmail;
            $client->ownersphone = str_replace('-', '', $request->ownersphone);
            $client->ownersbirthday = $request->ownersbirthday;
            $client->managersname = strtoupper($request->managersname);
            $client->managersdni = strtoupper($request->managersdni);
            $client->managersmail = $request->managersmail;
            $client->maximumorder = $request->maximumorder;
            $client->pennants = $request->pennants;
            $client->display = $request->display;
            $client->frame = $request->frame;
            $client->pop = $request->pop;
        }

        $client->user_id = Auth::user()->id;
        $client->ownersphone = $request->ownersphone;
        $client->client_type = $request->client_type;
        $client->businessname = strtoupper($request->businessname);
        $client->businesscode = strtoupper($client->businesscode ?? $request->businesscode);
        $client->client_date = $request->client_date;
        $client->specialtaxpayer = $request->specialtaxpayer ?? 1;
        $client->businesssmail = $request->businesssmail;
        $client->distribution_channel_id = $request->distribution_channel_id;
        $client->business_type_id = $request->business_type_id;
        $client->sales_type_id = $request->sales_type_id;
        $client->creditlimit = $request->creditlimit;
        $client->visitfrecuency = $request->visitfrecuency;
        $client->visitsequence = $request->visitsequence;
        $client->anniversary = $request->anniversary;
        $client->dropsizeprom = $request->dropsizeprom;

        $client->registered_name = strtoupper($request->registered_name ?? 'null');
        $client->registered_charge = strtoupper($request->registered_charge ?? 'null');
        $client->open_establishment = $request->open_establishment;
        $client->owner_attend = $request->owner_attend ?? 0;
        $client->persons_attending = $request->persons_attending;
        $client->stickers = $request->stickers;
        $client->other_pop = $request->other_pop;
        $client->facade = $request->facade;

        $client->chronuscontigo = $request->chronuscontigo;

        if($client->save()){
            if ($request->address_id || $request->urbanization || $request->sector || $request->street || $request->description)
                $this->updateAddress($request, $client->id);

            if ($request->products)
                $this->updateProducts($request, $client->id);

            return redirect("clients")->with('status', 'Cliente Actualizado!');
        }
    }

    public function updateProducts($request, $client_id)
    {
        ClientsProducts::where('client_id', $client_id)->delete();
        foreach ($request->products as $item)
            ClientsProducts::create([
                'product_id' => $item,
                'client_id' => $client_id
            ]);
    }

    public function delete(Request $request)
    {
        $client = Client::find($request->id);
        if($client->delete()){
            return redirect("clients")->with('status', 'Cliente Borrado!');
        }else{
            return redirect("clients")->with('warning', 'No se elimino ningun cliente!');
        }
    }

    public function lists(Request $request)
    {
        $clients = Client::all();
        $title = 'LISTADO DE CLIENTES';
        $pdf = PDF::loadView('client.pdf.clientall', ['clients' => $clients, 'title' => $title]);

        return $pdf->download('clientes.pdf');
    }

    public function bank(Request $request)
    {
        $clients = Client::all()->where('status',0);
        $title = 'BANCO';
        $pdf = PDF::loadView('client.pdf.clientall', ['clients' => $clients, 'title' => $title]);
        return $pdf->download('Banco_clientes.pdf');
    }

    public function pdfMaster(Request $request, $client_id)
    {
        $client = Client::with('address')->with('distribution_channels')->with('business_type')->find($client_id);
        if ($client){
            $title = 'FICHA MAESTRA DE CLIENTES';
            $categories = Category::get(['id', 'name']);
            $pdf = PDF::loadView('client.pdf.clientmaster', ['client' => $client, 'categories' => $categories, 'title' => $title]);
            $pdf->setPaper('letter', 'landscape');

            return $pdf->download('ficha_cliente.pdf');
        }
    }

    public function census(Request $request)
    {
        $brand = new Brand();
        $category = new Category();
        $client = Client::find($request->id);
        if($client){
            $phones = $client->phones()->get('number');
        }else{
            $phones = null;
        }

        //dd($client->phones()->get('number'));
        return view('client.census',
            ['client'=> $client,
            'clients'=>Client::whereNotNull('census_at')->orderBy('census_at', 'DESC')->get(),
            'business_types' => Business_type::All(),
            'sales_types' => Sales_type::All(),
            'categories' => $category->getCategoriesAssigned($request->id),
            'brands' => $brand->getBrandsAssigned($request->id),
            'distribution_routes' => Distributionroute::All(),
            'distribution_channels' => Distribution_channel::All(),
            'phones' => $phones
            ]);
    }

    public function searchNextCode($route)
    {
        $client = Client::where(\DB::raw('substr(businesscode, 3, 3)'), $route)->select('businesscode')->orderBy('businesscode', 'DESC')->first();
        return $client ? substr($client->businesscode, 6) + 1 : 1;
    }

    public function storeCensus(ClientRequest $request)
    {
       $client = ($request->id) ? Client::findOrFail($request->id) : null;
        if ( ! $client){
            $client = new Client();
            $client->user_id = Auth::user()->id;
            $client->client_date = $request->client_date;
            $client->specialtaxpayer = $request->specialtaxpayer ?? 1;
        }

        if ($request->rif)
            $client->rif = str_replace('_', '', $request->rif);
        $client->distribution_channel_id = $request->distribution_channel_id;
        $client->business_type_id = $request->business_type_id;
        $client->businessname = strtoupper($request->businessname);
        $client->businesscode = strtoupper($request->businesscode);
        $client->distributionroutes_id = $request->distributionroute_id-100;
        $client->ownersname = strtoupper($request->ownersname);
        $client->ownersdni = strtoupper($request->ownersdni);
        $client->ownersmail = $request->ownersmail;
        $client->ownersphone = str_replace('-', '', $request->ownersphone);

        $client->survey_number = Client::Where('survey_number','>',0)->count()+1;
        $client->sells_lubricants = $request->sells_lubricants;
        $client->sells_chronus = $request->sells_chronus;
        $client->anniversary = $request->anniversary;
        $client->area = strtoupper($request->area);
        $client->autoservice = $request->autoservice;
        $client->visibility = $request->visibility;
        $client->other_services = strtoupper($request->other_services);
        $client->purchase_interest = strtoupper($request->purchase_interest);
        $client->dependents_number = $request->dependents_number;
        $client->person_contact = strtoupper($request->person_contact);
        $client->charge_contact = strtoupper($request->charge_contact);
        $client->buyer = strtoupper($request->buyer);
        $client->other_contact = strtoupper($request->other_contact);
        $client->other_contact_charge = strtoupper($request->other_contact_charge);
        $client->quality_frame = $request->quality_frame;
        $client->census_talkers = $request->census_talkers;
        $client->census_posters = $request->census_posters;
        $client->census_exhibitor = $request->census_exhibitor;
        $client->census_pennants = $request->census_pennants;
        $client->observations = strtoupper($request->observations);
        $client->cdv = strtoupper($request->cdv);
        $client->dropsizeprom = $request->dropsizeprom;
        $client->maximumorder = $request->maximumorder;
        $client->brand_a = strtoupper($request->brand_a);
        $client->brand_b = strtoupper($request->brand_b);
        $client->brand_c = strtoupper($request->brand_c);

        $client->registered_name = strtoupper($request->registered_name);
        $client->registered_charge = strtoupper($request->registered_charge);
        $client->open_establishment = $request->open_establishment;
        $client->owner_attend = strtoupper($request->owner_attend);
        $client->persons_attending = $request->persons_attending;
        $client->stickers = $request->stickers;
        $client->other_pop = $request->other_pop;
        $client->facade = $request->facade;

        $client->census_by = Auth::user()->id;
        $client->census_at = \Carbon\Carbon::now();

        $client->save();

        if ($request->address_id || $request->urbanization || $request->sector || $request->street || $request->description)
            $this->updateAddress($request, $client->id);

        if ($request->categories)
                $this->updateCategories($request, $client->id);

        if ($request->brands)
                $this->updateBrands($request, $client->id);

        if ($request->ownersphone1 || $request->ownersphone1 || $request->ownersphone1 || $request->ownersphone1 || $request->ownersphone1)
            $this->updatePhones($request, $client->id);

        return redirect("census/$client->id")->with('status', 'Censo registrado!');

    }

    private function updateAddress($request, $client_id)
    {
        $address = Address::find($request->address_id);
        if (!$address)
            $address = new Address();

        $address->client_id = $client_id;
        $address->urbanization = strtoupper($request->urbanization);
        $address->sector = strtoupper($request->sector);
        $address->street = strtoupper($request->street);
        $address->description = strtoupper($request->description);
        $address->latitude = $request->latitude;
        $address->longitude = $request->longitude;
        $address->google_place_id = $request->google_place_id;
        $address->municipality = strtoupper($request->municipality);
        $address->parish = strtoupper($request->parish);
        $address->streets_between = strtoupper($request->streets_between);
        $address->city = strtoupper($request->city);
        $address->state = strtoupper($request->state);

        $address->save();
    }

    public function updateCategories($request, $client_id)
    {
        ClientCategory::where('client_id', $client_id)->delete();
        foreach ($request->categories as $item)
            ClientCategory::create([
                'category_id' => $item,
                'client_id' => $client_id
            ]);
    }

     public function updateBrands($request, $client_id)
    {
        ClientBrand::where('client_id', $client_id)->delete();
        foreach ($request->brands as $item)
            ClientBrand::create([
                'brand_id' => $item,
                'client_id' => $client_id
            ]);
    }

    private function updatePhones($request, $client_id)
    {
        Phone::Where('client_id', '=', $client_id)->delete();

        if ($request->ownersphone1){
            Phone::save_phones($request->ownersphone1, $client_id, 0, 0);
        }
        if ($request->ownersphone2){
            Phone::save_phones($request->ownersphone2, $client_id, 0, 0);
        }
        if ($request->ownersphone3){
            Phone::save_phones($request->ownersphone3, $client_id, 0, 0);
        }
        if ($request->ownersphone4){
            Phone::save_phones($request->ownersphone4, $client_id, 0, 0);
        }
        if ($request->ownersphone5){
            Phone::save_phones($request->ownersphone5, $client_id, 0, 0);
        }

    }

    public function censusReport()
    {
        $clients = Client::whereNotNull('census_at')->get();
        $title = 'FORMATO DE RESUMEN DE CENSO DE CLIENTES';
        $pdf = PDF::loadView('client.pdf.census_report', ['clients' => $clients, 'title' => $title, 'i' => 1 ])->setPaper('legal', 'landscape');
        return $pdf->download('Resumen_censo.pdf');
    }

    public function censusExcel()
    {
        return Excel::download(new CensusReportExport, 'ResumenCenso.xls');
    }

    public function censusMasterUsersReport()
    {
        $clients = Client::whereNotNull('census_at')->get();
        $title = 'RECOPILACION PARA MAESTRO DE USUARIOS';
        $pdf = PDF::loadView('client.pdf.census_master_users_report', ['clients' => $clients, 'title' => $title, 'i' => 1 ])->setPaper('legal', 'landscape');
        return $pdf->download('Recopilacion_maestro_usuario_censo.pdf');
    }

    public function censusMasterUsersExcel()
    {
        return Excel::download(new CensusMasterUserExport, 'MaestroDeUsuarios.xls');
    }

    public function censusChannelPointsReport()
    {
        $distributionChannels = Distribution_channel::All('id', 'name', 'description');
        $title = 'Cuadro de Puntos por Canal';
        foreach ($distributionChannels as $distributionChannel) {
            $channelCount = Client::whereNotNull('census_at')
                ->where('distribution_channel_id','=',$distributionChannel->id)
                ->count();
            $arrayitems[]= array('name' =>$distributionChannel->name, 'description' => $distributionChannel->description, 'count' => $channelCount);
        }
        $pdf = PDF::loadView('client.pdf.census_channel_point_report', ['items' => $arrayitems, 'title' => $title]);
        return $pdf->download('Cuadro_puntos_canal_pdf');
    }

    public function censusChannelPointsExcel()
    {
        return Excel::download(new CensusChannelPointsExport, 'PuntosPorCanal.xls');
    }

    public function changeType(Request $request)
    {
        $client = Client::find($request->id);
        $oldtype = $client->client_type;
        switch ($oldtype) {
            case 0;
                $client->client_type = 1;
                break;
            case 1:
                $client->client_type = 2;
                break;
            case 2:
                $client->client_type = 0;
                break;
            default:
                $client->client_type = 1;
                break;
        }
        $client->save();
        return redirect("clients");
    }
}
