<?php

namespace App\Http\Controllers;

use App\Models\Sales_type;
use Illuminate\Http\Request;

class SalesTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return view('sales_type.sales_typeform',
            ['sales_type'=>Sales_type::find($request->id),
            'sales_types'=>Sales_type::paginate()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $sales_types = Sales_type::find($request->id) ?? new sales_type();
        $sales_types->name = strtoupper($request->name);
        $sales_types->description = strtoupper($request->description);
        if($sales_types->save()){
            return redirect("sales_types")->with('status', 'Tipo de negocio Actualizado!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Sales_type  $sales_type
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        $sales_type = Sales_type::find($request->id);
        if($sales_type->delete()){
            return redirect("sales_types")->with('status', 'Tipo de negocio Borrado!');
        }else{
            return redirect("sales_types")->with('warning', 'No se elimino ningun tipo de negocio!');
        }
    }
}
