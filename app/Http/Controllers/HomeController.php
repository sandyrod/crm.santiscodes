<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Client;
use App\Models\Order;
use App\Models\Receivable;
use App\Models\Visit;
use Carbon\Carbon;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $today = date('Y-m-d').' 00:00:00';
        $month = date('Y-m-').'1 00:00:00';
        $clientsToday   = Client::where('created_at','>=', $today)->where('client_type', 1)->count();
        $clientsMonth   = Client::where('created_at','>=', $month)->where('client_type', 1)->count();
        $clientsAllTime = Client::where('client_type', 1)->count();
        $sales          = Order::where('created_at', '>=', $today)->count();
        $cxc            = Receivable::where('duedate_at', '=', $today)->count();
        $visits         = Visit::where('created_at', '>=', $today)->count();
        return view('home')
            ->with('sales', $sales)
            ->with('cxc', $cxc)
            ->with('visits', $visits)
            ->with('clientsAllTime', $clientsAllTime)
            ->with('clientsMonth', $clientsMonth)
            ->with('clientsToday', $clientsToday);
    }
}
