<?php

namespace App\Http\Controllers;

use App\Models\PlannedRouteDetail;
use Illuminate\Http\Request;


use App\Models\PlannedRoute;
use App\PlanedRouteDetail;

use JeroenNoten\LaravelAdminLte\Components\Widget\Alert;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\route_planning_import;
use App\Exports\PlanedRouteDetailExport;

class PlanedRouteDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    public function export() 
    {
        return Excel::download(new PlanedRouteDetailExport, 'route_planning.xlsx');
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\PlanedRouteDetail  $planedRouteDetail
     * @return \Illuminate\Http\Response
     */
    public function show(PlanedRouteDetail $planedRouteDetail)
    {
        $plannedroute = PlannedRoute::all();
        return view('routeplanner.export')->with('plannedroute',$plannedroute);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PlanedRouteDetail  $planedRouteDetail
     * @return \Illuminate\Http\Response
     */
    public function edit(PlanedRouteDetail $planedRouteDetail)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PlanedRouteDetail  $planedRouteDetail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PlanedRouteDetail $planedRouteDetail)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PlanedRouteDetail  $planedRouteDetail
     * @return \Illuminate\Http\Response
     */
    public function destroy(PlanedRouteDetail $planedRouteDetail)
    {
        //
    }
}
