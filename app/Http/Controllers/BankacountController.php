<?php

namespace App\Http\Controllers;

use App\bankacount;
use Illuminate\Http\Request;

class BankacountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\bankacount  $bankacount
     * @return \Illuminate\Http\Response
     */
    public function show(bankacount $bankacount)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\bankacount  $bankacount
     * @return \Illuminate\Http\Response
     */
    public function edit(bankacount $bankacount)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\bankacount  $bankacount
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, bankacount $bankacount)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\bankacount  $bankacount
     * @return \Illuminate\Http\Response
     */
    public function destroy(bankacount $bankacount)
    {
        //
    }
}
