<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ClientRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'businessname' =>  'required|string',
            'rif' =>  'required|string',
            'businesscode' => 'required|string',
            'urbanization' => 'required|string',
            'sector' => 'required|string',
            'street' => 'required|string',
            'description' => 'required|string',
            'municipality' => 'required|string',
            'parish' => 'required|string',
            'ownersname' => 'required|string',
            'ownersdni' =>  'required|string',
            'ownersmail' =>  'required|email',
            'ownersphone' =>  'required',
            'anniversary' => 'required',
            //'area' => 'required|string',
            'distribution_channel_id' => 'required|integer',
            'business_type_id' => 'required|integer',
            //'visibility' => 'required|string',
            //'purchase_interest' => 'required|integer',
            //'dependents_number' => 'required|integer',
            'person_contact' => 'required|string',
            'charge_contact' => 'required|string',
            'buyer' => 'required|string',
            'other_contact' => 'required|string',
            'other_contact_charge' => 'required|string',
            //'observations' => 'required|string',
            //'cdv' => 'required|string',
            //'dropsizeprom' => 'required|string',
            //'brand_a' => 'required|string',
            //'brand_b' => 'required|string',
            //'brand_c' => 'required|string'
        ];
    }

     public function attributes(){
        return [
            'businessname' => 'Razón Social',
            'rif' => 'RIF',
            'businesscode' => 'Código',
            'urbanization' => 'Urbanización',
            'sector' => 'Sector',
            'street' => 'Calle',
            'description' => 'Punto de referencia',
            'municipality' => 'Municipio',
            'parish' => 'Parroquia',
            'ownersname' => 'Nombre de Propietario',
            'ownersdni' => 'Cédula',
            'ownersmail' => 'Email',
            'ownersphone' => 'Teléfono',
            'anniversary' => 'Aniversario',
            'area' => 'Área',
            'distribution_channel_id' => 'Canal',
            'business_type_id' => 'Tipo',
            'visibility' => 'Visibilidad',
            'purchase_interest' => 'Interés',
            'dependents_number' => 'Nro. dependientes',
            'person_contact' => 'Persona Contacto',
            'charge_contact' => 'Cargo',
            'buyer' => 'Comprador',
            'other_contact' => 'Otro Contacto',
            'other_contact_charge' => 'Cargo',
            'observations' => 'Observaciones',
            'cdv' => 'CDV',
            'dropsizeprom' => 'Dropsize',
            'brand_a' => 'Marca A',
            'brand_b' => 'Marca B',
            'brand_c' => 'Marca C'
        ];
    }
}
