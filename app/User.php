<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use Notifiable;
    use HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function profile(){
        return $this->hasOne('App\Models\Profile');
    }

    public function client(){
        return $this->hasMany('App\Models\Client');
    }

    public function census(){
        return $this->hasMany('App\Models\Client');
    }

    public function rols(){
        return $this->hasMany('App\Models\ModelHasRol', 'model_id');

    }

    public function purchaseorder(){
        return $this->hasMany('App\Models\purchaseorder');
    }

}
