<?php

namespace App\Repositories;

use App\Models\Order;
use App\Models\OrderDetail;

use App\Models\Dispatchorder;
use App\Models\Dispatch_detail;

use App\Models\Visit;
use App\Models\VisitDetail;

use App\Models\Product;
use App\Models\Unit;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;


class OrdersRepository
{
    public static function setOrderStatus($order_id, $status)
    {
        $order=Order::find($order_id);
        $order->status=$status;
        $order->save();
    }

    public static function getOrderRemaining($order_id)
    {
        $dispatchs = Dispatchorder::select('id')->where('order_id', $order_id)->get();
        foreach ($dispatchs as $dispatch) {
            $dispatchs_id[]=$dispatch->id;
        }
        if(isset($dispatchs_id)){
            $dispatched = Dispatch_detail::whereIn('dispatch_id', $dispatchs_id)->get()->sum('quantity') ?? 0;
        }else{
            $dispatched = 0;
        }

        $requested = OrderDetail::where('order_id', $order_id)->get()->sum('quantity') ?? 0;

        return $requested-$dispatched;
    }

    public static function getQuantityRemaining($order_id, $product_id)
    {
        $dispatchs = Dispatchorder::select('id')->where('order_id', $order_id)->get();
        foreach ($dispatchs as $dispatch) {
            $dispatchs_id[]=$dispatch->id;
        }
        if(isset($dispatchs_id)){
            $dispatched = Dispatch_detail::whereIn('dispatch_id', $dispatchs_id)
                ->where('product_id', $product_id)
                ->get()->sum('quantity') ?? 0;
        }else{
            $dispatched = 0;
        }

        $requested = OrderDetail::where('order_id', $order_id)
            ->where('product_id', $product_id)
            ->get()->sum('quantity') ?? 0;

        return $requested-$dispatched;
    }

    public static function getTotalAmount($order_id)
    {
        $amount = 0;
        $details = OrderDetail::select('quantity', 'price')->where('order_id', $order_id)->get();
        foreach ($details as $item) {
            $itemAmount = $item->quantity * $item->price;
            $amount += $itemAmount;
        }
        return $amount ?? 0;
    }

    public static function generateOrder($visit_id)
    {
        $visit = Visit::find($visit_id);
        $visitDetails = visitDetail::where('visit_id', $visit_id)->get();
        $order = Order::create([
            'client_id'     => $visit->client_id,
            'payment'       => 'EFECTIVO',
            'other'         => 'Generada desde Ficha Visita'
        ]);
        //Ajustar aqui para que cargue los precios en el detalle del pedido
        foreach($visitDetails as $detail){
            $unidad = Product::find($detail->product_id)->units->first();
            $prices = Product::find($detail->product_id);
            if ($visit->document == 'FACTURA'){
                $price = $prices->sale_price;
            }else{
                $price = $prices->price;
            }
            $orderDetail = OrderDetail::create([
                'order_id'      => $order->id,
                'product_id'    => $detail->product_id,
                'unit_id'       => $unidad->id,
                'quantity'      => $detail->sold,
                'price'         => $price
            ]);
        }

        return $order;

    }

    public static function getTotalLiters($order_id)
    {
        $details = OrderDetail::where('order_id', $order_id)->get();
        $TotalLiters = 0;
        foreach($details as $detail){
            $quantity = $detail->quantity;
            $unidad = Unit::find($detail->unit_id);
            $liters = $unidad->capacity * $quantity;
            $TotalLiters += $liters;
        }
        return $TotalLiters ?? 0;
    }
}
