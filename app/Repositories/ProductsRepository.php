<?php

namespace App\Repositories;

use App\Models\Product;
use App\Models\VisitDetail;

use App\Models\Dispatchorder;
use App\Models\Dispatch_detail;

use App\Models\Purchaseorder;
use App\Models\Purchase_detail;

use App\Models\Stock;
use App\Models\StockLog;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
//use App\Repositories\BaseRepository;


class ProductsRepository extends Product
{

    public static function getProducts($search ='')
    {
        $products = ($search)
                ? Product::select('products.id as id', 'products.code', 'products.name', 'products.description', 'products.ue', DB::raw('0 as votes, 0 as distribution, 0 as sold'))
                        ->where('products.name', 'like', '%'. $search .'%')
                        ->orWhere('products.code', 'like', '%'. $search .'%')
                        ->orWhere('products.description', 'like', '%'. $search .'%')
                        ->orWhereIn('products.id', function( $query ) use ($search){
                            $query->select('product_id as id')
                            ->from('products_categories')
                            ->leftJoin('categories', 'categories.id', 'products_categories.category_id')
                            ->where('products.name', 'like', '%'. $search .'%');
                        })
                        ->orWhereIn('products.id', function( $query ) use ($search){
                            $query->select('product_id as id')
                            ->from('products_units')
                            ->leftJoin('units', 'units.id', 'products_units.unit_id')
                            ->where('products.name', 'like', '%'.$search.'%');
                        })
                        ->orWhereIn('products.id', function( $query ) use ($search){
                            $query->select('product_id as id')
                            ->from('products_providers')
                            ->leftJoin('providers', 'providers.id', 'products_providers.provider_id')
                            ->where('products.name', 'like', '%'.$search.'%');
                        })
                        ->get()
                : Product::select('products.id as id', 'products.code', 'products.name', 'products.description', 'products.ue', DB::raw('0 as votes, 0 as distribution, 0 as sold'))
                    ->get();

        return $products;
    }

    public static function getVisitProducts($products, $visit_id)
    {
        $visit_details=null;
        $visit_detail = VisitDetail::select('product_id as id', 'inventory', 'suggested', 'distribution', 'sold')
                 ->where('visit_id', $visit_id)->get();
        foreach ($visit_detail as $detail) {
            $visit_details[$detail->id] = $detail;
        }
        return $visit_details;
    }

    public static function getStock($product_id, $warehouse_id)
    {
        $dispatchs = Dispatchorder::select('id')
            ->where('warehouse_id', $warehouse_id)
            ->where('status', 1)
            ->get();
        foreach ($dispatchs as $dispatch) {
            $dispatchs_id[]=$dispatch->id;
        }
        $dispatched = Dispatch_detail::where('product_id', $product_id)
            ->whereIn('dispatch_id', $dispatchs_id)
            ->get()
            ->sum('quantity') ?? 0;

        $purchaces = Purchaseorder::select('id')
            ->where('warehouse_id', $warehouse_id)
            ->where('status', 1)
            ->get();
        foreach ($purchaces as $purchace) {
            $purchaces_id[]=$purchace->id;
        }
        $purchaced = Purchase_detail::where('product_id', $product_id)
            ->whereIn('purchase_id', $purchaces_id)
            ->get()
            ->sum('quantity') ?? 0;

        $merma = StockLog::where('product_id', $product_id)
            ->where('warehouse_id', $warehouse_id)
            ->where('operation', 0)
            ->get()
            ->sum('quantity') ?? 0;

        $ajustes = StockLog::where('product_id', $product_id)
            ->where('warehouse_id', $warehouse_id)
            ->where('operation', 1)
            ->get()
            ->sum('quantity') ?? 0;

        return $ajustes + $purchaced - $merma - $dispatched;
    }

    public static function setPrice($product_id, $price)
    {
        $product = Product::find($product_id);
        $product->price = $price;
        if($product->tax>0)
            $product->sale_price = $price * ($product->tax/100+1);
        $product->save();
        return true;
    }

}
