<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Gate;

use App\Models\Client;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\Dispatchorder;

use App\Observers\ClientObserver;
use App\Observers\OrderObserver;
use App\Observers\OrderDetailObserver;
use App\Observers\DispatchObserver;

use App\Models\Dispatch_detail;

class AppServiceProvider extends \Illuminate\Foundation\Support\Providers\AuthServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        $this->registerPolicies();

        // Implicitly grant "Super Admin" role all permissions
        // This works in the app by using gate-related functions like auth()->user->can() and @can()
        Gate::before(function ($user, $ability) {
            return $user->hasRole('Super Admin') ? true : null;
        });
        Client::observe(ClientObserver::class);
        Order::observe(OrderObserver::class);
        OrderDetail::observe(OrderDetailObserver::class);
        Dispatchorder::observe(DispatchObserver::class);

    }
}
