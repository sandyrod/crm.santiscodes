<?php

namespace App\Exports;

use App\Models\PlannedRouteDetail;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use SebastianBergmann\Environment\Console;
use Illuminate\Http\Request;



class PlanedRouteDetailExport implements FromCollection,FromQuery,WithHeadings
{

    /**
    * @return \Illuminate\Support\Collection
    */
    
    public function collection()
    {
        
        return PlannedRouteDetail::select('b.name', 'c.businessname', 'c.rif','a.order', 'a.visit_at')
        ->from('planed_route_details AS a')
        ->JOIN('planned_routes AS b',  'a.plannedroute_id', '=', 'b.id')
        ->JOIN('clients AS c', 'a.client_id', '=', 'c.id')
        ->get();
    }
    /**
    * @return \Illuminate\Support\Collection
    */  
    // use Exportable;

    public function headings(): array
    {
        return [
            'Nombre de plan de ruta',
            'Nombre de Cliente',
            'Rif',
            'Orden',
            'Visit_at'
        ];
    }
    public function query()
    {
        return PlannedRouteDetail::select('b.name','c.businessname','a.visit_at')
            ->from('planed_route_details AS a')
            ->JOIN('planned_routes AS b',  'a.plannedroute_id', '=', 'b.id')
            ->JOIN('clients AS c', 'a.client_id', '=', 'c.id')
            ->get(); 
    }
    public function map($PlannedRouteDetail): array
    {
        return [
            $PlannedRouteDetail->businessname,
            $PlannedRouteDetail->name,
            $PlannedRouteDetail->rif,
            $PlannedRouteDetail->order,
            $PlannedRouteDetail->visit_at->format('d-m-Y'),
            
        ];
    }
}