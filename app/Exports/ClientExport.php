<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use App\Models\Distribution_channel;
use App\Models\Client;

class ClientExport implements FromArray
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function array() :array
    {
        $distributionChannels = Distribution_channel::All('id', 'name', 'description');
        $title = 'Cuadro de Puntos por Canal';
        $arrayitems[]= array('name' =>'Nomenclatura', 'description' => 'Canal de Distribucion', 'count' => 'Cantidad de puntos');
        foreach ($distributionChannels as $distributionChannel) {
            $channelCount = Client::whereNotNull('census_at')
                ->where('distribution_channel_id','=',$distributionChannel->id)
                ->count();
            $arrayitems[]= array('name' =>$distributionChannel->name, 'description' => $distributionChannel->description, 'count' => $channelCount);
        }
        return $arrayitems;
    }
}
