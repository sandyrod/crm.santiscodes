<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use App\Models\Client;

class CensusReportExport implements FromView
{
    public function view(): View
    {
        $clients = Client::whereNotNull('census_at')->get();
        $title = 'FORMATO DE RESUMEN DE CENSO DE CLIENTES';
        return view('client.excel.census_report',
            ['clients' => $clients, 'title' => $title, 'i' => 1 ]);
    }
}
