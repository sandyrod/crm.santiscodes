<?php

namespace App\Exports;

use App\Models\Inscriptions;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;

class InscriptionsExport implements FromCollection,FromQuery,WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Inscriptions::select('identification', 'name', 'last_name', 'phone', 'email', 'rif_company', 'name_company', 'instagram_company', 'instagram_personal')
        ->from('inscriptions')
        ->where('id_events', 2)
        ->get();
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    // use Exportable;

    public function headings(): array
    {
        return [
            'Cedula',
            'Nombres',
            'Apellidos',
            'Telefono',
            'Correo',
            'Rif De la Empresa',
            'Nombre De la Empresa',
            'Instagram De la Empresa',
            'Instagram Personal',
        ];
    }
    public function query()
    {
        return Inscriptions::all();

    }
    public function map($Inscriptions): array
    {
        return [
            $Inscriptions->identification,
            $Inscriptions->name,
            $Inscriptions->last_name,
            $Inscriptions->phone,
            $Inscriptions->email,
            $Inscriptions->rif_company,
            $Inscriptions->name_company,
            $Inscriptions->instagram_company,
            $Inscriptions->instagram_personal,

        ];
    }
}
