<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use App\Models\Client;

class CensusMasterUserExport implements FromArray
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function array():array
    {
        $clients = Client::whereNotNull('census_at')->get();
        $title = 'RECOPILACION PARA MAESTRO DE USUARIOS';
        $ArrayClients[]=Array('Nro'=>'Nro',
          'Nombre_Cliente' => 'Nombre de Cliente',
          'Canal' => 'Canal',
          'Tipo' => 'Tipo',
          'Municipio' => 'Municipio',
          'Localidad' => 'Localidad',
          'CalleAvenida' => 'Calle/Avenida',
          'EntreCalle' => 'Entre calle y calle');
        $i=1;
        foreach($clients as $item){
              $ArrayClients[]=Array($i,
              @$item->businessname,
              @$item->distribution_channels->name,
              @$item->business_type->name,
              $item->address()->first()->municipality,
              $item->address()->first()->urbanization,
              $item->address()->first()->street,
              $item->address()->first()->street);
        }
        return $ArrayClients;
    }
}
