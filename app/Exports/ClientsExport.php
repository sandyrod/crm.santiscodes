<?php

namespace App\Exports;

use App\Models\client;
use Maatwebsite\Excel\Concerns\FromCollection;

class ClientsExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return client::Raw("clients.id", "trim(clients.rif, '-')","clients.businessname","clients.businesssmail","clients.ownersname","clients.ownersphone", "addresses.urbanization", "addresses.sector", "addresses.street", "addresses.latitude", "addresses.longitude", "addresses.description", "addresses.municipality", "addresses.parish", "addresses.streets_between", "addresses.city", "addresses.state")
            ->leftjoin("addresses", "addresses.client_id", "=" ,"clients.id" )
            ->get();

    }
}
