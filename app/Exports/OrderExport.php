<?php

namespace App\Exports;

use App\Models\Order;
use Maatwebsite\Excel\Concerns\FromCollection;

class OrderExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Order::Select('clients.rif','clients.businessname','products.code','products.name','order_details.quantity','order_details.price','orders.payment','orders.paidCondition')
        ->LEFTJOIN('clients','orders.client_id', '=', 'clients.id')
        ->LEFTJOIN('order_details', 'order_details.order_id', '=', 'orders.id')
        ->LEFTJOIN('products', 'order_details.product_id', '=', 'products.id')
        ->get();
    }
}
