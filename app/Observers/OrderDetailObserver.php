<?php

namespace App\Observers;

use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\Receivable;

use App\Repositories\OrdersRepository;

use Carbon\Carbon;

class OrderDetailObserver
{
    /**
     * Handle the order detail "created" event.
     *
     * @param  \App\OrderDetail  $orderDetail
     * @return void
     */
    public function created(OrderDetail $orderDetail)
    {
        $amount = OrdersRepository::getTotalAmount($orderDetail->order_id);
        $receivable = Receivable::where('order_id', $orderDetail->order_id)->first();
        $order = Order::find($orderDetail->order_id);
        if ($receivable instanceof Receivable){
            $receivable->amount = $amount;
            $receivable->save();
        }else{
            if($order->paidCondition != "PREPAGADO"){
                Receivable::create([
                'client_id'     => $order->client_id,
                'order_id'     => $order->id,
                'user_id'      => $order->user_id,
                'amount'        => OrdersRepository::getTotalAmount($order->id),
                'duedate_at'    => Carbon::now()->addDay(8* $order->client->visitfrecuency ?? 1),
                'status'        => 1,
            ]);
            }
        }
    }

    /**
     * Handle the order detail "updated" event.
     *
     * @param  \App\OrderDetail  $orderDetail
     * @return void
     */
    public function updated(OrderDetail $orderDetail)
    {
        $amount = OrdersRepository::getTotalAmount($orderDetail->order_id);
        $receivable = Receivable::where('order_id', $orderDetail->order_id)->first();
        $order = Order::find($orderDetail->order_id);
        if ($receivable instanceof Receivable){
            $receivable->amount = $amount;
            $receivable->save();
        }else{
            if($order->paidCondition != "PREPAGADO"){
                Receivable::create([
                    'client_id'     => $order->client_id,
                    'order_id'     => $order->id,
                    'user_id'      => $order->user_id,
                    'amount'        => OrdersRepository::getTotalAmount($order->id),
                    'duedate_at'    => Carbon::now()->addDay(8* $order->client->visitfrecuency ?? 1),
                    'status'        => 1,
                ]);
            }
        }
    }

    /**
     * Handle the order detail "deleted" event.
     *
     * @param  \App\OrderDetail  $orderDetail
     * @return void
     */
    public function deleted(OrderDetail $orderDetail)
    {
        $amount = OrdersRepository::getTotalAmount($orderDetail->order_id);
        $receivable = Receivable::where('order_id', $orderDetail->order_id)->first();
        $order = Order::find($orderDetail->order_id);
        if ($receivable instanceof Receivable){
            $receivable->amount = $amount;
            $receivable->save();
        }else{
            if($order->paidCondition != "PREPAGADO"){
                Receivable::create([
                    'client_id'     => $order->client_id,
                    'order_id'     => $order->id,
                    'user_id'      => $order->user_id,
                    'amount'        => OrdersRepository::getTotalAmount($order->id),
                    'duedate_at'    => Carbon::now()->addDay(8* $order->client->visitfrecuency ?? 1),
                    'status'        => 1,
                ]);
            }
        }
    }

    /**
     * Handle the order detail "restored" event.
     *
     * @param  \App\OrderDetail  $orderDetail
     * @return void
     */
    public function restored(OrderDetail $orderDetail)
    {
        //
    }

    /**
     * Handle the order detail "force deleted" event.
     *
     * @param  \App\OrderDetail  $orderDetail
     * @return void
     */
    public function forceDeleted(OrderDetail $orderDetail)
    {
        //
    }
}
