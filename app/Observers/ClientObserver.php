<?php

namespace App\Observers;
use App\Models\Client;
use Illuminate\Support\Facades\Log;
use Telegram;
class ClientObserver
{
    /**
     * Handle the client "created" event.
     *
     * @param  \App\Client  $client
     * @return void
     */
    public function created(Client $client)
    {
        Telegram::sendMessage(['chat_id' => '-526619011','text' => 'Nuevo cliente! '.$client->businessname]);
    }

    /**
     * Handle the client "updated" event.
     *
     * @param  \App\Client  $client
     * @return void
     */
    public function updated(Client $client)
    {

    }

    /**
     * Handle the client "deleted" event.
     *
     * @param  \App\Client  $client
     * @return void
     */
    public function deleted(Client $client)
    {
        Telegram::sendMessage(['chat_id' => '-526619011','text' => 'Cliente eliminado!'.$client->businessname]);
    }

    /**
     * Handle the client "restored" event.
     *
     * @param  \App\Client  $client
     * @return void
     */
    public function restored(Client $client)
    {

    }

    /**
     * Handle the client "force deleted" event.
     *
     * @param  \App\Client  $client
     * @return void
     */
    public function forceDeleted(Client $client)
    {
        
    }
}
