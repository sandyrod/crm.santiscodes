<?php

namespace App\Observers;

use App\Models\Dispatch_detail;
use App\Models\Dispatchorder;
use App\Models\StockLog;
use app\Models\Stock;

class DispatchObserver
{
    /**
     * Handle the odel= dispatch_detail "created" event.
     *
     * @param  \App\odel=Dispatch_detail  $odel=DispatchDetail
     * @return void
     */
    public function created(Dispatch_detail $dispatch_detail)
    {


    }

    /**
     * Handle the odel= dispatch_detail "updated" event.
     *
     * @param  \App\odel=Dispatch_detail  $odel=DispatchDetail
     * @return void
     */
    public function updated(Dispatch_detail $dispatch_detail)
    {

    }

    /**
     * Handle the odel= dispatch_detail "deleted" event.
     *
     * @param  \App\odel=Dispatch_detail  $odel=DispatchDetail
     * @return void
     */
    public function deleted(Dispatch_detail $dispatch_detail)
    {
        //
    }

    /**
     * Handle the odel= dispatch_detail "restored" event.
     *
     * @param  \App\odel=Dispatch_detail  $odel=DispatchDetail
     * @return void
     */
    public function restored(Dispatch_detail $dispatch_detail)
    {
        //
    }

    /**
     * Handle the odel= dispatch_detail "force deleted" event.
     *
     * @param  \App\odel=Dispatch_detail  $odel=DispatchDetail
     * @return void
     */
    public function forceDeleted(Dispatch_detail $dispatch_detail)
    {
        //
    }
}
