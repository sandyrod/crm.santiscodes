<?php

namespace App\Observers;

use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\Receivable;
use App\Repositories\OrdersRepository;
use Illuminate\Support\Facades\Log;

use Carbon\Carbon;
use Telegram;

class OrderObserver
{
    /**
     * Handle the order "created" event.
     *
     * @param  \App\Order  $order
     * @return void
     */
    public function created(Order $order)
    {
        $receivable = false;
        $message = '';
        switch (trim($order->paidCondition)) {
            case 'PREPAGADO':
                $message = '';
                break;
            case 'CONTADO':
                $receivable = true;
                $message = '';
                break;
            case 'FACILIDAD':
                $receivable = true;
                $message = 'Se generará una Cuenta por Cobrar';
                break;
            case 'OTRO':
                $receivable = true;
                $message = 'Se generará una Cuenta por Cobrar';
                break;
            default:
                # code...
                break;
        }
        if($receivable){
            Receivable::create([
                'client_id'     => $order->client_id,
                'order_id'     => $order->id,
                'user_id'      => $order->user_id,
                'amount'        => OrdersRepository::getTotalAmount($order->id),
                'duedate_at'    => Carbon::now()->addDay(8* $order->client->visitfrecuency ?? 1),
                'status'        => 1,
            ]);
            try{
                Telegram::sendMessage([
                'chat_id'   => '-526619011',
                'text'      => 'Nuevo pedido: '. $order->id . ' | ' .$message]);
            }catch (Exception $e) {
                Log::error('Error de Telegram: '. $e);
            }
        }
    }

    /**
     * Handle the order "updated" event.
     *
     * @param  \App\Order  $order
     * @return void
     */
    public function updated(Order $order)
    {
        $amount = OrdersRepository::getTotalAmount($order->id);
        $receivable = Receivable::where('order_id', $order->id)->first();
        if ($receivable instanceof Receivable){
            $receivable->amount = $amount;
            $receivable->save();
        }else{
            if($order->paidCondition != "PREPAGADO"){
                Receivable::create([
                    'client_id'     => $order->client_id,
                    'order_id'      => $order->order_id,
                    'user_id'       => $order->user_id,
                    'amount'        => OrdersRepository::getTotalAmount($order->id),
                    'duedate_at'    => Carbon::now()->addDay(8* $order->client->visitfrecuency ?? 1),
                    'status'        => 1,
                ]);
            }
        }
    }

    /**
     * Handle the order "deleted" event.
     *
     * @param  \App\Order  $order
     * @return void
     */
    public function deleted(Order $order)
    {
        $receivable = Receivable::where('order_id', $order->id)->first();
        if ($receivable instanceof Receivable){
            $receivable->delete();
        }
    }

    /**
     * Handle the order "restored" event.
     *
     * @param  \App\Order  $order
     * @return void
     */
    public function restored(Order $order)
    {
        //
    }

    /**
     * Handle the order "force deleted" event.
     *
     * @param  \App\Order  $order
     * @return void
     */
    public function forceDeleted(Order $order)
    {
        //
    }
}
