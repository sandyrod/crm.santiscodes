<?php

namespace App\Models;

use App\Models\Stock;
use Auth;

use Illuminate\Database\Eloquent\Model;

class StockLog extends Model
{
    protected $table = 'stock_logs';
    protected $fillable = ['product_id', 'warehouse_id', 'unit_id', 'quantity', 'operation', 'description'];

    protected static function boot()
    {
        parent::boot();
        static::creating(function ($stock_log) {
            $stock_log->user_id = Auth::user()->id;
        });

        static::created(function ($stock_log) {
            $stock = Stock::where('product_id', $stock_log->product_id)->where('warehouse_id', $stock_log->warehouse_id)->first();
            if (! $stock)
                return Stock::create([
                    'product_id' => $stock_log->product_id,
                    'warehouse_id' => $stock_log->warehouse_id,
                    'quantity' => $stock_log->quantity
                ]);

            $stock->product_id = $stock_log->product_id;
            $stock->warehouse_id = $stock_log->warehouse_id;
            $sum = $stock_log->where('product_id', $stock_log->product_id)->where('warehouse_id', $stock_log->warehouse_id)->where('operation', 1)->get()->sum('quantity') - $stock_log->where('product_id', $stock_log->product_id)->where('warehouse_id', $stock_log->warehouse_id)->where('operation', 0)->get()->sum('quantity');

            $stock->quantity = $sum>0 ? $sum : 0;
            $stock->save();
        });
    }

    public function product(){
        return $this->belongsTo(Product::class, 'product_id');
    }

    public function warehouse(){
        return $this->belongsTo(Warehouse::class, 'warehouse_id');
    }

    public function unit(){
        return $this->belongsTo(Unit::class, 'unit_id');
    }
}
