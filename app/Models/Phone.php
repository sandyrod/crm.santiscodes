<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Phone extends Model
{
    public function user(){
        return $this->belongsTo('App\User');
    }

    public function client(){
        return $this->belongsTo('App\Models\Client', 'client_id');
    }

    public static function save_phones($numphone, $client_id, $wsp, $principal){
        $phone = new Phone();
        $phone->number = str_replace('-', '', $numphone);
        $phone->client_id = $client_id;
        $phone->wsp = $wsp;
        $phone->principal = $principal;
        $phone->save();
    }
}
