<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Bank extends Model
{
    protected $fillable = ['name', 'isNational', 'country', 'description'];

    public function bankaccounts(){
        return $this->hasMany(Bankaccount::class, 'bank_id');
    }
}
