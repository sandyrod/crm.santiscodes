<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PlannedRouteDetail extends Model
{
    protected $table = 'planed_route_details';
    protected $fillable = ['plannedroute_id' , 'client_id', 'order', 'visit_at'];

    public function plannedroute()
    {
        return $this->belongsTo(PlannedRoute::class);
    }

    public function client()
    {
        return $this->hasOne('App\Models\Client', 'id', 'client_id');
    }

}
