<?php

namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
    protected $fillable = ['order_id', 'product_id', 'unit_id', 'price', 'quantity'];

    public function order(){
        return $this->belongsTo(Order::class, 'order_id');
    }

    public function unit(){
        return $this->belongsTo(Unit::class, 'unit_id');
    }

    public function product(){
        return $this->belongsTo('App\Models\Product', 'product_id');
    }


}
