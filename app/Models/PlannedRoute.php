<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PlannedRoute extends Model
{
    protected $table = 'planned_routes';
    protected $fillable = ['user_id', 'status', 'clientsperday', 'client_id',
        'name', 'distributionroute_id', 'monday', 'tuesday', 'wednesday',
        'thursday', 'friday', 'saturday', 'sunday', 'begin_at'];

    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    public function distributionroute()
    {
        return $this->belongsTo(Distributionroute::class);
    }

    public function plannedroutedetail()
    {
        return $this->hasMany(PlannedRouteDetail::class);
    }

    public function user()
    {
        return $this->belongsToMany('App\User', 'route_users');
    }
}
