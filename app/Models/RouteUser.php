<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RouteUser extends Model
{
    protected $fillable = ['planned_route_id', 'user_id'];

}
