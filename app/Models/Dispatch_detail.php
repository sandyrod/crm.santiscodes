<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Dispatch_detail extends Model
{
    protected $table = "dispatch_details";
    protected $fillable = ['product_id', 'dispatch_id', 'requested', 'quantity'];

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }
}
