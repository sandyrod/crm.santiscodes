<?php

namespace App\Models;

use App\Models\Product;

use Illuminate\Database\Eloquent\Model;

class ClientsProducts extends Model
{
    protected $table = 'clients_products';
    protected $fillable = ['product_id', 'client_id'];
}
