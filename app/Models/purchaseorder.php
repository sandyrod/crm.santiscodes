<?php

namespace App\Models;
use App\User;

use Illuminate\Database\Eloquent\Model;

class Purchaseorder extends Model
{
    protected $table = "purchases";
    protected $fillable = ['number', 'user_id', 'warehouse_id', 'provider_id'];

    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }

    public function provider(){
        return $this->belongsTo(Provider::class, 'provider_id');
    }

    public function warehouse(){
        return $this->belongsTo(Warehouse::class, 'warehouse_id');
    }
}
