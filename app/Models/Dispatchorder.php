<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Dispatchorder extends Model
{
    protected $table = "dispatchs";
    protected $fillable = ['user_id', 'client_id', 'order_id', 'warehouse_id', 'status'];

    public function warehouse(){
        return $this->belongsTo(Warehouse::class, 'warehouse_id');
    }

    public function client(){
        return $this->belongsTo(Client::class, 'client_id');
    }
}
