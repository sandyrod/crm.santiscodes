<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VisitDetail extends Model
{
	protected $fillable = ['visit_id', 'inventory', 'product_id', 'distribution', 'suggested', 'sold'];

    public function visit(){
        return $this->belongsTo('App\Models\Visit', 'id');
    }
}
