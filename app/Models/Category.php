<?php

namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'categories';
    protected $fillable = ['name', 'description', 'parent_id'];

    public function parent_category()
    {
        return $this->belongsTo(Category::class, 'parent_id');
    }

    public function products()
    {
        return $this->belongsToMany(Product::class, 'products_categories');
    }

    public function getCategoriesAssigned($client_id){
        $categories = $this->get(['id', 'name']);
        foreach ($categories as $category)
            $category->assigned = DB::table('clients_categories')->where('category_id', $category->id)->where('client_id', $client_id)->exists();
        
        return $categories;
    }

}
