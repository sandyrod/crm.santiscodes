<?php

namespace App\Models;

use App\Models\Product;
use App\Models\Provider;

use Illuminate\Database\Eloquent\Model;

class ProductProvider extends Model
{
    protected $table = 'products_providers';
    protected $fillable = ['product_id', 'provider_id'];
}
