<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DebtoPay extends Model
{
    protected $table = 'debtopays';
    protected $fillable = ['provider_id', 'user_id', 'amount', 'duedate_at',
        'status', 'description', 'close_at'];

    public function provider(){
        return $this->belongsTo(Provider::class);
    }

    public function purchaseorder(){
        return $this->belongsTo(Purchaseorder::class, 'purchase_id');
    }
}
