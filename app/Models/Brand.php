<?php

namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    protected $table = 'brands';
    protected $fillable = ['name', 'description'];


    public function getBrandsAssigned($client_id){
        $brands = $this->get(['id', 'name']);
        foreach ($brands as $brand)
            $brand->assigned = DB::table('clients_brands')->where('brand_id', $brand->id)->where('client_id', $client_id)->exists();

        return $brands;
    }

}
