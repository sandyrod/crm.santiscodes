<?php

namespace App\Models;

use Auth;
use Illuminate\Database\Eloquent\Model;

class Distributionroute extends Model
{
    protected $fillable = ['name', 'description'];

    protected static function boot()
    {
        parent::boot();
        static::creating(function ($route) {
        	if (! $route->user_id)
            	$route->user_id = Auth::user()->id;        	
        });
    }

    public function users(){
        return $this->hasMany(DistributionrouteUser::class, 'distributionroutes_id');
    }
}
