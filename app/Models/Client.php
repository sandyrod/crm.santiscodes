<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Client extends Model
{

    use SoftDeletes;

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function address(){
        return $this->hasMany(Address::class, 'client_id');
    }

    public function phones(){
        return $this->hasMany(Phone::class, 'client_id');
    }

    public function distribution_channels(){
        return $this->belongsTo(Distribution_channel::class, 'distribution_channel_id');
    }

    public function distribution_routes(){
        return $this->belongsTo(Distributionroute::class, 'distributionroutes_id');
    }

    public function business_type(){
        return $this->belongsTo(Business_type::class, 'business_type_id');
    }

    public function products(){
        return $this->hasMany('App\Models\Product', 'clients_products');
    }

    public function sales_type(){
        return $this->belongsTo('App\Models\Sales_type', 'sales_type_id');
    }

    public function client_brands(){
        return $this->hasMany(ClientBrand::class, 'client_id');
    }

    public function census(){
        return $this->belongsTo('App\User', 'census_by');
    }

    public function receivable(){
        return $this->hasMany(Receivable::class, 'client_id');
    }

    public function plannedroutes(){
        return $this->hasMany(PlannedRoute::class, 'client_id');
    }

    public function plannedroutesdetail(){
        return $this->belongsTo(PlannedRouteDetail::class, 'client_id');
    }
}
