<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RoleHasPermission extends Model
{
    protected $table = 'role_has_permissions';
    protected $fillable = ['permission_id', 'role_id'];
    protected $primaryKey = null;
    public $incrementing = false;
    public $timestamps = false;
}
