<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ClientBrand extends Model
{
    protected $table = 'clients_brands';
    protected $fillable = ['brand_id', 'client_id'];

    public function client(){
        return $this->belongsTo('App\Models\Client', 'client_id');
    }

}
