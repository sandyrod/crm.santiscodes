<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DistributionrouteUser extends Model
{
	protected $table = 'distributionroutes_users';
    protected $fillable = ['user_id', 'distributionroutes_id'];
}
