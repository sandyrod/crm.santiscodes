<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Receivable extends Model
{
    protected $fillable = ['client_id', 'order_id', 'description', 'amount',
        'status', 'duedate_at', 'close_at', 'user_id'];

    public function client(){
        return $this->belongsTo('App\Models\Client', 'client_id');
    }
}
