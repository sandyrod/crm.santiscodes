<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Visit extends Model
{
    protected $fillable = ['client_id', 'user_id'];

    public function visitdetails()
    {
        return $this->hasMany(VisitDetail::class, 'visit_id');
    }
}
