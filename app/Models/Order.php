<?php

namespace App\Models;

use DB;
use Auth;
use App\User;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = ['client_id', 'distributionroute_id', 'user_id', 'payment', 'other', 'paidCondition', 'document'];

    protected static function boot()
    {
        parent::boot();
        static::creating(function ($order) {
            $order->user_id = Auth::user()->id;
        });
    }

    public function distributionroute(){
        return $this->belongsTo(Distributionroute::class, 'distributionroute_id');
    }

    public function client(){
        return $this->belongsTo(Client::class, 'client_id');
    }

    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }

}
