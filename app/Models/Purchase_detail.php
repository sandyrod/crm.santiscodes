<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Purchase_detail extends Model
{
    protected $fillable = ['quantity', 'purchase_id', 'product_id', 'price'];

    public function product(){
        return $this->belongsTo('App\Models\Product', 'product_id');
    }
}
