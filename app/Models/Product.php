<?php

namespace App\Models;

use App\Models\Unit;
use App\Models\Provider;
use App\Models\Category;
use App\Models\ProductPhoto;
use DB;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ['name', 'description', 'price', 'ue', 'code', 'tax', 'sale_price'];

    public function categories()
    {
        return $this->belongsToMany(Category::class, 'products_categories');
    }

    public function units()
    {
        return $this->belongsToMany(Unit::class, 'products_units');
    }

    public function photos()
    {
        return $this->hasMany(ProductPhoto::class, 'product_id');
    }

    public function providers()
    {
        return $this->belongsToMany(Provider::class, 'products_providers');
    }

    public function getProductAssigned($client_id){
        $products = $this->get(['id', 'name']);
        foreach ($products as $product)
            $product->assigned = DB::table('clients_products')->where('product_id', $product->id)->where('client_id', $client_id)->exists();
        return $products;
    }

}
