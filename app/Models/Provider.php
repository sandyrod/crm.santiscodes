<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Provider extends Model
{
    protected $fillable = ['name', 'address', 'phone', 'email', 'rif'];

    public function debtopay(){
        return $this->hasMany('App\Models\Debtopay');
    }
}
