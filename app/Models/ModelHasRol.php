<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ModelHasRol extends Model
{
    protected $table = 'model_has_roles';
    protected $fillable = ['model_id', 'model_type', 'role_id'];
    protected $primaryKey = null;
    public $incrementing = false;
    public $timestamps = false;

    public function rols(){
        return $this->hasMany('Spatie\Permission\Models\Role', 'id', 'role_id');
    }
}
