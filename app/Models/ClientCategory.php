<?php

namespace App\Models;

use App\Models\Product;

use Illuminate\Database\Eloquent\Model;

class ClientCategory extends Model
{
    protected $table = 'clients_categories';
    protected $fillable = ['category_id', 'client_id'];
}
