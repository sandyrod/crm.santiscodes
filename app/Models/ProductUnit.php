<?php

namespace App\Models;

use App\Models\Product;
use App\Models\Unit;

use Illuminate\Database\Eloquent\Model;

class ProductUnit extends Model
{
    protected $table = 'products_units';
    
    protected $fillable = ['product_id', 'unit_id'];
}
