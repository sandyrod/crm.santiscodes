<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BankTransaction extends Model
{
    protected $fillable = ['amount', 'transactioncode', 'transaction_type_id',
        'transaction_method_id', 'status', 'description', 'bankaccount_id',
        'user_id'];

    public function bankaccount(){
        return $this->belongsTo('App\Models\Bankaccount', 'bankaccount_id');
    }
}
