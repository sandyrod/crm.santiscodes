<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Bankaccount extends Model
{
    protected $table = 'bankaccounts';
    protected $fillable = ['bank_id', 'number', 'owner', 'account_type', 'route', 'iban', 'swift'];

    public function bank(){
        return $this->belongsTo('App\Models\Bank', 'bank_id');
    }
}
