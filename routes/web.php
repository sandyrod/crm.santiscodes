<?php

use Illuminate\Support\Facades\Route;
use App\Exports\ClientExport;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::prefix('chronusoil_yaracuy')->group(function () {
    Route::get('inscripciones', 'InscriptionsController@index')->name('inscriptions');
    Route::post('inscription', 'InscriptionsController@store')->name('inscription');
    Route::get('evento/{slug}', 'EventsController@show')->name('events_incriptions');
});

    Route::post('inscription_events', 'EventsController@stores')->name('inscription_events');
    Route::get('client_rp', 'ReportController@report_excell_client')->name('client_rp');
    Route::get('product_rp', 'ReportController@report_excell_product')->name('product_rp');
    Route::get('orders_rp', 'ReportController@report_excell_orders')->name('orders_rp');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
##
#Rutas que requieren inicio de sesion
#
Route::middleware(['auth'])->group(function () {
    Route::get('password_reset', function(){
        return view('auth.passwords.reset');
    });
    Route::get('profileedit', 'ProfilesController@profileEdit')->name('profile_edit');
    Route::post('profilesave', 'ProfilesController@store')->name('profile_save');
    Route::get('mypasswordreset', 'ProfilesController@password')->name('profile_password_reset');
    Route::post('resetpass','ProfilesController@resetpass')->name('resetpass');

    Route::get('clients/{id?}', 'ClientController@index')->name('clients');
    Route::get('search-client/{rif?}', 'ClientController@search')->name('client_search');
    Route::get('search-next-code/{code}', 'ClientController@searchNextCode')->name('client_search_code');
    Route::post('client_save', 'ClientController@store')->name('client_save');
    Route::get('client_delete/{id}', 'ClientController@delete')->name('client_delete');
    Route::get('client_list', 'ClientController@lists')->name('client_list');
    Route::get('client_bank', 'ClientController@bank')->name('client_bank');
    Route::get('client_master/{id?}', 'ClientController@pdfMaster')->name('client_master');
    Route::get('client_change_type/{id}/{type}', 'ClientController@changeType')->name('change_type');

    Route::get('census/{id?}', 'ClientController@census')->name('census');
    Route::post('census_save', 'ClientController@storeCensus')->name('census_save');
    Route::get('census_report', 'ClientController@censusReport')->name('census_report');
    Route::get('census_excel', 'ClientController@censusExcel')->name('census_excel');
    Route::get('census_master_user_report', 'ClientController@censusMasterUsersReport')
        ->name('census_master_user_report');
    Route::get('census_master_user_excel', 'ClientController@censusMasterUsersExcel')
        ->name('census_master_user_excel');

    Route::get('census_channel_points_report', 'ClientController@censusChannelPointsReport')
        ->name('census_channel_points_report');
    Route::get('census_channel_points_excel', 'ClientController@censusChannelPointsExcel')
        ->name('census_channel_points_excel');
    Route::get('ventas', 'ProductsController@ventas')
        ->name('venta_diario');

    Route::get('phones/{id?}', 'PhonesController@index')->name('phones');
    Route::post('phone_save', 'PhonesController@store')->name('phone_save');
    Route::get('phone_delete/{id}', 'PhonesController@delete')->name('phone_delete');
    Route::get('search-phone/{id?}', 'PhonesController@search')->name('phone_search');

    Route::get('address/{id?}', 'AddressController@index')->name('address');
    Route::post('address_save', 'AddressController@store')->name('address_save');
    Route::get('address_delete/{id}', 'AddressController@delete')->name('address_delete');
    Route::get('search-address/{id?}', 'AddressController@search')->name('address_search');

    Route::get('business_types/{id?}','BusinessTypeController@index')->name('business_types');
    Route::post('business_type_save','BusinessTypeController@store')->name('business_type_save');
    Route::get('business_typedelete/{id}','BusinessTypeController@delete')->name('business_type_delete');

    Route::get('sales_types/{id?}','SalesTypeController@index')->name('sales_types');
    Route::post('sales_type_save','SalesTypeController@store')->name('sales_type_save');
    Route::get('sales_typedelete/{id}','SalesTypeController@delete')->name('sales_type_delete');

    Route::get('frequence/{id?}','FrequenceController@index')->name('frequence');
    Route::post('frequence_save','FrequenceController@store')->name('frequence_save');
    Route::get('frequence_delete/{id}','FrequenceController@delete')->name('frequence_delete');

    Route::get('sequence/{id?}','SequenceController@index')->name('sequence');
    Route::post('sequence_save','SequenceController@store')->name('sequence_save');
    Route::get('sequence_delete/{id}','SequenceController@delete')->name('sequence_delete');

    Route::get('distribution_channel/{id?}','DistributionChannelController@index')
        ->name('distribution_channel');
    Route::post('distribution_channel_save','DistributionChannelController@store')
        ->name('distribution_channel_save');
    Route::get('distribution_channel_delete/{id}','DistributionChannelController@delete')
        ->name('distribution_channel_delete');

    //Route::get('export_route_planning', 'PlanedRouteDetailController@show');
    Route::get('export', 'PlanedRouteDetailController@export')
    ->name('export');
    
    Route::prefix('eventos')->group(function () {
        Route::get('participantes', 'InscriptionsController@participantes')->name('participantes');
        Route::get('participantes_rp', 'InscriptionsController@report_excell')->name('reporteexcel');
        Route::get('participantes_rpdf', 'InscriptionsController@reporte')->name('reportepdf');
        Route::get('participantes_delete/{id}', 'InscriptionsController@delete')->name('participantes_delete');
        Route::get('participantes_update/{id}', 'InscriptionsController@edit')->name('participantes_update');
        Route::post('participantes_update/{id}','InscriptionsController@update')
        ->name('participantes_guardado');
    });

    Route::get('events', 'EventsController@index')->name('events');
    Route::get('events_create', 'EventsController@create')->name('events_create');
    Route::post('events_save', 'EventsController@store')->name('events_save');

    Route::get('events_rp', 'EventsController@report_excell')->name('reporte');
    Route::get('events_rpdf', 'EventsController@reporte')->name('reportepdf');
    Route::get('events_delete/{id}', 'EventsController@delete')->name('events_delete');
    Route::get('events_update/{id}', 'EventsController@edit')->name('events_update');
    Route::post('events_update/{id}','EventsController@update')->name('event_guardado');

    Route::get('events/{slug}', 'EventsController@show')->name('event_incriptions');
    // Route::post('inscription_events', 'EventsController@stores')->name('inscription_events');

    Route::get('events_list/{slug}', 'EventsController@event_list')->name('events_list');
    Route::get('report_pdf/{id}', 'EventsController@report_pdf')->name('report_pdf');

    Route::get('indicators', 'IndicatorsController@index')->name('indicador');

    Route::post('indicators_client', 'IndicatorsController@client')->name('indicators_client');

//Livewire Routes
    Route::view('distributionroutes', 'distributionroutes.index');
    Route::view('permanentroutes', 'permanentroutes.index');
    Route::view('units', 'units.index');
    Route::view('categories', 'categories.index');
    Route::view('warehouses', 'warehouses.index');
    Route::view('products', 'products.index');
    Route::view('stocks', 'stocks.index');
    Route::view('providers', 'providers.index');
    Route::get('print-products', 'ProductsController@print');
    Route::get('print-stocks', 'StocksController@print');
    Route::view('brands', 'brands.index');
    Route::view('visitcard/{id?}','visitcard.index')->name('visitcard');
    Route::view('orders/{id?}', 'orders.index')->name('orders');
    Route::view('rols', 'roles.index');
    Route::view('permissions', 'permisos.index');
    Route::view('users', 'users.index');
    Route::view('purchases', 'purchases.index');
    Route::view('dispatch', 'dispatch.index');
    Route::view('banks', 'banks.index');
    Route::view('banks-accounts', 'banks.accounts');
    Route::view('banktransactions', 'banks.transactions');
    Route::view('receivable', 'receivable.index');
    Route::view('debtopay', 'debtopay.index');
    Route::view('route-planner', 'routeplanner.index');

});
