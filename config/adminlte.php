<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Title
    |--------------------------------------------------------------------------
    |
    | Here you can change the default title of your admin panel.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#61-title
    |
    */

    'title' => 'SantisCodes',
    'title_prefix' => '',
    'title_postfix' => '',

    /*
    |--------------------------------------------------------------------------
    | Favicon
    |--------------------------------------------------------------------------
    |
    | Here you can activate the favicon.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#62-favicon
    |
    */

    'use_ico_only' => false,
    'use_full_favicon' => false,

    /*
    |--------------------------------------------------------------------------
    | Logo
    |--------------------------------------------------------------------------
    |
    | Here you can change the logo of your admin panel.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#63-logo
    |
    */

    'logo' => '<b>SantiCRM</b>',
    'logo_img' => 'img/Logo_Ajyca.png',
    'logo_img_class' => 'brand-image img-circle elevation-3',
    'logo_img_xl' => null,
    'logo_img_xl_class' => 'brand-image-xs',
    'logo_img_alt' => 'SantiCRM',

    /*
    |--------------------------------------------------------------------------
    | User Menu
    |--------------------------------------------------------------------------
    |
    | Here you can activate and change the user menu.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#64-user-menu
    |
    */

    'usermenu_enabled' => true,
    'usermenu_header' => false,
    'usermenu_header_class' => 'bg-primary',
    'usermenu_image' => false,
    'usermenu_desc' => false,
    'usermenu_profile_url' => false,

    /*
    |--------------------------------------------------------------------------
    | Layout
    |--------------------------------------------------------------------------
    |
    | Here we change the layout of your admin panel.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#65-layout
    |
    */

    'layout_topnav' => null,
    'layout_boxed' => null,
    'layout_fixed_sidebar' => null,
    'layout_fixed_navbar' => null,
    'layout_fixed_footer' => null,

    /*
    |--------------------------------------------------------------------------
    | Authentication Views Classes
    |--------------------------------------------------------------------------
    |
    | Here you can change the look and behavior of the authentication views.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#661-authentication-views-classes
    |
    */

    'classes_auth_card' => 'card-outline card-primary',
    'classes_auth_header' => '',
    'classes_auth_body' => '',
    'classes_auth_footer' => '',
    'classes_auth_icon' => '',
    'classes_auth_btn' => 'btn-flat btn-primary',

    /*
    |--------------------------------------------------------------------------
    | Admin Panel Classes
    |--------------------------------------------------------------------------
    |
    | Here you can change the look and behavior of the admin panel.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#662-admin-panel-classes
    |
    */

    'classes_body' => '',
    'classes_brand' => '',
    'classes_brand_text' => '',
    'classes_content_wrapper' => '',
    'classes_content_header' => '',
    'classes_content' => '',
    'classes_sidebar' => 'sidebar-dark-primary elevation-4',
    'classes_sidebar_nav' => '',
    'classes_topnav' => 'navbar-white navbar-light',
    'classes_topnav_nav' => 'navbar-expand',
    'classes_topnav_container' => 'container',

    /*
    |--------------------------------------------------------------------------
    | Sidebar
    |--------------------------------------------------------------------------
    |
    | Here we can modify the sidebar of the admin panel.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#67-sidebar
    |
    */

    'sidebar_mini' => true,
    'sidebar_collapse' => false,
    'sidebar_collapse_auto_size' => false,
    'sidebar_collapse_remember' => false,
    'sidebar_collapse_remember_no_transition' => true,
    'sidebar_scrollbar_theme' => 'os-theme-light',
    'sidebar_scrollbar_auto_hide' => 'l',
    'sidebar_nav_accordion' => true,
    'sidebar_nav_animation_speed' => 300,

    /*
    |--------------------------------------------------------------------------
    | Control Sidebar (Right Sidebar)
    |--------------------------------------------------------------------------
    |
    | Here we can modify the right sidebar aka control sidebar of the admin panel.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#68-control-sidebar-right-sidebar
    |
    */

    'right_sidebar' => false,
    'right_sidebar_icon' => 'fas fa-cogs',
    'right_sidebar_theme' => 'dark',
    'right_sidebar_slide' => true,
    'right_sidebar_push' => true,
    'right_sidebar_scrollbar_theme' => 'os-theme-light',
    'right_sidebar_scrollbar_auto_hide' => 'l',

    /*
    |--------------------------------------------------------------------------
    | URLs
    |--------------------------------------------------------------------------
    |
    | Here we can modify the url settings of the admin panel.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#69-urls
    |
    */

    'use_route_url' => false,

    'dashboard_url' => 'home',

    'logout_url' => 'logout',

    'login_url' => 'login',

    'register_url' => 'register',

    'password_reset_url' => 'password/reset',

    'password_email_url' => 'password/email',

    'profile_url' => false,

    /*
    |--------------------------------------------------------------------------
    | Laravel Mix
    |--------------------------------------------------------------------------
    |
    | Here we can enable the Laravel Mix option for the admin panel.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#610-laravel-mix
    |
    */

    'enabled_laravel_mix' => false,
    'laravel_mix_css_path' => 'css/app.css',
    'laravel_mix_js_path' => 'js/app.js',

    /*
    |--------------------------------------------------------------------------
    | Menu Items
    |--------------------------------------------------------------------------
    |
    | Here we can modify the sidebar/top navigation of the admin panel.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#611-menu
    |
    */

    'menu' => [
        // [
        //     'text' => 'search',
        //     'search' => false,
        //     'topnav' => true,
        // ],
        // [
        //     'text' => 'blog',
        //     'url'  => 'admin/blog',
        //     'can'  => 'manage-blog',
        // ],
        // [
        //     'text'        => 'Ventas',
        //     'url'         => '#',
        //     'icon'        => 'far fa-fw fa-file',
        //     'label'       => 4,
        //     'label_color' => 'success',
        // ],
        ['header' => 'account_settings'],
        [
            'text' => 'profile',
            'url'  => 'profileedit',
            'icon' => 'fas fa-fw fa-user',
        ],
        [
            'text' => 'change_password',
            'url'  => 'mypasswordreset',
            'icon' => 'fas fa-fw fa-lock',
        ],
        ['header' => 'Modulos'],
        [
            'text'    => 'Canal',
            'icon'    => 'fas fa-fw fa-share',
            'submenu' => [
                [
                    'text' => 'Clientes',
                    'url'  => 'clients',
                ],
                [
                    'text' => 'Censo',
                    'url'  => 'census',
                ],
                [
                    'text'    => 'Configuracion',
                    'icon_color' => 'red',
                    'url'     => '#',
                    'submenu' => [
                        [
                            'text' => 'Tipo de negocio',
                            'url'  => 'business_types',
                        ],
                        [
                            'text' => 'Freq de Visita',
                            'url'  => 'frequence',
                        ],
                        [
                            'text' => 'Seq de Visita',
                            'url'  => 'sequence',
                        ],
                        [
                            'text' => 'Cond Venta',
                            'url'  => 'sales_types',
                        ],
                        [
                            'text' => 'Ch Distrib',
                            'url'  => 'distribution_channel',
                        ],
                    ],
                ],
                [
                    'text' => 'Reportes',
                    'url'  => '#',
                    'submenu' => [
                        [
                            'text' => 'Excel',
                            'icon_color' => 'cyan',
                            'url' => '#',
                            'submenu' => [
                                [
                                    'text' =>'Maestro de usuarios',
                                    'url' => 'census_master_user_excel',
                                ],
                                [
                                    'text' => 'Puntos por Canal',
                                    'url' => 'census_channel_points_excel'
                                ],
                                [
                                    'text' => 'Resumen Censo',
                                    'url' => 'census_excel',
                                ],
                            ],
                        ],
                        [
                            'text' => 'PDF',
                            'icon_color' => 'cyan',
                            'url' => '#',
                            'submenu' => [
                                [
                                    'text' => 'Listado de Clientes',
                                    'url'  => 'client_list',
                                ],
                                [
                                    'text' => 'Banco',
                                    'url'  => 'client_bank',
                                ],
                                [
                                    'text' => 'Resumen Censo',
                                    'url' => 'census_report',
                                ],
                                [
                                    'text' => 'Maestro de usuarios',
                                    'url' => 'census_master_user_report'
                                ],
                                [
                                    'text' => 'Puntos por Canal',
                                    'url' => 'census_channel_points_report'
                                ],
                                [
                                    'text' => 'Ventas',
                                    'url' => 'ventas'
                                ]
                            ],
                        ],
                    ],
                ],
            ],
        ],
        [
            'text'  => 'Eventos',
            'icon'  => 'fas fa-fw fa-user',
            'url'   => 'events',
        ],
        [
            'text'    => 'Gestion Operativa',
            'icon'    => 'fas fa-fw fa-share',
            'submenu' => [
                [
                    'text' => 'Hoja de Ruta Permanente',
                    'url'  => 'permanentroutes',
                ],
                [
                    'text' => 'Hoja Pedido',
                    'url'  => 'orders',
                ],
                [
                    'text' => 'Reportes',
                    'url'  => '#',
                ],
                [
                    'text' => 'Configuracion',
                    'icon' => 'fas fa-fw fa-share',
                    'submenu' => [
                        [
                            'text'  => 'Rutas',
                            'url'   => 'distributionroutes',
                        ],
                        [
                            'text'  => 'Planificar Rutas',
                            'url'   => 'route-planner',
                        ],
                        [
                            'text'  => 'Exportar Planificacacion de Rutas',
                            'url'   => 'export',

                        ]
                    ]
                ]
            ],
        ],
        [
            'text'    => 'Marketing',
            'icon'    => 'fas fa-fw fa-share',
            'submenu' => [
                [
                    'text' => 'Campanas Marketing',
                    'url'  => '#',
                ],
                [
                    'text' => 'Analisis de Mercado',
                    'url'  => '#',
                ],
                [
                    'text' => 'Ident de Oportunidades',
                    'url'  => '#',
                ],
                [
                    'text' => 'Segmentacion',
                    'url'  => '#',
                ],
                [
                    'text' => 'Monitoreo de Eventos',
                    'url'  => '#',
                ],
                [
                    'text' => 'Promociones Email',
                    'url'  => '#',
                ],
                [
                    'text' => 'Promocion Wsp',
                    'url'  => '#',
                ],
            ],
        ],
        [
            'text' => 'Finanzas',
            'icon' => 'fas fa-dollar-sign',
            'icon_color' => 'green',
            'submenu' => [
                [
                    'text'  => 'Bancos',
                    'url'   => 'banks',
                ],
                [
                    'text'  => 'Cuentas Bancarias',
                    'url'   => 'banks-accounts',
                ],
                [
                    'text'  =>  'CxC',
                    'url'   =>  'receivable',
                    'icon_color' => 'green',
                ],
                [
                    'text'  =>  'CxP',
                    'url'   =>  'debtopay',
                    'icon_color' => 'red',
                ],
                [
                    'text'  => 'Transacciones',
                    'url'   =>  'banktransactions',
                ]
            ],
        ],
        [
            'text'    => 'Productos',
            'icon'    => 'fas fa-fw fa-share',
            'submenu' => [
                [
                    'text' => 'Portafolio',
                    'url'  => 'products',
                ],
                [
                    'text' => 'Stock',
                    'url'  => 'stocks',
                ],
                [
                    'text' => 'Compras',
                    'url'  => 'purchases',
                ],
                [
                    'text' => 'Despachos',
                    'url' => 'dispatch',
                ],
                [
                    'text'    => 'Ajustes',
                    'url'     => '#',
                    'submenu' => [
                        [
                            'text' => 'Categorias',
                            'url'  => 'categories',
                        ],
                        [
                            'text' => 'Marcas',
                            'url'  => 'brands',
                        ],
                        [
                            'text' => 'Unidades',
                            'url'  => 'units',
                        ],
                        [
                            'text' => 'Almacenes',
                            'url'  => 'warehouses',
                        ],
                        [
                            'text' => 'Proveedores',
                            'url'  => 'providers',
                        ],
                    ],
                ],
            ],
        ],
        [
            'text' => 'Configuracion del Sistema',
            'url' => '#',
            'icon' => 'fa fa-cog',
            'icon_color' => 'red',
            'submenu' => [
                [
                    'text' => 'Roles',
                    'url' => 'rols',
                    'icon' => 'fa fa-user-tag',
                    'icon_color' => 'red',
                ],
                [
                    'text' => 'Permisos',
                    'url' => 'permissions',
                    'icon' => 'fa fa-key',
                    'icon_color' => 'red',
                ],
                [
                    'text' => 'Usuarios',
                    'url' => 'users',
                    'icon' => 'fa fa-users',
                    'icon_color' => 'red',
                ]
            ],
        ],
        ['header' => 'labels'],
        [
            'text'       => 'Configuración',
            'icon_color' => 'red',
            'url'        => '#',
        ],
        [
            'text'       => 'Información',
            'icon_color' => 'cyan',
            'url'        => '#',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Menu Filters
    |--------------------------------------------------------------------------
    |
    | Here we can modify the menu filters of the admin panel.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#612-menu-filters
    |
    */

    'filters' => [
        JeroenNoten\LaravelAdminLte\Menu\Filters\HrefFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\SearchFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\ActiveFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\ClassesFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\GateFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\LangFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\DataFilter::class,
    ],

    /*
    |--------------------------------------------------------------------------
    | Plugins Initialization
    |--------------------------------------------------------------------------
    |
    | Here we can modify the plugins used inside the admin panel.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#613-plugins
    |
    */

    'plugins' => [
        'Datatables' => [
            'active' => false,
            'files' => [
                [
                    'type' => 'js',
                    'asset' => false,
                    'location' => '//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js',
                ],
                [
                    'type' => 'js',
                    'asset' => false,
                    'location' => '//cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js',
                ],
                [
                    'type' => 'css',
                    'asset' => false,
                    'location' => '//cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css',
                ],
            ],
        ],
        'Select2' => [
            'active' => false,
            'files' => [
                [
                    'type' => 'js',
                    'asset' => false,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js',
                ],
                [
                    'type' => 'css',
                    'asset' => false,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.css',
                ],
            ],
        ],
        'Chartjs' => [
            'active' => false,
            'files' => [
                [
                    'type' => 'js',
                    'asset' => false,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.0/Chart.bundle.min.js',
                ],
            ],
        ],
        'Sweetalert2' => [
            'active' => false,
            'files' => [
                [
                    'type' => 'js',
                    'asset' => false,
                    'location' => '//cdn.jsdelivr.net/npm/sweetalert2@8',
                ],
            ],
        ],
        'Pace' => [
            'active' => false,
            'files' => [
                [
                    'type' => 'css',
                    'asset' => false,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/themes/blue/pace-theme-center-radar.min.css',
                ],
                [
                    'type' => 'js',
                    'asset' => false,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/pace.min.js',
                ],
            ],
        ],
    ],
];
