<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'reset' => 'Su password ha sido reiniciado!',
    'sent' => 'Hemos enviado a su correo un enlace para reiniciar su clave!',
    'throttled' => 'Espere un momento por favor.',
    'token' => 'El token para reiniciar su password es invalido.',
    'user' => "No existe usuario con esta direccion de email.",

];
