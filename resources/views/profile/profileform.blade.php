@extends('layouts.dashboard')
@section('title', 'Perfil')
@section('content')
<div class="container">
    @section('content_header')
    Perfil
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif
    @endsection
    <div class="container-fluid">
        <form method="post" action="{{ route('profile_save') }}">
            @csrf
            <div class="card card-default">
              <div class="card-header">
                <h3 class="card-title">Editar Perfil</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                  <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i></button>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body" style="display: block;">
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Nombres</label>
                      <input type="text" id="firstname" name="firstname" required="" value="{{ $profile->firstname ?? '' }}"class="form-control">
                      <input type="hidden" id="id" name="id" value="{{ $profile->id ?? '' }}">
                    </div>
                  </div>
                  <!-- /.col -->
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Apellidos</label>
                      <input type="text" id="lastname" name="lastname" required="" value="{{ $profile->lastname ?? '' }}"class="form-control">
                    </div>
                  </div>
                  <!-- /.col -->
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Cedula</label>
                      <input type="text" id="dni" name="dni" required="" value="{{ $profile->dni ?? '' }}"class="form-control">
                    </div>
                  </div>
                  <!-- /.col -->
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Rif</label>
                      <input type="text" id="rif" name="rif" required="" value="{{ $profile->rif ?? '' }}"class="form-control">
                    </div>
                  </div>
                  <!-- /.col -->
                </div>

              </div>
              <!-- /.card-body -->
              <div class="card-footer" style="display: block;">
                <input type="submit" class="btn btn-primary" value="Guardar"> <input type="reset" value="Cancelar" class="btn btn-outline-danger">
              </div>
            </div>
        </form>
        <!-- /.card -->

      </div>
</div>
@endsection
