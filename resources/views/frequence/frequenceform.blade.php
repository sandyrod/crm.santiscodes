@extends('layouts.dashboard')
@section('title', 'Clientes')
@section('content')
<div class="container">
    @section('content_header')
    Clientes
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif
    @endsection

    <div class="container-fluid">
      @can('admin')
      <form method="post" action="{{ route('frequence_save') }}">
            @csrf
            <div class="card card-default">
              <div class="card-header">
                <h3 class="card-title">Frequencia</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-tool" data-card-widget="remove">
                    <i class="fas fa-remove"></i>
                  </button>
                </div>
              </div>

              <div class="card-body" style="display: block;">
                <div class="row">
                  <div class="col-md-4">
                    <div class="form-group">
                      <label>Nombre</label>
                      <input type="text" id="name" name="name"  value="{{ $frequence->name ?? '' }}"class="form-control">
                      <input type="hidden" id="id" name="id" value="{{ $frequence->id ?? '' }}">
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Descripcion</label>
                      <input type="text" id="description" name="description" value="{{ $frequence->description ?? '' }}"class="form-control">
                    </div>
                  </div>
                </div>
              </div>

              <div class="card-footer" style="display: block;">
                <input type="submit" class="btn btn-primary" value="Guardar">
                <a class="btn btn-outline-danger" href="{{ route('frequence')}}">Cancelar</a>
            </div>
            </div>

      </form>
      @endcan
        <div class="card card-default">
              <div class="card-header">
                <h3 class="card-title">Frequencias</h3>
                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                  <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i></button>
                </div>
              </div>

              <div class="card-body" style="display: block;">
                <div class="table-responsive">
                  <table class="table table-striped">
                      <tr>
                          <th>Id</th>
                          <th>Frequencia</th>
                          <th>Descripcion</th>
                          <th>Opciones</th>
                      </tr>
                  @foreach($frequences as $rowfrequence)
                      <tr>
                          <td>{{ $rowfrequence->id }}</td>
                          <td>{{ $rowfrequence->name }}</td>
                          <td>{{ $rowfrequence->description }}</td>
                          <td><a class="btn btn-sm btn-outline-success" href="{{ route('frequence', $rowfrequence->id) }}"><i class="fa fa-edit"></i></a><span> </span><a class="btn btn-sm btn-outline-danger" href="{{ route('frequence_delete', $rowfrequence->id)}}"><i class="fa fa-trash"></i></a></td>
                      </tr>
                  @endforeach
                  </table>
                </div>
              </div>
              <!-- /.card-body -->
              <div class="card-footer" style="display: block;">
              </div>
        </div>

    </div>
</div>
@endsection
