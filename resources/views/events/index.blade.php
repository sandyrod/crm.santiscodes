@extends('layouts.dashboard')
@section('title', 'Eventos')
@section('content')
  <div class="container-fluid">
    <div class="card-body" style="display: block;">
      <div class="row">
        <div class="col-md-2">
          <a class="btn btn-primary btn-block" id="btn_new" href="{{ route('events_create')}}"">Crear Nuevo Evento</a>
        </div>
      </div>
    </div>
    <div class="card card-default">
      <div class="card-header">
        <h3 class="card-title">Listado de Eventos</h3>
        @if (session('status'))
          <div class="alert alert-success">
            {{ session('status') }}
          </div>
        @endif
        <div class="card-tools">
          <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
          <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i></button>
        </div>
      </div>
      <div class="card-body" style="display: block;">
        <div class="table-responsive">
          <table class="table table-striped table-bordered table-hover" >
            <tr>
              <th>Nombre</th>
              <th>Slug</th>
              <th>Fecha de Inicio</th>
              <th>Fecha de Culminacion</th>
              <th>Limite de Personas</th>
              <th>Logo</th>
              <th>Opciones</th>
            </tr>
            @foreach ($events as $e)
              <tr>
                <td>{{$e->name}}</td>
                <td>{{$e->slug}}</td>
                <td>{{$e->start_date}}</td>
                <td>{{$e->due_date}}</td>
                <td>{{$e->limit}}</td>
                <td>
                  @if($e->logo == "")
                    <img src="{{ asset('img/logo.jpg')}}" width="50"/>
                  @elseif(Str::startsWith($e->logo,  "http"))
                    <img src="{{@$e->logo}}" width="50"/>
                  @else
                    <img src="/img/event/{{@$e->logo}}"  width="50"/>
                  @endif
                </td>                            
                <td><span></span>
                  <a class="btn btn-sm btn-outline-primary" href="{{ route('event_incriptions', $e->slug)}}" >
                    <i class="fa fa-male" style="color:000" ></i>
                  </a>
                  <a class="btn btn-sm btn-outline-success" href="{{ route('events_update', $e->id)}}" >
                    <i class="fa fa-edit" style="color:#006400"></i>
                  </a><span> </span>
                  <a class="btn btn-sm btn-outline-danger" href="{{ route('events_delete', $e->id)}}" >
                    <i class="fa fa-trash" style="color:#f00" ></i>
                  </a>
                  <a href="{{ route('events_list', $e->slug)}}" class="btn btn-sm btn-outline-warning"> 
                    <i class="fa fa-users" style="color:#000"" ></i>
                  </a>
                </td>
              </tr>    
            @endforeach
            <tr>
              <td colspan="5">{{ $events->links() }}</td>
            </tr>
          </table>  
          {{-- <a href="{{ route('reporte')}}" class="btn btn-outline-success" style="position: center;">Exportar Excel</a> --}}
          <a href="{{ route('reportepdf')}}" class="btn btn-outline-success" style="position: center;">Exportar Pdf</a>
        </div>
      </div>
    </div>
  </div>
@endsection
@section('js')
  <script type="text/javascript">
    $(".alert").delay(2000).slideUp(900, function () {
      $(this).alert('close');
    });
  </script>
@stop