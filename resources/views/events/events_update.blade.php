@extends('layouts.dashboard')
@section('title', 'Inscriptions')
@section('content')
    @if(session('status'))
        <div class="alert alert-success">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> {{ session('status') }}
        </div>
    @endif
    <form method="post" action="{{ route('events_update',$events->id) }}">
        @csrf
        <div class="container">
            <div class="card card-default">
                <h3 class="card-title">Datos del Evento</h3>
                <div class="card-header">   
                    <div class="form-row">
                        <div class="form-group col-md-4">    
                            <label for="" class="form-label">Nombre</label>
                            <input id="name" name="name" type="text" value="{{ @$events->name }}"  class="form-control" tabindex="1">        
                            @error('name')
                                <div class="alert alert-danger" role="alert">
                                    <small>*{{ $message}} </small>
                                </div>
                            @enderror            
                        </div>
                        <div class="form-group col-md-4">    
                            <label for="" class="form-label">Slug</label>
                            <input id="slug" name="slug" type="text" value="{{ @$events->slug }}" class="form-control" tabindex="1">        
                            @error('slug')
                                <div class="alert alert-danger" role="alert">
                                    <small>*{{ $message}} </small>
                                </div>
                            @enderror            
                        </div>
                        @if(@$events->logo === "")
                            <div class="col-md-4">
                                <div class="form-group col-md-4">    
                                    <label for="" class="form-label">Logo</label><br>
                                    <img src="https://guiamamaybebe.com/wp-content/uploads/2018/01/no-imagen.jpg" class="img-thumbnail" alt="Cinque Terre" width="200" height="200">        
                                </div>
                            </div>
                        @elseif(Str::startsWith($events->logo,  "http"))
                            <div class="form-group col-md-4">    
                                <label for="" class="form-label">Logo</label><br>
                                <img src="{{@$events->logo}}" class="img-thumbnail" alt="Cinque Terre" width="50" height="50">        
                            </div>
                        @else
                            <div class="form-group col-md-4">    
                                <label for="" class="form-label">Logo</label><br>
                                <img src="/img/event/{{@$events->logo}}" class="img-thumbnail" alt="Cinque Terre" width="50" height="50">        
                            </div>
                        @endif
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="" class="form-label">Fecha de Inicio</label>
                            <div class='input-group date'>
                                <input type='date' id='start_date' value="{{ @$events->start_date }}"  name='start_date' class="form-control" tabindex="1"/>
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                            @error('start_date')
                                <div class="alert alert-danger" role="alert">
                                    <small>*{{ $message}} </small>
                                </div>
                            @enderror 
                        </div>
                        <div class="form-group col-md-4">
                            <label for="" class="form-label">Fecha de Culminacion</label>
                            <div class='input-group date'>
                                <input type='date' id='due_date' value="{{ @$events->due_date }}" name='due_date' class="form-control" tabindex="1"/>
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                            @error('due_date')
                                <div class="alert alert-danger" role="alert">
                                    <small>*{{ $message}} </small>
                                </div>
                            @enderror 
                        </div>
                        <div class="form-group col-md-4">
                            <label for="" class="form-label">Limite de participantes</label>
                            <input id="limit" name="limit" type="number" value="{{ @$events->limit}}" class="form-control" tabindex="1">
                            @error('limit')
                                <div class="alert alert-danger" role="alert">
                                    <small>*{{ $message}} </small>
                                </div>
                            @enderror 
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card card-default">
            <div class="card-footer" style="display: block;">
                <a href="/home" disable class="btn btn-outline-danger" style="position: center;">Cancelar</a>
                <input type="submit" class="btn btn-primary" data-form="form" data-loading-text="Guardando..." value="Guardar">
            </div>
        </div>
    </form>
@endsection
@include('events.function')

