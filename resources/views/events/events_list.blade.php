@extends('layouts.dashboard')
@section('title', 'Eventos')
@section('content')
  <div class="container-fluid">
    <div class="card card-default">
      <div class="card-header">
          <h3 class="card-title">Listado de personas Inscritas en el curso 
          @if (session('status'))
            <div class="alert alert-success">
              {{ session('status') }}
            </div>
          @endif
          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
            <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i></button>
          </div>
        </div>
        <div class="card-body" style="display: block;">
          <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover" >
              <tr>
                <th>Cedula</th>
                <th>Nombres</th>
                <th>Rif De la Empresa</th>
                <th>Nombre de la Empresa</th>
                <th>Fecha de creacion</th>
                {{--<th>Opciones</th>--}}
              </tr>
              @foreach ($participantes as $e)
                <tr>
                  <td>{{$e->identification}}</td>
                  <td>{{$e->name}}</td>
                  <td>{{$e->rif_company}}</td>
                  <td>{{$e->name_company}}</td>
                  <td>{{$e->created_at->format('d-m-Y')}}</td>
                  {{-- <td><span></span>
                    <a class="btn btn-sm btn-outline-success" href="{{ route('participantes_update', $p->id)}}" >
                      <i class="fa fa-edit" style="color:#006400"></i>
                    </a><span> </span>
                    <a class="btn btn-sm btn-outline-danger" href="{{ route('participantes_delete', $p->id)}}" >
                      <i class="fa fa-trash" style="color:#f00" ></i>
                    </a>
                  </td> --}}
                </tr>
              @endforeach
            </table>  
            <a href="{{ route('report_pdf', $events->id)}}" class="btn btn-outline-success" style="position: center;">Exportar Pdf</a>
            @if( $events->id == 2)
            <a href="{{ route('reporteexcel')}}" class="btn btn-outline-success" style="position: center;">Exportar Excel</a>
            @endif
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
