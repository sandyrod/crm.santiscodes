@extends('layouts.dashboard')
@section('title', 'Eventos')
@section('content')
    <form method="post" enctype="multipart/form-data" autocomplete="off" action="{{ route('events_save') }}">
        @csrf
        <div class="container">
            <div class="card card-default">
                <h3 class="card-title">Datos del Evento</h3>
                <div class="card-header">   
                    <div class="form-row">
                        <div class="form-group col-md-4">    
                            <label for="" class="form-label">Nombre</label>
                            <input id="name" name="name" type="text" value="{{old('name')}}" class="form-control" tabindex="1">        
                            @error('name')
                                <div class="alert alert-danger" role="alert">
                                    <small>*{{ $message}} </small>
                                </div>
                            @enderror            
                        </div>
                        <div class="form-group col-md-4">    
                            <label for="" class="form-label">Slug</label>
                            <input id="slug" name="slug" type="text" value="{{old('slug')}}" class="form-control" tabindex="1">        
                            @error('slug')
                                <div class="alert alert-danger" role="alert">
                                    <small>*{{ $message}} </small>
                                </div>
                            @enderror            
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Cargar Logo</label>
                                <select required oninput="setCustomValidity('')" oninvalid="this.setCustomValidity('Requerido')" class="form-control" id="opcion" name="opcion" onchange="seleccionado()"tabindex="1">
                                    <option value="no">Sin Imagen</option>
                                    <option value="img">Imagen desde mi Equipo</option>
                                    <option value="url">Url</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group col-md-4" id="img" style="display:none;">
                            <label for="" class="form-label">Logo</label>
                            <input id="logo" name="logo" type="file" value="{{old('logo')}}" class="form-control" tabindex="1">        
                        </div>
                        <div class="form-group col-md-4" id="url" style="display:none;">
                            <label for="" class="form-label">Logo Url</label>
                            <input id="logo" name="logo" type="text" value="{{old('url')}}" class="form-control" tabindex="1">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="" class="form-label">Fecha de Inicio</label>
                            <div class='input-group date'>
                                <input type='date' id='start_date' name='start_date' class="form-control" tabindex="1"/>
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                            @error('start_date')
                                <div class="alert alert-danger" role="alert">
                                    <small>*{{ $message}} </small>
                                </div>
                            @enderror 
                        </div>
                        <div class="form-group col-md-4">
                            <label for="" class="form-label">Fecha de Culminacion</label>
                            <div class='input-group date'>
                                <input type='date' id='due_date' name='due_date' class="form-control" tabindex="1"/>
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                            @error('due_date')
                                <div class="alert alert-danger" role="alert">
                                    <small>*{{ $message}} </small>
                                </div>
                            @enderror 
                        </div>
                        <div class="form-group col-md-4">
                            <label for="" class="form-label">Limite de participantes</label>
                            <input id="limit" name="limit" type="number" value="{{old('limit')}}" class="form-control" tabindex="1">
                            @error('limit')
                                <div class="alert alert-danger" role="alert">
                                    <small>*{{ $message}} </small>
                                </div>
                            @enderror 
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card card-default">
            <div class="card-footer" style="display: block;">
                <a href="/home" disable class="btn btn-outline-danger" style="position: center;">Cancelar</a>
                <input type="submit" class="btn btn-primary" data-form="form" data-loading-text="Guardando..." value="Guardar">
            </div>
        </div>
    </form>
@endsection
@include('events.function')
