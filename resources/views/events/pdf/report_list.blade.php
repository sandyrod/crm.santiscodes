@extends('layouts.print_events')

@section('titulo', $title)

@section('img')


@endsection

@section('content')

  <div class="row">
    <div class="col-12 table-responsive">
      <table class="table table-striped" style="border: solid 1px; width:100%;">
        <tr style="background-color:#e9ecef;">
          <th style="width: 2%;">Nro</th>
          <th style="width: 5%;">Identificacion</th>
          <th style="width: 5%;">Nombre</th>
          <th style="width: 10%;">Rif de la Compañia</th>
          <th style="width: 10%;">Nombre de la Compañia</th>
          <th style="width: 10%;">Fecha de Creacion</th>
        </tr>
        @php $i=1; @endphp
        @foreach($events as $e)
            <tr style="background-color: {{ $i%2 ? '':'#e9ecef' }};">
              <td>{{ $i++ }}
              <td>{{ $e['identification'] }}</td>
              <td>{{ $e['name'] }}</td>
              <td>{{ $e['rif_company'] }}</td>
              <td>{{ $e['name_company'] }}</td>
              <td>{{ $e['created_at'] }}</td>
              <td>

              </td>
            </tr>
        @endforeach
      </table>
    </div>
  </div>

@endsection