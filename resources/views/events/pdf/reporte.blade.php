@extends('layouts.print_landscape')

@section('titulo', $title)

@section('content')

  <div class="row">
    <div class="col-12 table-responsive">
      <table class="table table-striped" style="border: solid 1px; width:100%;">
        <tr style="background-color:#e9ecef;">
          <th style="width: 2%;">Nro</th>
          <th style="width: 5%;">name</th>
          <th style="width: 5%;">slug</th>
          <th style="width: 10%;">Fecha de Inicio</th>
          <th style="width: 10%;">Fecha de Culminacion</th>
          <th style="width: 10%;">Limite</th>
          <th style="width: 10%;">Logo</th>
        </tr>
        @php $i=1; @endphp
        @foreach($events as $e)
            <tr style="background-color: {{ $i%2 ? '':'#e9ecef' }};">
              <td>{{ $i++ }}
              <td>{{ $e['name'] }}</td>
              <td>{{ $e['slug'] }}</td>
              <td>{{ $e['start_date'] }}</td>
              <td>{{ $e['due_date'] }}</td>
              <td>{{ $e['limit'] }}</td>
              <td>
              </td>
            </tr>
        @endforeach
      </table>
    </div>
  </div>

@endsection