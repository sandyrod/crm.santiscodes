@push('js')
<script>
    function seleccionado(){
        var opt = $('#opcion').val();
        
        // alert(opt);
        if(opt=="img"){
            $('#img').show();
            $('#url').hide();
            
        }else{
            if(opt=="url"){
                $('#img').hide();
                $('#url').show();
                
            }else{
                $('#img').hide();
                $('#url').hide();
                
            }
        }
    }
</script>
<script>
    $(document).on('change','input[type="file"]',function(){
        // this.files[0].size recupera el tamaño del archivo
        // alert(this.files[0].size);
        
        var fileName = this.files[0].name;
        var fileSize = this.files[0].size;

        if(fileSize > 2000000){
            alert('El archivo no debe superar los 2MB');
            this.value = '';
            this.files[0].name = '';
        }else{
            // recuperamos la extensión del archivo
            var ext = fileName.split('.').pop();
            
            // Convertimos en minúscula porque 
            // la extensión del archivo puede estar en mayúscula
            ext = ext.toLowerCase();
        
            // console.log(ext);
            switch (ext) {
                case 'jpg':
                case 'jpeg':
                case 'png':
                case 'pdf': break;
                default:
                    alert('El archivo no tiene la extensión adecuada');
                    this.value = ''; // reset del valor
                    this.files[0].name = '';
            }
        }
    });
</script>
<script type="text/javascript">
    $(".alert").delay(2000).slideUp(900, function () {
        $(this).alert('close');
    });
</script>
@endpush