@extends('layouts.print')

@section('titulo', 'LISTADO DE STOCKS')

@section('content')

  <div class="row">
    <div class="col-12 table-responsive">
      <table class="table table-striped" style="border: solid 1px; width:100%;">
        <tr>
          <th style="width: 40%;">Productos</th>
          <th style="width: 20%;">Almacén</th>
          <th style="width: 10%;">Cantidad</th>
          @can('admin')
          <th style="width: 10%;">Capacidad</th>
          <th style="width: 10%;">Monto</th>
          @endcan
        </tr>
        @foreach($stocks as $item)
            <tr>
               <td>{{ $item->product->name ?? '' }}</td>
                <td>{{ $item->warehouse->name ?? '' }}</td>
                <td>{{ $item->quantity ?? 0 }}</td>
                @can('admin')
                <td>{{ $item->product->units->first()->capacity * $item->quantity ?? 0}} L
                </td>
                <td>$ {{ $item->product->sale_price * $item->quantity ?? 0}}</td>
                @endcan
            </tr>
        @endforeach
      </table>
    </div>
  </div>
@endsection
