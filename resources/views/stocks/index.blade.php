@extends('layouts.dashboard')
@section('title', 'Stock')
@section('content')
<div class="container">
    @section('content_header')
        Stocks por Almacenes
    @endsection
    
    <div class="container-fluid">
      
      @livewire('stocks')

    </div>
</div>
@endsection
