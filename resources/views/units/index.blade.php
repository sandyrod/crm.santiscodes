@extends('layouts.dashboard')
@section('title', 'Unidades')
@section('content')
<div class="container">
    @section('content_header')
        Unidades de Producto
    @endsection
    
    <div class="container-fluid">
      
      @livewire('units')

    </div>
</div>
@endsection
