@extends('layouts.dashboard')
@section('title', 'Ficha de Visita')
@section('content')
<div class="container">
    @section('content_header')
      Ficha de Visita
    @endsection

    <div class="container-fluid">

      @livewire('visitcard')

    </div>
</div>
@endsection
