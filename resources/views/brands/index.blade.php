@extends('layouts.dashboard')
@section('title', 'Marcas')
@section('content')
<div class="container">
    @section('content_header')
      Marcas
    @endsection
    @can('admin')
    <div class="container-fluid">
      
      @livewire('brands')

    </div>
    @endcan
</div>
@endsection
