@extends('layouts.inscription')
@section('title', 'Inscriptions')
@section('content')
@if(session('status'))
    <div class="alert alert-success">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> {{ session('status') }}
    </div>
@endif
<form method="post" oncopy="return false" onpaste="return false" autocomplete="off" action="{{ route('inscription') }}">
        @csrf
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-5" >
                <img src="{{ asset('img/logo.jpg') }}" height="80%">
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-3" >
                <h3><strong>Inscripciones</strong></h3>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-6" >
                <h4>Segundo Seminario de Lubricación</h4>
            </div>
        </div>
        <div class="card card-default">
            <div class="card-header">
                <h3 class="card-title">Datos Personales </h3>
                <br><br>
                <div class="form-row">
                    <div class="form-group col-md-4">    
                        <label for="" class="form-label">Cedula</label>
                        <input id="identification"  name="identification" type="number" value="{{old('identification')}}" class="form-control" tabindex="1">        
                        @error('identification')
                            <div class="alert alert-danger" role="alert">
                                <small>*{{ $message}} </small>
                            </div>
                        @enderror            
                    </div>
                    <div class="form-group col-md-4">    
                        <label for="" class="form-label">Nombre</label>
                        <input id="name"  name="name" type="text" value="{{old('name')}}" class="form-control" tabindex="2" autocomplete="new-text">
                        @error('name')
                            <div class="alert alert-danger"  role="alert">
                                <small>*{{ $message}}</small>
                            </div>
                        @enderror            
                    </div>
                    <div class="form-group col-md-4">
                        <label for="" class="form-label">Apellido</label>
                        <input id="last_name"  name="last_name" type="text" value="{{old('last_name')}}" class="form-control" tabindex="3" autocomplete="new-text" >
                        @error('last_name')
                            <div class="alert alert-danger" role="alert">
                                <small>*{{ $message}}</small>
                            </div>
                        @enderror   
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="" class="form-label">Telefono</label>
                        <input id="phone"  name="phone" type="number" value="{{old('phone')}}" class="form-control" tabindex="3" autocomplete="new-text">
                        @error('phone')
                            <div class="alert alert-danger" role="alert">
                                <small>*{{ $message}}</small>
                            </div>
                        @enderror
                    </div>
                    <div class="form-group col-md-4">
                        <label for="" class="form-label">Correo Electronico</label>
                        <input type="email"  name="email" class="form-control" value="{{old('email')}}" id="email"  tabindex="3" autocomplete="new-text">
                        @error('email')
                            <div class="alert alert-danger" role="alert">
                                <small>*{{ $message}}</small>
                            </div>
                        @enderror
                    </div>
                    <div class="form-group col-md-4">
                        <label for="" class="form-label">Instagram Personal</label>
                        <input id="instagram_personal"  name="instagram_personal" type="text" value="{{old('instagram_personal')}}" required onkeypress="return soloLetras2(event);" onkeyup="this.value = this.value.charAt(0).toUpperCase() + this.value.slice(1);" class="form-control" tabindex="3">
                        @error('instagram_personal')
                            <div class="alert alert-danger" role="alert">
                                <small>*{{ $message}} </small>
                            </div>
                        @enderror            
                    </div>
                </div>
            </div>
        </div>
        <div class="card card-default">
            <div class="card-header">
                <h3 class="card-title">Datos de la Empresa </h3>
                <br><br>
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="" class="form-label">Nombre de Empresa</label>
                        <input id="name_company"  name="name_company" type="text" value="{{old('name_company')}}" class="form-control" tabindex="3" autocomplete="new-text">
                        @error('name_company')
                            <div class="alert alert-danger" role="alert">
                                <small>*{{ $message}}</small>
                            </div>
                        @enderror            
                    </div>
                    <div class="form-group col-md-4">
                        <label for="" class="form-label">Instagram de Empresa</label>
                        <input id="instagram_company"  name="instagram_company" type="text" value="{{old('instagram_company')}}" required onkeypress="return soloLetras(event);" onkeyup="this.value = this.value.charAt(0).toUpperCase() + this.value.slice(1);" class="form-control" tabindex="3">
                        @error('instagram_company')
                            <div class="alert alert-danger" role="alert">
                                <small>*{{ $message}}</small>
                            </div>
                        @enderror            
                    </div>
                    <div class="form-group col-md-4">
                        <label for="" class="form-label">Rif Empresa</label>
                        <input id="rif_company"  name="rif_company" type="text" value="{{old('rif_company')}}" class="form-control" tabindex="3" required onkeypress="return soloLetras3(event);" onkeyup="this.value = this.value.charAt(0).toUpperCase() + this.value.slice(1);" class="form-control">
                        @error('rif_company')
                            <div class="alert alert-danger" role="alert">
                                <small>*{{ $message}}</small>
                            </div>
                        @enderror            
                    </div>
                </div>
            </div>
        </div>
        <div class="card card-default">
            <div class="card-footer" style="display: block;">
                <a href="/home" class="btn btn-outline-danger" style="position: center;">Cancelar</a>
                <input type="submit"  class="btn btn-primary" data-form="form" data-loading-text="Guardando..." data-class="Census" value="Guardar">
            </div>
        </div>  
    </form>
</div> 
@endsection
@section('js') 
    <script type="text/javascript">
        $(".alert").delay(2000).slideUp(900, function () {
            $(this).alert('close');
        });
    </script>
    <script>
        function soloLetras(e) {
            textoArea = document.getElementById("instagram_company").value;
            var total = textoArea.length;
            if (total == 0) {
                key = e.keyCode || e.which;
                tecla = String.fromCharCode(key).toString();
                letras = "@"; //Se define todo el abecedario que se quiere que se muestre.
                especiales = [8, 9, 37, 39, 46, 6]; //Es la validación del KeyCodes, que teclas recibe el campo de texto.
                tecla_especial = false
                for (var i in especiales) {
                    if (key == especiales[i]) {
                    tecla_especial = true;
                    break;
                    }
                }
                if (letras.indexOf(tecla) == -1 && !tecla_especial) {
                    return false;
                    alert('No puedes comenzar escribiendo numeros o letras');
                }
            }
        }
    </script>
    <script>
        function soloLetras2(e) {
            textoArea = document.getElementById("instagram_personal").value;
            var total = textoArea.length;
            if (total == 0) {
                key = e.keyCode || e.which;
                tecla = String.fromCharCode(key).toString();
                letras = "@"; //Se define todo el abecedario que se quiere que se muestre.
                especiales = [8, 9, 37, 39, 46, 6]; //Es la validación del KeyCodes, que teclas recibe el campo de texto.

                tecla_especial = false
                for (var i in especiales) {
                    if (key == especiales[i]) {
                    tecla_especial = true;
                    break;
                    }
                }
                if (letras.indexOf(tecla) == -1 && !tecla_especial) {
                    return false;
                    alert('No puedes comenzar escribiendo numeros o letras');
                }
            }
        }
    </script>
    <script>
        function soloLetras3(e) {
            textoArea = document.getElementById("rif_company").value;
            var total = textoArea.length;
            if (total == 0) {
                key = e.keyCode || e.which;
                tecla = String.fromCharCode(key).toString();
                letras = "áéíóúabcdefghijklmnñopqrstuvwxyz"; //Se define todo el abecedario que se quiere que se muestre.
                especiales = [8, 9, 37, 39, 46, 6]; //Es la validación del KeyCodes, que teclas recibe el campo de texto.

                tecla_especial = false
                for (var i in especiales) {
                    if (key == especiales[i]) {
                    tecla_especial = true;
                    break;
                    }
                }
                if (letras.indexOf(tecla) == -1 && !tecla_especial) {
                    return false;
                    alert('No puedes comenzar escribiendo numeros o letras');
                }
            }
            if (total == 1) {
                key = e.keyCode || e.which;
                tecla = String.fromCharCode(key).toString();
                letras = "-"; //Se define todo el abecedario que se quiere que se muestre.
                especiales = [8, 9, 37, 39, 46, 6]; //Es la validación del KeyCodes, que teclas recibe el campo de texto.

                tecla_especial = false
                for (var i in especiales) {
                    if (key == especiales[i]) {
                    tecla_especial = true;
                    break;
                    }
                }
                if (letras.indexOf(tecla) == -1 && !tecla_especial) {
                    return false;
                    
                }
            }
            if (total > 1 ) {
                key = e.keyCode || e.which;
                tecla = String.fromCharCode(key).toString();
                letras = "1234567890"; //Se define todo el abecedario que se quiere que se muestre.
                especiales = [8, 9, 37, 39, 46, 6]; //Es la validación del KeyCodes, que teclas recibe el campo de texto.

                tecla_especial = false
                for (var i in especiales) {
                    if (key == especiales[i]) {
                    tecla_especial = true;
                    break;
                    }
                }
                if (letras.indexOf(tecla) == -1 && !tecla_especial) {
                    return false;
                    
                }
            }
        }
    </script>

@stop
