@extends('layouts.print_landscape')

@section('titulo', $title)

@section('content')

  <div class="row">
    <div class="col-12 table-responsive">
      <table class="table table-striped" style="border: solid 1px; width:100%;">
        <tr style="background-color:#e9ecef;">
          <th style="width: 2%;">Nro</th>
          <th style="width: 5%;">Cedula</th>
          <th style="width: 5%;">Nombres</th>
          <th style="width: 10%;">Apellidos</th>
          <th style="width: 10%;">Telefono</th>
          <th style="width: 10%;">Correo</th>
          <th style="width: 10%;">Rif de la Empresa</th>
          <th style="width: 10%;">Nombre de la Empresa</th>
          <th style="width: 10%;">Instagram Personal</th>
          <th style="width: 10%;">Instagram de la Empresa</th>
        </tr>
        @php $i=1; @endphp
        @foreach($participantes as $p)
            <tr style="background-color: {{ $i%2 ? '':'#e9ecef' }};">
              <td>{{ $i++ }}
              <td>{{ $p['identification'] }}</td>
              <td>{{ $p['name'] }}</td>
              <td>{{ $p['last_name'] }}</td>
              <td>{{ $p['phone'] }}</td>
              <td>{{ $p['email'] }}</td>
              <td>{{ $p['rif_company'] }}</td>
              <td>{{ $p['name_company'] }}</td>
              <td>{{ $p['instagram_personal'] }}</td>
              <td>{{ $p['instagram_company'] }}</td>
              
              
            </tr>
        @endforeach

      </table>
    </div>
  </div>

@endsection
