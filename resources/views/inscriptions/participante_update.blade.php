@extends('layouts.inscription')
@section('title', 'Inscriptions')
@section('content')
@if(session('status'))
    <div class="alert alert-success">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> {{ session('status') }}
    </div>
@endif
<form method="post" action="{{ route('participantes_update',$participante->id) }}">
        @csrf
        
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-5" >
                <img src="{{ asset('img/logo.jpg') }}" height="80%">
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-3" >
                <h3><strong>Inscripciones</strong></h3>
            </div>
        </div>
        <div class="card card-default">
            <div class="card-header">
                <h3 class="card-title">Datos Personales </h3>
                <br><br>
                <div class="form-row">
                    <div class="form-group col-md-4">    
                        <label for="" class="form-label">Cedula</label>
                        <input id="identification" name="identification" type="number" value="{{ @$participante->identification }}"  class="form-control" tabindex="1">        
                        @error('identification')
                            <div class="alert alert-danger" role="alert">
                                <small>*Identification es Requerido </small>
                            </div>
                        @enderror            
                    </div>
                    <div class="form-group col-md-4">    
                        <label for="" class="form-label">Nombre</label>
                        <input id="name" name="name" type="text" value="{{@$participante->name}}" class="form-control" tabindex="2">
                        @error('name')
                            <div class="alert alert-danger"  role="alert">
                                <small>*Nombre es Requerido</small>
                            </div>
                        @enderror            
                    </div>
                    <div class="form-group col-md-4">
                        <label for="" class="form-label">Apellido</label>
                        <input id="last_name" name="last_name" type="text" value="{{@$participante->last_name}}" class="form-control" tabindex="3">
                        @error('last_name')
                            <div class="alert alert-danger" role="alert">
                                <small>*Apellido es Requerido</small>
                            </div>
                        @enderror   
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="" class="form-label">Telefono</label>
                        <input id="phone" name="phone" type="number" value="{{@$participante->phone}}" class="form-control" tabindex="3">
                        @error('phone')
                            <div class="alert alert-danger" role="alert">
                                <small>*Telefono es Requerido</small>
                            </div>
                        @enderror
                    </div>
                    <div class="form-group col-md-4">
                        <label for="" class="form-label">Correo Electronico</label>
                        <input id="email" name="email" type="email" value="{{@$participante->email}}" class="form-control" tabindex="3">
                        @error('email')
                            <div class="alert alert-danger" role="alert">
                                <small>*Email es Requerido</small>
                            </div>
                        @enderror
                    </div>
                    <div class="form-group col-md-4">
                        <label for="" class="form-label">Instagram Personal</label>
                        <input id="instagram_personal" name="instagram_personal" type="text" value="{{@$participante->instagram_personal}}" class="form-control" tabindex="3">
                        @error('instagram_personal')
                            <div class="alert alert-danger" role="alert">
                                <small>*Instagram Personal es Requerido </small>
                            </div>
                        @enderror            
                    </div>
                </div>
            </div>
        </div>
        <div class="card card-default">
            <div class="card-header">
                <h3 class="card-title">Datos de la Empresa </h3>
                <br><br>
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="" class="form-label">Nombre de Empresa</label>
                        <input id="name_company" name="name_company" type="text" value="{{@$participante->name_company}}" class="form-control" tabindex="3">
                        @error('name_company')
                            <div class="alert alert-danger" role="alert">
                                <small>*Nombre de Empresa es Requerido</small>
                            </div>
                        @enderror            
                    </div>
                    <div class="form-group col-md-4">
                        <label for="" class="form-label">Instagram de Empresa</label>
                        <input id="instagram_company" name="instagram_company" type="text" value="{{@$participante->instagram_company}}" class="form-control" tabindex="3">
                        @error('instagram_company')
                            <div class="alert alert-danger" role="alert">
                                <small>*Instagram de Empresa es Requerido</small>
                            </div>
                        @enderror            
                    </div>
                    <div class="form-group col-md-4">
                        <label for="" class="form-label">Rif Empresa</label>
                        <input id="rif_company" name="rif_company" type="text" value="{{@$participante->rif_company}}" class="form-control" tabindex="3">
                        @error('rif_company')
                            <div class="alert alert-danger" role="alert">
                                <small>*Rif de Empresa es Requerido</small>
                            </div>
                        @enderror            
                    </div>
                </div>
            </div>
        </div>
        <div class="card card-default">
            <div class="card-footer" style="display: block;">
                <a href="/home" class="btn btn-outline-danger" style="position: center;">Cancelar</a>
                <input type="submit" class="btn btn-primary" data-form="form" data-loading-text="Guardando..." data-class="Census" value="Guardar">
            </div>
        </div>  
    </form>
</div> 
@endsection
@section('js') 
    <script type="text/javascript">
        $(".alert").delay(2000).slideUp(900, function () {
            $(this).alert('close');
        });
    </script>

@stop
