@extends('layouts.dashboard')
@section('title', 'Participantes')
@section('content')
  <div class="container-fluid">
    <div class="card card-default">
      <div class="card-header">
        <h3 class="card-title">Listado de Participantes</h3>
        @if (session('status'))
          <div class="alert alert-success">
              {{ session('status') }}
          </div>
        @endif
        <div class="card-tools">
          <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
          <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i></button>
        </div>
      </div>
      <div class="card-body" style="display: block;">
        <div class="table-responsive">
          <table class="table table-striped table-bordered table-hover" >
            <tr>
              <th>Cedula</th>
              <th>Nombres</th>
              <th>Rif De la Empresa</th>
              <th>Nombre de la Empresa</th>
              <th>Fecha de creacion</th>
              <th>Opciones</th>
            </tr>
            @foreach ($participantes as $p)
              <tr>
                <td>{{$p->identification}}</td>
                <td>{{$p->name}}</td>
                <td>{{$p->rif_company}}</td>
                <td>{{$p->name_company}}</td>
                <td>{{$p->created_at->format('d-m-Y')}}</td>
                <td><span></span>
                  <a class="btn btn-sm btn-outline-success" href="{{ route('participantes_update', $p->id)}}" >
                    <i class="fa fa-edit" style="color:#006400"></i>
                  </a><span> </span>
                  <a class="btn btn-sm btn-outline-danger" href="{{ route('participantes_delete', $p->id)}}" >
                    <i class="fa fa-trash" style="color:#f00" ></i>
                  </a>
                </td>
              </tr>    
            @endforeach
            <tr>
              <td colspan="5">{{ $participantes->links() }}</td>
            </tr>
          </table>  
          <a href="{{ route('reporte')}}" class="btn btn-outline-success" style="position: center;">Exportar Excel</a>
          <a href="{{ route('reportepdf')}}" class="btn btn-outline-success" style="position: center;">Exportar Pdf</a>
        </div>
      </div>
    </div>
  </div>
@endsection
@section('js') 
    <script type="text/javascript">
        $(".alert").delay(2000).slideUp(900, function () {
            $(this).alert('close');
        });
    </script>
@stop


