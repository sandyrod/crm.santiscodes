@extends('layouts.dashboard')
@section('title', 'Permisos')
@section('content')
<div class="container">
    @section('content_header')
      Permisos
    @endsection

    <div class="container-fluid">

      @livewire('permisos')

    </div>
</div>
@endsection
