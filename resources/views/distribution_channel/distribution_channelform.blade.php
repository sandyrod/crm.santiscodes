@extends('layouts.dashboard')
@section('title', 'Clientes')
@section('content')
<div class="container">
    @section('content_header')
    Canales de Distribución
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif
    @endsection
    <div class="container-fluid">
        @can('admin')
        <form method="post" action="{{ route('distribution_channel_save') }}">
            @csrf
            <div class="card card-default">
              <div class="card-header">
                <h3 class="card-title">Canales de Distribución</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-tool" data-card-widget="remove">
                    <i class="fas fa-remove"></i>
                  </button>
                </div>
              </div>

              <div class="card-body" style="display: block;">
                <div class="row">
                  <div class="col-md-4">
                    <div class="form-group">
                      <label>Nombre</label>
                      <input type="text" id="name" name="name"  value="{{ $distribution_channel->name ?? '' }}"class="form-control">
                      <input type="hidden" id="id" name="id" value="{{ $distribution_channel->id ?? '' }}">
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Descripcion</label>
                      <input type="text" id="description" name="description" value="{{ $distribution_channel->description ?? '' }}"class="form-control">
                    </div>
                  </div>
                </div>
              </div>

              <div class="card-footer" style="display: block;">
                <input type="submit" class="btn btn-primary" value="Guardar">
                <a class="btn btn-outline-danger" href="{{ route('distribution_channel')}}">Cancelar</a>
            </div>
            </div>

        </form>
        @endcan
        <div class="card card-default">
              <div class="card-header">
                <h3 class="card-title">Canales de Distribución</h3>
                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                  <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i></button>
                </div>
              </div>

              <div class="card-body" style="display: block;">
                <div class="table-responsive">
                  <table class="table table-striped">
                      <tr>
                          <th>Id</th>
                          <th>Canal</th>
                          <th>Descripción</th>
                          <th>Opciones</th>
                      </tr>
                  @foreach($distribution_channels as $rowchannel)
                      <tr>
                          <td>{{ $rowchannel->id }}</td>
                          <td>{{ $rowchannel->name }}</td>
                          <td>{{ $rowchannel->description }}</td>
                          <td><a class="btn btn-sm btn-outline-success" href="{{ route('distribution_channel', $rowchannel->id) }}"><i class="fa fa-edit"></i></a><span> </span><a class="btn btn-sm btn-outline-danger" href="{{ route('distribution_channel_delete', $rowchannel->id)}}"><i class="fa fa-trash"></i></a></td>
                      </tr>
                  @endforeach
                  </table>
                </div>
              </div>
              <!-- /.card-body -->
              <div class="card-footer" style="display: block;">
              </div>
        </div>

      </div>
</div>
@endsection
