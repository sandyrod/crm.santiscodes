@extends('layouts.dashboard')
@section('title', 'Bancos')
@section('content')
<div class="container">
    @section('content_header')
      Definicion de Bancos
    @endsection

    <div class="container-fluid">
      @can('admin')
      @livewire('banks')
      @endcan
    </div>
</div>
@endsection
