@extends('layouts.dashboard')
@section('title', 'Bancos-Cuentas')
@section('content')
<div class="container">
    @section('content_header')
      Cuentas Bancarias
    @endsection

    <div class="container-fluid">
      @can('admin')
      @livewire('banksaccounts')
      @endcan
    </div>
</div>
@endsection
