@extends('layouts.dashboard')
@section('title', 'Transacciones Bancarias')
@section('content')
<div class="container">
    @section('content_header')
      Tansacciones Bancarias
    @endsection

    <div class="container-fluid">
      @can('admin')
      @livewire('bank-transactions-log')
      @endcan
    </div>
</div>
@endsection
