@extends('layouts.dashboard')
@section('title', 'Indicadores')
@section('content')
    @if(session('status'))
        <div class="alert alert-success">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> {{ session('status') }}
        </div>
    @endif
    <form method="post" autocomplete="off" action="{{ route('indicators_client') }}">
        @csrf
        <div class="container">
            <div class="card card-default">
                <div class="card-header">
                    <h3 class="card-title">Clientes Nuevos </h3>
                    <br><br>
                    <div class="form-row">
                        <div class="form-group col-md-4">    
                            <label for="" class="form-label">Desde</label>
                            <div class='input-group date'>
                                    <input type='date' id='date_from' name='date_from' class="form-control" tabindex="1"/>
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            @error('date_from')
                                <div class="alert alert-danger" role="alert">
                                    <small>*{{ $message}} </small>
                                </div>
                            @enderror            
                        </div>
                        <div class="form-group col-md-4">    
                            <label for="" class="form-label">Hasta</label>
                            <div class='input-group date'>
                                    <input type='date' id='date_to' name='date_to' class="form-control" tabindex="1"/>
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            @error('date_to')
                                <div class="alert alert-danger"  role="alert">
                                    <small>*{{ $message}}</small>
                                </div>
                            @enderror            
                        </div>
                    </div>
                </div>
            </div>
            <div class="card card-default">
                <div class="card-footer" style="display: block;">
                    <a href="/home" disable class="btn btn-outline-danger" style="position: center;">Cancelar</a>
                    <input type="submit" class="btn btn-primary" data-form="form" data-loading-text="Guardando..." value="Consultar">
                </div>
            </div>
        </div>
        
        <div class="container">
            <div class="card card-default">
                <div class="card-header">
                    <h3 class="card-title">Meta de Vendedores </h3>
                    <br><br>
                    <div class="form-row">
                        <div class="form-group col-md-4">    
                            <label for="" class="form-label">Cuotas</label>
                            <input id="Cuota" name="cuota" type="number" value="{{old('cuota')}}" class="form-control" tabindex="1">        
                            @error('date_from')
                                <div class="alert alert-danger" role="alert">
                                    <small>*{{ $message}} </small>
                                </div>
                            @enderror            
                        </div>
                        <div class="form-group col-md-4">    
                            <label for="" class="form-label">Hasta</label>
                            <div class='input-group date'>
                                    <input type='date' id='date_to' name='date_to' class="form-control" tabindex="1"/>
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            @error('date_to')
                                <div class="alert alert-danger"  role="alert">
                                    <small>*{{ $message}}</small>
                                </div>
                            @enderror            
                        </div>
                    </div>
                </div>
            </div>
            <div class="card card-default">
                <div class="card-footer" style="display: block;">
                    <a href="/home" disable class="btn btn-outline-danger" style="position: center;">Cancelar</a>
                    <input type="submit" class="btn btn-primary" data-form="form" data-loading-text="Guardando..." value="Consultar">
                </div>
            </div>
        </div>
        













    </form>
@endsection
@section('js')
  <script type="text/javascript">
    $(".alert").delay(2000).slideUp(900, function () {
      $(this).alert('close');
    });
  </script>
@stop