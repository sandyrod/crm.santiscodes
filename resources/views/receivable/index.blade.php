@extends('layouts.dashboard')
@section('title', 'CxC')
@section('content')
<div class="container">
    @section('content_header')
      Cuentas por Cobrar
    @endsection

    <div class="container-fluid">
      @can('admin')
      @livewire('account-receivable')
      @endcan
    </div>
</div>
@endsection
