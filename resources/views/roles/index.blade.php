@extends('layouts.dashboard')
@section('title', 'Roles')
@section('content')
<div class="container">
    @section('content_header')
      Roles
    @endsection

    <div class="container-fluid">

      @livewire('roles')

    </div>
</div>
@endsection
