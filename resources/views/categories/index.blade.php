@extends('layouts.dashboard')
@section('title', 'Categorias')
@section('content')
<div class="container">
    @section('content_header')
      Categorias
    @endsection
    
    <div class="container-fluid">
      
      @livewire('categories')

    </div>
</div>
@endsection
