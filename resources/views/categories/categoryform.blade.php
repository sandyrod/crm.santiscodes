@extends('layouts.dashboard')
@section('title', 'Categorias')
@section('content')
<div class="container">
    @section('content_header')
    Categorias de Producto
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif
    @endsection
    <div class="container-fluid">
        <form method="post" action="{{ route('category_save') }}">
            @csrf
            <div class="card card-default">
              <div class="card-header">
                <h3 class="card-title">Categorias de Producto</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-tool" data-card-widget="remove">
                    <i class="fas fa-remove"></i>
                  </button>
                </div>
              </div>

              <div class="card-body" style="display: block;">
                <div class="row">
                  <div class="col-md-4">
                    <div class="form-group">
                      <label>Nombre</label>
                      <input type="text" id="name" name="name"  value="{{ $category->name ?? '' }}"class="form-control">
                      <input type="hidden" id="id" name="id" value="{{ $category->id ?? '' }}">
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Descripcion</label>
                      <input type="text" id="description" name="description" value="{{ $category->description ?? '' }}"class="form-control">
                    </div>
                  </div>
                </div>
              </div>

              <div class="card-footer" style="display: block;">
                <input type="submit" class="btn btn-primary" value="Guardar"> <a href="{{route('categories')}}"  class="btn btn-outline-danger">Cancelar</a>
            </div>
            </div>

        </form>
        <div class="card card-default">
              <div class="card-header">
                <h3 class="card-title">Listado de Categorias</h3>
                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                  <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i></button>
                </div>
              </div>

              <div class="card-body" style="display: block;">
                <div class="table-responsive">
                  <table class="table table-striped">
                      <tr>
                          <th>Id</th>
                          <th>Categoria</th>
                          <th>Descripcion</th>
                          <th>Opciones</th>
                      </tr>
                  @foreach($categories as $rowcategory)
                      <tr>
                          <td>{{ $rowcategory->id }}</td>
                          <td>{{ $rowcategory->name }}</td>
                          <td>{{ $rowcategory->description }}</td>
                          <td><a class="btn btn-sm btn-outline-success" href="{{ route('categories', $rowcategory->id) }}"><i class="fa fa-edit"></i></a><span> </span><a class="btn btn-sm btn-outline-danger" href="{{ route('category_delete', $rowcategory->id)}}"><i class="fa fa-trash"></i></a></td>
                      </tr>
                  @endforeach
                  </table>
                </div>
              </div>
              <!-- /.card-body -->
              <div class="card-footer" style="display: block;">
              </div>
        </div>

      </div>
</div>
@endsection
