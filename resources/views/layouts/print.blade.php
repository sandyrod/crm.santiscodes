<!DOCTYPE html>
<html lang="es">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title>@yield('titulo', 'Reporte')</title>

        <link rel="stylesheet" href="{{ asset('vendor/fontawesome-free/css/all.min.css') }}">
        <link rel="stylesheet" href="{{ asset('vendor/overlayScrollbars/css/OverlayScrollbars.min.css') }}">

        {{-- Configured Stylesheets --}}
        @include('adminlte::plugins', ['type' => 'css'])

        {{-- <link rel="stylesheet" href="{{ asset('vendor/adminlte/dist/css/adminlte.min.css') }}"> --}}

    </head>

    <body class="hold-transition sidebar-mini layout-fixed">

        <div class="wrapper">

           <div class="container">

            <div class="row">

                <div class="col-md-3">
                  <img class="responsive" width="150" src="{{public_path('img/logo.jpg')}}" alt="">
                </div>

                <div class="col-md-9" align="center">
                    <h3>@yield('titulo', 'Reporte')</h3>
                </div>

            </div>

          @yield('content')

            </div>
      </div>
    </body>
</html>
