@extends('adminlte::page2')

@section('title', 'SantiCRM')

@section('content_header')

@stop

@section('content')
    @yield('content')


@stop

@section('css')
    <link rel="stylesheet" href="{{asset('/css/admin_custom.css')}}">
    <link rel="stylesheet" href="/vendor/select2/css/select2.min.css">
    <link rel="stylesheet" href="/vendor/select2-bootstrap4-theme/select2-bootstrap4.min.css">
    <link rel="stylesheet" href="{{ asset('vendor/sweetalert2/sweetalert2.min.css') }}">
    <link rel="stylesheet" href="{!! asset('vendor/datetimepicker/css/jquery-datetimepicker.min.css') !!}">
    <style type="text/css">
    .select2-container--default .select2-selection--single {
      height: 37px;
      
    }
</style>
@stop

@section('js')
	<script src="/vendor/select2/js/select2.full.min.js"></script>
    <script src="{!! asset('vendor/datetimepicker/js/jquery-datetimepicker.full.min.js')!!}"></script>
    <script src="{{ asset('vendor/sweetalert2/sweetalert2.min.js') }}"></script>
    <script src="{{ asset('vendor/moment/moment.min.js') }}"></script>
    <script src="{{ asset('vendor/inputmask/min/jquery.inputmask.bundle.min.js') }}"></script>
    <script>
    	$('.select2').select2()

	    //Initialize Select2 Elements
	    $('.select2bs4').select2({
	      theme: 'bootstrap4'
	    })
    </script>
    <script>
     $(document).ready( function () {
      $("input").on("keypress", function () {
       $input=$(this);
       setTimeout(function () {
        $input.val($input.val().toUpperCase());
       },50);
      });

      const ToastAlert = (type, title, text = '', timeout = 4000, position= 'top-end') => {
          Swal.mixin({
               toast: true,
               position: position,
               showConfirmButton: false,
               timer: timeout
            }).fire({
               icon: type,
               title: title,
               text: text
          });
       }

       window.livewire.on('notify:toast', data => {
          ToastAlert(data.type, data.title, data.text, data.timeout, data.position);
       })
     })
    </script>
{{-- <script data-ad-client="ca-pub-1925175974816883" async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script> --}}
    @yield('js')
@stop
