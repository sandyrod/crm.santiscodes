@extends('layouts.dashboard')
@section('title', 'Rutas')
@section('content')
<div class="container">
    @section('content_header')
      Rutas
    @endsection

    <div class="container-fluid">
      @can('admin')
      @livewire('distribution-routes')
      @endcan
    </div>
</div>
@endsection
