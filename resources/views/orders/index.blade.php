@extends('layouts.dashboard')
@section('title', 'Pedidos')
@section('content')
<div class="container">
    @section('content_header')
      Pedidos
    @endsection

    <div class="container-fluid">

      @livewire('orders')

    </div>
</div>
@endsection
