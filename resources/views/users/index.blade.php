@extends('layouts.dashboard')
@section('title', 'Usuarios')
@section('content')
<div class="container">
    @section('content_header')
      Usuarios
    @endsection

    <div class="container-fluid">

      @livewire('users')

    </div>
</div>
@endsection
