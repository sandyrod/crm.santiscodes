@extends('layouts.dashboard')
@section('title', 'Almacenes')
@section('content')
<div class="container">
    @section('content_header')
      Almacenes de Producto
    @endsection
    
    <div class="container-fluid">
      
      @livewire('warehouses')

    </div>
</div>
@endsection
