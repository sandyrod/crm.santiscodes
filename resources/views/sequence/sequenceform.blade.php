@extends('layouts.dashboard')
@section('title', 'Clientes')
@section('content')
<div class="container">
    @section('content_header')
    Clientes
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif
    @endsection
    <div class="container-fluid">
        @can('admin')
        <form method="post" action="{{ route('sequence_save') }}">
            @csrf
            <div class="card card-default">
              <div class="card-header">
                <h3 class="card-title">Secuencia</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-tool" data-card-widget="remove">
                    <i class="fas fa-remove"></i>
                  </button>
                </div>
              </div>

              <div class="card-body" style="display: block;">
                <div class="row">
                  <div class="col-md-4">
                    <div class="form-group">
                      <label>Nombre</label>
                      <input type="text" id="name" name="name"  value="{{ $sequence->name ?? '' }}"class="form-control">
                      <input type="hidden" id="id" name="id" value="{{ $sequence->id ?? '' }}">
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Descripcion</label>
                      <input type="text" id="description" name="description" value="{{ $sequence->description ?? '' }}"class="form-control">
                    </div>
                  </div>
                </div>
              </div>

              <div class="card-footer" style="display: block;">
                <input type="submit" class="btn btn-primary" value="Guardar">
                <a class="btn btn-outline-danger" href="{{ route('sequence')}}">Cancelar</a>
            </div>
            </div>

        </form>
        @endcan
        <div class="card card-default">
              <div class="card-header">
                <h3 class="card-title">Secuencias</h3>
                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                  <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i></button>
                </div>
              </div>

              <div class="card-body" style="display: block;">
                <div class="table-responsive">
                  <table class="table table-striped">
                      <tr>
                          <th>Id</th>
                          <th>Secuencia</th>
                          <th>Descripcion</th>
                          <th>Opciones</th>
                      </tr>
                  @foreach($sequences as $rowsequence)
                      <tr>
                          <td>{{ $rowsequence->id }}</td>
                          <td>{{ $rowsequence->name }}</td>
                          <td>{{ $rowsequence->description }}</td>
                          <td><a class="btn btn-sm btn-outline-success" href="{{ route('sequence', $rowsequence->id) }}"><i class="fa fa-edit"></i></a><span> </span><a class="btn btn-sm btn-outline-danger" href="{{ route('sequence_delete', $rowsequence->id)}}"><i class="fa fa-trash"></i></a></td>
                      </tr>
                  @endforeach
                  </table>
                </div>
              </div>
              <!-- /.card-body -->
              <div class="card-footer" style="display: block;">
              </div>
        </div>

      </div>
</div>
@endsection
