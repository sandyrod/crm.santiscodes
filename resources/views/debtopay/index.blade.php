@extends('layouts.dashboard')
@section('title', 'CxP')
@section('content')
<div class="container">
    @section('content_header')
      Cuentas por pagar
    @endsection

    <div class="container-fluid">
      @can('admin')
      @livewire('debtopays')
      @endcan
    </div>
</div>
@endsection
