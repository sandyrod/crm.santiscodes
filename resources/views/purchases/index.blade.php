@extends('layouts.dashboard')
@section('title', 'Entradas')
@section('content')
<div class="container">
    @section('content_header')
      Compras e Ingresos
    @endsection

    <div class="container-fluid">

      @livewire('purchase')

    </div>
</div>
@endsection
