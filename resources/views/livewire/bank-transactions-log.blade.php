<div>
    <div class="card card-default">
        <div class="card-header">
            <h3 class="card-title">Cuentas Bancarias</h3>

            <div class="card-tools">
                <span wire:loading>
                    <a href="#" class="btn btn-tool btn-sm text-primary hint--top" aria-label="Procesando...">
                        <i class='nav-icon fas fa-circle-notch fa-2x fa-spin fa-fw'></i>
                    </a>
                </span>

                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-minus"></i>
                </button>
                <button type="button" class="btn btn-tool" data-card-widget="remove">
                    <i class="fas fa-remove"></i>
                </button>
            </div>
        </div>
        <div class="card-body" style="display: block;">
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Banco</label>
                        <div wire:ignore>
                            <select class="form-control select2 banks_select">
                                <option value="">Seleccione...</option>
                                @forelse($banks as $item)
                                <option value="{{$item->id}}">{{$item->name}}</option>
                                @empty
                                @endforelse
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Cuenta</label>
                        <div wire:ignore>
                            <select class="form-control select2 bankaccounts_select">
                                <option value="">Seleccione...</option>
                                @forelse($bankaccounts as $item)
                                <option value="{{ $item->id }}">{{ $item->number }}</option>
                                @empty
                                @endforelse
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                      <label>Referencia</label>
                      <input class="form-control" wire:model.defer="transactioncode" type="text" placeholder="">
                      @error('transactioncode')
                          <span class="text-danger">{{ $message }}</span>
                      @enderror
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                      <label>Monto</label>
                      <input class="form-control" wire:model.defer="amount" type="text" placeholder="">
                      @error('amount')
                          <span class="text-danger">{{ $message }}</span>
                      @enderror
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label>
                            Tipo de Movimiento
                        </label>
                        <br>
                        <input wire:model.defer="transaction_type_id" name="transaction_type_id" type="radio" value="1" />
                        Debito
                        <input wire:model.defer="transaction_type_id" name="transaction_type_id" type="radio" value="0" />Credito
                        @error('transaction_type_id')
                          <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Metodo</label>
                        <div wire:ignore>
                            <select class="form-control select2 method_select">
                                <option value="">Seleccione...</option>
                                @forelse($methods as $key => $item)
                                <option value="{{ $key }}">{{ $item }}</option>
                                @empty
                                @endforelse
                            </select>
                        </div>
                        @error('transaction_method_id')
                          <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>
                            Estatus
                        </label>
                        <br>
                        <input wire:model.defer="status" name="status" type="radio" value="1" />
                        Activa
                        <input wire:model.defer="status" name="status" type="radio" value="0" />Anulada
                        @error('status')
                          <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Observación</label>
                        <input class="form-control" wire:model="bankaccounts" type="text" placeholder="">
                        @error('description')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
            </div>
        </div>

        <div class="card-footer" style="display: block;">
            <a href="#" wire:click="save" class="btn btn-primary">Guardar</a>

            <a href="#" wire:click="resetInput" class="btn btn-outline-danger">Cancelar</a>
        </div>
    </div>

    <div class="card card-default">
          <div class="card-header">
            <h3 class="card-title">Transacciones Bancarias</h3>
            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
              <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i></button>
            </div>
          </div>

          <div class="card-body" style="display: block;">
            <div class="form-group">
                <input class="form-control" wire:model="search" type="text" placeholder="Buscar...">
            </div>
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover" >
                    <tr>
                        <th>Id</th>
                        <th>Banco</th>
                        <th>Cuenta</th>
                        <th>Monto</th>
                        <th>Opciones</th>
                    </tr>

                    @forelse($data as $item)
                      <tr>
                          <td>{{ $item->id }}</td>
                          <td>{{ $item->bankaccount->bank->name }}</td>
                          <td>{{ $item->bankaccount->number }}</td>
                          <td>
                            @if(!$item->transaction_type_id)
                                -
                            @endif
                            {{ $item->amount }}
                          </td>
                          <td>
                            <a class='btn btn-sm btn-outline-success' wire:click="edit({{ $item->id }})"><i class="fa fa-edit" style="color:#006400"></i></a><span> </span>
                            <a class='btn btn-sm btn-outline-danger' wire:click="destroy({{ $item->id }})"><i class="fa fa-trash" style="color:#C11D1D"></i></a>
                          </td>
                      </tr>
                    @empty
                          <tr class="text-center">
                              <td colspan="6" class="py-3">No hay información</td>
                          </tr>
                    @endforelse
                </table>
            </div>
          </div>
          <!-- /.card-body -->
          <div class="card-footer" style="display: block;">
          </div>
    </div>

</div>
@push('js')
    <script>
        $(document).ready(function(){
            window.livewire.on('selectUpdates', data => {
                $(".banks_select").val(data.bankid).trigger('change');
                $(".bankaccounts_select").val(data.bankaccount_id).trigger('change');
                $(".method_select").val(data.transaction_method_id).trigger('change');
            });
            $('.banks_select').select2();
            $('.banks_select').on('change', function (e){
                let data = $('.banks_select').select2("val");
                @this.set('bankid', data);
            });
            $('.bankaccounts_select').select2();
            $('.bankaccounts_select').on('change', function (e){
                let data = $('.bankaccounts_select').select2("val");
                @this.set('bankaccount_id', data);
            });
            $('.method_select').select2();
            $('.method_select').on('change', function (e){
                let data = $('.method_select').select2("val");
                @this.set('transaction_method_id', data);
            });
        })
    </script>
@endpush
