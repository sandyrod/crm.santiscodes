<div>
       <div class="card card-default">
          <div class="card-header">
            <h3 class="card-title">Roles</h3>
            <div class="card-tools">
                <span wire:loading>
                    <a href="#" class="btn btn-tool btn-sm text-primary hint--top" aria-label="Procesando...">
                        <i class='nav-icon fas fa-circle-notch fa-2x fa-spin fa-fw'></i>
                    </a>
                </span>
              <button type="button" class="btn btn-tool" data-card-widget="collapse">
                <i class="fas fa-minus"></i>
              </button>
              <button type="button" class="btn btn-tool" data-card-widget="remove">
                <i class="fas fa-remove"></i>
              </button>
            </div>
          </div>
          <div class="card-body" style="display: block;">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                              <label>Nombre del Rol</label>
                              <input class="form-control" wire:model.debounce.5000ms="name" type="text" placeholder="Rol...">
                              @error('code')
                                  <span class="text-danger">{{ $message }}</span>
                              @enderror
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                              <label>Permisos
                              </label>
                                  <div wire:ignore>
                                      <select class="form-control select2 permissions_select" multiple="multiple" >
                                            <option value="">Seleccione...</option>
                                            @forelse($permissions as $item)
                                                @if (sizeof($permission_id) && in_array($item->id, $permission_id))
                                                    <option selected="selected" value="{{$item->id}}">{{$item->name}}</option>
                                                @else
                                                    <option value="{{$item->id}}">{{$item->name}}</option>
                                                @endif
                                            @empty

                                            @endforelse
                                      </select>
                                  </div>
                            </div>
                        </div>
                    </div>


            </div>

          <div class="card-footer" style="display: block;">
            <a href="#" wire:click="save" class="btn btn-primary">Guardar</a>
            <a href="#" wire:click="resetInput" class="btn btn-outline-danger">Cancelar</a>
        </div>
    </div>

    <div class="card card-default">
          <div class="card-header">
            <h3 class="card-title">Roles</h3>
            <div class="card-tools">
              <a href="{{url('print-products')}}" target="_blank" class="btn btn-tool" ><i class="fas fa-print"></i></a>
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
              <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i></button>
            </div>
          </div>

          <div class="card-body" style="display: block;">
            <div class="form-group">
                <input class="form-control" wire:model="search" type="text" placeholder="Buscar...">
            </div>
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover" >
                    <tr>
                        <th>Rol</th>
                        <th>Permisos</th>
                        <th>Opciones</th>
                    </tr>

                    @forelse($data as $item)
                      <tr>
                          <td>{{ $item->name }}</td>
                          <td>
                            @foreach( $item->permissions as $cat )
                                <span class="badge badge-info badge-sm"> {{ $cat->name }}</span>
                            @endforeach
                          </td>
                          <td>
                            <a class='btn btn-sm btn-outline-success' wire:click="edit({{ $item->id }})"><i class="fa fa-edit" style="color:#006400"></i></a><span> </span>
                            <a class='btn btn-sm btn-outline-danger' wire:click="destroy({{ $item->id }})"><i class="fa fa-trash" style="color:#C11D1D"></i></a>
                          </td>
                      </tr>
                    @empty
                          <tr class="text-center">
                              <td colspan="4" class="py-3">No hay información</td>
                          </tr>
                    @endforelse
                </table>
            </div>
          </div>
          <!-- /.card-body -->
          <div class="card-footer" style="display: block;">
          </div>
    </div>

</div>


 @push('js')

    <script>

        $(document).ready(function(){
            window.livewire.on('selectUpdates', data => {
                $(".permissions_select").val(data.permission_id).trigger('change');
            });

            $('.permissions_select').select2();
            $('.permissions_select').on('change', function (e){
                let data = $('.permissions_select').select2("val");
                @this.set('permission_id', data);
            });

        });
    </script>

@endpush
