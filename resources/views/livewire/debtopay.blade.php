<div>
    <div class="card card-default">
      <div class="card-header">
        <h3 class="card-title">Cuentas x Pagar</h3>

        <div class="card-tools">
            <span wire:loading>
                <a href="#" class="btn btn-tool btn-sm text-primary hint--top" aria-label="Procesando...">
                    <i class='nav-icon fas fa-circle-notch fa-2x fa-spin fa-fw'></i>
                </a>
            </span>

            <button type="button" class="btn btn-tool" data-card-widget="collapse">
                <i class="fas fa-minus"></i>
            </button>
              <button type="button" class="btn btn-tool" data-card-widget="remove">
                <i class="fas fa-remove"></i>
              </button>
            </div>
        </div>

        <div class="card-body" style="display: block;">
            <div class="row">
                <div class="col-md-4">
                  <div class="form-group">
                    <input type="hidden" wire:model.defer="receivable_id">
                    <label>Proveedor</label>
                    <div wire:ignore>
                      <select class="form-control select2 provider_select">
                      <option value="">Seleccione...</option>
                      @forelse($providers as $item)
                        <option value="{{ $item->id }}">
                            {{ $item->name }}
                            {{ $item->rif }}
                        </option>
                      @empty
                      @endforelse
                      </select>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Compra o Factura</label>
                    <div wire:ignore>
                      <select class="form-control select2 order_select">
                      <option value="">Seleccione...</option>
                      @forelse($orders as $item)
                        <option value="{{$item->id}}">
                            {{ $item->id }} -
                            {{ $item->created_at }}
                        </option>
                      @empty
                      @endforelse
                      </select>
                      @error('provider_id')
                          <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                  </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                      <label>Monto</label>
                      <input class="form-control" wire:model.defer="amount" type="text" placeholder="9,999.99">
                      @error('amount')
                          <span class="text-danger">{{ $message }}</span>
                      @enderror
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2">
                    <div class="form-group">
                        <label>
                            Fecha Vencimiento
                        </label>
                        <input class="form-control" wire:model.defer="duedate_at" type="date">
                        @error('duedate_at')
                          <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label>
                            Status
                        </label>
                        <div wire:ignore>
                      <select class="form-control select2 status_select">
                      <option value="">Seleccione...</option>
                      @forelse($statuslist as $item => $value)
                        <option value="{{ $item }}">
                            {{ $value }}
                        </option>
                      @empty
                      @endforelse
                      </select>
                      @error('status')
                          <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label>
                            Fecha de Cierre
                        </label>
                        <input class="form-control" wire:model.defer="close_at" type="date" id="close_at">
                        @error('close_at')
                          <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                      <label>Descripción</label>
                      <input class="form-control" wire:model.defer="description" type="text" placeholder="Descripción...">
                      @error('description')
                          <span class="text-red-500 text-xs italic">{{ $message }}</span>
                      @enderror
                    </div>
                </div>
              </div>
          </div>

          <div class="card-footer" style="display: block;">
            <a href="#" wire:click="save" class="btn btn-primary">Guardar</a>

            <a href="#" wire:click="resetInput" class="btn btn-outline-danger">Cancelar</a>
        </div>
    </div>

    <div class="card card-default">
          <div class="card-header">
            <h3 class="card-title">Cuentas por Pagar</h3>
            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
              <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i></button>
            </div>
          </div>

          <div class="card-body" style="display: block;">
            <div class="form-group">
                <input class="form-control" wire:model="search" type="text" placeholder="Buscar...">
            </div>
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover" >
                    <tr>
                        <th>Id</th>
                        <th>Proveedor</th>
                        <th>Orden</th>
                        <th>Monto</th>
                        <th>Opciones</th>
                    </tr>

                    @forelse($data as $item)
                      <tr>
                          <td>{{ $item->id }}</td>
                          <td>{{ $item->provider->name ?? '' }}</td>
                          <td>
                            {{ $item->purchaseorder->id ?? '' }} -
                            {{ $item->purchaseorder->number ?? '' }} -
                            {{ $item->purchaseorder->created_at ?? '' }}
                        </td>
                          <td>{{ $item->amount }}</td>
                          <td>
                            <a class='btn btn-sm btn-outline-success' wire:click="edit({{ $item->id }})"><i class="fa fa-edit" style="color:#006400"></i></a><span> </span>
                            <a class='btn btn-sm btn-outline-danger' wire:click="destroy({{ $item->id }})"><i class="fa fa-trash" style="color:#C11D1D"></i></a>
                          </td>
                      </tr>
                    @empty
                          <tr class="text-center">
                              <td colspan="4" class="py-3">No hay información</td>
                          </tr>
                    @endforelse
                </table>
            </div>
          </div>
          <!-- /.card-body -->
          <div class="card-footer" style="display: block;">
          </div>
    </div>
</div>
@push ('js')
    <script>
        $(document).ready(function(){
            window.livewire.on('selectUpdates', data => {
                $(".provider_select").val(data.provider_id).trigger('change');
                $(".order_select").val(data.order_id).trigger('change');
                $(".status_select").val(data.status).trigger('change');
            });

            $('.provider_select').select2();
            $('.provider_select').on('change', function (e){
                let data = $('.provider_select').select2("val");
                @this.set('provider_id', data);
            });
            $('.order_select').select2();
            $('.order_select').on('change', function (e){
                let data = $('.order_select').select2("val");
                @this.set('order_id', data);
            });
            $('.status_select').select2();
            $('.status_select').on('change', function (e){
                let data = $('.status_select').select2("val");
                @this.set('status', data);
            });
        });
    </script>
@endpush
