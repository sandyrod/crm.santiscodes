<div>
	   <div class="card card-default">
	      <div class="card-header">
	        <h3 class="card-title">Stock</h3>

	        <div class="card-tools">
		        <span wire:loading>
	           		<a href="#" class="btn btn-tool btn-sm text-primary hint--top" aria-label="Procesando...">
				        <i class='nav-icon fas fa-circle-notch fa-2x fa-spin fa-fw'></i>
				    </a>
				</span>
	          <button type="button" class="btn btn-tool" data-card-widget="collapse">
	            <i class="fas fa-minus"></i>
	          </button>
	          <button type="button" class="btn btn-tool" data-card-widget="remove">
	            <i class="fas fa-remove"></i>
	          </button>
	        </div>
	      </div>

	      <div class="card-body" style="display: block;">

		        	<div class="row">
			          <div class="col-md-6">
			            <div class="form-group">
			              <label>Producto</label>
			              <div wire:ignore>
				              <select class="form-control select2 products_select">
							    <option value="">Seleccione...</option>
				             		@foreach($products as $item)
					             		<option value="{{$item->id}}">{{$item->code}} {{$item->name}}</option>
						            @endforeach
				                </select>
				            </div>

			              @error('product_id')
			                  <span class="text-danger">{{ $message }}</span>
			              @enderror

			            </div>
			          </div>

			           <div class="col-md-6">
			            <div class="form-group">
			              <label>Almacén</label>
			              <div wire:ignore>
				              <select class="form-control select2 warehouses_select">
							    <option value="">Seleccione...</option>
				             		@foreach($warehouses as $item)
					             		<option value="{{$item->id}}">{{$item->name}}</option>
						            @endforeach
				                </select>
				            </div>
			              @error('warehouse_id')
			                  <span class="text-danger">{{ $message }}</span>
			              @enderror
			            </div>
			          </div>

			      </div>
			      <div class="row">

			          <div class="col-md-4 col-sm-12 col-xs-12">
			          	<div class="form-group">
			              	<label>Descripción</label>
				            <input class="form-control" wire:model.debounce.5000ms="description" type="text" placeholder="Descripción...">

			              @error('description')
			                  <span class="text-danger">{{ $message }}</span>
			              @enderror

			            </div>
			          </div>

			          <div class="col-md-4 col-sm-12 col-xs-12">
			            <div class="form-group">
			              <label>Operación</label>
			              <select class="form-control" wire:model="operation">
						   		<option value="1">Entrada (+)</option>
			             		<option value="0">Salida (-)</option>
					        </select>

			              @error('operation')
			                  <span class="text-danger">{{ $message }}</span>
			              @enderror

			            </div>
			          </div>

			          <div class="col-md-4 col-sm-12 col-xs-12">
				            <div class="form-group">
				             	<label>Cantidad</label>
				              <input class="form-control" wire:model.debounce.5000ms="quantity" type="text" placeholder="Cantidad...">
				              @error('quantity')
				                  <span class="text-danger">{{ $message }}</span>
				              @enderror
				               @if ($error_stock)
				              	<div class="text-red">{{$error_stock}} ({{$act}})</div>
				              @endif
				            </div>
			          </div>
			      </div>

            </div>

	      <div class="card-footer" style="display: block;">
	        <a href="#" wire:click="save" class="btn btn-primary">Guardar</a>

	        <a href="#" wire:click="resetInput" class="btn btn-outline-danger">Cancelar</a>
	    </div>
    </div>

	<div class="card card-default">
	      <div class="card-header">
	        <h3 class="card-title">Stock</h3>
	        <div class="card-tools">
	        	<a href="{{url('print-stocks')}}" target="_blank" class="btn btn-tool" ><i class="fas fa-print"></i></a>
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
	          <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i></button>
	        </div>
	      </div>

	      <div class="card-body" style="display: block;">
	      	<div class="form-group">
           		<input class="form-control" wire:model="search" type="text" placeholder="Buscar...">
           	</div>
	      	<div class="table-responsive">
                <table class="table table-striped table-bordered table-hover" >
		            <tr>
		                <th>Producto</th>
		                <th>Almacén</th>
		                <th>Cantidad</th>
		            </tr>

		            @forelse($data as $item)
		              <tr>
		                  <td>{{ $item->product->name ?? '' }}</td>
		                  <td>{{ $item->warehouse->name ?? '' }}</td>
		                  <td>{{ $item->quantity ?? 0 }}</td>
		              </tr>
		            @empty
		                  <tr class="text-center">
		                      <td colspan="4" class="py-3">No hay información</td>
		                  </tr>
		            @endforelse
		        </table>
		    </div>
	      </div>
	      <!-- /.card-body -->
	      <div class="card-footer" style="display: block;">
	      </div>
	</div>

</div>


 @push('js')

	<script>

		$(document).ready(function(){

			window.livewire.on('selectUpdates', data => {
			    $(".products_select").val(data.products_id).trigger('change');
			    $(".warehouses_select").val(data.warehouses_id).trigger('change');
			});

			$('.products_select').select2();
			$('.products_select').on('change', function (e){
				let data = $('.products_select').select2("val");
				@this.set('product_id', data);
				Livewire.emit('getStock');
			});
			$('.warehouses_select').select2();
			$('.warehouses_select').on('change', function (e){
				let data = $('.warehouses_select').select2("val");
				@this.set('warehouse_id', data);
				Livewire.emit('getStock');
			});

		});

	</script>

@endpush
