<div>
	   <div class="card card-default">
	      <div class="card-header">
	        <h3 class="card-title">Productos</h3>

	        <div class="card-tools">
		        <span wire:loading>
	           		<a href="#" class="btn btn-tool btn-sm text-primary hint--top" aria-label="Procesando...">
				        <i class='nav-icon fas fa-circle-notch fa-2x fa-spin fa-fw'></i>
				    </a>
				</span>

	          <button type="button" class="btn btn-tool" data-card-widget="collapse">
	            <i class="fas fa-minus"></i>
	          </button>
	          <button type="button" class="btn btn-tool" data-card-widget="remove">
	            <i class="fas fa-remove"></i>
	          </button>
	        </div>
	      </div>

	      <div class="card-body" style="display: block;">

		        	<div class="row">
		        		<div class="col-md-3">
				            <div class="form-group">
				              <label>Codigo</label>
				              <input class="form-control" wire:model.defer="code" type="text" placeholder="Codigo...">
				              @error('code')
				                  <span class="text-danger">{{ $message }}</span>
				              @enderror
				            </div>
			          </div>
			          <div class="col-md-3">
			            <div class="form-group">
			              <label>Nombre</label>
			              <input class="form-control" wire:model.defer="name" type="text" placeholder="Nombre...">
			              @error('name')
			                  <span class="text-danger">{{ $message }}</span>
			              @enderror

			            </div>
			          </div>
			          <div class="col-md-6">
			            <div class="form-group">
			              <label>Categorías
			              </label>
				              <div wire:ignore>
					              <select class="form-control select2 categories_select" multiple="multiple" >
									    <option value="">Seleccione...</option>
					             		@forelse($categories as $item)
					             			@if (sizeof($categories_id) && in_array(strval($item->id), $categories_id))
						             			<option selected="selected" value="{{$item->id}}">{{$item->name}}</option>
						             		@else
						             			<option value="{{$item->id}}">{{$item->name}}</option>
							            	@endif
							            @empty

							            @endforelse
					              </select>
					          </div>
			            </div>
			          </div>
			        </div>

			        <div class="row">
			        	<div class="col-md-3 col-sm-12 col-xs-12">
				            <div class="form-group">
				              <label>Unidades</label>
				              <div wire:ignore>
					              <select class="form-control select2 units_select" multiple="multiple" >
									    <option value="">Seleccione...</option>
					             		@forelse($units as $item)
						             			<option value="{{$item->id}}">{{$item->name}}</option>
							            @empty

							            @endforelse
					              </select>

								</div>
				            </div>
				        </div>
				        <div class="col-md-3 col-sm-12 col-xs-12">
				            <div class="form-group">
				              <label>UE</label>
				              <input class="form-control" wire:model.defer="ue" type="text" placeholder="UE...">
				              @error('ue')
				                  <span class="text-danger">{{ $message }}</span>
				              @enderror
				            </div>
				        </div>

			          <div class="col-md-6 col-sm-12 col-xs-12">
			            <div class="form-group">
			              <label>Proveedores</label>
			              <div wire:ignore>
				              <select class="form-control select2 providers_select" multiple="multiple" >
								    <option value="">Seleccione...</option>
				             		@forelse($providers as $item)
					             			<option value="{{$item->id}}">{{$item->name}}</option>
						            @empty

						            @endforelse
				           	   </select>
							</div>
			            </div>
			          </div>
			      </div>
			      <div class="row">
			      	<div class="col-md-2 col-sm-12 col-xs-12">
			      		<div class="form-group">
			              <label>Base imponible</label>
			              <input class="form-control" wire:model.defer="price" type="text" placeholder="" id="price">
			              @error('price')
			                  <span class="text-danger">{{ $message }}</span>
			              @enderror

			            </div>
			      	</div>
			      	<div class="col-md-2 col-sm-12 col-xs-12">
			      		<div class="form-group">
			              <label>Impuesto</label>
			              <input class="form-control" wire:model.defer="tax" type="text" placeholder="0" id="tax">
			              @error('tax')
			                  <span class="text-danger">{{ $message }}</span>
			              @enderror

			            </div>
			      	</div>
			      	<div class="col-md-2 col-sm-12 col-xs-12">
			      		<div class="form-group">
			              <label>Precio de Venta</label>
			              <input class="form-control" wire:model.defer="sale_price" type="text" placeholder="0" id="sale_price">
			              @error('sale_price')
			                  <span class="text-danger">{{ $message }}</span>
			              @enderror

			            </div>
			      	</div>
			      	<div class="col-md-6 col-sm-12 col-xs-12">
			      		<div class="form-group">
			              <label>Descripción</label>
			              <input class="form-control" wire:model.defer="description" type="text" placeholder="Descripcion...">
			              @error('description')
			                  <span class="text-danger">{{ $message }}</span>
			              @enderror

			            </div>
			      	</div>
			      </div>

			     @if ($product_id)
               		<form wire:submit.prevent="savePhoto">
              	 		<div class="row">
	                		<div class="col-md-4 col-sm-12 col-xs-12">
	                				<input type="file" class="tn btn-outline-primary btn-block" wire:model="photo">
									@error('photo') <span class="text-danger">{{ $message }}</span> @enderror
							</div>

	                		<div class="col-md-4 col-sm-12 col-xs-12">
	                			@if ($photo)
	                				<button class="btn btn-outline-primary" type="submit">Agregar Foto</button>
	                			@endif
	                		</div>
    	        		</div>
    	            </form>
                	<div class="row">
			            <div class="col-12 col-sm-6">
			              <div class="col-12 product-image-thumbs">


			              	@foreach($product_photos as $product_photo)
				            <div>
				                <div class="product-image-thumb active">
				                	<img width="100" src="{{asset('storage/'.$product_photo->photo)}}" alt="Foto">
				                </div> <br />
				                <div class="text-center">
					                @if  ($product_photo->main)
					                	<i class="fa fa-check" style="color:#28D432"></i>
					                @else
					                	<a class='btn btn-sm btn-outline-success' wire:click="setMainPhoto({{ $product_photo->id }})">
					                		<i class="fa fa-eye" style="color:#8B9466"></i>
					                	</a>
	                          		@endif
				                	<a class='btn btn-sm btn-outline-danger' wire:click="deletePhoto({{ $product_photo->id }})">
				                		<i class="fa fa-trash" style="color:#C11D1D"></i>
				                	</a>
				                </div>

				            </div>
				            @endforeach

			              </div>
			            </div>
			        </div>

         		@endif

            </div>

	      <div class="card-footer" style="display: block;">
	        <a href="#" wire:click="save" class="btn btn-primary">Guardar</a>

	        <a href="#" wire:click="resetInput" class="btn btn-outline-danger">Cancelar</a>
	    </div>
    </div>

	<div class="card card-default">
	      <div class="card-header">
	        <h3 class="card-title">Listado de Productos</h3>
	        <div class="card-tools">
              <a href="{{url('print-products')}}" target="_blank" class="btn btn-tool" ><i class="fas fa-print"></i></a>
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
	          <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i></button>
	        </div>
	      </div>

	      <div class="card-body" style="display: block;">
	      	<div class="form-group">
           		<input class="form-control" wire:model="search" type="text" placeholder="Buscar...">
           	</div>
	      	<div class="table-responsive">
                <table class="table table-striped table-bordered table-hover" >
		            <tr>
		                <th>Codigo</th>
		                <th>Producto</th>
		                <th>Categorias</th>
		                <th>Presentación</th>
		                <th>Precio Venta</th>
		                <th>Opciones</th>
		            </tr>

		            @forelse($data as $item)
		              <tr>
		                  <td>{{ $item->code }}</td>
		                  <td>{{ $item->name }}</td>
		                  <td>
		                  	@foreach( $item->categories as $cat )
	                            <span class="badge badge-info badge-sm"> {{ $cat->name }}</span>
		                  	@endforeach
		                  </td>
		                  <td>
		                  	@foreach( $item->units as $unit )
		                  		<span class="badge badge-warning badge-sm"> {{ $unit->name }}</span>
		                  		{{ $unit->capacity }}L
		                  	@endforeach
		                  	<span class="badge badge-info badge-sm"> {{ $item->ue }} </span>
		                  </td>
		                  <td>{{ $item->sale_price }}</td>
		                  <td>
		                  	<a class='btn btn-sm btn-outline-success' wire:click="edit({{ $item->id }})"><i class="fa fa-edit" style="color:#006400"></i></a><span> </span>
			                <a class='btn btn-sm btn-outline-danger' wire:click="destroy({{ $item->id }})"><i class="fa fa-trash" style="color:#C11D1D"></i></a>
		                  </td>
		              </tr>
		            @empty
		                  <tr class="text-center">
		                      <td colspan="4" class="py-3">No hay información</td>
		                  </tr>
		            @endforelse
		        </table>
		    </div>
	      </div>
	      <!-- /.card-body -->
	      <div class="card-footer" style="display: block;">
	      </div>
	</div>

</div>


 @push('js')

	<script>
		$(document).ready(function(){
			window.livewire.on('selectUpdates', data => {
			    $(".categories_select").val(data.categories_id).trigger('change');
			    $(".providers_select").val(data.providers_id).trigger('change');
			    $(".units_select").val(data.units_id).trigger('change');
			});

			$('.categories_select').select2();
			$('.categories_select').on('change', function (e){
				let data = $('.categories_select').select2("val");
				@this.set('categories_id', data);
			});
			$('.providers_select').select2();
			$('.providers_select').on('change', function (e){
				let data = $('.providers_select').select2("val");
				@this.set('providers_id', data);
			});
			$('.units_select').select2();
			$('.units_select').on('change', function (e){
				let data = $('.units_select').select2("val");
				@this.set('units_id', data);
			});

			$('#price').on('change', function(e){
				getSalePrice()
			});
			$('#tax').on('change', function(e){
				getSalePrice()
			});

			function getSalePrice(){
				if($('#tax').val()>=1)
					$('#sale_price').val($('#price').val()*( ($('#tax').val() / 100)+1 ));
					@this.set('sale_price',$('#sale_price').val());
			}

		});
	</script>

@endpush
