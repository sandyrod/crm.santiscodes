<div>
    <div class="card card-default">
        <div class="card-header">
            <h3 class="card-title">Ficha de Visita: {{ $visit_id }}</h3>

            <div class="card-tools">
                <span wire:loading>
                    <a href="#" class="btn btn-tool btn-sm text-primary hint--top" aria-label="Procesando...">
                        <i class='nav-icon fas fa-circle-notch fa-2x fa-spin fa-fw'></i>
                    </a>
                </span>
              <button type="button" class="btn btn-tool" data-card-widget="collapse">
                <i class="fas fa-minus"></i>
              </button>
              <button type="button" class="btn btn-tool" data-card-widget="remove">
                <i class="fas fa-remove"></i>
              </button>
            </div>
        </div>
        <div class="card-body">
                <div class="row">
                    <div class="col-sm-12 col-4">
                        @if (@$cliente)
                          <label>Cliente: </label><br />
                          <span>{{ $cliente }}</span>
                          <input type="hidden" name="client_id" id="client_id">
                            @error('seller_name')
                                <span class="text-danger">{{ $message }}</span>
                            @enderror
                          @else
                            <div wire:ignore>
                              <label>Cliente: </label>
                              <select class="form-control select2 clients_select">
                                <option value="">Seleccione...</option>
                                  @forelse($clients as $item)
                                    <option value="{{$item->id}}">{{$item->businessname}}</option>
                                  @empty

                                  @endforelse
                                </select>
                                @if($error)
                                    <span class="text-danger">{{ $error }}</span>
                                @endif
                            </div>
                          @endif
                    </div>
                    <div class="col-sm-12 col-4">
                        <label>Vendedor: </label><br />
                        <span>{{ auth()->user()->name }}</span>
                        <input type="hidden" name="seller_id" id="seller_id">
                          @error('seller_name')
                              <span class="text-danger">{{ $message }}</span>
                          @enderror
                    </div>
                    <div class="col-sm-12">
                      <div class="form-group">
                        <label>DOCUMENTO</label>
                        <div wire:ignore>
                            <select class="form-control select2 document">
                        <option value="NOTA DE DESPACHO">NOTA DE DESPACHO</option>
                        <option value="FACTURA">FACTURA</option>
                            </select>
                      </div>
                      </div>
                    </div>
                    <div class="col-sm-12 col-4">

                        <button wire:click="generateOrder">
                          Generar Nuevo Pedido
                        </button>
                    </div>
                </div>
        </div>
    </div>
    <div class="card card-default">
          <div class="card-header">
            <h3 class="card-title">Listado de Productos</h3>
            <div class="card-tools">
              <a href="{{url('print-products')}}" target="_blank" class="btn btn-tool" ><i class="fas fa-print"></i></a>
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
              <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i></button>
            </div>
          </div>

          <div class="card-body" style="display: block;">
            <div class="row">
              <div class="col-6">
                <div class="form-group">
                  <input class="form-control" wire:model="search" type="text" placeholder="Buscar...">
                </div>
              </div>
              <div class="col-6">
                  <a href="#" wire:click="save" class="btn btn-primary">Guardar</a>
              </div>
            </div>
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover" >
                    <tr>
                        <th width="10%">Codigo</th>
                        <th width="50%">Producto</th>
                        <th width="15%">Presentación</th>
                        <th width="5%">Prom/Mes</th>
                        <th width="5%">D</th>
                        <th width="5%">I</th>
                        <th width="5%">S</th>
                        <th width="5%">V</th>
                    </tr>

                    @forelse($data as $index => $item)
                      <tr>
                          <td>{{ $item->code }}</td>
                          <td>{{ $item->name }}</td>
                          <td>
                            @foreach( $item->units as $unit )
                                <span class="badge badge-warning badge-sm"> {{ $unit->name }}</span>
                                {{ $unit->capacity }}L
                            @endforeach
                            <span class="badge badge-info badge-sm"> {{ $item->ue }} </span>
                          </td>
                          <td>

                          </td>
                          <td>
                            <input type="number" size="3" min="0" maxlength="3"  wire:model.defer="products_sel.{{ $item->id }}.distribution" class="inputVisit">
                          </td>
                          <td>
                            <input type="number" size="3" min="0" maxlength="3"  wire:model.defer="products_sel.{{ $item->id }}.inventory" class="inputVisit">
                          </td>
                          <td>
                            <input type="number" size="3" min="0" maxlength="3" wire:model.defer="products_sel.{{ $item->id }}.suggested" class="inputVisit">
                          </td>
                          <td><input type="number" size="3" min="0" maxlength="3" wire:model.defer="products_sel.{{ $item->id }}.sold" class="inputVisit">
                          </td>
                      </tr>
                    @empty
                          <tr class="text-center">
                              <td colspan="4" class="py-3">No hay información</td>
                          </tr>
                    @endforelse
                </table>
            </div>
          </div>
          <!-- /.card-body -->
          <div class="card-footer" style="display: block;">
            <a href="#" wire:click="save" class="btn btn-primary">Guardar</a>
          </div>
    </div>
</div>


 @push('js')

  <script>

    $(document).ready(function(){

      $('.clients_select').select2();
      $('.clients_select').on('change', function (e){
        let data = $('.clients_select').select2("val");
        @this.set('client_id', data);
      });
     
    });
  </script>

@endpush
