<div>
       <div class="card card-default">
          <div class="card-header">
            <h3 class="card-title">Despacho</h3>

            <div class="card-tools">
                <span wire:loading>
                    <a href="#" class="btn btn-tool btn-sm text-primary hint--top" aria-label="Procesando...">
                        <i class='nav-icon fas fa-circle-notch fa-2x fa-spin fa-fw'></i>
                    </a>
                </span>

              <button type="button" class="btn btn-tool" data-card-widget="collapse">
                <i class="fas fa-minus"></i>
              </button>
              <button type="button" class="btn btn-tool" data-card-widget="remove">
                <i class="fas fa-remove"></i>
              </button>
            </div>
          </div>

          <div class="card-body" style="display: block;">
            <div class="row">
              <div class="col-md-3">
                <div class="form-group">
                  <div wire:ignore>
                    <label>Pedido</label>
                    <div wire:ignore>
                        <select class="form-control select2 orders_select" >
                            <option value="">Seleccione...</option>
                            @forelse($orders as $item)
                                    <option value="{{$item->id}}">{{$item->id}} - {{ $item->client->rif }}</option>
                            @empty

                            @endforelse
                        </select>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                    <label>Cliente</label>
                    <input value="{{ $client }}" class="form-control">
                    <input type="hidden" name="client_id" id="client_id">
                      @error('client')
                          <span class="text-danger">{{ $message }}</span>
                      @enderror
                </div>
              </div>
              <div class="col-md-3">
                <label>Usuario</label>
                <input value="{{ auth()->user()->name }}" class="form-control">
                <input type="hidden" name="seller_id" id="seller_id">
                  @error('seller_name')
                      <span class="text-danger">{{ $message }}</span>
                  @enderror
              </div>
              <div class="col-md-3 col-sm-12 col-xs-12">
                    <div class="form-group">
                        <label>Almacen</label>
                        <input value="{{ $warehouse_name }}" class="form-control">
                        <input type="hidden" name="warehouse_id" id="warehouse_id">
                          @error('seller_name')
                              <span class="text-danger">{{ $message }}</span>
                          @enderror
                    </div>
              </div>
            </div>

            <div class="row">
              @if ($updateMode)
                <div>
              @else
                <div class="hide">
              @endif
              <hr />
                   <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                          <label>Producto</label>
                          <input class="form-control" wire:model.debounce.5000ms="product_name" type="text" placeholder="0">
                          <input class="form-control" wire:model.debounce.5000ms="product_id" type="hidden">
                          <input class="form-control" wire:model.debounce.5000ms="detail_id" type="hidden">
                          @error('product_name')
                              <span class="text-danger">{{ $message }}</span>
                          @enderror
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-12 col-xs-12">
                        <div class="form-group">
                          <label>Solicitada</label>
                          <input class="form-control" wire:model.debounce.5000ms="requested" type="text">
                          @error('requested')
                              <span class="text-danger">{{ $message }}</span>
                          @enderror
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-12 col-xs-12">
                        <div class="form-group">
                          <label>Entregada</label>
                          <input class="form-control" wire:model.debounce.5000ms="quantity" type="text"  name="quantity" id="quantity">
                          @error('quantity')
                              <span class="text-danger">{{ $message }}</span>
                          @enderror
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-12 col-xs-12">
                      <br />
                      <a href="#" wire:click="addProduct" class="btn btn-success btn-block"> Agregar</a>
                    </div>
                  </div> 

                <div class="row">
                <div class="col-md-12">
                  <div class="table-responsive">
                          <table class="table table-striped table-bordered table-hover" >
                          <tr>
                              <th>Producto</th>
                              <th>Solicitada</th>
                              <th>Entregada</th>
                              <th>Faltante</th>
                              <th>OPC</th>
                          </tr>
                        @if ($updateMode)
                        @php $total=0; @endphp
                          @forelse(@$dispatchdetails as $item)
                            <tr>
                                <td>{{ $item->product->name ?? '' }}</td>
                                <td>{{ $item->requested ?? '' }}</td>
                                <td>{{ $item->quantity ?? '' }}</td>
                                <td>{{ $item->requested - $item->quantity  ?? 0 }}</td>
                                <td>
                                  <a class='btn btn-sm btn-outline-success' wire:click="editProduct({{ $item->id }})"><i class="fa fa-edit" style="color:#006400"></i></a><span> </span>
                                </td>
                            </tr>
                            @php $total+=$item->requested - $item->quantity ?? 0; @endphp
                          @empty
                                <tr class="text-center">
                                    <td colspan="7" class="py-3">No hay información</td>
                                </tr>
                          @endforelse
                        @endif
                        <tr>
                          <td></td>
                          <td></td>
                          <th>Total faltante: </th>
                          <th>{{ $total ?? 0 }}</th>
                          <td></td>
                        </tr>
                      </table>
                  </div>
                </div>
              </div>
              </div>
            </div>


            <div class="card-footer" style="display: block;">
                {{-- <a href="#" wire:click="save" class="btn btn-primary">Guardar</a> --}}
                <a href="#" wire:click="resetInput" class="btn btn-outline-danger">Cancelar</a>
            </div>
        </div>

    <div class="card card-default">
        <div class="card-header">
          <h3 class="card-title">Salidas - Despachos</h3>
          <div class="card-tools">
            <a href="{{url('print-stocks')}}" target="_blank" class="btn btn-tool" ><i class="fas fa-print"></i></a>
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
            <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i></button>
          </div>
        </div>

        <div class="card-body" style="display: block;">
          <div class="form-group">
              <input class="form-control" wire:model="search" type="text" placeholder="Buscar...">
            </div>
          <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover" >
                <tr>
                    <th>DESPACHO</th>
                    <th>PEDIDO</th>
                    <th>LITROS</th>
                    <th>STATUS</th>
                    <th>ALMACEN</th>
                    <th>OPCIONES</th>
                </tr>

                @forelse($data as $item)
                  <tr>
                      <td>{{ $item->id ?? '' }}</td>
                      <td>{{ $item->order_id ?? '' }}</td>
                      <td>{{ $item->user->name ?? '' }}</td>
                      <td>{{ $item->status ?? '' }}</td>
                      <td>{{ $item->warehouse->name ?? '' }}</td>
                      <td>
                        <a class='btn btn-sm btn-outline-success' wire:click="edit({{ $item->id }})"><i class="fa fa-edit" style="color:#006400"></i></a><span> </span>
                        <a class='btn btn-sm btn-outline-danger' wire:click="destroy({{ $item->id }})"><i class="fa fa-trash" style="color:#C11D1D"></i></a>
                      </td>
                  </tr>
                @empty
                      <tr class="text-center">
                          <td colspan="6" class="py-3">No hay información</td>
                      </tr>
                @endforelse
            </table>
        </div>
        </div>

        <div class="card-footer" style="display: block;">
        </div>
  </div>

</div>


 @push('js')

    <script>

        $(document).ready(function(){

            window.livewire.on('selectUpdates', data => {
                $(".orders_select").val(data.order_id).trigger('change');

            });

            $('.orders_select').select2();
            $('.orders_select').on('change', function (e){
                let data = $('.orders_select').select2("val");
                @this.set('order_id', data);
                Livewire.emit('getOrder');
            });

        });
    </script>

@endpush
