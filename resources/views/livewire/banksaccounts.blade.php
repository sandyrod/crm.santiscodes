<div>
       <div class="card card-default">
          <div class="card-header">
            <h3 class="card-title">Cuentas Bancarias</h3>

            <div class="card-tools">
                <span wire:loading>
                    <a href="#" class="btn btn-tool btn-sm text-primary hint--top" aria-label="Procesando...">
                        <i class='nav-icon fas fa-circle-notch fa-2x fa-spin fa-fw'></i>
                    </a>
                </span>

              <button type="button" class="btn btn-tool" data-card-widget="collapse">
                <i class="fas fa-minus"></i>
              </button>
              <button type="button" class="btn btn-tool" data-card-widget="remove">
                <i class="fas fa-remove"></i>
              </button>
            </div>
          </div>

          <div class="card-body" style="display: block;">
            <div class="row">
                <div class="col-md-4">
                  <div class="form-group">
                    <label>Banco</label>
                    <div wire:ignore>
                      <select class="form-control select2 banks_select">
                      <option value="">Seleccione...</option>
                      @forelse($banks as $item)
                        <option value="{{$item->id}}">{{$item->name}}</option>
                      @empty
                      @endforelse
                      </select>
                    </div>
                  </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                      <label>Numero</label>
                      <input class="form-control" wire:model.defer="number" type="text" placeholder="">
                      <input class="form-control" wire:model.defer="bankaccount_id" type="hidden">
                      @error('number')
                          <span class="text-danger">{{ $message }}</span>
                      @enderror
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                      <label>Titular</label>
                      <input class="form-control" wire:model.defer="owner" type="text" placeholder="">
                      @error('owner')
                          <span class="text-danger">{{ $message }}</span>
                      @enderror
                    </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label>
                            Tipo de Cuenta
                        </label>
                        <br>
                        <input wire:model.defer="account_type" name="account_type" type="radio" value="1" />
                        Ahorro
                        <input wire:model.defer="account_type" name="account_type" type="radio" value="0" /> Corriente - Cheques
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                      <label>Saldo</label>
                      <input class="form-control" wire:model.defer="amount" type="text" placeholder="">
                      @error('amount')
                          <span class="text-danger">{{ $message }}</span>
                      @enderror
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                      <label>IBAN</label>
                      <input class="form-control" wire:model.defer="iban" type="text" placeholder="">
                      @error('iban')
                          <span class="text-danger">{{ $message }}</span>
                      @enderror
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                      <label>Nro de Ruta</label>
                      <input class="form-control" wire:model.defer="route" type="text" placeholder="">
                      @error('route')
                          <span class="text-danger">{{ $message }}</span>
                      @enderror
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                      <label>Swift</label>
                      <input class="form-control" wire:model.defer="swift" type="text" placeholder="">
                      @error('swift')
                          <span class="text-danger">{{ $message }}</span>
                      @enderror
                    </div>
                </div>
              </div>
          </div>

          <div class="card-footer" style="display: block;">
            <a href="#" wire:click="save" class="btn btn-primary">Guardar</a>

            <a href="#" wire:click="resetInput" class="btn btn-outline-danger">Cancelar</a>
        </div>
    </div>

    <div class="card card-default">
          <div class="card-header">
            <h3 class="card-title">Cuentas Bancarias</h3>
            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
              <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i></button>
            </div>
          </div>

          <div class="card-body" style="display: block;">
            <div class="form-group">
                <input class="form-control" wire:model="search" type="text" placeholder="Buscar...">
            </div>
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover" >
                    <tr>
                        <th>Id</th>
                        <th>Banco</th>
                        <th>Tipo de Cuenta</th>
                        <th>Titular</th>
                        <th>Saldo</th>
                        <th>Opciones</th>
                    </tr>

                    @forelse($data as $item)
                      <tr>
                          <td>{{ $item->id }}</td>
                          <td>{{ $item->bank->name }}</td>
                          <td>{{ $item->account_type == 0 ? 'Corriente' : 'Ahorro' }}</td>
                          <td>{{ $item->owner }}</td>
                          <td>{{ $item->amount }}</td>
                          <td>
                            <a class='btn btn-sm btn-outline-success' wire:click="edit({{ $item->id }})"><i class="fa fa-edit" style="color:#006400"></i></a><span> </span>
                            <a class='btn btn-sm btn-outline-danger' wire:click="destroy({{ $item->id }})"><i class="fa fa-trash" style="color:#C11D1D"></i></a>
                          </td>
                      </tr>
                    @empty
                          <tr class="text-center">
                              <td colspan="6" class="py-3">No hay información</td>
                          </tr>
                    @endforelse
                </table>
            </div>
          </div>
          <!-- /.card-body -->
          <div class="card-footer" style="display: block;">
          </div>
    </div>

</div>
@push('js')
    <script>
        $(document).ready(function(){
            window.livewire.on('selectUpdates', data => {
                $(".banks_select").val(data.bank_id).trigger('change');
            });

            $('.banks_select').select2();
            $('.banks_select').on('change', function (e){
                let data = $('.banks_select').select2("val");
                @this.set('bank_id', data);
            });
        })
    </script>
@endpush
