<div>
       <div class="card card-default">
          <div class="card-header">
            <h3 class="card-title">Compras</h3>

            <div class="card-tools">
                <span wire:loading>
                    <a href="#" class="btn btn-tool btn-sm text-primary hint--top" aria-label="Procesando...">
                        <i class='nav-icon fas fa-circle-notch fa-2x fa-spin fa-fw'></i>
                    </a>
                </span>

              <button type="button" class="btn btn-tool" data-card-widget="collapse">
                <i class="fas fa-minus"></i>
              </button>
              <button type="button" class="btn btn-tool" data-card-widget="remove">
                <i class="fas fa-remove"></i>
              </button>
            </div>
          </div>

          <div class="card-body" style="display: block;">
            <div class="row">
              <div class="col-md-3">
                <div class="form-group">
                  <div wire:ignore>
                    <label>Nro de Factura/OC/otro</label>
                    <input class="form-control" type="text" placeholder="1234" name="number" id="number" wire:model.debounce.5000ms="number">
                    @error('number')
                        <span class="text-danger">{{ $message }}</span>
                    @enderror
                  </div>
                </div>
              </div>
              <div class="col-md-3">
                <label>Usuario: </label>
                <input value="{{ auth()->user()->name }}" class="form-control">
                <input type="hidden" name="seller_id" id="seller_id">
                  @error('seller_name')
                      <span class="text-danger">{{ $message }}</span>
                  @enderror
              </div>
              <div class="col-md-3 col-sm-12 col-xs-12">
                    <div class="form-group">
                      <label>Proveedor</label>
                      <div wire:ignore>
                          <select class="form-control select2 providers_select" >
                                <option value="">Seleccione...</option>
                                @forelse($providers as $item)
                                        <option value="{{$item->id}}">{{$item->name}}</option>
                                @empty

                                @endforelse
                          </select>

                        </div>
                    </div>
              </div>
              <div class="col-md-3 col-sm-12 col-xs-12">
                <div class="form-group">
                  <label>Almacén</label>
                  <div wire:ignore>
                    <select class="form-control select2 warehouses_select">
                      <option value="">Seleccione...</option>
                      @foreach($warehouses as $item)
                        <option value="{{$item->id}}">{{$item->name}}</option>
                      @endforeach
                    </select>
                  </div>
                  @error('warehouse_id')
                      <span class="text-danger">{{ $message }}</span>
                  @enderror
                </div>
              </div>
            </div>

            <div class="row">
              @if ($updateMode)
                <div>
              @else
                <div class="hide">
              @endif
              <hr />
                  <div class="row">
                    <div class="col-md-6">
                          <div class="form-group">
                            <label>Producto</label>
                            <div wire:ignore>
                                <select class="form-control select2 products_select">
                                  <option value="">Seleccione...</option>
                                      @foreach($products as $item)
                                          <option value="{{$item->id}}">{{$item->code}} {{$item->name}}</option>
                                      @endforeach
                                  </select>
                              </div>
                            @error('product_id')
                                <span class="text-danger">{{ $message }}</span>
                            @enderror
                          </div>
                    </div>
                    <div class="col-md-2 col-sm-12 col-xs-12">
                        <div class="form-group">
                          <label>Precio</label>
                          <input class="form-control" wire:model.debounce.5000ms="price" type="text" placeholder="0">
                          @error('price')
                              <span class="text-danger">{{ $message }}</span>
                          @enderror
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-12 col-xs-12">
                        <div class="form-group">
                          <label>Cantidad</label>
                          <input class="form-control" wire:model.debounce.5000ms="quantity" type="text" placeholder="1" min="1">
                          @error('quantity')
                              <span class="text-danger">{{ $message }}</span>
                          @enderror
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-12 col-xs-12">
                      <br />
                      <a href="#" wire:click="addProduct" class="btn btn-success btn-block"> Agregar</a>
                    </div>
                  </div>

                  <div class="row">
                <div class="col-md-12">
                  <div class="table-responsive">
                          <table class="table table-striped table-bordered table-hover" >
                          <tr>
                              <th>DESCRIPCIÓN</th>
                              <th>PRES.</th>
                              <th>CANT.</th>
                              <th>PRECIO UE</th>
                              <th>SUBTOTAL</th>
                              <th>OPC</th>
                          </tr>
                        @if ($updateMode)
                        @php $total=0; @endphp
                          @forelse(@$purchasedetails as $item)
                            <tr>
                                <td>{{ $item->product->name ?? '' }}</td>
                                <td>{{ $item->unit->name ?? '' }}|{{ $item->product->ue ?? '' }}</td>
                                <td>{{ $item->quantity ?? '' }}</td>
                                <td>{{ $item->price ?? 0 }}</td>
                                <td>{{ $item->quantity * $item->price ?? 0 }}</td>
                                <td>
                                  <a class='btn btn-sm btn-outline-danger' wire:click="destroyProduct({{ $item->id }})"><i class="fa fa-trash" style="color:#C11D1D"></i></a>
                                </td>
                            </tr>
                            @php $total+=$item->quantity * $item->price ?? 0; @endphp
                          @empty
                                <tr class="text-center">
                                    <td colspan="7" class="py-3">No hay información</td>
                                </tr>
                          @endforelse
                        @endif
                        <tr>
                          <td></td>
                          <td></td>
                          <td></td>
                          <th>Total: </th>
                          <th>{{ $total ?? 0 }}</th>
                          <td></td>
                        </tr>
                      </table>
                  </div>
                </div>
              </div>

              </div>
            </div>
          <div class="card-footer" style="display: block;">
            <a href="#" wire:click="save" class="btn btn-primary">Guardar</a>

            <a href="#" wire:click="resetInput" class="btn btn-outline-danger">Cancelar</a>
        </div>
    </div>

    <div class="card card-default">
        <div class="card-header">
          <h3 class="card-title">Entradas - Compras</h3>
          <div class="card-tools">
            <a href="{{url('print-stocks')}}" target="_blank" class="btn btn-tool" ><i class="fas fa-print"></i></a>
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
            <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i></button>
          </div>
        </div>

        <div class="card-body" style="display: block;">
          <div class="form-group">
              <input class="form-control" wire:model="search" type="text" placeholder="Buscar...">
            </div>
          <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover" >
                <tr>
                    <th>ID</th>
                    <th>NUMERO</th>
                    <th>USUARIO</th>
                    <th>PROVEEDOR</th>
                    <th>ALMACEN</th>
                    <th>OPCIONES</th>
                </tr>

                @forelse($data as $item)
                  <tr>
                      <td>{{ $item->id ?? '' }}</td>
                      <td>{{ $item->number ?? '' }}</td>
                      <td>{{ $item->user->name ?? '' }}</td>
                      <td>{{ $item->provider->name ?? '' }}</td>
                      <td>{{ $item->warehouse->name ?? '' }}</td>
                      <td>
                        <a class='btn btn-sm btn-outline-success' wire:click="edit({{ $item->id }})"><i class="fa fa-edit" style="color:#006400"></i></a><span> </span>
                        <a class='btn btn-sm btn-outline-danger' wire:click="destroy({{ $item->id }})"><i class="fa fa-trash" style="color:#C11D1D"></i></a>
                      </td>
                  </tr>
                @empty
                      <tr class="text-center">
                          <td colspan="6" class="py-3">No hay información</td>
                      </tr>
                @endforelse
            </table>
        </div>
        </div>

        <div class="card-footer" style="display: block;">
        </div>
  </div>

</div>


 @push('js')

    <script>

        $(document).ready(function(){

            window.livewire.on('selectUpdates', data => {
                $(".providers_select").val(data.providers_id).trigger('change');
                $(".warehouses_select").val(data.warehouses_id).trigger('change');
            });

            window.livewire.on('selectDetailUpdates', data => {
                $(".products_select").val(data.product_id).trigger('change');
            });

            $('.providers_select').select2();
            $('.providers_select').on('change', function (e){
                let data = $('.providers_select').select2("val");
                @this.set('providers_id', data);
            });

            $('.warehouses_select').select2();
            $('.warehouses_select').on('change', function (e){
              let data = $('.warehouses_select').select2("val");
              @this.set('warehouses_id', data);
              Livewire.emit('getStock');
            });

            $('.products_select').select2();
            $('.products_select').on('change', function (e){
              let data = $('.products_select').select2("val");
              @this.set('product_id', data);
              Livewire.emit('getStock');
            });

        });
    </script>

@endpush
