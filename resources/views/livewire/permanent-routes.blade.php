<div>
    <div class="card card-default">
          <div class="card-header">
            <h3 class="card-title">Planificacion de Ruta</h3>
            <div class="card-tools">
              {{--<a href="{{url('print-products')}}" target="_blank" class="btn btn-tool" ><i class="fas fa-print"></i></a> --}}
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
              <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i></button>
            </div>
          </div>

          <div class="card-body" style="display: block;">
            {{-- <div class="form-group">
                <input class="form-control" wire:model="search" type="text" placeholder="Buscar...">
            </div> --}}

            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover" >
                    <tr>
                        <th>Orden</th>
                        <th>Cliente</th>
                        <th>Direccion</th>
                        @role('ADMIN')
                        <th>Fecha</th>
                        @endrole
                        <th>Opciones</th>
                    </tr>

                    @forelse($data as $item)
                      @role('ADMIN')
                        @if($item->visit_at == date('Y-m-d'))
                        <tr style="background: #007bff;">
                        @else
                        <tr>
                        @endif
                      @else
                        <tr>
                      @endrole
                          <td>{{ $item->order }}</td>
                          <td>{{ $item->businessname }}</td>
                          <td>{{ $item->municipality }} {{ $item->urbanization }}</td>
                          @role('ADMIN')
                          <td>{{ $item->visit_at}}
                          @endrole
                          <td>
                            <a class='btn btn-sm btn-outline-success' href="{{ route('clients', $item->id) }}">
                              <i class="fa fa-user"></i>
                            </a>
                            @if (substr($item->rif,0,1)!='T')
                            <a class='btn btn-sm btn-outline-success' href="{{ route('visitcard', $item->id) }}">
                              <i class="fa fa-edit" style="color:#006400"></i>
                            </a>
                            @endif
                            @role('ADMIN')
                            @else
                            @foreach($visit as $visited)
                              @if($visited->client_id == $item->id)
                                <i class="fa fa-user-check" style="color:#006400" title="Visitado" alt="Visitado"></i>
                              @endif
                            @endforeach
                            @endrole
                            <span></span>
                            {{-- <a class='btn btn-sm btn-outline-danger' wire:click="destroy({{ $item->id }})"><i class="fa fa-trash" style="color:#C11D1D"></i></a> --}}
                          </td>
                      </tr>
                    @empty
                          <tr class="text-center">
                              <td colspan="4" class="py-3">No hay información</td>
                          </tr>
                    @endforelse
                </table>
            </div>
          </div>
          <!-- /.card-body -->
          <div class="card-footer" style="display: block;">
          </div>
    </div>
</div>
