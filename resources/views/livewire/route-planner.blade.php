<div>
    <div class="card card-default">
        <div class="card-header">
            <h3 class="card-title">Planificador de Rutas</h3>

            <div class="card-tools">
                <span wire:loading>
                    <a href="#" class="btn btn-tool btn-sm text-primary hint--top" aria-label="Procesando...">
                        <i class='nav-icon fas fa-circle-notch fa-2x fa-spin fa-fw'></i>
                    </a>
                </span>

                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-minus"></i>
                </button>
                <button type="button" class="btn btn-tool" data-card-widget="remove">
                    <i class="fas fa-remove"></i>
                </button>
            </div>
        </div>
        <div class="card-body" style="display: block;">
            <div class="row">
                <div class="col-md-2">
                    <div class="form-group">
                        <label>Ruta</label>
                        <div wire:ignore>
                            <select class="form-control select2 routes_select">
                                @forelse($routes as $item)
                                <option value="{{$item->id}}">{{$item->name}}</option>
                                @empty
                                @endforelse
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Vendedor</label>
                        <div wire:ignore>
                            <select class="form-control select2 users_select" multiple="multiple" >
                                  @forelse($users as $item)
                                    @if (sizeof($users_id) && in_array($item->id, $users_id))
                                      <option selected="selected" value="{{$item->id}}">{{$item->name}}</option>
                                    @else
                                      <option value="{{$item->id}}">{{$item->name}}</option>
                                    @endif
                                  @empty

                                  @endforelse
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                      <label>Clientes por Dia</label>
                      <input class="form-control" wire:model.defer="clientsperday" type="text" placeholder="">
                      @error('clientsperday')
                          <span class="text-danger">{{ $message }}</span>
                      @enderror
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Punto de Inicio</label>
                        <div wire:ignore>
                            <select class="form-control select2 clients_select">
                                <option value="">Seleccione...</option>
                                @forelse($clients as $item)
                                <option value="{{ $item->id }}">{{ $item->businessname }} - {{ $item->rif }}</option>
                                @empty
                                @endforelse
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                  <div class="form-group">
                    <label>
                      Dia de inicio
                    </label>
                    <input class="form-control" wire:model.defer="begin_at" type="date">
                    @error('begin_at')
                    <span class="text-danger">{{ $message }}</span>
                    @enderror
                  </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                      <label>Nombre de esta planificacion</label>
                      <input class="form-control" wire:model.defer="name" type="text" placeholder="">
                      @error('name')
                          <span class="text-danger">{{ $message }}</span>
                      @enderror
                    </div>
                </div>
                <div class="col-sm-12 col-md-9">
                  <label class="mx-2 px-2">Lunes</label>
                  <input name="monday" value="{{ $monday }}" wire:model.defer="monday" type="checkbox">
                  <label class="mx-2 px-2">Martes</label>
                  <input name="tuesday" value="{{ $tuesday }}" wire:model.defer="tuesday" type="checkbox" class="checkbox">
                  <label class="mx-2 px-2">Miercoles</label>
                  <input name="wednesday" value="{{ $wednesday }}" wire:model.defer="wednesday" type="checkbox" class="checkbox">
                  <label class="mx-2 px-2">Jueves</label>
                  <input name="thursday" value="{{ $thursday }}" wire:model.defer="thursday" type="checkbox" class="checkbox">
                  <label class="mx-2 px-2">Viernes</label>
                  <input name="friday" value="{{ $friday }}" wire:model.defer="friday" type="checkbox" class="checkbox">
                  <label class="mx-2 px-2">Sabado</label>
                  <input name="saturday" value="{{ $saturday }}" wire:model.defer="saturday" type="checkbox" class="checkbox">
                  <label class="mx-2 px-2">Domingo</label>
                  <input name="sunday" value="{{ $sunday }}" wire:model.defer="sunday" type="checkbox" class="checkbox">
                </div>
            </div>
        </div>

        <div class="card-footer" style="display: block;">
            <a href="#" wire:click="save" class="btn btn-primary">Guardar</a>
            <a href="#" wire:click="generatePlan" class="btn btn-primary">Generar Plan</a>
            <a href="#" wire:click="resetInput" class="btn btn-outline-danger">Cancelar</a>
        </div>
    </div>
    <div class="card card-default">
        <div class="card-header">
            <h3 class="card-title">Rutas planificadas</h3>
            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
              <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i></button>
            </div>
        </div>

          <div class="card-body" style="display: block;">
            <div class="form-group">
                <input class="form-control" wire:model="search" type="text" placeholder="Buscar...">
            </div>
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover" >
                    <tr>
                        <th>Id</th>
                        <th>Nombre de la Ruta</th>
                        <th>Codigo de la ruta</th>
                        <th>Cliente inicial</th>
                        <th>Opciones</th>
                    </tr>

                    @forelse($data as $item)
                      <tr>
                          <td>{{ $item->id }}</td>
                          <td>{{ $item->name }}</td>
                          <td>{{ $item->distributionroute_id + 100 }}</td>
                          <td>{{ $item->client->businessname ?? '' }}</td>
                          <td>
                            <a class='btn btn-sm btn-outline-success' wire:click="edit({{ $item->id }})"><i class="fa fa-edit" style="color:#006400"></i></a><span> </span>
                            <a class='btn btn-sm btn-outline-danger' wire:click="destroy({{ $item->id }})"><i class="fa fa-trash" style="color:#C11D1D"></i></a>
                          </td>
                      </tr>
                    @empty
                          <tr class="text-center">
                              <td colspan="5" class="py-3">No hay información</td>
                          </tr>
                    @endforelse
                </table>
            </div>
          </div>
          <!-- /.card-body -->
          <div class="card-footer" style="display: block;">
          </div>
    </div>
</div>
@push('js')
    <script>
        $(document).ready(function(){
            window.livewire.on('selectUpdates', data => {
                $(".routes_select").val(data.routeid).trigger('change');
                $(".clients_select").val(data.client_id).trigger('change');
                $(".users_select").val(data.users_id).trigger('change');
            });

            $('.routes_select').select2();
            $('.routes_select').on('change', function (e){
                let data = $('.routes_select').select2("val");
                @this.set('routeid', data);
            });

            $('.users_select').select2();
            $('.users_select').on('change', function (e){
                let data = $('.users_select').select2("val");
                @this.set('users_id', data);
            });

            $('.clients_select').select2();
            $('.clients_select').on('change', function (e){
                let data = $('.clients_select').select2("val");
                @this.set('client_id', data);
            });

            window.livewire.on('updateclients', data =>{
              var options = [];
              $.each(data.data, function (key, value) {
                   options.push({
                       text: value.businessname + ' - ' + value.rif,
                       id: value.id
                   });
              });
              $('.clients_select').empty().select2({data: options});
            });

            // $('.checkbox').on('change', function (e){
            //     @this.set('tuesday', 1);
            // })

        });
          document.addEventListener('livewire:load', function (event) {
            @this.on('refreshDropdown', function () {
                // $('.clients_select').select2();
                // console.log('ejecutando refreshDropdown');
            });
        });
    </script>

@endpush
