<div>
	   <div class="card card-default">
	      <div class="card-header">
	        <h3 class="card-title">Proveedores</h3>

	        <div class="card-tools">
		        <span wire:loading>
	           		<a href="#" class="btn btn-tool btn-sm text-primary hint--top" aria-label="Procesando...">
				        <i class='nav-icon fas fa-circle-notch fa-2x fa-spin fa-fw'></i>
				    </a>
				</span>

	          <button type="button" class="btn btn-tool" data-card-widget="collapse">
	            <i class="fas fa-minus"></i>
	          </button>
	          <button type="button" class="btn btn-tool" data-card-widget="remove">
	            <i class="fas fa-remove"></i>
	          </button>
	        </div>
	      </div>

	      <div class="card-body" style="display: block;">
	        <div class="row">
	          <div class="col-md-4">
	            <div class="form-group">
	              <label>Nombre</label>
	              <input class="form-control" wire:model.debounce.5000ms="name" type="text" placeholder="Nombre...">
	              @error('name')
	                  <span class="text-danger">{{ $message }}</span>
	              @enderror
	            </div>
	          </div>
			  <div class="col-md-4">
	            <div class="form-group">
	              <label>Rif</label>
	              <input class="form-control" wire:model="rif" type="text" id="rif">
	              @error('rif')
	                  <span class="text-danger">{{ $message }}</span>
	              @enderror
	            </div>
	          </div>
	          <div class="col-md-4">
	            <div class="form-group">
	              <label>Teléfono</label>
	              <input class="form-control" wire:model.debounce.5000ms="phone" type="text" placeholder="Teléfono...">
	              @error('phone')
	                  <span class="text-danger">{{ $message }}</span>
	              @enderror
	            </div>
	          </div>
	        </div>
	        <div class="row">
	          <div class="col-md-4">
	            <div class="form-group">
	              <label>Email</label>
	              <input class="form-control" wire:model.debounce.3000ms="email" type="text" placeholder="Email...">
	              @error('email')
	                  <span class="text-danger">{{ $message }}</span>
	              @enderror
	            </div>
	          </div>
	          <div class="col-md-8">
	            <div class="form-group">
	              <label>Dirección</label>
	              <input class="form-control" wire:model.debounce.3000ms="address" type="text" placeholder="Dirección...">
	              @error('address')
	                  <span class="text-danger">{{ $message }}</span>
	              @enderror
	            </div>
	          </div>
	         

	        </div>

	      </div>

	      <div class="card-footer" style="display: block;">
	        <a href="#" wire:click="save" class="btn btn-primary">Guardar</a>

	        <a href="#" wire:click="resetInput" class="btn btn-outline-danger">Cancelar</a>
	    </div>
    </div>

	<div class="card card-default">
	      <div class="card-header">
	        <h3 class="card-title">Listado de Proveedores</h3>
	        <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
	          <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i></button>
	        </div>
	      </div>

	      <div class="card-body" style="display: block;">
	      	<div class="form-group">
           		<input class="form-control" wire:model="search" type="text" placeholder="Buscar...">
           	</div>
	      	<div class="table-responsive">
                <table class="table table-striped table-bordered table-hover" >
		            <tr>
		                <th>Id</th>
		                <th>Proveedor</th>
		                <th>Rif</th>
		                <th>Teléfono</th>
		                <th>Email</th>
		                <th>Dirección</th>
		                <th>Opciones</th>
		            </tr>

		            @forelse($data as $item)
		              <tr>
		                  <td>{{ $item->id }}</td>
		                  <td>{{ $item->name }}</td>
		                  <td>{{ $item->rif }}</td>
		                  <td>{{ $item->phone }}</td>
		                  <td>{{ $item->email }}</td>
		                  <td>{{ $item->address }}</td>
		                  <td>
		                    <a class='btn btn-sm btn-outline-success' wire:click="edit({{ $item->id }})"><i class="fa fa-edit" style="color:#006400"></i></a><span> </span>
		                    <a class='btn btn-sm btn-outline-danger' wire:click="destroy({{ $item->id }})"><i class="fa fa-trash" style="color:#C11D1D"></i></a>
		                  </td>
		              </tr>
		            @empty
		                  <tr class="text-center">
		                      <td colspan="5" class="py-3">No hay información</td>
		                  </tr>
		            @endforelse
		        </table>
		    </div>
	      </div>
	      <!-- /.card-body -->
	      <div class="card-footer" style="display: block;">
	      </div>
	</div>

</div>
