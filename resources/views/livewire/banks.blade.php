<div>
    <div class="card card-default">
          <div class="card-header">
            <h3 class="card-title">Bancos</h3>

            <div class="card-tools">
                <span wire:loading>
                    <a href="#" class="btn btn-tool btn-sm text-primary hint--top" aria-label="Procesando...">
                        <i class='nav-icon fas fa-circle-notch fa-2x fa-spin fa-fw'></i>
                    </a>
                </span>

              <button type="button" class="btn btn-tool" data-card-widget="collapse">
                <i class="fas fa-minus"></i>
              </button>
              <button type="button" class="btn btn-tool" data-card-widget="remove">
                <i class="fas fa-remove"></i>
              </button>
            </div>
          </div>

          <div class="card-body" style="display: block;">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                      <label>Nombre</label>
                      <input class="form-control" wire:model.defer="name" type="text" placeholder="Nombre...">
                      @error('name')
                          <span class="text-danger">{{ $message }}</span>
                      @enderror
                    </div>
                </div>
                <div class="col-md-2">
                  <div class="form-group">
                    <label>
                      Codigo
                    </label>
                    <input class="form-control" wire:model.defer="code" type="text" placeholder="0000">
                    @error('code')
                      <span class="text-danger">{{ $message }}</span>
                    @enderror
                  </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label>
                            Tipo de Banco
                        </label>
                        <br>
                        <input wire:model="isNational" name="isNational" type="radio" value="1" /> Nacional
                        <input wire:model="isNational" name="isNational" type="radio" value="0" /> Extranjero
                    </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label>Pais</label>
                    <div wire:ignore>
                      <select class="form-control select2 countries_select">
                      <option value="">Seleccione...</option>
                      @forelse($countries as $item)
                        <option value="{{$item->id}}">{{$item->name}}</option>
                      @empty
                      @endforelse
                      </select>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                      <label>Descripción</label>
                      <input class="form-control" wire:model.defer="description" type="text" placeholder="Descripción...">
                      @error('description')
                          <span class="text-red-500 text-xs italic">{{ $message }}</span>
                      @enderror
                    </div>
                </div>
              </div>
          </div>

          <div class="card-footer" style="display: block;">
            <a href="#" wire:click="save" class="btn btn-primary">Guardar</a>

            <a href="#" wire:click="resetInput" class="btn btn-outline-danger">Cancelar</a>
        </div>
    </div>

    <div class="card card-default">
          <div class="card-header">
            <h3 class="card-title">Listado de Bancos</h3>
            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
              <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i></button>
            </div>
          </div>

          <div class="card-body" style="display: block;">
            <div class="form-group">
                <input class="form-control" wire:model="search" type="text" placeholder="Buscar...">
            </div>
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover" >
                    <tr>
                        <th>Id</th>
                        <th>Banco</th>
                        <th>Descripción</th>
                        <th>Opciones</th>
                    </tr>

                    @forelse($data as $item)
                      <tr>
                          <td>{{ $item->id }}</td>
                          <td>{{ $item->name }}</td>
                          <td>{{ $item->description }}</td>
                          <td>
                            <a class='btn btn-sm btn-outline-success' wire:click="edit({{ $item->id }})"><i class="fa fa-edit" style="color:#006400"></i></a><span> </span>
                            <a class='btn btn-sm btn-outline-danger' wire:click="destroy({{ $item->id }})"><i class="fa fa-trash" style="color:#C11D1D"></i></a>
                          </td>
                      </tr>
                    @empty
                          <tr class="text-center">
                              <td colspan="4" class="py-3">No hay información</td>
                          </tr>
                    @endforelse
                </table>
            </div>
          </div>
          <!-- /.card-body -->
          <div class="card-footer" style="display: block;">
          </div>
    </div>

</div>
