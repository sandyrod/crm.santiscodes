<div>
       <div class="card card-default">
          <div class="card-header">
            <h3 class="card-title">Usuarios</h3>
            <div class="card-tools">
                <span wire:loading>
                    <a href="#" class="btn btn-tool btn-sm text-primary hint--top" aria-label="Procesando...">
                        <i class='nav-icon fas fa-circle-notch fa-2x fa-spin fa-fw'></i>
                    </a>
                </span>
              <button type="button" class="btn btn-tool" data-card-widget="collapse">
                <i class="fas fa-minus"></i>
              </button>
              <button type="button" class="btn btn-tool" data-card-widget="remove">
                <i class="fas fa-remove"></i>
              </button>
            </div>
          </div>
          <div class="card-body" style="display: block;">
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                      <label>Nombres</label>
                      <input class="form-control" wire:model.debounce.5000ms="name" type="text" placeholder="Nombres...">
                      @error('code')
                          <span class="text-danger">{{ $message }}</span>
                      @enderror
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                      <label>Email</label>
                      <input class="form-control" wire:model.debounce.5000ms="email" type="text" placeholder="email@email...">
                      @error('email')
                          <span class="text-danger">{{ $message }}</span>
                      @enderror
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                      <label>Password</label>
                      <input class="form-control" wire:model.debounce.5000ms="password" type="password">
                      @error('password')
                          <span class="text-danger">{{ $message }}</span>
                      @enderror
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                      <label>
                      Roles
                      </label>
                          <div wire:ignore>
                              <select class="form-control select2 rol_select" multiple="multiple" >
                                    <option value="">Seleccione...</option>
                                    @forelse($rols as $item)
                                        @if (sizeof($rol_id) && in_array($item->id, $rol_id))
                                            <option selected="selected" value="{{$item->id}}">{{$item->name}}</option>
                                        @else
                                            <option value="{{$item->id}}">{{$item->name}}</option>
                                        @endif
                                    @empty

                                    @endforelse
                              </select>
                          </div>
                      </div>
                  </div>
            </div>
          </div>

          <div class="card-footer" style="display: block;">
            <a href="#" wire:click="save" class="btn btn-primary">Guardar</a>
            <a href="#" wire:click="resetInput" class="btn btn-outline-danger">Cancelar</a>
        </div>
    </div>

    <div class="card card-default">
          <div class="card-header">
            <h3 class="card-title">Usuarios</h3>
            <div class="card-tools">
              <a href="{{url('print-products')}}" target="_blank" class="btn btn-tool" ><i class="fas fa-print"></i></a>
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
              <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i></button>
            </div>
          </div>

          <div class="card-body" style="display: block;">
            <div class="form-group">
                <input class="form-control" wire:model="search" type="text" placeholder="Buscar...">
            </div>
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover" >
                    <tr>
                        <th>Usuario</th>
                        <th>Email</th>
                        <th>Roles</th>
                        <th>Actualizado</th>
                        <th>Opciones</th>
                    </tr>

                    @forelse($data as $item)
                      <tr>
                          <td>{{ $item->name }}</td>
                          <td>{{ $item->email }}</td>
                          <td>
                            @foreach( $item->rols as $cat )
                                <span class="badge badge-info badge-sm"> {{ $cat->rols[0]->name }}</span>
                            @endforeach</td>
                          <td>{{ $item->updated_at }}</td>
                          <td>
                            <a class="btn btn-sm btn-outline-success" wire:click="edit({{ $item->id }})"><i class="fa fa-edit" style="color:#006400"></i></a>
                            <a class="btn btn-sm btn-outline-danger" wire:click="destroy({{ $item->id }})"><i class="fa fa-trash" style="color:#C11D1D"></i></a>
                          </td>
                      </tr>
                    @empty
                          <tr class="text-center">
                              <td colspan="4" class="py-3">No hay información</td>
                          </tr>
                    @endforelse
                </table>
            </div>
          </div>
          <!-- /.card-body -->
          <div class="card-footer" style="display: block;">
          </div>
    </div>

</div>


@push('js')

    <script>

        $(document).ready(function(){
            window.livewire.on('selectUpdates', data => {
                $(".rol_select").val(data.rol_id).trigger('change');
            });

            $('.rol_select').select2();
            $('.rol_select').on('change', function (e){
                let data = $('.rol_select').select2("val");
                @this.set('rol_id', data);
            });

        });
    </script>

@endpush

