<div>
	   <div class="card card-default">
	      <div class="card-header">
	        <h3 class="card-title">Pedidos</h3>

	        <div class="card-tools">
		        <span wire:loading>
	           		<a href="#" class="btn btn-tool btn-sm text-primary hint--top" aria-label="Procesando...">
				        <i class='nav-icon fas fa-circle-notch fa-2x fa-spin fa-fw'></i>
				    </a>
				</span>

	          <button type="button" class="btn btn-tool" data-card-widget="collapse">
	            <i class="fas fa-minus"></i>
	          </button>
	          <button type="button" class="btn btn-tool" data-card-widget="remove">
	            <i class="fas fa-remove"></i>
	          </button>
	        </div>
	      </div>

	      <div class="card-body" style="display: block;">

		        	<div class="row">
			          <div class="col-md-6">
			            <div class="form-group">
			              <label>CLIENTE</label>
			              <div wire:ignore>
				              <select class="form-control select2 client_select">
							    <option value="">SELECCIONE...</option>
				             		@foreach($clients as $item)
					             		<option value="{{$item->id}}">{{$item->businessname}}</option>
						            @endforeach
				                </select>
				            </div>

			              @error('client_id')
			                  <span class="text-danger">{{ $message }}</span>
			              @enderror

			            </div>
			          </div>

			           <div class="col-md-3">
			            <div class="form-group">
			              <label>RUTA</label>
			              <div>
				          	<input disabled="disabled" type="text" id="route" name="route" class="form-control" wire:model.debounce.5000ms="route">
				          </div>
			            </div>
			          </div>

			          <div class="col-md-3">
			            <div class="form-group">
			              <label>DOCUMENTO</label>
			              <div wire:ignore>
				              	<select class="form-control select2 document">
							    	<option value="NOTA DE DESPACHO">NOTA DE DESPACHO</option>
							    	<option value="FACTURA">FACTURA</option>
				              	</select>
				          </div>
			            </div>
			          </div>

			      </div>

			      <div class="row">
			      	<div class="col-md-3">
			      		 <div class="form-group">
			              <label>FORMA DE PAGO</label>
			              <div wire:ignore>
				              	<select class="form-control select2 payment_select">
							    	<option value="EFECTIVO">EFECTIVO</option>
							    	<option value="TRANSFERENCIA">TRANSFERENCIA</option>
				              	</select>
				          </div>
			            </div>
			      	</div>
			      	<div class="col-md-3">
			      		<div class="form-group">
			      			<label>CONDICION DE PAGO</label>
			      			<div wire:ignore>
			      				<select class="form-control select2 payment_condition">
			      					<option value="PREPAGADO">PREPAGADO</option>
							    	<option value="CONTADO">CONTADO (EN DESPACHO)</option>
							    	<option value="FACILIDAD">FACILIDAD DE PAGO (VUELTA DE VEND.)</option>
							    	<option value="OTRO">OTRO</option>
			      				</select>
			      			</div>
			      		</div>
			      	</div>

			      	<div class="col-md-6">
			      		 <div class="form-group">
			              <label>ESPECIFIQUE</label>
			              <div>
				          	<input type="text" id="other" name="other" class="form-control" wire:model.debounce.5000ms="other">
				          </div>

			            </div>
			      	</div>
			      </div>

			      @if ($updateMode)
			      	<div class="row">
			      @else
			      	<div class="hide">
			      @endif
					    <hr />
					    <div class="row">
					    	<div class="col-md-5">
					    		<div class="form-group">
					              <label>PRODUCTO</label>
					              <div wire:ignore>
						              <select class="form-control select2 products_select">
									    <option value="">SELECCIONE...</option>
						             		@foreach(@$products as $item)
							             		<option value="{{$item->id}}">{{ $item->code }} - {{$item->name}}-{{ $item->ue }}</option>
								            @endforeach
						                </select>
						            </div>
					            </div>
					    	</div>
					    	<div class="col-md-2">
					    		<div class="form-group">
					              <label>Presentacion</label>
					              <div>
						              <input type="hidden" id="unit_id" name="unit_id" class="form-control" wire:model.debounce.5000ms="unit_id">
						              <input disabled="disabled" type="text" id="unit" name="unit" class="form-control" wire:model.debounce.5000ms="unit">
						            </div>
					            </div>
					    	</div>
					    	<div class="col-md-1">
					    		<div class="form-group">
					              <label>CANTIDAD</label>
					              <div wire:ignore>
						          	<input required oninput="setCustomValidity('')" oninvalid="this.setCustomValidity('Requerido')" type="number" id="quantity" name="quantity" value="1" class="form-control" wire:model.debounce.5000ms="quantity" min="1">
						          </div>
					            </div>
					    	</div>
					    	<div class="col-md-1">
					    		<div class="form-group">
					              <label>PRECIO</label>
					              <div>
						          	<input disabled="disabled"  type="text" id="price" name="price" value="1" class="form-control" wire:model.debounce.5000ms="price">
						          </div>
					            </div>
					    	</div>
					    	<div class="col-md-1">
					    		<div class="form-group">
					              <label>TOTAL</label>
					              <div>
						          	<input disabled="disabled"  type="text" id="total" name="total" value="1" class="form-control" >
						          </div>
					            </div>
					    	</div>
					    	<div class="col-md-2">
			      	    		<br />
	        					<a href="#" wire:click="addProduct" class="btn btn-success btn-block"> Agregar</a>
			      	    	</div>
					    </div>

					    <div class="row">
					    	<div class="col-md-12">
					    		<div class="table-responsive">
					                <table class="table table-striped table-bordered table-hover" >
							            <tr>
							                <th>DESCRIPCIÓN</th>
							                <th>PRES.</th>
							                <th>CANT.</th>
							                <th>PRECIO UE</th>
							                <th>SUBTOTAL</th>
							                <th>OPC</th>
							            </tr>
								      	@if ($updateMode)
								      	@php $total=0; @endphp
							            @forelse(@$orderdetails as $item)
							              <tr>
							                  <td>{{ $item->product->name ?? '' }}</td>
							                  <td>{{ $item->unit->name ?? '' }}|{{ $item->product->ue ?? '' }}</td>
							                  <td>{{ $item->quantity ?? '' }}</td>
							                  <td>{{ $item->price ?? 0 }}</td>
							                  <td>{{ $item->quantity * $item->price ?? 0 }}</td>
							                  <td>
							                    <a class='btn btn-sm btn-outline-danger' wire:click="destroyProduct({{ $item->id }})"><i class="fa fa-trash" style="color:#C11D1D"></i></a>
							                  </td>
							              </tr>
							              @php $total+=$item->quantity * $item->price ?? 0; @endphp
							            @empty
							                  <tr class="text-center">
							                      <td colspan="7" class="py-3">No hay información</td>
							                  </tr>
							            @endforelse
								      	@endif
								      	<tr>
								      		<td></td>
								      		<td></td>
								      		<td></td>
								      		<th>Total: </th>
								      		<th>{{ $total ?? 0 }}</th>
								      		<td></td>
								      	</tr>
							        </table>
							    </div>
					    	</div>
					    </div>
					</div>


            </div>

	      <div class="card-footer" style="display: block;">

	        <a href="#" wire:click="save" class="btn btn-primary">Guardar</a>

	        <a href="#" wire:click="resetInput" class="btn btn-outline-danger">Cancelar</a>
	    </div>
    </div>

	<div class="card card-default">
	      <div class="card-header">
	        <h3 class="card-title">Pedidos</h3>
	        <div class="card-tools">
	        	<a href="{{url('print-stocks')}}" target="_blank" class="btn btn-tool" ><i class="fas fa-print"></i></a>
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
	          <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i></button>
	        </div>
	      </div>

	      <div class="card-body" style="display: block;">
	      	<div class="form-group">
           		<input class="form-control" wire:model="search" type="text" placeholder="Buscar...">
           	</div>
	      	<div class="table-responsive">
                <table class="table table-striped table-bordered table-hover" >
		            <tr>
		                <th>ID</th>
		                <th>CLIENTE</th>
		                <th>RIF</th>
		                <th>USUARIO</th>
		                <th>LITROS.</th>
		                <th>OPCIONES</th>
		            </tr>

		            @forelse($data as $item)
		              <tr>
		                  <td>{{ $item->id ?? '' }}</td>
		                  <td>{{ $item->client->businessname ?? '' }}</td>
		                  <td>{{ $item->client->rif ?? '' }}</td>
		                  <td>{{ $item->user->name ?? '' }}</td>
		                  <td>{{ @\App\Repositories\OrdersRepository::getTotalLiters($item->id) }}</td>
		                  <td>
		                  	@if ($item->status==1)
		                    <a class='btn btn-sm btn-outline-success' wire:click="edit({{ $item->id }})"><i class="fa fa-edit" style="color:#006400"></i></a><span> </span>
		                    <a class='btn btn-sm btn-outline-danger' wire:click="destroy({{ $item->id }})"><i class="fa fa-trash" style="color:#C11D1D"></i></a>
		                    @else
		                    	@if(($item->status>1))
		                    		Despachada
		                    	@else
		                    		Anulada
		                    	@endif
		                    @endif
		                  </td>
		              </tr>
		            @empty
		                  <tr class="text-center">
		                      <td colspan="6" class="py-3">No hay información</td>
		                  </tr>
		            @endforelse
		        </table>
		    </div>
	      </div>

	      <div class="card-footer" style="display: block;">
	      </div>
	</div>

</div>


 @push('js')

	<script>

		$(document).ready(function(){
			window.livewire.on('selectUpdates', data => {
			    $(".client_select").val(data.client_id).trigger('change');
			    $(".payment_select").val(data.payment).trigger('change');
			    $(".payment_condition").val(data.paidCondition).trigger('change');
			    $(".document").val(data.document).tigger('change');
			});

			$('.client_select').select2();
			$('.client_select').on('change', function (e){
				let data = $('.client_select').select2("val");
				@this.set('client_id', data);
				Livewire.emit('changeClient');
			});
			$('.payment_select').select2();
			$('.payment_select').on('change', function (e){
				let data = $('.payment_select').select2("val");
				@this.set('payment', data);
			});

			$('.payment_condition').select2();
			$('.payment_condition').on('change', function (e){
				let data = $('.payment_condition').select2("val");
				@this.set('paidCondition', data);
			});

			window.livewire.on('selectDetailUpdates', data => {
			    $(".products_select").val(data.product_id).trigger('change');
			});

			$('.products_select').select2();
			$('.products_select').on('change', function (e){
				let data = $('.products_select').select2("val");
				@this.set('product_id', data);
				Livewire.emit('changeProduct');
			});

			$('#quantity').on('change', function (e){
				$('#total').val($('#quantity').val()*$('#price').val());
			});

			$('.document').select2();
			$('.document').on('change', function (e){
				let data = $('.document').select2("val");
				@this.set('document', data);
			});
		});

	</script>

@endpush
