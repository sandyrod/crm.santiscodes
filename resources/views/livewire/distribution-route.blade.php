<div>
           <div class="card card-default">
          <div class="card-header">
            <h3 class="card-title">Rutas</h3>

            <div class="card-tools">
                <span wire:loading>
                    <a href="#" class="btn btn-tool btn-sm text-primary hint--top" aria-label="Procesando...">
                        <i class='nav-icon fas fa-circle-notch fa-2x fa-spin fa-fw'></i>
                    </a>
                </span>

              <button type="button" class="btn btn-tool" data-card-widget="collapse">
                <i class="fas fa-minus"></i>
              </button>
              <button type="button" class="btn btn-tool" data-card-widget="remove">
                <i class="fas fa-remove"></i>
              </button>
            </div>
          </div>

          <div class="card-body" style="display: block;">

                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label>Nombre</label>
                          <input class="form-control" wire:model.debounce.5000ms="name" type="text" placeholder="Nombre...">
                          @error('name')
                              <span class="text-danger">{{ $message }}</span>
                          @enderror

                        </div>
                      </div>
                      <div class="col-md-6">
                        {{-- <div class="form-group">
                          <label>Vendedor</label>
                          <div wire:ignore>
                            <select class="form-control select2 users_select" multiple="multiple" >
                                  @forelse($users as $item)
                                    @if (sizeof($users_id) && in_array($item->id, $users_id))
                                      <option selected="selected" value="{{$item->id}}">{{$item->name}}</option>
                                    @else
                                      <option value="{{$item->id}}">{{$item->name}}</option>
                                    @endif
                                  @empty

                                  @endforelse
                                </select>
                            </div>

                        </div> --}}
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label>Descripción</label>
                          <input class="form-control" wire:model.debounce.5000ms="description" type="text" placeholder="Descripcion...">
                          @error('name')
                              <span class="text-danger">{{ $message }}</span>
                          @enderror

                        </div>
                      </div>
                    </div>

            </div>

          <div class="card-footer" style="display: block;">
            <a href="#" wire:click="save" class="btn btn-primary">Guardar</a>

            <a href="#" wire:click="resetInput" class="btn btn-outline-danger">Cancelar</a>
            </div>
        </div>

    <div class="card card-default">
          <div class="card-header">
            <h3 class="card-title">Listado de Rutas</h3>
            <div class="card-tools">
              {{--<a href="{{url('print-products')}}" target="_blank" class="btn btn-tool" ><i class="fas fa-print"></i></a> --}}
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
              <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i></button>
            </div>
          </div>

          <div class="card-body" style="display: block;">
            <div class="form-group">
                <input class="form-control" wire:model="search" type="text" placeholder="Buscar...">
            </div>
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover" >
                    <tr>
                        <th>Id</th>
                        <th>Ruta</th>
                        <th>Descripcion</th>
                        <th>Opciones</th>
                    </tr>

                    @forelse($data as $item)
                      <tr>
                          <td>{{ $item->id }}</td>
                          <td>{{ $item->name }}</td>
                          <td>{{ $item->description }}</td>
                          <td>
                            <a class="btn btn-sm btn-outline-success" wire:click="edit({{ $item->id }})"><i class="fa fa-edit" style="color:#006400"></i></a>
                            <a class="btn btn-sm btn-outline-danger" wire:click="destroy({{ $item->id }})"><i class="fa fa-trash" style="color:#C11D1D"></i></a>
                          </td>
                      </tr>
                    @empty
                          <tr class="text-center">
                              <td colspan="4" class="py-3">No hay información</td>
                          </tr>
                    @endforelse
                </table>
            </div>
          </div>
          <!-- /.card-body -->
          <div class="card-footer" style="display: block;">
          </div>
    </div>
</div>

@push('js')

  <script>

    $(document).ready(function(){

      window.livewire.on('selectUpdates', data => {
          $(".users_select").val(data.users_id).trigger('change');
      });

      $('.users_select').select2();
      $('.users_select').on('change', function (e){
        let data = $('.users_select').select2("val");
        @this.set('users_id', data);
      });

    });
  </script>

@endpush
