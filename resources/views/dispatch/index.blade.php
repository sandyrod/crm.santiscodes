@extends('layouts.dashboard')
@section('title', 'Despachos')
@section('content')
<div class="container">
    @section('content_header')
      Despachos
    @endsection

    <div class="container-fluid">

      @livewire('dispatch')

    </div>
</div>
@endsection
