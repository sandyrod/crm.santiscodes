@extends('layouts.dashboard')
@section('title', 'Planeacion de Rutas')
@section('content')
<div class="container">
    @section('content_header')
      Planeacion de Rutas
    @endsection

    <div class="container-fluid">
      @can('PLANIFICAR')
      @livewire('route-planner')
      @else
      No tiene permisos
      @endcan
    </div>
</div>
@endsection
