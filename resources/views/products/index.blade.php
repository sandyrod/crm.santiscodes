@extends('layouts.dashboard')
@section('title', 'Portafolio')
@section('content')
<div class="container">
    @section('content_header')
      Productos
    @endsection
    
    <div class="container-fluid">
      
      @livewire('products')

    </div>
</div>
@endsection
