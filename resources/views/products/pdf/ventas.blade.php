@extends('layouts.print')

@section('titulo', $title)

@section('content')

  <div class="row">
    <div class="col-12">
      <table class="table table-striped" style="border: solid 1px; width:100%;">
        <tr style="background-color:#e9ecef;">
          <th style="width: 100px;">Nro</th>
          <th style="width: 250px;">Codigo</th>
          <th style="width: 460px;">Producto</th>
          <th style="width: 70px;" >Litros</th>
          <th style="width: 90px;" >Cantidad</th>
          <th style="width: 100px;">Venta en $</th>
        </tr>

        <?php $sum = 0; ?>
        @foreach($venta as $v)
          <tr style="background-color: {{ $i%2 ? '':'#e9ecef' }};">
            <td style="text-align: center;">{{ $i++ }}
            <td style="text-align: center;">{{$v->code}}</td>
            <td style="text-align: left;">{{$v->name}}</td>
            <td style="text-align: right;">{{$v->capacity}}</td> 
            <td style="text-align: right;">{{$v->quantity}}</td>                                                                        
            <td style="text-align: right;">{{ number_format($v->tota, 2 ) }}</td>
          </tr>
        <?php $sum += $v->tota; ?>       
        @endforeach               
      </table>    
      <div class="col-md-3" style="position: absolute;  right: 175px;"><br>
        <b>Total:</b>
      </div>
      <div class="col-md-3" style="position: absolute;  right: 0px;"><br>
        {{ number_format($sum, 2 ) }}        
      </div>
      
    </div>
  </div>  
@endsection
