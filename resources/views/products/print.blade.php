@extends('layouts.print')

@section('titulo', 'LISTADO DE PRODUCTOS')

@section('content')

  <div class="row">
    <div class="col-12 table-responsive">
      <table class="table table-striped" style="border: solid 1px; width:100%;">
        <tr style="border: solid 1px;">
          <th style="width: 40%;">Productos</th>
          <th style="width: 60%;">Categorias</th>
        </tr>
        
        @foreach($products as $product)
            <tr>
              <td>{{$product->name}}</td>
              <td>
                @foreach( $product->categories as $cat )
                  <span> | {{ $cat->name }}</span>
                @endforeach
              </td>
            </tr>
        @endforeach
        
      </table>
    </div>
  </div>      
        
@endsection
