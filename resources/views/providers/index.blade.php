@extends('layouts.dashboard')
@section('title', 'Proveedores')
@section('content')
<div class="container">
    @section('content_header')
        Proveedores de Producto
    @endsection
    
    <div class="container-fluid">
      
      @livewire('providers')

    </div>
</div>
@endsection
