@extends('layouts.print_landscape')


@section('content')

  <div class="row">
    <div class="col-12 table-responsive">
      <table class="table table-striped" style="border: solid 1px; width:100%;">
        <tr>
          <th colspan="11">
            {{ $title }}
          </th>
        </tr>
        <tr style="background-color:#e9ecef;">
          <th style="">Nro</th>
          <th style="">Nombre de Cliente</th>
          <th style="">Canal</th>
          <th style="">Tipo</th>
          <th style="">Vende Lubricantes</th>
          <th style="">Principales marcas visibles</th>
          <th style="">Tiene Chronus</th>
          <th style="">Municipio</th>
          <th style="">Localidad</th>
          <th style="">Calle/Avenida</th>
          <th style="">Entre calle y calle</th>
        </tr>
        @foreach($clients as $item)
            <tr style="background-color: {{ $i%2 ? '':'#e9ecef' }};">
              <td>{{ $i++ }}
              <td>{{ $item->businessname }}</td>
              <td>{{ @$item->distribution_channels->name}}</td>
              <td>{{ @$item->business_type->name }}</td>
              <td align="center">{{ $item->sells_lubricants ? 'SI' : 'NO' }}</td>
              <td>{{ $item->branda }}-{{ $item->brandb }} - {{ $item->brandc }}</td>
              <td>{{ $item->sells_chronus ? 'SI' : 'NO' }}</td>
              <td>{{ $item->address()->first()->municipality }}</td>
              <td>{{ $item->address()->first()->urbanization }} {{ $item->address()->first()->sector }}</td>
              <td>{{ $item->address()->first()->street }} </td>
              <td>{{ $item->address()->first()->street }} </td>
            </tr>
        @endforeach
      </table>
    </div>
  </div>

@endsection
