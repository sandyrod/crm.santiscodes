@extends('layouts.dashboard')
@section('title', 'Censo')
@section('content')
<div class="container">
    @section('content_header')
    Censo
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif

    @if (session('errors'))
        <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
              {{ $error }} <br>
            @endforeach
        </div>
    @endif
    @endsection
    <div class="container-fluid">
      @can('censar')
        <form method="post" action="{{ route('census_save') }}">
            @csrf

            <div class="card card-default">
                <div class="card-header">
                  <h3 class="card-title">Buscar Cliente</h3>

                  <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse">
                      <i class="fas fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove">
                      <i class="fas fa-remove"></i>
                    </button>
                  </div>
                </div>

                <div class="card-body" style="display: block;">
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <input type="text" id="rif_search" name="rif_search" placeholder="RIF/Cédula"  value="{{ @$client->rif ?? '' }}"class="form-control">
                      </div>
                    </div>

                    <div class="col-md-2">
                      <a class="btn btn-success btn-block" id="btn_search" href="#"><i class="fas fa-search"></i> Buscar</a>
                    </div>

                    <div class="col-md-4">
                      <a class="btn btn-primary btn-block" id="btn_new" href="#">Crear Nuevo Cliente</a>
                    </div>

                  </div>


              </div>
            </div>

            <div class="hide" id="client-basic">
              <div class="card card-default">
                <div class="card-header">
                  <h3 class="card-title">Datos del Cliente</h3>

                  <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse">
                      <i class="fas fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove">
                      <i class="fas fa-remove"></i>
                    </button>
                  </div>
                </div>

                <div class="card-body">
                  <div class="row">
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Razon Social</label>
                        <input type="text" id="businessname" required oninput="setCustomValidity('')" oninvalid="this.setCustomValidity('Requerido')" name="businessname"  value="{{ @$client->businessname ?? '' }}"class="form-control">
                        <input type="hidden" id="id" name="id" value="{{ @$client->id ?? '' }}">
                      </div>
                    </div>

                    <div class="col-md-2">
                      <div class="form-group">
                        <label>RIF</label>
                        <input type="text" id="rif" required oninput="setCustomValidity('')" oninvalid="this.setCustomValidity('Requerido')" name="rif" value="{{ @$client->rif ?? '' }}"class="form-control">
                      </div>
                    </div>

                    <div class="col-md-2">
                      <div class="form-group">
                        <label>Número de Encuesta</label>
                        <input type="text" oninput="setCustomValidity('')" oninvalid="this.setCustomValidity('Requerido')" id="survey_number" name="survey_number"  value="{{ @$client->survey_number ?? '' }}"class="form-control" disabled="">
                      </div>
                    </div>

                    <div class="col-md-2">
                      <div class="form-group">
                        <label>Ruta</label>
                        <select required oninput="setCustomValidity('')" oninvalid="this.setCustomValidity('Requerido')"  id="distributionroute_id" name="distributionroute_id"  class="form-control">
                          @if(isset($distribution_routes))
                            @foreach($distribution_routes as $distribution_route)
                              @if($distribution_route->name == (substr(@$client->businesscode, 2,3) ?? 0))
                                <option value="{{ $distribution_route->name }}" selected="">{{ $distribution_route->name }}</option>
                              @else
                                <option value="{{ $distribution_route->name }}">{{ $distribution_route->name }}</option>
                              @endif
                            @endforeach
                          @endif
                        </select>
                      </div>
                    </div>

                    <div class="col-md-2">
                      <div class="form-group">
                        <label>Codigo</label>
                        <input type="text" id="businesscode" readonly="readonly" name="businesscode" value="{{ @$client->businesscode ?? '' }}"class="form-control">
                      </div>
                    </div>

                  </div>

                  @include('client.address')

                <div class="row">
                  <div class="col-md-2">
                      <div class="form-group">
                        <label>Vende Lubricantes</label>
                        <select required oninput="setCustomValidity('')" oninvalid="this.setCustomValidity('Requerido')" class="form-control" id="sells_lubricants" name="sells_lubricants">
                            @if (@$client->sells_lubricants==1)
                              <option value="">NO</option>
                              <option selected="selected" value="1">SI</option>
                            @else
                              <option selected="selected" value="0">NO</option>
                              <option value="1">SI</option>
                            @endif
                        </select>
                      </div>
                    </div>

                    <div class="col-md-2">
                      <div class="form-group">
                        <label>Vende Chronus</label>
                        <select required oninput="setCustomValidity('')" oninvalid="this.setCustomValidity('Requerido')" class="form-control" id="sells_chronus" name="sells_chronus">
                            @if (@$client->sells_chronus==1)
                              <option value="">NO</option>
                              <option selected="selected" value="1">SI</option>
                            @else
                              <option selected="selected" value="0">NO</option>
                              <option value="1">SI</option>
                            @endif
                        </select>
                      </div>
                    </div>

                    <div class="col-md-2">
                      <div class="form-group">
                        <label>Establec. Abierto</label>
                        <select required oninput="setCustomValidity('')" oninvalid="this.setCustomValidity('Requerido')" class="form-control" id="open_establishment" name="open_establishment">
                            @if (@$client->open_establishment==1)
                              <option value="">NO</option>
                              <option selected="selected" value="1">SI</option>
                            @else
                              <option selected="selected" value="0">NO</option>
                              <option value="1">SI</option>
                            @endif
                        </select>
                      </div>
                    </div>

                    <div class="col-md-2">
                      <div class="form-group">
                        <label>Atendido por dueño</label>
                        <select required oninput="setCustomValidity('')" oninvalid="this.setCustomValidity('Requerido')" class="form-control" id="owner_attend" name="owner_attend">
                            @if (@$client->owner_attend==1)
                              <option value="">NO</option>
                              <option selected="selected" value="1">SI</option>
                            @else
                              <option selected="selected" value="0">NO</option>
                              <option value="1">SI</option>
                            @endif
                        </select>
                      </div>
                    </div>


                    <div class="col-md-2">
                      <div class="form-group">
                        <label>Personas en mostrador</label>
                        <input required oninput="setCustomValidity('')" oninvalid="this.setCustomValidity('Requerido')"  type="number" id="persons_attending" name="persons_attending" value="{{ @$client->persons_attending }}" class="form-control">
                      </div>
                    </div>

                    <div class="col-md-2">
                      <div class="form-group">
                        <label>Interés Compra</label>
                        <select disabled class="form-control" id="purchase_interest" name="purchase_interest">
                            @if (@$client->purchase_interest=="0")
                              <option selected="selected" value="0">0</option>
                              <option value="1">1</option>
                              <option value="2">2</option>
                            @else
                              @if (@$client->purchase_interest=="1")
                                <option value="0">0</option>
                                <option selected="selected" value="1">1</option>
                                <option value="2">2</option>
                              @else
                                <option value="0">0</option>
                                <option value="1">1</option>
                                <option selected="selected" value="2">2</option>
                              @endif
                            @endif
                        </select>

                      </div>
                    </div>

                </div>

                <div class="row">
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Nombre del Propietario</label>
                        <input type="text" required oninput="setCustomValidity('')" oninvalid="this.setCustomValidity('Requerido')" id="ownersname" name="ownersname" value="{{ @$client->ownersname }}" class="form-control">
                      </div>
                    </div>

                    <div class="col-md-2">
                      <div class="form-group">
                        <label>Cédula</label>
                        <input type="text" required oninput="setCustomValidity('')" oninvalid="this.setCustomValidity('Requerido')" id="ownersdni" name="ownersdni" value="{{ @$client->ownersdni }}" class="form-control">
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Correo</label>
                        <input type="email" required oninput="setCustomValidity('')" oninvalid="this.setCustomValidity('Email Requerido')" id="ownersmail" name="ownersmail" value="{{ @$client->ownersmail }}" class="form-control">

                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-2">
                      <div class="form-group">
                        <label>Teléfono Principal</label>
                        <input type="text" class="form-control phones" required oninput="setCustomValidity('')" oninvalid="this.setCustomValidity('Requerido')" id="ownersphone" name="ownersphone" value="{{ @$client->ownersphone }}">
                      </div>
                    </div>
                    <div class="col-md-2">
                      <div class="form-group">
                        <label>Teléfono</label>
                        <input type="text" class="form-control phones" oninput="setCustomValidity('')" id="ownersphone1" name="ownersphone1" value="{{ @$phones[1] }}">
                      </div>
                    </div>
                    <div class="col-md-2">
                      <div class="form-group">
                        <label>Teléfono</label>
                        <input type="text" class="form-control phones" oninput="setCustomValidity('')" id="ownersphone2" name="ownersphone2" value="{{ @$phones[2] }}">
                      </div>
                    </div>
                    <div class="col-md-2">
                      <div class="form-group">
                        <label>Teléfono</label>
                        <input type="text" class="form-control phones" oninput="setCustomValidity('')" id="ownersphone3" name="ownersphone3" value="{{ @$phones[3] }}">
                      </div>
                    </div>
                    <div class="col-md-2">
                      <div class="form-group">
                        <label>Teléfono</label>
                        <input type="text" class="form-control phones" oninput="setCustomValidity('')" id="ownersphone4" name="ownersphone4" value="{{ @$phones[4] }}">
                      </div>
                    </div>
                    <div class="col-md-2">
                      <div class="form-group">
                        <label>Teléfono</label>
                        <input type="text" class="form-control phones" oninput="setCustomValidity('')" id="ownersphone5" name="ownersphone5" value="{{ @$phones[5] }}">
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Nombre del Censado</label>
                        <input type="text" required oninput="setCustomValidity('')" oninvalid="this.setCustomValidity('Requerido')" id="registered_name" name="registered_name" value="{{ @$client->registered_name }}" class="form-control">
                      </div>
                    </div>

                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Cargo</label>
                        <input type="text" required oninput="setCustomValidity('')" oninvalid="this.setCustomValidity('Requerido')" id="registered_charge" name="registered_charge" value="{{ @$client->registered_charge }}" class="form-control">
                      </div>
                    </div>



                    <div class="col-md-4">
                      <div class="form-group">
                       <label>Observaciones</label>
                        <input oninput="setCustomValidity('')" type="text" id="observations" name="observations" value="{{ @$client->observations }}" class="form-control">

                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-2">
                      <div class="form-group">
                        <label>Aniv. Negoc.</label>
                        <input type="text" required readonly="readonly" oninput="setCustomValidity('')" oninvalid="this.setCustomValidity('Requerido')"  id="anniversary" name="anniversary" value="{{ @$client->anniversary }}" class="form-control birthday" style="background-color: #fff">
                      </div>
                    </div>

                    <div class="col-md-2">
                      <div class="form-group">
                        <label>Área</label>
                        <input type="text" disabled="" id="area" name="area" value="{{ @$client->area }}" class="form-control">
                      </div>
                    </div>

                    <div class="col-md-2">
                      <div class="form-group">
                        <label>Canal</label>
                        <select required oninput="setCustomValidity('')" oninvalid="this.setCustomValidity('Requerido')"  id="distribution_channel_id" name="distribution_channel_id"  class="form-control">
                          @if(isset($distribution_channels))
                            @foreach($distribution_channels as $distribution_channel)
                              @if($distribution_channel->id == ($client->distribution_channel_id ?? 0))
                                <option value="{{ $distribution_channel->id }}" selected="">{{ $distribution_channel->name }}</option>
                              @else
                                <option value="{{ $distribution_channel->id }}">{{ $distribution_channel->name }}</option>
                              @endif
                            @endforeach
                          @endif
                        </select>
                      </div>
                    </div>

                    <div class="col-md-2">
                      <div class="form-group">
                        <label>Autoservicio</label>
                        <select  disabled class="form-control" id="autoservice" name="autoservice">
                            @if (@$client->autoservice==1)
                              <option value="0">NO</option>
                              <option selected="selected" value="1">SI</option>
                            @else
                              <option selected="selected" value="0">NO</option>
                              <option value="1">SI</option>
                            @endif
                        </select>

                      </div>
                    </div>

                    <div class="col-md-2">
                      <div class="form-group">
                        <label>Tipo</label>
                        <select  required oninput="setCustomValidity('')" oninvalid="this.setCustomValidity('Requerido')" id="business_type_id" name="business_type_id"  class="form-control">
                          @if(isset($business_types))
                            @foreach($business_types as $business_type)
                              @if($business_type->id == ($client->business_type_id ?? 0))
                              <option value="{{ $business_type->id }}" selected="">{{ $business_type->name }}</option>
                              @else
                              <option value="{{ $business_type->id }}">{{ $business_type->name }}</option>
                              @endif
                            @endforeach
                          @endif
                        </select>
                      </div>
                    </div>

                    <div class="col-md-2">
                      <div class="form-group">
                        <label>Visibilidad</label>
                        <select disabled class="form-control" id="visibility" name="visibility">
                            @if (@$client->visibility=="E")
                              <option selected="selected" value="E">E</option>
                              <option value="B">B</option>
                              <option value="R">R</option>
                            @else
                              @if (@$client->visibility=="B")
                                <option value="E">E</option>
                                <option selected="selected" value="B">B</option>
                                <option value="R">R</option>
                              @else
                                <option value="E">E</option>
                                <option value="B">B</option>
                                <option selected="selected" value="R">R</option>
                              @endif
                            @endif
                        </select>

                      </div>
                    </div>


                  </div>

                  <div class="row">

                    <div class="col-md-2">
                      <div class="form-group">
                        <label># Depend.</label>
                        <input disabled type="number" id="dependents_number" name="dependents_number" value="{{ @$client->dependents_number }}" class="form-control">
                      </div>
                    </div>

                    <div class="col-md-2">
                      <div class="form-group">
                        <label>Persona Clave</label>
                        <input  required oninput="setCustomValidity('')" oninvalid="this.setCustomValidity('Requerido')" type="text" id="person_contact" name="person_contact" value="{{ @$client->person_contact }}" class="form-control">
                      </div>
                    </div>

                    <div class="col-md-2">
                      <div class="form-group">
                        <label>Cargo</label>
                        <input  required oninput="setCustomValidity('')" oninvalid="this.setCustomValidity('Requerido')" type="text" id="charge_contact" name="charge_contact" value="{{ @$client->charge_contact }}" class="form-control">
                      </div>
                    </div>

                    <div class="col-md-2">
                      <div class="form-group">
                        <label>Comprador</label>
                        <input  required oninput="setCustomValidity('')" oninvalid="this.setCustomValidity('Requerido')" type="text" id="buyer" name="buyer" value="{{ @$client->buyer }}" class="form-control">
                      </div>
                    </div>

                    <div class="col-md-2">
                      <div class="form-group">
                        <label>Otro Personal</label>
                        <input  required oninput="setCustomValidity('')" oninvalid="this.setCustomValidity('Requerido')" type="text" id="other_contact" name="other_contact" value="{{ @$client->other_contact }}" class="form-control">
                      </div>
                    </div>

                    <div class="col-md-2">
                      <div class="form-group">
                        <label>Cargo</label>
                        <input  required oninput="setCustomValidity('')" oninvalid="this.setCustomValidity('Requerido')" type="text" id="other_contact_charge" name="other_contact_charge" value="{{ @$client->other_contact_charge }}" class="form-control">
                      </div>
                    </div>

                  </div>

                  <div class="row">
                      <div class="col-md-3">
                        <label>Calif. Bastidor</label>
                        <select  required oninput="setCustomValidity('')" oninvalid="this.setCustomValidity('Requerido')" class="form-control" id="quality_frame" name="quality_frame">
                            @if (@$client->quality_frame==1)
                              <option value="0">NO</option>
                              <option selected="selected" value="1">SI</option>
                            @else
                              <option selected="selected" value="0">NO</option>
                              <option value="1">SI</option>
                            @endif
                        </select>
                      </div>

                      <div class="col-md-3">
                        <label>Habladores</label>
                        <select  required oninput="setCustomValidity('')" oninvalid="this.setCustomValidity('Requerido')" class="form-control" id="census_talkers" name="census_talkers">
                            @if (@$client->census_talkers==1)
                              <option value="0">NO</option>
                              <option selected="selected" value="1">SI</option>
                            @else
                              <option selected="selected" value="0">NO</option>
                              <option value="1">SI</option>
                            @endif
                        </select>
                      </div>

                      <div class="col-md-3">
                        <label>Afiche</label>
                        <select  required oninput="setCustomValidity('')" oninvalid="this.setCustomValidity('Requerido')" class="form-control" id="census_posters" name="census_posters">
                            @if (@$client->census_posters==1)
                              <option value="0">NO</option>
                              <option selected="selected" value="1">SI</option>
                            @else
                              <option selected="selected" value="0">NO</option>
                              <option value="1">SI</option>
                            @endif
                        </select>
                      </div>

                      <div class="col-md-3">
                        <label>Exhibidor</label>
                        <select  required oninput="setCustomValidity('')" oninvalid="this.setCustomValidity('Requerido')" class="form-control" id="census_exhibitor" name="census_exhibitor">
                            @if (@$client->census_exhibitor==1)
                              <option value="0">NO</option>
                              <option selected="selected" value="1">SI</option>
                            @else
                              <option selected="selected" value="0">NO</option>
                              <option value="1">SI</option>
                            @endif
                        </select>
                      </div>

                  </div>

                  <div class="row">
                     <div class="col-md-3">
                        <label>Banderines</label>
                        <select required oninput="setCustomValidity('')" oninvalid="this.setCustomValidity('Requerido')" class="form-control" id="census_pennants" name="census_pennants">
                            @if (@$client->census_pennants==1)
                              <option value="0">NO</option>
                              <option selected="selected" value="1">SI</option>
                            @else
                              <option selected="selected" value="0">NO</option>
                              <option value="1">SI</option>
                            @endif
                        </select>
                      </div>

                      <div class="col-md-3">
                        <label>Calcomanias</label>
                        <select  required oninput="setCustomValidity('')" oninvalid="this.setCustomValidity('Requerido')" class="form-control" id="stickers" name="stickers">
                            @if (@$client->stickers==1)
                              <option value="0">NO</option>
                              <option selected="selected" value="1">SI</option>
                            @else
                              <option selected="selected" value="0">NO</option>
                              <option value="1">SI</option>
                            @endif
                        </select>
                      </div>

                      <div class="col-md-3">
                        <label>Fachada</label>
                        <select  required oninput="setCustomValidity('')" oninvalid="this.setCustomValidity('Requerido')" class="form-control" id="facade" name="facade">
                            @if (@$client->facade==1)
                              <option value="0">NO</option>
                              <option selected="selected" value="1">SI</option>
                            @else
                              <option selected="selected" value="0">NO</option>
                              <option value="1">SI</option>
                            @endif
                        </select>
                      </div>

                      <div class="col-md-3">
                        <label>Otros (POP)</label>
                        <input required oninput="setCustomValidity('')" oninvalid="this.setCustomValidity('Requerido')" type="text" id="other_pop" name="other_pop" value="{{ @$client->other_pop }}" class="form-control">
                      </div>



                  </div>

                  <div class="row">

                    <div class="col-md-2">
                      <div class="form-group">
                        <label>CDV</label>
                        <input disabled type="text" id="cdv" name="cdv" value="{{ @$client->cdv }}" class="form-control">
                      </div>
                    </div>

                    <div class="col-md-2">
                      <div class="form-group">
                        <label>Drop Size Prom</label>
                        <input disabled type="text" id="dropsizeprom" name="dropsizeprom" value="{{ @$client->dropsizeprom }}" class="form-control">
                      </div>
                    </div>

                    <div class="col-md-2">
                      <div class="form-group">
                        <label>Ped. Máx</label>
                        <input disabled type="number" id="maximumorder" name="maximumorder" value="{{ @$client->maximumorder }}" class="form-control">
                      </div>
                    </div>

                    <div class="col-md-2">
                      <div class="form-group">
                        <label>Marca A</label>
                        <input disabled type="text" id="brand_a" name="brand_a" value="{{ @$client->brand_a }}" class="form-control">
                      </div>
                    </div>

                    <div class="col-md-2">
                      <div class="form-group">
                        <label>Marca B</label>
                        <input disabled type="text" id="brand_b" name="brand_b" value="{{ @$client->brand_b }}" class="form-control">
                      </div>
                    </div>

                    <div class="col-md-2">
                      <div class="form-group">
                        <label>Marca C</label>
                        <input disabled type="text" id="brand_c" name="brand_c" value="{{ @$client->brand_c }}" class="form-control">
                      </div>
                    </div>

                </div>

                <div class="row">

                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Categorias</label>
                        <select disabled class="select2" multiple="multiple" name="categories[]" data-placeholder="Seleccione Categorias" style="width: 100%;">
                          @foreach ($categories as $category) 
                              @if ($category->assigned)
                                  <option selected="selected" value="{{$category->id}}">{{ $category->name }}</option>
                              @else    
                                  <option value="{{$category->id}}">{{ $category->name }}</option>
                              @endif
                          @endforeach
                          </select>
                      </div>
                    </div>

                    <div class="col-md-4 hide">
                      <div class="form-group">
                        <label>Marcas</label>
                        <select class="select2" multiple="multiple" name="brands[]" data-placeholder="Seleccione Marcas" style="width: 100%;">
                          @foreach ($brands as $brand) 
                              @if ($brand->assigned)
                                  <option selected="selected" value="{{$brand->id}}">{{ $brand->name }}</option>
                              @else    
                                  <option value="{{$brand->id}}">{{ $brand->name }}</option>
                              @endif
                          @endforeach
                          </select>
                      </div>
                    </div>

                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Otros Servic.</label>
                        <input disabled type="text" id="other_services" name="other_services" value="{{ @$client->other_services }}" class="form-control">
                      </div>
                    </div>
                </div>

                <div class="card-footer">
                  {{--<input type="submit" class="btn btn-primary" value="Guardar"> <input type="reset" value="Cancelar" class="btn btn-primary"> --}}
                </div>
              </div>
            </div>
          </div>
            <div class="card card-default hide" id="btns">
                <div class="card-footer" style="display: block;">
                <input type="submit" class="btn btn-primary" value="Guardar" data-class="Census"> <a href="{{url('census')}}" class="btn btn-outline-danger">Cancelar</a>
              </div>
            </div>

        </form>
      @endcan
        <div class="card card-default">
              <div class="card-header">
                <h3 class="card-title">Listado de Censos</h3>
                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                  <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i></button>
                </div>
              </div>

              <div class="card-body" style="display: block;">
                <div class="table-responsive">
                  <table class="table table-striped table-bordered table-hover" >
                      <tr>
                          <th>Censo</th>
                          <th>Razon Social</th>
                          <th>Rif</th>
                          <th>Email</th>
                          <th>Empadronador</th>
                          <th>Opciones</th>
                      </tr>
                  @foreach(@$clients as $rowclient)
                      <tr>
                          <td>{{ $rowclient->survey_number }}</td>
                          <td>{{ $rowclient->businessname }}</td>
                          <td>{{ $rowclient->rif }}</td>
                          <td>{{ $rowclient->businesssmail }} {{ $rowclient->ownersmail }}</td>
                          <td>{{ $rowclient->census->name }}</td>
                          <td>
                            <a href="{{ route('census', $rowclient->id) }}">
                              <i class="fa fa-edit" style="color:#006400"></i>
                            </a>
                          </td>
                      </tr>
                  @endforeach
                  </table>
                </div>
              </div>
              <!-- /.card-body -->
              <div class="card-footer" style="display: block;">

              </div>

        </div>

      </div>
</div>
@endsection

@include('client.functions_census')

