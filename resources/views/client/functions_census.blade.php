

 @push('js')

  <script>

   	const TOKEN = '{{csrf_token()}}';

    $(document).ready(function(){

      $("#rif").inputmask({mask:"a-999999999"});
      $("#rif").inputmask({mask:"a-999999999"});
      $("#ownersdni").inputmask({mask:"a-99999999"});
      $("#ownersdni1").inputmask({mask:"a-99999999"});
      $("#rif_search").inputmask({mask:"a-999999999"});
      $(".phones").inputmask({"mask": "9999-9999999"});

	    let activate = (element) => {
	      	$('#'+element).removeClass('hide');
	      	$('#'+element).addClass('show-div');
	    };

	    let deactivate = (element) => {
	      	$('#'+element).removeClass('show-div');
	      	$('#'+element).addClass('hide');
	    };

	    let activate_all = (element) => {
	    	activate('client-basic');
      	activate('btns');
      	deactivate('btn_search');
	    };

      let search_client = () => {
        $.get("{{url('search-client')}}/"+$('#rif_search').val(), function ( response ) {
          if (response.client){
                  window.location.href = '{{url('census/')}}/'+response.client.id;
                }else{
                Swal.fire({
                    title: "Atención: RIF No Encontrado", text: "Desea crear un nuevo cliente", icon: "warning", showCancelButton: true, confirmButtonColor: '#5ECF48', confirmButtonText: 'Si, Crear!', cancelButtonText: "Cancelar", showLoaderOnConfirm: true,
                }).then(function (response) {
                    if (response.value)
                        $('#btn_new').trigger('click');
                });
                }
            });
      }

    	if ($('#rif_search').val()){
    		activate_all();
    		$('#rif_search').attr('readonly','readonly');
    		$('#rif_search').attr("disabled",true);
    	}

	    $('#btn_new').on('click', function (e){
	      	activate_all();
          clear_all();
	    });

	    $('#btn_search').on('click', function (e){
	      	search_client();
	    });

      let get_client_code = () => {
        let route = $('#distributionroute_id').val();
        $.get("{{url('search-next-code')}}/"+route, function ( response ) {
          if (response)
            $('#businesscode').val('R-'+route+'-'+response);
          else
            $('#businesscode').val('R-'+route+'-1');
        });
      };

      $('#distributionroute_id').on('click', function (e){
          get_client_code();
      });

      get_client_code();

      $('#anniversary').datetimepicker({
          format: 'Y-m-d',
          closeOnDateSelect: true,
          timepicker: false
      });


	    $( "#rif_search" ).on( "keydown", function(event) {
	      	if(event.which == 13) {
	    		event.preventDefault();
	         	search_client();
	    	 }
	    });

      let clear_all = () => {
        let elems = ['id', 'address_id', 'businessname', 'rif', 'survey_number', 'distributionroute_id', 'businesscode', 'urbanization', 'sector', 'street', 'description', 'municipality', 'parish', 'sells_lubricants', 'sells_chronus', 'ownersname', 'ownersdni', 'ownersphone', 'ownersmail', 'anniversary', 'area', 'distribution_channel_id', 'autoservice', 'business_type_id', 'visibility', 'categories', 'brands', 'other_services', 'purchase_interest', 'dependents_number', 'person_contact', 'charge_contact', 'buyer', 'other_contact', 'other_contact_charge', 'quality_frame', 'census_talkers', 'census_posters', 'census_exhibitor', 'census_pennants', 'observations', 'cdv', 'dropsizeprom', 'maximumorder', 'brand_a', 'brand_b', 'brand_c'];
        elems.forEach(function(element) {
          $('#'+element).val('');
        });
      };

    });
  </script>

@endpush
