@extends('layouts.print_landscape')

@section('titulo', $title)

@section('content')

  <div class="row">
    <div class="col-12 table-responsive">
      <table class="table table-striped" style="border: solid 1px; width:100%;">
        <tr style="background-color:#e9ecef;">
          <th style="width: 2%;">Nro</th>
          <th style="width: 18%;">Nombre de Cliente</th>
          <th style="width: 5%;">Canal</th>
          <th style="width: 5%;">Tipo</th>
          <th style="width: 10%;">Municipio</th>
          <th style="width: 10%;">Localidad</th>
          <th style="width: 10%;">Calle/Avenida</th>
          <th style="width: 10%;">Entre calle y calle</th>
        </tr>
        @foreach($clients as $item)
            <tr style="background-color: {{ $i%2 ? '':'#e9ecef' }};">
              <td>{{ $i++ }}
              <td>{{ @$item->businessname }}</td>
              <td>{{ @$item->distribution_channels->name}}</td>
              <td>{{ @$item->business_type->name }}</td>
              <td>{{ $item->address()->first()->municipality }}</td>
              <td>{{ $item->address()->first()->urbanization }} {{ $item->address()->first()->sector }}</td>
              <td>{{ $item->address()->first()->street }} </td>
              <td>{{ $item->address()->first()->street }} </td>
            </tr>
        @endforeach

      </table>
    </div>
  </div>

@endsection
