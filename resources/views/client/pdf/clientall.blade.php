@extends('layouts.print')

@section('titulo', $title)

@section('content')

  <div class="row">
    <div class="col-12">
      <table class="table table-striped" style="border: solid 1px; width:100%;">
        <tr >
          <th style="width: 90px;">RIF</th>
          <th style="width: 70px;">Código</th>
          <th style="width: 300px;">Razón Social</th>
          <th style="width: 250px%;">Propietario</th>
          <th style="width: 200px;">Correo</th>
        </tr>
        @foreach($clients as $item)
            <tr>
              <td>{{$item->rif}}</td>
              <td>{{$item->businesscode}}</td>
              <td>{{ $item->businessname }}</td>
              <td>{{$item->ownersname}}</td>
              <td>{{$item->ownersmail}}</td>
            </tr>
        @endforeach

      </table>
    </div>
  </div>
@endsection
