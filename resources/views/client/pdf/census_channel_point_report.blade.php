@extends('layouts.print_landscape')

@section('titulo', $title)

@section('content')

  <div class="row">
    <div class="col-12 table-responsive">
      <table class="table table-striped" style="border: solid 1px; width:100%;">
        <tr style="background-color:#e9ecef;">
          <th style="width: 2%;">Nro</th>
          <th style="width: 5%;">Nomenclatura</th>
          <th style="width: 5%;">Canal</th>
          <th style="width: 10%;">Cantidad</th>
        </tr>
        @php $i=1; @endphp
        @foreach($items as $item)
            <tr style="background-color: {{ $i%2 ? '':'#e9ecef' }};">
              <td>{{ $i++ }}
              <td>{{ $item['name'] }}</td>
              <td>{{ $item['description'] }}</td>
              <td>{{ $item['count'] }}</td>
            </tr>
        @endforeach

      </table>
    </div>
  </div>

@endsection
