@extends('layouts.print')

@section('titulo', $title)

@section('content')

  <div class="row" style="font-size: 10px !important;">
    <div class="col-12 table-responsive">
      <table class="table table-striped" style="border: solid 1px; width:100%;">
        <tr >
          <td align="left" colspan="4"><strong>Razón Social: </strong>{{ $client->businessname }}</td>
          <td align="left" colspan="2"><strong>RIF: </strong>{{$client->rif}}</td>
          <td align="left" colspan="2"><strong>Código: </strong>{{$client->businesscode}}</td>
        </tr>
    
        <tr >
          <th align="left" colspan="8"><strong>Dirección: </strong>
            @if ($client->address)
              {{$client->address[0]->description}}
            @endif
          </th>
        </tr>
        
        <tr >
          <th align="left" colspan="3"><strong>Urb./Zona: </strong>
            @if ($client->address)
              {{$client->address[0]->urbanization}}
            @endif
          </th>
          <th align="left" colspan="3"><strong>Sector: </strong>
            @if ($client->address)
              {{$client->address[0]->sector}}
            @endif
          </th>
          <th align="left" colspan="2"><strong>Calle: </strong>
            @if ($client->address)
              {{$client->address[0]->street}}
            @endif
          </th>
        </tr>
    
        <tr >
          <th align="left" colspan="2"><strong>Punto de Referencia: </strong></th>
          <th align="left" colspan="3"><strong>¿La dirección despacho es dirección fiscal? </strong></th>
          <th align="left" colspan="1"><strong>¿Contribuyente especial? </strong>
            @if ($client->specialtaxpayer)
              SI
            @else
              NO
            @endif
          </th>
          <th align="left" colspan="2"><strong>Teléfono Oficina: </strong></th>
        </tr>
    
        <tr >
          <th align="left" colspan="2"><strong>Nombre Propietario: </strong>{{$client->ownersname}}</th>
          <th align="left" colspan="2"><strong>Cédula: </strong>{{$client->ownersdni}}</th>
          <th align="left" colspan="2"><strong>Teléfono: </strong></th>
          <th align="left" colspan="2"><strong>Correo: </strong>{{$client->ownersmail}}</th>
        </tr>

        <tr >
          <th align="left" colspan="2"><strong>Nombre del Comprador o Encargado: </strong>{{$client->managersname}}</th>
          <th align="left" colspan="2"><strong>Cédula: </strong>{{$client->managersdni}}</th>
          <th align="left" colspan="2"><strong>Teléfono: </strong></th>
          <th align="left" colspan="2"><strong>Correo: </strong>{{$client->managersmail}}</th>
        </tr>
    
        <tr>
          <th align="left"><strong>Canal de Distribución: </strong>
            @if ($client->distribution_channels)
              {{$client->distribution_channels->name}}
            @endif
          </th>
          <th align="left"><strong>Tipo Negocio: </strong>
             @if ($client->business_type)
              {{$client->business_type->name}}
            @endif
          </th>
          <th align="left"><strong>Condic. Venta: </strong>
             @if ($client->sales_type)
              {{$client->sales_type->name}}
            @endif
          </th>
          <th align="left"><strong>Lím. Crédito: </strong>{{$client->creditlimit}}</th>
          <th align="left"><strong>Frecuencia Visita: </strong>{{$client->visitfrecuency}}</th>
          <th align="left"><strong>Secuencia Visita: </strong>{{$client->visitsequence}}</th>
          <th align="left"><strong>Cliente Desde: </strong>{{$client->created_at}}</th>
          <th align="left"><strong>Cumpleaños Dueño: </strong>{{$client->ownersbirthday}}</th>
        </tr>
    
        <tr>
          <th align="left"><strong>Fecha Aniversario: </strong>{{$client->anniversary}}</th>
          <th align="left"><strong>Drop Size Prom.: </strong>{{$client->dropsizeprom}}</th>
          <th align="left"><strong>Pedido Máximo: </strong>{{$client->maximumorder}}</th>
          <th align="left"><strong>Banderines: </strong>
            @if ($client->pennants)
              SI
            @else
              NO
            @endif
          </th>
          <th align="left"><strong>Exhibición: </strong>
          @if ($client->display)
              SI
            @else
              NO
            @endif
          </th>
          <th align="left"><strong>Bastidor: </strong>
          @if ($client->frame)
              SI
            @else
              NO
            @endif
          </th>
          <th align="left"><strong>POP: </strong>
          @if ($client->pop)
              SI
            @else
              NO
            @endif
          </th>
          <th align="left"></th>
        </tr>
       
      </table>
      <table class="table table-striped" style="border: solid 1px; width:100%;">
        @foreach (@$categories as $category)
          @if ($category->products)
            @php ($printed = 0)
            @foreach ($category->products as $product)
              @if ($product->units)
                @if ($printed == 0)
                  @php ($printed = 1)
                  <tr>
                    <th align="left">{{$category->name}}</th>
                    <th align="left">Pres.</th>
                    <th align="left">UE</th>
                    <th align="left">Cantidad</th>
                    <th align="left">Fecha</th>
                  </tr>
                @endif
                @foreach ($product->units as $unit)
                  <tr>
                    <td>{{$product->name}}</td>
                    <td>{{$unit->name}}</td>
                    <td>{{$unit->packing_unit}}</td>
                    <td></td>
                    <td></td>
                  </tr>
                @endforeach
              @endif
            @endforeach
          @endif
          
        @endforeach

      </table>
    </div>
  </div>      
        
@endsection
