<div class="row">
  <div class="col-md-3">
    <div class="form-group">
      <label>Dirección: Urb./Zona</label>
      @if (@$client->address)
        <input type="hidden" id="address_id" name="address_id" value="{{ @$client->address[0]->id }}">
        <input type="text" id="urbanization" name="urbanization" value="{{ @$client->address[0]->urbanization }}" class="form-control">
      @else
        <input type="hidden" id="address_id" value="">
        <input type="text" id="urbanization" name="urbanization" value="" class="form-control">
      @endif
    </div>
  </div>

  <div class="col-md-2">
    <div class="form-group">
      <label>Sector</label>
      @if (@$client->address)
        <input type="text" id="sector" name="sector" value="{{ @$client->address[0]->sector }}" class="form-control">
      @else
        <input type="text" id="sector" name="sector" value="" class="form-control">
      @endif
    </div>
  </div>

  <div class="col-md-2">
    <div class="form-group">
      <label>Calle</label>
      @if (@$client->address)
        <input type="text" id="street" name="street" value="{{ @$client->address[0]->street }}" class="form-control">
      @else
        <input type="text" id="street" name="street" value="" class="form-control">
      @endif
    </div>
  </div>

  <div class="col-md-2">
    <div class="form-group">
      <label>Entre Calles</label>
      @if (@$client->address)
        <input type="text" id="streets_between" name="streets_between" value="{{ @$client->address[0]->streets_between }}" class="form-control">
      @else
        <input type="text" id="streets_between" name="streets_between" value="" class="form-control">
      @endif
    </div>
  </div>

  <div class="col-md-3">
    <div class="form-group">
      <label>Punto de Referencia</label>
      @if (@$client->address)
        <input type="text" id="description" name="description" value="{{ @$client->address[0]->description }}" class="form-control">
      @else
        <input type="text" id="description" name="description" value="" class="form-control">
      @endif
    </div>
  </div>

</div>

<div class="row">

  <div class="col-md-2">
    <div class="form-group">
      <label>Municipio</label>
      @if (@$client->address)
        <input type="text" id="municipality" name="municipality" value="{{ @$client->address[0]->municipality }}" class="form-control">
      @else
        <input type="text" id="municipality" name="municipality" value="" class="form-control">
      @endif
    </div>
  </div>

  <div class="col-md-2">
    <div class="form-group">
      <label>Parroquia</label>
      @if (@$client->address)
        <input type="text" id="parish" name="parish" value="{{ @$client->address[0]->parish }}" class="form-control">
      @else
        <input type="text" id="parish" name="parish" value="" class="form-control">
      @endif
    </div>
  </div>

  <div class="col-md-2">
    <div class="form-group">
      <label>Ciudad</label>
      @if (@$client->address)
        <input type="text" id="city" name="city" value="{{ @$client->address[0]->city }}" class="form-control">
      @else
        <input type="text" id="city" name="city" value="" class="form-control">
      @endif
    </div>
  </div>

  <div class="col-md-2">
    <div class="form-group">
      <label>Estado</label>
      @if (@$client->address)
        <input type="text" id="state" name="state" value="{{ @$client->address[0]->state }}" class="form-control">
      @else
        <input type="text" id="state" name="state" value="" class="form-control">
      @endif
    </div>
  </div>

  <div class="col-md-2">
    <div class="form-group">
      <label>Coord. X</label>
      @if (@$client->address)
        <input type="text" id="latitude" name="latitude" value="{{ @$client->address[0]->latitude }}" class="form-control">
      @else
        <input type="text" id="latitude" name="latitude" value="" class="form-control">
      @endif
    </div>
  </div>

  <div class="col-md-2">
    <div class="form-group">
      <label>Coord. Y</label>
      @if (@$client->address)
        <input type="text" id="longitude" name="longitude" value="{{ @$client->address[0]->longitude }}" class="form-control">
      @else
        <input type="text" id="longitude" name="longitude" value="" class="form-control">
      @endif
    </div>
  </div>

</div>
