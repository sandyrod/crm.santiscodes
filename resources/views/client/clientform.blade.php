@extends('layouts.dashboard')
@section('title', 'Clientes')
@section('content')
<div class="container">
    @section('content_header')
    Clientes
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif
    @endsection
    <div class="container-fluid">
        <form method="post" action="{{ route('client_save') }}">
            @csrf
            <div class="card card-default">
                <div class="card-header">
                  <h3 class="card-title">Buscar Cliente</h3>

                  <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse">
                      <i class="fas fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove">
                      <i class="fas fa-remove"></i>
                    </button>
                  </div>
                </div>

                <div class="card-body" style="display: block;">
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <input type="text" id="rif" name="rif" placeholder="RIF/Cédula"  value="{{ $client->rif ?? '' }}"class="form-control">
                      </div>
                    </div>

                    <div class="col-md-2">
                      <a class="btn btn-success btn-block" id="btn_search" href="#"><i class="fas fa-search"></i> Buscar</a>
                    </div>

                    <div class="col-md-4">
                      <a class="btn btn-primary btn-block" id="btn_new" href="#">Crear Nuevo Cliente</a>
                    </div>
                  </div>


              </div>
            </div>

            <div class="hide" id="client-basic">
              <div class="card card-default">
                <div class="card-header">
                  <h3 class="card-title">Datos del Cliente</h3>

                  <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse">
                      <i class="fas fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove">
                      <i class="fas fa-remove"></i>
                    </button>
                  </div>
                </div>

                <div class="card-body">
                  <div class="row">
                    <div class="col-md-3">
                      <div class="form-group">
                        <label>Chronus Contigo:</label>
                        <select class="form-control" id="chronuscontigo" name="chronuscontigo">
                          @if (@$client->chronuscontigo==0)
                            <option selected="selected" value="0">Cliente Normal</option>
                            <option value="1">Chronus Contigo</option>
                          @else
                            <option selected="selected" value="1">Chronus Contigo</option>
                            <option value="0">Cliente Normal</option>
                          @endif
                          </select>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-2">
                      <div class="form-group">
                        <label>Tipo</label>
                        <select class="form-control" id="client_type" name="client_type">
                          @if (@$client->client_type==1)
                            <option value="">Prospecto</option>
                            <option selected="selected" value="1">Cliente</option>
                            <option value="2">Banco</option>
                           @elseif (@$client->client_type==2)
                            <option value="">Prospecto</option>
                            <option value="1">Cliente</option>
                            <option selected="selected" value="2">Banco</option>
                          @else
                            <option selected="selected" value="">Prospecto</option>
                            <option value="1">Cliente</option>
                            <option value="2">Banco</option>
                          @endif
                          </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Razon Social</label>
                        <input type="text" id="businessname" name="businessname"  value="{{ $client->businessname ?? '' }}"class="form-control">
                        <input type="hidden" id="id" name="id" value="{{ $client->id ?? '' }}">
                      </div>
                    </div>

                    <div class="col-md-2">
                      <div class="form-group">
                        <label>Ruta</label>
                        <select required oninput="setCustomValidity('')" oninvalid="this.setCustomValidity('Requerido')"  id="distributionroute_id" name="distributionroute_id"  class="form-control">
                          @if(isset($distribution_routes))
                            @foreach($distribution_routes as $distribution_route)
                              @if($distribution_route->id == ($client->distributionroutes_id ?? 1))
                                <option value="{{ $distribution_route->name }}" selected="">{{ $distribution_route->name }}</option>
                              @else
                                <option value="{{ $distribution_route->name }}">{{ $distribution_route->name }}</option>
                              @endif
                            @endforeach
                          @endif
                        </select>
                      </div>
                    </div>
                    <div class="col-md-2">
                      <div class="form-group">
                        <label>Codigo</label>
                        <input type="text" readonly="readonly" id="businesscode" name="businesscode" value="{{ $client->businesscode ?? '' }}"class="form-control">
                      </div>
                    </div>
                    <div class="col-md-2">
                      <div class="form-group">
                        <label>Cliente desde</label>
                        <input type="text" readonly="readonly" id="client_date" name="client_date"  value="{{ $client->client_date ?? date('Y-m-d') }}"class="form-control">
                        <input type="hidden" id="id" name="id" value="{{ $client->id ?? '' }}">
                      </div>
                    </div>

                  </div>
                  <div class="row">
                    <div class="col-md-2">
                      <div class="form-group">
                        <label>Rif</label>
                        <input type="text" id="rif1" name="rif"  value="{{ $client->rif ?? '' }}"class="form-control">
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="form-group">
                        <label>Contribuyente Especial</label>
                        <select class="form-control" id="specialtaxpayer" name="specialtaxpayer">
                          @if (@$client->specialtaxpayer==1)
                            <option value="">NO</option>
                            <option selected="selected" value="1">SI</option>
                          @else
                            <option selected="selected" value="">NO</option>
                            <option value="1">SI</option>
                          @endif
                          </select>
                      </div>
                    </div>

                    <div class="col-md-3">
                      <div class="form-group">
                        <label>Email Cliente</label>
                        <input type="email" id="businesssmail" name="businesssmail"  value="{{ $client->businesssmail ?? '' }}"class="form-control">
                      </div>
                    </div>

                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Canal de distribucion</label>
                        <select id="distribution_channel_id" name="distribution_channel_id"  class="form-control">
                          @if(isset($distribution_channels))
                            @foreach($distribution_channels as $distribution_channel)
                              @if($distribution_channel->id == ($client->distribution_channel_id ?? 0))
                                <option value="{{ $distribution_channel->id }}" selected="">{{ $distribution_channel->name }} {{ $distribution_channel->description }}</option>
                              @else
                                <option value="{{ $distribution_channel->id }}">{{ $distribution_channel->name }} {{ $distribution_channel->description }}</option>
                              @endif
                            @endforeach
                          @endif
                        </select>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-3">
                      <div class="form-group">
                        <label>Tipo de Negocio</label>
                        <select id="business_type_id" name="business_type_id"  class="form-control">
                          @if(isset($business_types))
                          @foreach($business_types as $business_type)
                            @if($business_type->id == ($client->business_type_id ?? 0))
                            <option value="{{ $business_type->id }}" selected="">{{ $business_type->name }}</option>
                            @else
                            <option value="{{ $business_type->id }}">{{ $business_type->name }}</option>
                            @endif
                          @endforeach
                          @endif
                        </select>
                      </div>
                    </div>

                    <div class="col-md-3">
                      <div class="form-group">
                        <label>Condicion de venta</label>
                        <select id="sales_type_id" name="sales_type_id"  class="form-control">
                          @if(isset($sales_types))
                            @foreach($sales_types as $sales_type)
                              @if($sales_type->id == ($client->sales_type_id ?? 0))
                              <option value="{{ $sales_type->id }}" selected="">{{ $sales_type->name }}</option>
                              @else
                              <option value="{{ $sales_type->id }}">{{ $sales_type->name }}</option>
                              @endif
                            @endforeach
                          @endif
                        </select>
                      </div>
                    </div>

                    <div class="col-md-3">
                      <div class="form-group">
                        <label>Limite de Credito</label>
                        <input type="text" id="creditlimit" name="creditlimit"  value="{{ $client->creditlimit ?? '' }}"class="form-control">
                      </div>
                    </div>

                    <div class="col-md-3">
                      <div class="form-group">
                        <label>Pedido Maximo</label>
                        <input type="text" id="maximumorder" name="maximumorder"  value="{{ $client->maximumorder ?? '' }}"class="form-control">
                      </div>
                    </div>

                  </div>
                  <div class="row">
                      <div class="col-md-3">
                          <div class="form-group">
                            <label>Frecuencia de visita</label>
                            <select id="visitfrecuency" name="visitfrecuency"  class="form-control">
                              @if(isset($frecuencies))
                                @foreach($frecuencies as $visitfrecuency)
                                  @if($visitfrecuency->id == ($client->visitfrecuency ?? 0))
                                  <option value="{{ $visitfrecuency->id }}" selected="">{{ $visitfrecuency->name }}</option>
                                  @else
                                  <option value="{{ $visitfrecuency->id }}">{{ $visitfrecuency->name }}</option>
                                  @endif
                                @endforeach
                              @endif
                            </select>
                          </div>
                      </div>

                      <div class="col-md-3">
                          <div class="form-group">
                            <label>Secuencia de visita</label>
                            <select id="visitsequence" name="visitsequence"  class="form-control">
                              @if(isset($sequences))
                                @foreach($sequences as $visitsequence)
                                  @if($visitsequence->id == ($client->visitsequence ?? 0))
                                  <option value="{{ $visitsequence->id }}" selected="">{{ $visitsequence->name }}</option>
                                  @else
                                  <option value="{{ $visitsequence->id }}">{{ $visitsequence->name }}</option>
                                  @endif
                                @endforeach
                              @endif
                            </select>
                          </div>
                      </div>

                      <div class="col-md-3">
                          <div class="form-group">
                            <label>Drop Size Prom</label>
                            <input type="text" id="dropsizeprom" name="dropsizeprom"  value="{{ $client->dropsizeprom ?? '' }}"class="form-control">
                          </div>
                      </div>

                      <div class="col-md-3">
                          <div class="form-group">
                            <label>Aniversario:</label>
                            <input type="text" required readonly="readonly" oninput="setCustomValidity('')" oninvalid="this.setCustomValidity('Requerido')"  id="anniversary" name="anniversary" value="{{ @$client->anniversary }}" class="form-control birthday" style="background-color: #fff">
                          </div>
                      </div>
                  </div>
                  <div class="row">
                    <div class="col-md-2">
                      <div class="form-group">
                        <label>Teléfono Principal</label>
                        <input type="text" class="form-control phones" required oninput="setCustomValidity('')" oninvalid="this.setCustomValidity('Requerido')" id="ownersphone" name="ownersphone" value="{{ @$client->ownersphone }}">
                      </div>
                    </div>
                    <div class="col-md-2">
                      <div class="form-group">
                        <label>Teléfono</label>
                        <input type="text" class="form-control phones" oninput="setCustomValidity('')" id="ownersphone1" name="ownersphone1" value="{{ @$phones[1] }}">
                      </div>
                    </div>
                    <div class="col-md-2">
                      <div class="form-group">
                        <label>Teléfono</label>
                        <input type="text" class="form-control phones" oninput="setCustomValidity('')" id="ownersphone2" name="ownersphone2" value="{{ @$phones[2] }}">
                      </div>
                    </div>
                    <div class="col-md-2">
                      <div class="form-group">
                        <label>Teléfono</label>
                        <input type="text" class="form-control phones" oninput="setCustomValidity('')" id="ownersphone3" name="ownersphone3" value="{{ @$phones[3] }}">
                      </div>
                    </div>
                    <div class="col-md-2">
                      <div class="form-group">
                        <label>Teléfono</label>
                        <input type="text" class="form-control phones" oninput="setCustomValidity('')" id="ownersphone4" name="ownersphone4" value="{{ @$phones[4] }}">
                      </div>
                    </div>
                    <div class="col-md-2">
                      <div class="form-group">
                        <label>Teléfono</label>
                        <input type="text" class="form-control phones" oninput="setCustomValidity('')" id="ownersphone5" name="ownersphone5" value="{{ @$phones[5] }}">
                      </div>
                    </div>
                  </div>
                  @include('client.address')
                </div>

                <div class="card-footer">
                </div>
              </div>
            </div>

            <div class="hide" id="client-advanced">
              <div class="card card-default">
                <div class="card-header">
                  <h3 class="card-title">Datos del propietario</h3>

                  <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i></button>
                  </div>
                </div>

                <div class="card-body" style="display: block;">
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Nombre y Apellido</label>
                        <input type="text" id="ownersname" name="ownersname"  value="{{ $client->ownersname ?? '' }}" class="form-control">
                      </div>
                    </div>

                    <div class="col-md-3">
                      <div class="form-group">
                        <label>Cedula</label>
                        <input type="text" id="ownersdni" name="ownersdni"  value="{{ $client->ownersdni ?? '' }}" class="form-control">
                      </div>
                    </div>

                    <div class="col-md-3">
                      <div class="form-group">
                        <label>Teléfono</label>
                        <input type="text" class="form-control" oninput="setCustomValidity('')" oninvalid="this.setCustomValidity('Requerido')" id="ownersphone" name="ownersphone" value="{{ @$client->ownersphone }}">
                      </div>
                    </div>
                  </div>
                  <div class="row">

                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Email</label>
                        <input type="text" id="rownersmail" name="ownersmail"  value="{{ $client->ownersmail ?? '' }}" class="form-control">
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Cumpleaños</label>
                        <input type="text" id="ownersbirthday" name="ownersbirthday" readonly="readonly" value="{{ $client->ownersbirthday ?? '' }}" class="form-control birthday" style="background-color: #fff">
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Atendido por el Dueño</label>
                        <select required oninput="setCustomValidity('')" oninvalid="this.setCustomValidity('Requerido')" class="form-control" id="owner_attend" name="owner_attend">
                            @if (@$client->owner_attend==1)
                              <option value="">NO</option>
                              <option selected="selected" value="1">SI</option>
                            @else
                              <option selected="selected" value="0">NO</option>
                              <option value="1">SI</option>
                            @endif
                        </select>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="card-footer" style="display: block;">

                </div>
              </div>
              <div class="card card-default">
                <div class="card-header">
                  <h3 class="card-title">Datos del Encargado</h3>

                  <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i></button>
                  </div>
                </div>

                <div class="card-body" style="display: block;">
                  <div class="row">
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Nombre y Apellido</label>
                        <input type="text" id="managersname" name="managersname"  value="{{ $client->managersname ?? '' }}" class="form-control">
                      </div>
                    </div>

                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Cedula</label>
                        <input type="text" id="managersdni" name="managersdni"  value="{{ $client->managersdni ?? '' }}" class="form-control">
                      </div>
                    </div>

                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Email</label>
                        <input type="text" id="managersmail" name="managersmail"  value="{{ $client->managersmail ?? '' }}" class="form-control">
                      </div>
                    </div>

                  </div>

                </div>

                <div class="card-footer" style="display: block;">

                </div>
              </div>
                <div class="card card-default">
                  <div class="card-header">
                    <h3 class="card-title">Publicidad en el establecimiento</h3>

                    <div class="card-tools">
                      <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                      <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i></button>
                    </div>
                  </div>

                  <div class="card-body" style="display: block;">
                    <div class="row">
                      <div class="col-md-3">
                        <div class="form-group">
                          <label>Banderines</label>
                          <select class="form-control" id="pennants" name="pennants">
                            @if (@$client->pennants==1)
                              <option value="">NO</option>
                              <option selected="selected" value="1">SI</option>
                            @else
                              <option selected="selected" value="">NO</option>
                              <option value="1">SI</option>
                            @endif
                          </select>
                        </div>
                      </div>

                      <div class="col-md-3">
                        <div class="form-group">
                          <label>Exhibicion</label>
                          <select class="form-control" id="display" name="display">
                            @if (@$client->display==1)
                              <option value="">NO</option>
                              <option selected="selected" value="1">SI</option>
                            @else
                              <option selected="selected" value="">NO</option>
                              <option value="1">SI</option>
                            @endif
                          </select>
                        </div>
                      </div>

                      <div class="col-md-3">
                        <div class="form-group">
                          <label>Bastidor</label>
                          <select class="form-control" id="frame" name="frame">
                            @if (@$client->frame==1)
                              <option value="">NO</option>
                              <option selected="selected" value="1">SI</option>
                            @else
                              <option selected="selected" value="">NO</option>
                              <option value="1">SI</option>
                            @endif
                          </select>
                        </div>
                      </div>

                      <div class="col-md-3">
                        <div class="form-group">
                          <label>POP</label>
                          <select class="form-control" id="pop" name="pop">
                            @if (@$client->pop==1)
                              <option value="">NO</option>
                              <option selected="selected" value="1">SI</option>
                            @else
                              <option selected="selected" value="">NO</option>
                              <option value="1">SI</option>
                            @endif
                          </select>
                        </div>
                      </div>

                    </div>

                  </div>

                  <div class="card-footer" style="display: block;">

                  </div>
                </div>

                <div class="card card-default">
                  <div class="card-header">
                    <h3 class="card-title">Productos</h3>

                    <div class="card-tools">
                      <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                      <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i></button>
                    </div>
                  </div>

                  <div class="card-body" style="display: block;">
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label>Productos</label>
                          <br>
                          <select class="select2" multiple="multiple"  name="products[]" data-placeholder="Seleccione Productos" style="width: 100%;">
                          @foreach ($products as $product)
                              @if ($product->assigned)
                                  <option selected="selected" value="{{$product->id}}">{{ $product->name }}</option>
                              @else
                                  <option value="{{$product->id}}">{{ $product->name }}</option>
                              @endif
                          @endforeach
                          </select>
                        </div>
                      </div>


                    </div>

                  </div>

                  <div class="card-footer" style="display: block;">
                  </div>
                </div>

                @if (@$client->rif)
                  <div class="card card-default">
                    <div class="card-header">
                      <h3 class="card-title">Teléfonos</h3>

                      <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                        <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i></button>
                      </div>
                    </div>

                    <div class="card-body" style="display: block;">
                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group">
                            <a href="#" class="btn btn-success btn_phone"><i class="fas fa-phone"></i> Agregar Teléfono</a>
                          </div>
                        </div>
                      </div>
                      <div class="row">

                        <div class="col-md-12">
                          <div id="phones" class="table-responsive">
                            <table class="table table-striped table-bordered table-hover" >
                                <tr>
                                    <th>Número</th>
                                    <th>Whatsapp</th>
                                    <th>Principal</th>
                                    <th>Opciones</th>
                                </tr>
                                  @foreach($client->phones as $phone)
                                      <tr>
                                          <td>{{ $phone->number }}</td>
                                          <td>
                                            @if ($phone->wsp)
                                              SI
                                            @else
                                              NO
                                            @endif
                                          </td>
                                          <td>
                                            @if ($phone->principal)
                                              SI
                                            @else
                                              NO
                                            @endif
                                          </td>
                                          <td><a href="#" onclick="edit_phone({{$phone->id}});"><i class="fa fa-edit" style="color:#006400"></i></a><span> </span><a href="#" onclick="delete_phone({{$phone->id}});"><i class="fa fa-trash delete_phone" style="color:#f00"></i></a><span> </span></td>
                                      </tr>
                                  @endforeach
                            </table>
                          </div>
                        </div>
                      </div>

                   </div>
                 </div>

                 <div class="card card-default">
                    <div class="card-header">
                      <h3 class="card-title">Dirección</h3>

                      <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                        <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i></button>
                      </div>
                    </div>

                    <div class="card-body" style="display: block;">
                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group">
                            <a href="#" class="btn btn-success btn_address"><i class="fas fa-map-marker"></i> Agregar Dirección</a>
                          </div>
                        </div>
                      </div>
                      <div class="row">

                        <div class="col-md-12">
                          <div id="addresses" class="table-responsive">
                            <table class="table table-striped table-bordered table-hover" >
                                <tr>
                                    <th>Dirección</th>
                                    <th>Urb./Zona</th>
                                    <th>Sector</th>
                                    <th>Calle</th>
                                    <th>Opciones</th>
                                </tr>
                                  @foreach($client->address as $address)
                                      <tr>
                                          <td>{{ $address->description }}</td>
                                          <td>{{ $address->urbanization }}</td>
                                          <td>{{ $address->sector }}</td>
                                          <td>{{ $address->street }}</td>
                                          <td><a href="#" onclick="edit_address({{$address->id}});"><i class="fa fa-edit" style="color:#006400"></i></a><span> </span><a href="#" onclick="delete_address({{$address->id}});"><i class="fa fa-trash delete_phone" style="color:#f00"></i></a><span> </span></td>
                                      </tr>
                                  @endforeach
                            </table>
                          </div>
                        </div>
                      </div>

                   </div>
                 </div>

                @endif

                @if (@$client)
                  @include('client.address_modal')
                @endif

            </div>

            <div class="card card-default hide" id="btns">
                <div class="card-footer" style="display: block;">
                <input type="submit" class="btn btn-primary" data-form="form" data-loading-text="Guardando..." data-class="Census" value="Guardar">
                <a href="{{url('clients')}}" class="btn btn-outline-danger">Cancelar</a>
              </div>
            </div>

        </form>
        <div class="card card-default">
              <div class="card-header">
                <h3 class="card-title">Listado de Cliente</h3>
                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                  <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i></button>
                </div>
              </div>

              <div class="card-body" style="display: block;">
                <div class="table-responsive">
                  <table class="table table-striped table-bordered table-hover" >
                      <tr>
                          <th>Razon Social</th>
                          <th>Rif</th>
                          <th>Email</th>
                          <th>Opciones</th>
                      </tr>
                  @foreach($clients as $rowclient)
                      <tr>
                          <td>{{ $rowclient->businessname }}</td>
                          <td>{{ $rowclient->rif }}</td>
                          <td>{{ $rowclient->businesssmail }}</td>
                          <td>
                            <a class="btn btn-sm btn-outline-info" href="{{ route('change_type', array($rowclient->id, $rowclient->client_type ?? 0)) }}">
                            @if ($rowclient->client_type == 2)
                            <i class="fa fa-user" style="color:#f00" title="Banco"></i>
                            @elseif ($rowclient->client_type == 1)
                            <i class="fa fa-user-check" style="color:#006400" title="Cliente"></i>
                            @else
                            <i class="fa fa-user" style="color:#FFFF00" title="Prospecto"></i>
                            @endif
                            </a>
                            <span></span>
                            <a class="btn btn-sm btn-outline-success" href="{{ route('clients', $rowclient->id) }}">
                              <i class="fa fa-edit" style="color:#006400"></i>
                            </a>
                            <span> </span>
                            <a class="btn btn-sm btn-outline-danger" href="{{ route('client_delete', $rowclient->id)}}">
                              <i class="fa fa-trash" style="color:#f00"></i>
                            </a>
                            <span> </span>
                            <a class="btn btn-sm btn-outline-primary" href="{{route('client_master', $rowclient->id)}}" target="blank">
                              <i class="fa fa-print"></i>
                            </a>
                          </td>
                      </tr>
                  @endforeach
                    <tr>
                      <td colspan="5">{{ $clients->links() }}</td>
                    </tr>
                  </table>
                </div>
              </div>
              <!-- /.card-body -->
              <div class="card-footer" style="display: block;">

              </div>

        </div>

      </div>
</div>
@endsection

@include('client.functions')

