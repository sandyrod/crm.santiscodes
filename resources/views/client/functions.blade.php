

 @push('js')

  <script>

   	const TOKEN = '{{csrf_token()}}';

   	let update_phone = (phone_id='', number='') => {
   		num = (number) ? 'Número de Teléfono ('+number+')' : 'Número de Teléfono';
        Swal.fire({
          title: num,
          icon: 'question',
          input: 'text',
          inputPlaceholder: 'Ingrese el Número de Teléfono',
          showLoaderOnConfirm: true,
          showCancelButton: true
        }).then(function (response) {
            if (response.value)
                is_wsp(response.value, phone_id);
        });
    };

    let is_wsp = (phone, phone_id) => {
         Swal.fire({
            title: "Whatsapp", text: "Confirme si el teléfono posee Whatsapp", icon: "warning", showCancelButton: true, confirmButtonColor: '#7CD534', confirmButtonText: 'Si', cancelButtonText: "No", showLoaderOnConfirm: true,
        }).then(function (response) {
            let wsp = (response.value) ? 1 : 0;
            is_main(phone, wsp, phone_id);
        });
    };

    let is_main = (phone, wsp, phone_id) => {
         Swal.fire({
            title: "Principal", text: "Marcar como teléfono principal", icon: "warning", showCancelButton: true, confirmButtonColor: '#7CD534', confirmButtonText: 'Si', cancelButtonText: "No", showLoaderOnConfirm: true,
        }).then(function (response) {
            let main = (response.value) ? 1 : 0;
            save_phone(phone, wsp, main, phone_id);
        });
    };

    let save_phone = (phone, wsp, main, phone_id) => {
		    $.ajax({
           url :  "{{route('phone_save')}}", type : 'POST', data: { _token: TOKEN, phone_id: phone_id, client_id: $('#id').val(), wsp: wsp, phone: phone, main: main }, dataType : 'json',
            success : function(  response  ) {
                if (response.phones)
                	reload_phones(response.phones);
            },
            error : function(xhr, status) {
                err = xhr.responseJSON.errors.code[0] ? xhr.responseJSON.errors.code[0] : 'Error de Datos...';
                Swal.fire("¡El registro no pudo ser procesado!", err, "error");
            }
        });

    };

  	let edit_phone = (phone_id) => {
    	$.get("{{url('search-phone')}}/"+phone_id, function ( response ) {
    		if (response.phone){
            	update_phone(response.phone.id, response.phone.number);
            }
        });
    };

    let delete_phone = (id) => {
        Swal.fire({
            title: "Atención", text: "Confirme que desea eliminar este registro.", icon: "warning", showCancelButton: true, confirmButtonColor: '#D62E21', confirmButtonText: 'Si, Eliminar!', cancelButtonText: "Cancelar", showLoaderOnConfirm: true,
        }).then(function (response) {
            if (response.value)
                $.ajax({
	               url: "{{url('phone_delete/')}}/"+id, type : 'GET', data: { _token : TOKEN }, dataType : 'json',
	                success : function(  response  ) {
	                    reload_phones(response.phones);
	                },
	                error : function(xhr, status) {
	                    err = xhr.responseJSON.errors.code[0] ? xhr.responseJSON.errors.code[0] : 'Error de Datos...';
	                    Swal.fire("¡El registro no pudo ser procesado!", err, "error");
	                }
	            });
        });
    };

    let reload_phones = (phones) => {
    	let cad = '<table class="table table-striped table-bordered table-hover"><tr><th>Número</th><th>Whatsapp</th>';
    	cad += '<th>Principal</th><th>Opciones</th></tr>';
    	phones.forEach( function(element, index) {
    		cad += '<tr><td>'+element.number+'</td><td>';
            item = (element.wsp) ? 'SI' : 'NO';
            cad += item+'</td><td>';
            item = (element.principal) ? 'SI' : 'NO';
            cad += item+'</td><td>';
            cad += '<a href="#" onclick="edit_phone('+element.id+');"><i class="fa fa-edit" style="color:#006400"></i></a>';
            cad += '<span> </span><a href="#" onclick="delete_phone('+element.id+');"><i class="fa fa-trash delete_phone"';
            cad += ' style="color:#f00"></i></a><span> </span></td></tr>';
    	});
    	cad += '</table>';
    	$('#phones').html(cad);
    	$('#phone_id').val('');
    };

    $('.btn_phone').on('click', function (e){
      	update_phone();
    });

   	let delete_address = (id) => {
        Swal.fire({
            title: "Atención", text: "Confirme que desea eliminar este registro.", icon: "warning", showCancelButton: true, confirmButtonColor: '#D62E21', confirmButtonText: 'Si, Eliminar!', cancelButtonText: "Cancelar", showLoaderOnConfirm: true,
        }).then(function (response) {
            if (response.value)
                $.ajax({
	               url: "{{url('address_delete/')}}/"+id, type : 'GET', data: { _token : TOKEN }, dataType : 'json',
	                success : function(  response  ) {
	                    reload_address(response.addresses);
	                },
	                error : function(xhr, status) {
	                    err = xhr.responseJSON.errors.code[0] ? xhr.responseJSON.errors.code[0] : 'Error de Datos...';
	                    Swal.fire("¡El registro no pudo ser procesado!", err, "error");
	                }
	            });
        });
    };
    let reload_address = (addresses) => {
    	let cad = '<table class="table table-striped table-bordered table-hover"><tr><th>Dirección</th><th>Urb./Zona</th><th>Sector</th>';
        cad += '<th>Calle</th><th>Opciones</th></tr>';
    	addresses.forEach( function(element, index) {
    		cad += '<tr><td>'+element.description+'</td><td>'+element.urbanization+'</td><td>'+element.sector+'</td><td>'+element.street+'</td>';
            cad += '<td><a href="#" onclick="edit_address('+element.id+');"><i class="fa fa-edit" style="color:#006400"></i></a>';
            cad += '<span> </span><a href="#" onclick="delete_address('+element.id+');"><i class="fa fa-trash delete_phone"';
            cad += ' style="color:#f00"></i></a><span> </span></td></tr>';
    	});
    	cad += '</table>';
    	$('#addresses').html(cad);
    	$('#address_id').val('');
    };

    let edit_address = (address_id) => {
      $.get("{{url('search-address')}}/"+address_id, function ( response ) {
        if (response.address){
              load_address(response.address);
              $('#modal-lg').modal('toggle');
            }
        });
    };

    let save_address = () => {
        let address_id = $('#address_id').val();
        let urbanization = $('#urbanization').val();
        let sector = $('#sector').val();
        let street = $('#street').val();
        let streets_between = $('#streets_between').val();
        let description = $('#description').val();
        let municipality = $('#municipality').val();
        let parish = $('#parish').val();
        let city = $('#city').val();
        let state = $('#state').val();
        let latitude = $('#latitude').val();
        let longitude = $('#longitude').val(); 

        if (urbanization || sector || street || description || municipality || parish || city || state || latitude || longitude){
           $.ajax({
               url :  "{{route('address_save')}}", type : 'POST', data: { _token: TOKEN, address_id: address_id, client_id: $('#id').val(), description: description, urbanization: urbanization, sector: sector, street: street, streets_between: streets_between, municipality: municipality, parish: parish, city: city, state: state, latitude: latitude, longitude: longitude }, dataType : 'json',
                success : function(  response  ) {
                    if (response.addresses)
                      reload_address(response.addresses);
                    $('#modal-lg').modal('hide');
                },
                error : function(xhr, status) {
                    err = xhr.responseJSON.errors.code[0] ? xhr.responseJSON.errors.code[0] : 'Error de Datos...';
                    Swal.fire("¡El registro no pudo ser procesado!", err, "error");
                }
          });
           return 1;
         } else {
           Swal.fire("¡Debe ingresar información acerca de la dirección!", 'Atención', "error");
           return 0;
         }

    };

    $('.btn_address').on('click', function (e){
      	clean_address();
        $('#modal-lg').modal('toggle');
    });

    let clean_address = () => {
      let fields = ['address_id', 'urbanization', 'sector', 'street', 'streets_between', 'description', 'municipality', 'parish', 'city', 'state', 'latitude', 'longitude']

      fields.forEach(function(entry) {
        $('#'+entry).val('');
      });
    };

    let load_address = (address) => {
      $('#address_id').val(address.id);
      $('#urbanization').val(address.urbanization);
      $('#sector').val(address.sector);
      $('#street').val(address.street);
      $('#streets_between').val(address.streets_between);
      $('#description').val(address.description);
      $('#municipality').val(address.municipality);
      $('#parish').val(address.parish);
      $('#city').val(address.city);
      $('#state').val(address.state);
      $('#latitude').val(address.latitude);
      $('#longitude').val(address.longitude);
    };


    $(document).ready(function(){

      $('#btn_save_address').on('click', function (e){
        //$('#btn_save_address').html('<i class="fa fa-refresh fa-spin"></i> Guardando...')

        let dataLoadingText = $(this).attr("data-loading-text");
        if (typeof dataLoadingText !== typeof undefined && dataLoadingText !== false) {
          $(this).text(dataLoadingText).addClass("disabled");
        }

        save_address();

        $(this).text('Guardar').removeClass("disabled");
      });

      $("#rif").inputmask({mask:"a-999999999"});
      $("#rif1").inputmask({mask:"a-999999999"});
      $('#anniversary').datetimepicker({
          format: 'Y-m-d',
          closeOnDateSelect: true,
          timepicker: false
      });
      $('#client_date').datetimepicker({
          format: 'Y-m-d',
          closeOnDateSelect: true,
          timepicker: false
      });

      $('#ownersbirthday').datetimepicker({
          format: 'Y-m-d',
          closeOnDateSelect: true,
          timepicker: false
      });

	    let activate = (element) => {
	      	$('#'+element).removeClass('hide');
	      	$('#'+element).addClass('show-div');
	    };

	    let deactivate = (element) => {
	      	$('#'+element).removeClass('show-div');
	      	$('#'+element).addClass('hide');
	    };

	    let activate_all = (element) => {
	    	  activate('client-basic');
	      	activate('btns');
	      	deactivate('btn_search');
	      	deactivate('btn_new');
	    };

	    let eval_client_type = () => {
	    	if ($('#client_type').val()==1){
	      		activate('client-advanced');
	      		activate('btns');
	      	}else{
            activate('btns');
	      		deactivate('client-advanced');
	      	}
	    }

	    let search_client = () => {
	    	$.get("{{url('search-client')}}/"+$('#rif').val(), function ( response ) {
	    		if (response.client){
                	window.location.href = '{{url('clients/')}}/'+response.client.id;
                }else{
		            Swal.fire({
		                title: "Atención: RIF No Encontrado", text: "Desea crear un nuevo cliente", icon: "warning", showCancelButton: true, confirmButtonColor: '#5ECF48', confirmButtonText: 'Si, Crear!', cancelButtonText: "Cancelar", showLoaderOnConfirm: true,
		            }).then(function (response) {
		                if (response.value)
		                    $('#btn_new').trigger('click');
		            });
                }
            });
	    }

      let get_client_code = () => {
        let route = $('#distributionroute_id').val();
        $.get("{{url('search-next-code')}}/"+route, function ( response ) {
          if (response)
            $('#businesscode').val('R-'+route+'-'+response);
          else
            $('#businesscode').val('R-'+route+'-1');
        });
      };

      $('#distributionroute_id').on('click', function (e){
          get_client_code();
      });

      get_client_code();

    	if ($('#rif').val()){
    		activate_all();
    		eval_client_type();
    		$('#rif').attr('readonly','readonly');
    		$('#rif').attr("disabled",true);
    	}

	    $('#btn_new').on('click', function (e){
	      	if($('#rif').val().length==0){
                activate_all();
            }else{
                search_client();
            }
	    });

	    $('#btn_search').on('click', function (e){
	      	search_client();
	    });

	    $( "#rif" ).on( "keydown", function(event) {
	      	if(event.which == 13) {
	    		event.preventDefault();
	         	search_client();
	    	 }
	    });

	    $('#client_type').on('change', function (e){
	    	eval_client_type();
	    });


    });

  </script>

@endpush
